import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActosadministracionComponent } from './actosadministracion.component';

describe('ActosadministracionComponent', () => {
  let component: ActosadministracionComponent;
  let fixture: ComponentFixture<ActosadministracionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActosadministracionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActosadministracionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
