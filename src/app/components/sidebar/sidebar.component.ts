import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { SinabipmueblesService } from '../../services/sinabipmuebles.service';
import { NgxSpinnerService } from 'ngx-spinner';

	@Component({
		selector: 'app-sidebar',
		templateUrl: './sidebar.component.html',
		styleUrls: ['./sidebar.component.css']
	})
	export class SidebarComponent implements OnInit {
		datos: Array<any> = [];

		id: string = sessionStorage.getItem("Cod_Entidad");
		Entidad_Arg : Array<any> = [];
		NombreEntidad: string; 
		fechaActual: string;

		constructor(private Sinabip: SinabipmueblesService,
		private spinner: NgxSpinnerService) {
			//alert('lljjego...');
			// setTimeout(function(){
				this.cargarMenuMuebles();
				this.BuscarEntidad()
			// },500);
			//debugger
		}
			
		ngOnInit() {
		}
		
		cargarMenuMuebles() {
			this.id = sessionStorage.getItem("Cod_Entidad");
			if(this.id === null){
				location.reload(true);
				//window.location.replace("./SGISBN/System/sinabip.php");
			} 
			this.id = sessionStorage.getItem("Cod_Entidad");
			let token = sessionStorage.getItem("token");
			this.Sinabip.getModuloMuebles(this.id,token).subscribe((data: any) => {
				this.datos = data.data;
				console.log(this.datos);
				// console.log(this.datos['3'].estado);
				sessionStorage.setItem("Est_Menu", this.datos['3'].estado);
			});
		}
			
		onLoggedout() {
			sessionStorage.removeItem('isLoggedin');
		}
		
		BuscarEntidad(){
			this.id = sessionStorage.getItem("Cod_Entidad");
			if(this.id === null){ 
				location.reload(true);
				//window.location.replace("./SGISBN/System/sinabip.php");
			  	return false;
			} 
			this.id = sessionStorage.getItem("Cod_Entidad");
			this.spinner.show();
			let token = sessionStorage.getItem("token");
			this.Sinabip.getBuscarEntidad(this.id,token).subscribe((data : any) =>{
				this.spinner.hide();
				this.Entidad_Arg = data.data;
				this.NombreEntidad = this.Entidad_Arg[0].NOM_ENTIDAD;
				this.fechaActual = this.Entidad_Arg[0].FECHA
			});
		  }
	
	}