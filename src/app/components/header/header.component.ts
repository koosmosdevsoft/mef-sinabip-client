import { Component, OnInit } from '@angular/core';
import { SinabipmueblesService } from '../../services/sinabipmuebles.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

	id: string = sessionStorage.getItem("Cod_Entidad");
  RutaSinabip : string = sessionStorage.getItem("RutaSinabip");
  Entidad_Arg : Array<any> = [];
  NombreEntidad: string; 
  fechaActual: string;

  constructor(private Sinabip: SinabipmueblesService,
		private spinner: NgxSpinnerService) { 
      //alert('llego.kk..');
      // setTimeout(function(){
        this.BuscarEntidad()
      // },500);
      //debugger
      }
      
  ngOnInit() {
  }
  
  BuscarEntidad(){
    //alert('buscarent');
    this.id = sessionStorage.getItem("Cod_Entidad");
    if(this.id === null){ 
      location.reload(true);
      //window.location.replace("./SGISBN/System/sinabip.php");
      return false;
    } 
    this.id = sessionStorage.getItem("Cod_Entidad");
    this.spinner.show();
    let token = sessionStorage.getItem("token");
    this.Sinabip.getBuscarEntidad(this.id,token).subscribe((data : any) =>{
    this.spinner.hide();
        this.Entidad_Arg = data.data;
        this.NombreEntidad = this.Entidad_Arg[0].NOM_ENTIDAD;
        this.fechaActual = this.Entidad_Arg[0].FECHA;
    });
  }

  cerrar_session(){
    // sessionStorage.setItem("Cod_Entidad", null);
    // sessionStorage.setItem("Cod_Usuario", null);
    sessionStorage.clear();
    localStorage.clear();
    window.location.replace(this.RutaSinabip+"SGISBN/sinabip.php");
  }

  ir_pagina_principal(){
    window.location.replace(this.RutaSinabip+"SGISBN/System/sinabip.php");
    
  }


  

}
