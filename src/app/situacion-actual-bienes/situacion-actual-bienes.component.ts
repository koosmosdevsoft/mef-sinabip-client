import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { SinabipmueblesService } from '../services/sinabipmuebles.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
// import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbModalConfig, NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';


@Component({
	selector: 'app-situacion-actual-bienes',
	templateUrl: './situacion-actual-bienes.component.html',
	styleUrls: ['./situacion-actual-bienes.component.css'],
	animations: [routerTransition()],
	providers: [NgbModalConfig, NgbModal]
})
export class SituacionActualBienesComponent implements OnInit {

	id: string = sessionStorage.getItem("Cod_Entidad");
	idusuario: string = sessionStorage.getItem("Cod_Usuario");
	RutaSinabip : string = sessionStorage.getItem("RutaSinabip");

	Cod_Entidad : string = this.id;
	Cod_Usuario : string = this.idusuario;

	ListGrupo_Arg: Array<any> = [];
	ListClase_Arg: Array<any> = [];
	Listestadobien_Arg: Array<any> = [];
	Listtipobien_Arg: Array<any> = [];
	Listsituacionactual_Arg: Array<any> = [];
	ListLimpiar_Arg: Array<any> = [];
	ListCaracteristica_Arg: Array<any> = [];
	LisCuentaCont_Arg: Array<any> = [];
	ListUbicacionActual_Arg: Array<any> = [];
	Pdf_descarga: string;
	closeResult: string;
	//Paginacion 
	itemsPerPage: number = 20;
	page: any = 1;
	previousPage: any;
	total: any = 0;

	//filtro 
	cod_entidad: string;
	cod_patrimonial: string;
	denominacion: string;
	cod_grupo: string;
	cod_clase: string;
	estado_bien: string;
	cod_tipo_mov: string;
	modal;
	dataDescargas : Array<any> = [];
	dataDescargasXGrupo : Array<any> = [];
	flag_ImportarFormatoExcel : number;
	

	filtro = {
		id: this.id, cod_patrimonial: '', denominacion: '', cod_grupo: '', cod_clase: '',
		estado_bien: '', cod_tipo_mov: '', page: this.page, records: this.itemsPerPage
	}

	paramBien = {
		codigopatri: '', denomBien: '', cod_grupo: '', cod_clase: '', catalogobien: '',
		marca: '', modelo: '', tipo: '', color: '', nroSerie: '',
		nroChasis: '', anioFabric: '', otrasCaract: '', usoCta: '', TipoCta: '', CtaContable: '',
		valorAdquis: '', porcDeprec: '', asegurado: '', estadoBien: '', observacion: ''
	}

	paramBien2 = {
		codigopatri: '', denomBien: '', cod_grupo: '', cod_clase: '', catalogobien: '',
		nrdocumento: '', personal: '', local: '', area: '', estadoactual: '', nroasig: '', fechaasig: ''
	}

	paramDescarga = {
		id_entidad : this.Cod_Entidad,
		nro_grupo: 1
	}

	constructor(config: NgbModalConfig, public Sinabip: SinabipmueblesService,
		private route: ActivatedRoute,
		private router: Router,
		private spinner: NgxSpinnerService,
		private modalService: NgbModal,
		private SinabipMuebles : SinabipmueblesService
		) {
			let token = sessionStorage.getItem("token");  
			let ruta = sessionStorage.getItem("RutaSinabip")
			if(token === null){
				// window.location.replace(ruta+"SGISBN/System/sinabip.php");
				window.location.replace("./SGISBN/System/sinabip.php");
			}
		this.ListadoGrupo();
		this.ListadoEstadoBien();
		this.ListadoTipoMotivo();
		this.ListadoSituacionActual();
		this.ListadoCuentaContable();
		config.backdrop = 'static';
		config.keyboard = false;
		this.Actualizar_PDF();
	}

	ngOnInit() {
	}

	Actualizar_PDF() {
		this.cod_entidad = this.filtro.id;
		this.cod_patrimonial = this.filtro.cod_patrimonial;
		this.denominacion = this.filtro.denominacion;
		this.cod_grupo = this.filtro.cod_grupo;
		this.cod_clase = this.filtro.cod_clase;
		this.estado_bien = this.filtro.estado_bien;
		this.cod_tipo_mov = this.filtro.cod_tipo_mov;

		if (this.cod_entidad == '') { this.cod_entidad = '-1' }
		if (this.cod_patrimonial == '') { this.cod_patrimonial = '-1' }
		if (this.denominacion == '') { this.denominacion = '-1' }
		if (this.cod_grupo == '') { this.cod_grupo = '-1' }
		if (this.cod_clase == '') { this.cod_clase = '-1' }
		if (this.estado_bien == '') { this.estado_bien = '-1' }
		if (this.cod_tipo_mov == '') { this.cod_tipo_mov = '-1' }

		this.Pdf_descarga = this.cod_entidad + '.' + this.cod_patrimonial + '.' + this.denominacion + '.' + this.cod_grupo + '.' + this.cod_clase + '.' + this.estado_bien + '.' + this.cod_tipo_mov;
		this.Pdf_descarga = btoa(this.Pdf_descarga);
	}

	ListadoGrupo() {
		this.spinner.show();
		this.Sinabip.GetListadoGrupo().subscribe((data: any) => {
			this.spinner.hide();
			this.ListGrupo_Arg = data.data;
		});
	}

	ListadoClase(COD_GRUPO) {
		this.filtro.cod_clase = '';
		if (COD_GRUPO == '') {
			COD_GRUPO = 0;
		}
		this.ListadoGrupo();
		this.spinner.show();
		this.Sinabip.GetListadoClase(COD_GRUPO).subscribe((data: any) => {
			this.spinner.hide();
			this.ListClase_Arg = data.data;
		});
	}

	ListadoEstadoBien() {
		this.spinner.show();
		this.Sinabip.GetListadoEstadoBien().subscribe((data: any) => {
			this.spinner.hide();
			this.Listestadobien_Arg = data.data;
		});
	}

	ListadoCuentaContable() {
		this.spinner.show();
		this.Sinabip.GetListadoCuentaContable().subscribe((data: any) => {
			this.spinner.hide();
			this.LisCuentaCont_Arg = data.data;
		});
	}

	ListadoTipoMotivo() {
		this.spinner.show();
		this.Sinabip.GetListadoTipoMotivo().subscribe((data: any) => {
			this.spinner.hide();
			this.Listtipobien_Arg = data.data;
		});
	}

	ListadoSituacionActual() {
		//this.filtro.page = 1;
		this.Actualizar_PDF();
		this.spinner.show();
		this.Sinabip.PostListadoSituacionActual(this.filtro).subscribe((data: any) => {
			this.spinner.hide();

			if (data.data.error == true){
				if (data.data.reco.hasOwnProperty('id')) { 
					this.router.navigate(['error500']);
					return;
				}else if (data.data.reco.hasOwnProperty('cod_patrimonial')) {
					swal({
						type: 'error',
						title: 'Validacion de Datos',
						text: data.data.reco.cod_patrimonial[0],
					  })
					  return;
				
				}else if (data.data.reco.hasOwnProperty('page')) {
					this.router.navigate(['error500']);
					return;
				}else if (data.data.reco.hasOwnProperty('records')) {
					this.router.navigate(['error500']);
					return;
				}
				
			}

			this.Listsituacionactual_Arg = data.data;
			this.total = (this.Listsituacionactual_Arg.length > 0) ? this.Listsituacionactual_Arg[0].Total : 0;
		});
	}

	resetearpag() {
		this.filtro.page = 1;
		this.ListadoSituacionActual();
    }

	loadPage(page: number) {
		if (page !== this.previousPage) {
			this.previousPage = page;
			this.ListadoSituacionActual();
		}
	}

	Quitar_Filtro() {
		this.filtro = {
			id: this.id, cod_patrimonial: '', denominacion: '', cod_grupo: '', cod_clase: '',
			estado_bien: '', cod_tipo_mov: '', page: this.page, records: this.itemsPerPage
		}
		this.Actualizar_PDF();
		this.spinner.show();
		this.Sinabip.PostListadoSituacionActual(this.filtro).subscribe((data: any) => {
			this.spinner.hide();
			this.Listsituacionactual_Arg = data.data;
			this.total = (this.Listsituacionactual_Arg.length > 0) ? this.Listsituacionactual_Arg[0].Total : 0;
		});
	}

	// private getDismissReason(reason: any): string {
	//   if (reason === ModalDismissReasons.ESC) {
	//     return 'by pressing ESC';
	//   } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
	//     return 'by clicking on a backdrop';
	//   } else {
	//     return  `with: ${reason}`;
	//   }
	// }

	open(content, cod_bien_patrimonial) {
		this.flag_ImportarFormatoExcel = 0;
		this.ListadoCaracteristicas(cod_bien_patrimonial);
		this.modalService.open(content, {  ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
			this.closeResult = `Closed with: ${result}`;
		}, (reason) => {
		});

	}


	open2(content2, cod_bien_patrimonial) {
		this.ListadoUbicacionActual(cod_bien_patrimonial);
		this.modalService.open(content2, {  ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
			this.closeResult = `Closed with: ${result}`;
		}, (reason) => {
		});

	}

	ListadoCaracteristicas(cod_bien_patrimonial) {
		this.spinner.show();
		this.Sinabip.GetListadoCaracteristicas(this.id, cod_bien_patrimonial).subscribe((data: any) => {
			this.spinner.hide();

			if (data.data.error == true){
				if (data.data.reco.hasOwnProperty('id')) { 
					this.router.navigate(['error500']);
					return;
				}else if (data.data.reco.hasOwnProperty('codBienPat')) {
					  this.router.navigate(['error500']);
					  return;
				}
				
			}

			this.ListCaracteristica_Arg = data.data;

			this.paramBien = {
				codigopatri: this.ListCaracteristica_Arg[0].CODIGO_PATRIMONIAL, denomBien: this.ListCaracteristica_Arg[0].DENOMINACION_BIEN,
				cod_grupo: this.ListCaracteristica_Arg[0].DESC_GRUPO, cod_clase: this.ListCaracteristica_Arg[0].DESC_CLASE,
				catalogobien: this.ListCaracteristica_Arg[0].CATALOGO_BIEN, marca: this.ListCaracteristica_Arg[0].MARCA,
				modelo: this.ListCaracteristica_Arg[0].MODELO, tipo: this.ListCaracteristica_Arg[0].TIPO,
				color: this.ListCaracteristica_Arg[0].COLOR, nroSerie: this.ListCaracteristica_Arg[0].SERIE,
				nroChasis: this.ListCaracteristica_Arg[0].NRO_CHASIS, anioFabric: this.ListCaracteristica_Arg[0].ANIO_FABRICACION,
				otrasCaract: this.ListCaracteristica_Arg[0].OTRAS_CARACT, usoCta: this.ListCaracteristica_Arg[0].USO_CUENTA,
				TipoCta: this.ListCaracteristica_Arg[0].TIP_CUENTA, CtaContable: this.ListCaracteristica_Arg[0].COD_CTA_CONTABLE,
				valorAdquis: this.ListCaracteristica_Arg[0].VALOR_ADQUIS, porcDeprec: this.ListCaracteristica_Arg[0].DEPRECIACION,
				asegurado: this.ListCaracteristica_Arg[0].OPC_ASEGURADO, estadoBien: this.ListCaracteristica_Arg[0].COD_ESTADO_BIEN,
				observacion: this.ListCaracteristica_Arg[0].OBSERVACION
			}
		});
	}

	ListadoUbicacionActual(cod_bien_patrimonial) {
		this.spinner.show();
		this.Sinabip.GetListadoUbicacionActual(this.id, cod_bien_patrimonial).subscribe((data: any) => {
			this.spinner.hide();

			if (data.data.error == true){
				if (data.data.reco.hasOwnProperty('id')) { 
					this.router.navigate(['error500']);
					return;
				}else if (data.data.reco.hasOwnProperty('codBienPat')) {
					  this.router.navigate(['error500']);
					  return;
				}
				
			}

			this.ListUbicacionActual_Arg = data.data;

			this.paramBien2 = {
				codigopatri: this.ListUbicacionActual_Arg[0].CODIGO_PATRIMONIAL, denomBien: this.ListUbicacionActual_Arg[0].DENOMINACION_BIEN,
				cod_grupo: this.ListUbicacionActual_Arg[0].DESC_GRUPO, cod_clase: this.ListUbicacionActual_Arg[0].DESC_CLASE,
				catalogobien: this.ListUbicacionActual_Arg[0].CATALOGO_BIEN, nrdocumento: this.ListUbicacionActual_Arg[0].NRO_DOCUMENTO,
				personal: this.ListUbicacionActual_Arg[0].PERSONAL, local: this.ListUbicacionActual_Arg[0].DENOMINACION_PREDIO,
				area: this.ListUbicacionActual_Arg[0].DESC_AREA, estadoactual: this.ListUbicacionActual_Arg[0].ESTADO,
				nroasig: this.ListUbicacionActual_Arg[0].NUMERO, fechaasig: this.ListUbicacionActual_Arg[0].FECHA
			}
		});
	}

	private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
		  return 'by pressing ESC';
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
		  return 'by clicking on a backdrop';
		} else {
		  return  `with: ${reason}`;
		}
	}

	ImportarFormatoTXT(content){
		/* parametros */
		this.paramDescarga.id_entidad = this.Cod_Entidad;
		// console.log('entidad: ' +  this.paramDescarga.id_entidad);
		this.spinner.show();
		this.modal=this.SinabipMuebles.postImportarFormatoTXT(this.paramDescarga).subscribe((data : any) =>{  
		this.spinner.hide();

		let reg = data.data.GrupoDescargas[0].NRO_GRUPO;
		this.dataDescargas = [];
		for (let index = 0; index < reg; index++) {
			this.dataDescargas.push({
				index : index
			});
		}
		// let acto = data.data;		
		// this.cantidadduplicado_bienes = 0;
		// this.cantidadduplicado_actos = 0;  
		// this.cantidaderrores_columnas = 0;
		// this.cantidadNo_existe_codigos_pat = 0;
		// this.cantidadNo_disponible_bienes = 0;
		});
		this.flag_ImportarFormatoExcel = 1;

		//this.eliminacionDetalleBajas = true;
		this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', 
		keyboard: false,
		size: 'lg',
		backdrop: 'static'}).result.then((result) => { 
			this.closeResult = `Closed with: ${result}`;
		}, (reason) => {
			this.closeResult = `Dismissed ${this.getDismissReason(reason)}`; 
		});

	}

}
