import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SituacionActualBienesComponent } from './situacion-actual-bienes.component';

describe('SituacionActualBienesComponent', () => {
  let component: SituacionActualBienesComponent;
  let fixture: ComponentFixture<SituacionActualBienesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SituacionActualBienesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SituacionActualBienesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
