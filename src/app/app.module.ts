import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { NgbModule, NgbModalModule, NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { NgxSpinnerModule } from 'ngx-spinner';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFileUploaderModule } from "angular-file-uploader";


// Componentes
import { ActosadministracionComponent } from './actosadministracion/actosadministracion.component';
import { LogoPrincipalComponent } from './logo-principal/logo-principal.component';
import { HeaderComponent } from './components/header/header.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { DevolucionBienesComponent } from './devolucion-bienes/devolucion-bienes.component';
import { DevolucionBienesDetComponent } from './devolucion-bienes-det/devolucion-bienes-det.component';
import { SafePipe } from './services/safe';
import { TrasladoBienesComponent } from './traslado-bienes/traslado-bienes.component';
import { TrasladoBienesDetComponent } from './traslado-bienes-det/traslado-bienes-det.component';
import { TrasladoBienesResumenComponent } from './traslado-bienes-resumen/traslado-bienes-resumen.component';
import { ModValornetoComponent } from './mod-valorneto/mod-valorneto.component';
import { AsigBienesPersonComponent } from './asig-bienes-person/asig-bienes-person.component';
import { AsigBienesPersonDetComponent } from './asig-bienes-person-det/asig-bienes-person-det.component';
import { ActosadquisicionComponent } from './actosadquisicion/actosadquisicion.component';
import { ActosbajaComponent } from './actobaja/actobaja.component';
import { ActosdisposicionComponent } from './actosdisposicion/actosdisposicion.component';
import { EliminacionComponent } from './eliminacion/eliminacion.component';
import { RecodificacionComponent } from './recodificacion/recodificacion.component';
import { PersonalComponent } from './personal/personal.component';
import { InvPrincipalComponent } from './inv-principal/inv-principal.component';
import { InvListaPrediosComponent } from './inv-lista-predios/inv-lista-predios.component';
import { ReporteactosComponent } from './reporteactos/reporteactos.component';
import { SituacionActualBienesComponent } from './situacion-actual-bienes/situacion-actual-bienes.component';
import { ConfigDepreciacionComponent } from './config-depreciacion/config-depreciacion.component';
import { InvResumenComponent } from './inv-resumen/inv-resumen.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AccesoComponent } from './acceso/acceso.component';
import { CatalogosComponent } from './catalogos/catalogos.component';
import { AreaComponent } from './area/area.component';
import { ResponsablePatrComponent } from './responsable-patr/responsable-patr.component';
import { ListPredioComponent } from './list-predio/list-predio.component';
import { FichatecnicavehComponent } from './fichatecnicaveh/fichatecnicaveh.component';
import { ModdatosbienComponent } from './moddatosbien/moddatosbien.component';
import { TransfGestionMbComponent } from './transf-gestion-mb/transf-gestion-mb.component'; 
import { CuentaContableComponent } from './cuenta-contable/cuenta-contable.component';
import { ModFechdepreComponent } from './mod-fechdepre/mod-fechdepre.component';
import { AsigBienesReporteComponent } from './asig-bienes-reporte/asig-bienes-reporte.component';
import { FichaDatosReporteComponent } from './ficha-datos-reporte/ficha-datos-reporte.component';

import { ListadoComponent } from './ingresoactosadministracion/listado/listado.component';
import { FormaMasivaComponent } from './ingresoactosadministracion/forma-masiva/forma-masiva.component';
import { FormaIndividualComponent } from './ingresoactosadministracion/forma-individual/forma-individual.component';
import { TipoRegistroComponent } from './ingresoactosadministracion/tipo-registro/tipo-registro.component';



//import { ListadoCatalogoBienesComponent } from './ingresoactosadministracion/Listado-catalogo-bienes/listado-catalogo-bienes.component'
import { ListadoReingresoComponent } from './ReingresoBienesactosadministracion/listado/listadoReingreso.component';
import { FormaMasivaReingresoComponent } from './ReingresoBienesactosadministracion/forma-masiva/forma-masivaReingreso.component';
import { FormaIndividualReingresoComponent } from './ReingresoBienesactosadministracion/forma-individual/forma-individualReingreso.component';
import { TipoRegistroReingresoComponent } from './ReingresoBienesactosadministracion/tipo-registro/tipo-registroReingreso.component';


import { DevolucionactoadministracionComponent } from './devolucionactoadministracion/devolucionactoadministracion.component';
import { ConfigCargaDocumentoComponent } from './config-carga-documento/config-carga-documento.component';

import {Error500Component} from './shared/error500/error500.component';


const appRoutes: Routes = [
	{ path: '', component: LogoPrincipalComponent },
	{ path: 'actosadministracion', component: ActosadministracionComponent },
	{ path: 'devolucion-bienes', component: DevolucionBienesComponent },
	{ path: 'devolucion-bienes-det', component: DevolucionBienesDetComponent },
	{ path: 'traslados-bienes', component: TrasladoBienesComponent },
	{ path: 'traslados-bienes-det', component: TrasladoBienesDetComponent },
	{ path: 'traslados-bienes-resumen', component: TrasladoBienesResumenComponent },
	{ path: 'mod-valorneto', component: ModValornetoComponent },
	{ path: 'asig-bienes', component: AsigBienesPersonComponent },
	{ path: 'asig-bienes-det', component: AsigBienesPersonDetComponent },
	{ path: 'actosadquisicion', component: ActosadquisicionComponent },
	{ path: 'actosbaja', component: ActosbajaComponent },
	{ path: 'actosdisposicion', component: ActosdisposicionComponent },
	{ path: 'eliminacion', component: EliminacionComponent },
	{ path: 'recodificacion', component: RecodificacionComponent },
	{ path: 'personal', component: PersonalComponent },
	{ path: 'Inventario', component: InvPrincipalComponent },
	{ path: 'inv-lista-predios', component: InvListaPrediosComponent },
	{ path: 'reporteactos', component: ReporteactosComponent },
	{ path: 'situacion-actual', component: SituacionActualBienesComponent },
	{ path: 'config-depreciacion', component: ConfigDepreciacionComponent },
	{ path: 'inv-resumen', component: InvResumenComponent },
	{ path: 'catalogos', component: CatalogosComponent },
	{ path: 'area', component: AreaComponent },
	{ path: 'responsablePatr', component: ResponsablePatrComponent },
	{ path: 'CuentaContable', component: CuentaContableComponent },
	{ path: 'transf-gestion-mb', component: TransfGestionMbComponent },
	{ path: 'list-Predio', component: ListPredioComponent },
	{ path: 'fichatecnicaveh', component: FichatecnicavehComponent },
	{ path: 'moddatosbien', component: ModdatosbienComponent },
	{ path: 'mod-fechdepre', component: ModFechdepreComponent },
	{ path: 'asig-bienes-report', component: AsigBienesReporteComponent },
	{ path: 'ficha-datos-report', component: FichaDatosReporteComponent },
	{ path: 'acceso/:id', component: AccesoComponent },
	{ path: 'null', component: NotFoundComponent },
	{ path: 'ingresoactosadministracion', component: ListadoComponent },
	{ path: 'devolucionactoadministracion', component: DevolucionactoadministracionComponent },
	{ path: 'reingresobienesactosadministracion', component: ListadoReingresoComponent },
	{ path: 'config-carga-documento', component: ConfigCargaDocumentoComponent },
	
	{ path: 'error500', component: Error500Component }
];

@NgModule({
	declarations: [
		AppComponent,
		ActosadministracionComponent,
		LogoPrincipalComponent,
		HeaderComponent,
		SidebarComponent,
		DevolucionBienesComponent,
		DevolucionBienesDetComponent,
		SafePipe,
		TrasladoBienesComponent,
		TrasladoBienesDetComponent,
		TrasladoBienesResumenComponent,
		ModValornetoComponent,
		AsigBienesPersonComponent,
		AsigBienesPersonDetComponent,
		ActosadquisicionComponent,
		ActosbajaComponent,
		ActosdisposicionComponent,
		EliminacionComponent,
		RecodificacionComponent,
		PersonalComponent,
		InvPrincipalComponent,
		InvListaPrediosComponent,
		ReporteactosComponent,
		SituacionActualBienesComponent,
		ConfigDepreciacionComponent,
		InvResumenComponent,
		NotFoundComponent,
		AccesoComponent,
		CatalogosComponent,
		AreaComponent,
		ResponsablePatrComponent,
		ListPredioComponent,
		FichatecnicavehComponent,
		ModdatosbienComponent,
		TransfGestionMbComponent,
		CuentaContableComponent,
		ModFechdepreComponent,
		AsigBienesReporteComponent,
		FichaDatosReporteComponent,
		TipoRegistroComponent,
		FormaMasivaComponent,
		FormaIndividualComponent,
		//ListadoCatalogoBienesComponent,
		ListadoReingresoComponent,
		FormaMasivaReingresoComponent,
		FormaIndividualReingresoComponent,
		TipoRegistroReingresoComponent,
		ListadoComponent,		
		DevolucionactoadministracionComponent, ConfigCargaDocumentoComponent,
		Error500Component
	],
	imports: [
		NgbModule,
		BrowserModule,
		HttpClientModule,
		NgxSpinnerModule,
		FormsModule,
		ReactiveFormsModule,
		AngularFileUploaderModule,
		RouterModule.forRoot(
			appRoutes,
			{enableTracing: true } // <-- debugging purposes only
		),
		NgbDropdownModule.forRoot(),
		NgbModalModule.forRoot()
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
