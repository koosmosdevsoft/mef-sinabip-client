import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../router.animations';
import { SinabipmueblesService } from '../services/sinabipmuebles.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AngularFileUploaderComponent } from 'angular-file-uploader';
import swal from 'sweetalert2';

@Component({
	selector: 'app-inv-resumen',
	templateUrl: './inv-resumen.component.html',
	styleUrls: ['./inv-resumen.component.css'],
	animations: [routerTransition()],
	providers: [NgbModalConfig, NgbModal]
})
export class InvResumenComponent implements OnInit {

	ListPrev: any = [];
	ListUsua: any = [];
	NomEnt: any = [];
	ListPredDet: any = [];
	RegCabInvNuev: any = [];
	RegCabInvNuev2: any = [];
	NomArch_arg: any = [];
	NomArch2_arg: any = [];
	ElimArg_arg: any = [];
	ElimCab_arg: any = [];
	FinalInv_arg: any = [];
	ValidaDeprec_Arg: any = [];
	TotalAsig: any = [];
	EliminarfiltroPDF_Arg: any = [];
	id: string = sessionStorage.getItem("Cod_Entidad");
	id_usuario: string = sessionStorage.getItem("Cod_Usuario");
	periodo: string = sessionStorage.getItem("Periodo");
	cod_inv: string = sessionStorage.getItem("Cod_Inv");

	Elimina_Cab = { id: this.id, cod_inv: this.cod_inv }
	filtro_ListPrev = { id: this.id, periodo: this.periodo, usu_id: this.id_usuario }
	RegCabInv = { id: this.id, cod_inv: this.cod_inv, anno: this.periodo, idusuario: this.id_usuario }
	NombArch_Dat = { cod_inventario_ent: '0', estado: 0 }
	EliminarfiltroPDF = { id: this.id, COD_INVENTARIO_ENT: 0, ARCHIVO_NOMBRE_GENERADO: '', Peso_Arch: '',ESTADO : 0 }
	NombArch: string = "";
	TotalInf: number = 0;
	TotalConc: number = 0;
	NomInf: string = "";
	NomConc: string = "";
	FechInfo: string = "";
	FechConci: string = "";
	EstadoValDepre: string = "";
	mostrar_carga = null;
	//Paginacion 
	itemsPerPage: number = 10;
	page: any = 1;
	previousPage: any;
	total: any = 0;
	Adj_Doc: string = "";
	Adj_InfFinal: string = "";
	Adj_ActaConc: string = "";
	informe_cont: any = 0;
	concili_conc: any = 0;
	habilitar_finalizar: string = "";
	
	EstFinal_Arg;
	EstFinal = { id: this.id, cod_inv: this.cod_inv }

	afuConfig = {
		uploadAPI: {
			url: sessionStorage.getItem("Adjuntar_InfFinal"),
		},
		hideResetBtn: true,
		uploadBtnText: "Adjuntar Archivo",
		uploadMsgText: "",
		formatsAllowed: ".PDF,.pdf",
		maxSize: 50
	};

	afuConfig2 = {
		uploadAPI: {
			url: sessionStorage.getItem("Adjuntar_Conciliacion"),
		},
		hideResetBtn: true,
		uploadBtnText: "Adjuntar Archivo",
		uploadMsgText: "",
		formatsAllowed: ".PDF,.pdf",
		maxSize: 50
	};

	resetVar = false;
	closeResult: string;
	urlPDF;
	NomArchEstado = -1;
	@ViewChild('fileUpload1')
	private fileUpload1: AngularFileUploaderComponent;

	@ViewChild('fileUpload2')
	private fileUpload2: AngularFileUploaderComponent;

	constructor(config: NgbModalConfig, private Sinabip: SinabipmueblesService,
		private router: Router,
		private route: ActivatedRoute,
		private spinner: NgxSpinnerService,
		private modalService: NgbModal) {
		this.id = sessionStorage.getItem("Cod_Entidad");
		this.id_usuario = sessionStorage.getItem("Cod_Usuario");
		this.periodo = sessionStorage.getItem("Periodo");
		this.cod_inv = sessionStorage.getItem("Cod_Inv");
		this.ListadoPreviaInvFinal();
		this.ListadoUsuario_Id();
		this.NombreEntidad();
		this.load();
		this.EstadoFinalInv();
		config.backdrop = 'static';
		config.keyboard = false;
	}

	ngOnInit() {
	}

	Actualiza_InfFinal() {
		this.NomArchEstado = 1;
	}

	Actualiza_ActaConc() {
		this.NomArchEstado = 2;
	}

	load() {
		this.spinner.show();
		this.Sinabip.getListadoPreviaDetalleInvFinal(this.filtro_ListPrev.id, this.page, this.itemsPerPage).subscribe((data: any) => {
			this.spinner.hide();
			this.ListPredDet = data.data;
			this.total = (this.ListPredDet.length > 0) ? this.ListPredDet[0].Total : 0;
		});
	}

	desabilitar_ventana() {
		this.mostrar_carga = null;
	}

	nuevo(predio, estado) {
		this.mostrar_carga = {
			predio: predio, estado: estado
		}
	}

	open(content) {
		this.NombArch_Dat = { cod_inventario_ent: this.RegCabInvNuev.COD_INVENTARIO_ENT, estado: this.NomArchEstado }

		this.spinner.show();
		this.Sinabip.PostNombArchInffActConc(this.NombArch_Dat).subscribe((data: any) => {
			this.spinner.hide();
			this.NomArch_arg = data.data;
			this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
				this.closeResult = `Closed with: ${result}`;
			}, (reason) => {
			});

			this.NombArch = this.NomArch_arg.NOM_ARCHIVO;
			this.urlPDF = this.Sinabip.API_URL + "VerPD/" + this.NomArchEstado + "/" + this.NombArch;
		});
	}

	FinalizarInvertario(content2) {
		console.log(this.RegCabInv)
		this.spinner.show();
		this.Sinabip.getValidaAsignacionInv(this.id).subscribe((data: any) => {
			this.spinner.hide();
			this.TotalAsig = data.data;
			if(this.TotalAsig.CANT >= 1) {
				swal({
					title: 'Existen bienes sin Asignar desea continuar?',
					text: "",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Acepta!'
				}).then((result) => {
					if (result.value) {
						this.spinner.show();
						this.Sinabip.PostFinalizarInvertario(this.RegCabInv).subscribe((data: any) => {
							this.spinner.hide();
							this.FinalInv_arg = data.data;
							this.open_Sustento(content2);
						});
					}
				})
			}else{
				this.spinner.show();
				this.Sinabip.PostFinalizarInvertario(this.RegCabInv).subscribe((data: any) => {
					this.spinner.hide();
					this.FinalInv_arg = data.data;
					this.open_Sustento(content2);
				});
			}
			

		});
		
	}

	open_Sustento(content2) {
		this.modalService.open(content2, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
			this.closeResult = `Closed with: ${result}`;
		}, (reason) => {
		});
		this.urlPDF = this.Sinabip.API_URL + "Pdf_SustentoInventario/" + this.RegCabInv.id + "/" + this.RegCabInv.anno;
	}

	ListadoPreviaInvFinal() {

		this.spinner.show();
		this.Sinabip.getListadoPreviaInvFinal(this.filtro_ListPrev.id, this.filtro_ListPrev.periodo).subscribe((data: any) => {
			this.spinner.hide();
			this.ListPrev = data.data;
		});
	}

	NombArchInffActConc(cod_inventario_ent_ini, estado_ini) {
		this.spinner.show();
		this.NombArch_Dat = { cod_inventario_ent: cod_inventario_ent_ini, estado: estado_ini }
		this.Sinabip.PostNombArchInffActConc(this.NombArch_Dat).subscribe((data: any) => {
			this.spinner.hide();
			this.NomArch_arg = data.data;
			this.NombArch = this.NomArch_arg.NOM_ARCHIVO;
		});
	}

	AceptarAdjunto() {
		this.NombArchInffActConcUnion(this.RegCabInvNuev.COD_INVENTARIO_ENT);
	}

	NombArchInffActConcUnion(cod_inventario_ent_ini) {
		this.spinner.show();
		this.Sinabip.getNombArchInffActConcUnion(cod_inventario_ent_ini).subscribe((data: any) => {
			this.spinner.hide();
			this.NomArch2_arg = data.data;
			this.TotalInf = this.NomArch2_arg[0].TOTAL;
			this.TotalConc = this.NomArch2_arg[1].TOTAL;
			this.NomInf = this.NomArch2_arg[0].NOM_ARCHIVO;
			this.NomConc = this.NomArch2_arg[1].NOM_ARCHIVO;
			this.FechInfo = this.NomArch2_arg[0].FECHA_ARCHIVO;
			this.FechConci = this.NomArch2_arg[1].FECHA_ARCHIVO;

			this.informe_cont = this.NomInf.length;
			this.concili_conc = this.NomConc.length;
			if (this.informe_cont == 0) {
				//SIN INFORME
				this.habilitar_finalizar = '1'
			} else {
				this.habilitar_finalizar = '0'
				if (this.concili_conc == 0) {
					//SIN CONCILIACION
					this.habilitar_finalizar = '1'
				} else {
					this.habilitar_finalizar = '0'
				}
			}

		});
	}

	ListadoUsuario_Id() {
		// console.log(this.cod_inv)
		this.spinner.show();
		this.Sinabip.getListadoUsuario_Id(this.filtro_ListPrev.id,this.cod_inv).subscribe((data: any) => {
			this.spinner.hide();
			this.ListUsua = data.data;
		});
	}

	NombreEntidad() {
		this.spinner.show();
		this.Sinabip.getNombreEntidad(this.filtro_ListPrev.id).subscribe((data: any) => {
			this.spinner.hide();
			this.NomEnt = data.data;
		});
	}

	loadPage(page: number) {
		if (page !== this.previousPage) {
			this.previousPage = page;
			this.load();
		}
	}

	ver() {
		swal({
			title: 'Confirma si desea cancelar el resumen',
			text: "",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Acepta!'
		}).then((result) => {
			if (result.value) {
				this.EliminarcabInventario();
				this.router.navigate(["Inventario"]);
			}
		})
	}

	verinicio() {
		this.router.navigate(["Inventario"]);
	}

	EstadoFinalInv() {
		this.cod_inv = sessionStorage.getItem("Cod_Inv");
		this.EstFinal = { id: this.id, cod_inv: this.cod_inv }
		this.Sinabip.PostEstadoFinalInv(this.EstFinal).subscribe((data: any) => {
			this.EstFinal_Arg = data.data;
		});
	}

	// ValidaDepreciacioInv() {
	// 	this.spinner.show();
	// 	this.Sinabip.getValidaDepreciacioInv(this.id).subscribe((data: any) => {
	// 		this.spinner.hide();
	// 		this.ValidaDeprec_Arg = data.data;
	// 		this.EstadoValDepre = this.ValidaDeprec_Arg[0].RESULTADO;
	// 	});
	// }


	RegistrarcabInventario(id_ini, cod_inv_ini, anno_ini, idusuario_ini) {
		// this.ValidaDepreciacioInv();
		// setTimeout(() => {
		// 	if (this.EstadoValDepre == '0') {
				swal({
					title: 'Confirma si el resumen es el correcto para continuar..',
					text: "",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Acepta, Confirmar Resumen!'
				}).then((result) => {
					if (result.value) {
						this.RegCabInv = { id: id_ini, cod_inv: cod_inv_ini, anno: anno_ini, idusuario: idusuario_ini }
						this.spinner.show();
						this.Sinabip.PostRegistrarcabInventario(this.RegCabInv).subscribe((data: any) => {
							this.spinner.hide();

							if (data.data.error == true){
								if (data.data.reco.hasOwnProperty('id')) { 
									this.router.navigate(['error500']);
									return;
								}else if (data.data.reco.hasOwnProperty('cod_inv')) {
									  this.router.navigate(['error500']);
									  return;
								}else if (data.data.reco.hasOwnProperty('anno')) {
									this.router.navigate(['error500']);
									return;
								}else if (data.data.reco.hasOwnProperty('idusuario')) {
									this.router.navigate(['error500']);
									return;
								}
							}

							this.RegCabInvNuev = data.data;
							this.Adj_InfFinal = this.Sinabip.API_URL + "SubirActaInfFinal/" + this.RegCabInvNuev.COD_ENTIDAD + "-" + this.RegCabInvNuev.COD_INVENTARIO + "-" + this.RegCabInvNuev.COD_INVENTARIO_ENT + "-" + "1";
							sessionStorage.setItem("Adjuntar_InfFinal", this.Adj_InfFinal);
							this.afuConfig.uploadAPI.url = this.Adj_InfFinal;
							this.fileUpload1.uploadAPI = this.Adj_InfFinal;
							this.Adj_ActaConc = this.Sinabip.API_URL + "SubirActaInfFinal/" + this.RegCabInvNuev.COD_ENTIDAD + "-" + this.RegCabInvNuev.COD_INVENTARIO + "-" + this.RegCabInvNuev.COD_INVENTARIO_ENT + "-" + "2";
							sessionStorage.setItem("Adjuntar_Conciliacion", this.Adj_ActaConc);
							this.afuConfig2.uploadAPI.url = this.Adj_ActaConc;
							this.fileUpload2.uploadAPI = this.Adj_ActaConc;
							sessionStorage.setItem("cod_inventario_ent", this.RegCabInvNuev.COD_INVENTARIO_ENT);
							this.NombArchInffActConcUnion(this.RegCabInvNuev.COD_INVENTARIO_ENT);
						});
						this.nuevo(1, 2);
					}
				})
		// 	} else {
		// 		swal('Aviso: Para continuar con la Finalización del inventario se debe actualizar los valoras de depreciación, ingresar al menú Administración Interna / Modificación de Valor Neto.');
		// 	}
		// }, 800);
	}

	EliminarcabInventario() {
		this.Elimina_Cab = { id: this.id, cod_inv: this.cod_inv }
		this.spinner.show();
		this.Sinabip.PostEliminarcabInventario(this.Elimina_Cab).subscribe((data: any) => {
			this.spinner.hide();

			if (data.data.error == true){
				if (data.data.reco.hasOwnProperty('id')) { 
					this.router.navigate(['error500']);
					return;
				}else if (data.data.reco.hasOwnProperty('cod_inv')) {
					  this.router.navigate(['error500']);
					  return;
				}
				
			}

			this.ElimCab_arg = data.data;
		});
	}

	EliminarArchivo() {
		this.spinner.show();
		this.Sinabip.getEliminarArchivo(this.NomArchEstado, this.NombArch).subscribe((data: any) => {
			this.spinner.hide();
			this.ElimArg_arg = data.data;
		});
		if (this.NomArchEstado == 1) {
			this.fileUpload1.resetFileUpload();
		} else {
			this.fileUpload2.resetFileUpload();
		}
	}

	EliminarArchivoInvPdf(estadoPDF) {
		this.EliminarfiltroPDF = { id: this.id, COD_INVENTARIO_ENT: this.RegCabInvNuev.COD_INVENTARIO_ENT ,ARCHIVO_NOMBRE_GENERADO: '', Peso_Arch: '',ESTADO : estadoPDF }
		this.spinner.show();
		this.Sinabip.PostEliminarArchivoInvPdf(this.EliminarfiltroPDF).subscribe((data: any) => {
			this.spinner.hide();
			this.EliminarfiltroPDF_Arg = data.data;
			console.log(this.EliminarfiltroPDF_Arg)
			if (this.EliminarfiltroPDF_Arg == true) {
				if(estadoPDF = 3){
					this.TotalInf = 0;
				}
				if(estadoPDF = 4){
					this.TotalConc = 0;
				}
			}


		});
	}


}
