import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvResumenComponent } from './inv-resumen.component';

describe('InvResumenComponent', () => {
  let component: InvResumenComponent;
  let fixture: ComponentFixture<InvResumenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvResumenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvResumenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
