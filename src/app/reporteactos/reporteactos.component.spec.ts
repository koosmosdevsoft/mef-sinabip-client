import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteactosComponent } from './reporteactos.component';

describe('ReporteactosComponent', () => {
  let component: ReporteactosComponent;
  let fixture: ComponentFixture<ReporteactosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteactosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteactosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
