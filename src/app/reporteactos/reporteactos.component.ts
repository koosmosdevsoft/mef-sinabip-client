import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { SinabipmueblesService } from '../services/sinabipmuebles.service';
import {NgbModalConfig, NgbModal, ModalDismissReasons, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import swal from'sweetalert2';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
	selector: 'app-reporteactos',
	templateUrl: './reporteactos.component.html',
	styleUrls: ['./reporteactos.component.css'],
	animations: [routerTransition()],
	providers: [NgbModalConfig, NgbModal]
})
export class ReporteactosComponent implements OnInit {

	registerForm: FormGroup;
	RegistroTipoBienForm: FormGroup;
	submitted = false;
	seleccionRows: string = null;

	resultado: void;
	titularAlerta: string = '';
	logros: ILogro[];
	titulo: string = 'Bienvenidos';
	dataActosAdquisicion: any = [];
	dataFormaIndividual: Array<any> = [];
	databienesEliminados: Array<any> = [];
	dataAdjuntado: any = [];
	dataEditarActos: any = [];
	dataverDetalleActos: any = [];
	dataListado_Forma_Acto: any = [];
	dataEliminarBien: any = [];
	dataAgregarBien: any = [];
	dataGuardarBien: any = [];
	mostrarVentanaTipoRegistro: boolean = false;
	mostrarVentanaRegistroIndividual: boolean = false;
	mostrarVentanaRegistroMasivo: boolean = false;
	closeResult: string;
	seleccionBien1: boolean = false;
	seleccionBien2: boolean = false;
	b_ventanaObservacionesValidacion: boolean = false;
	mostrarVentanaCaracteristicas: boolean = false;
	ResultadoGrabar: number = 0;
	TipoagregarBien: string = "";
	id_altas: any = [];
	dataCaracteristicasBien: Array<any> = [];
	dataObservacionValidacion: Array<any> = [];
	cantidadduplicado_actos: number = 0;
	cantidadduplicado_bienes: number = 0;
	cantidaderrores_columnas: number = 0;
	nro_documento_url: string = '';

	Cod_Entidad: string = sessionStorage.getItem("Cod_Entidad");
	Cod_Usuario: string = sessionStorage.getItem("Cod_Usuario");

	afuConfig = {
		uploadAPI: {
			url: this.SinabipMuebles.API_URL + "SubirAltaTxt/" + this.Cod_Entidad,
		},
		hideResetBtn: true,
		uploadBtnText: "Adjuntar Archivo",
		uploadMsgText: "",
		formatsAllowed: ".TXT,.txt"
	};

	heroForm: any = [];

	//Paginacion 
	itemsPerPage: number = 20;
	page: any = 1;
	previousPage: any;
	total: any = 0;




	filtro = {
		fecha:
		{
			month: 10,
			year: 2018
		},
		cod_entidad: this.Cod_Entidad,
		forma_adquisicion: -1,
		tipo_acto: '-1',
		nro_documento: '',
		estado: '1',
		// fechaDesde: {date:16,month:0,year:2019},
		// fechaHasta: {date:16,month:0,year:2019},
		fechaDesde:null,
		fechaHasta:null,
		page: this.page, records: this.itemsPerPage
	}


	parametros = {
		cod_entidad: this.Cod_Entidad,
		nro_grupo: "-1",
		nro_clase: "",
		denom_bien: "",
		nro_bien: "",
		cod_patrimonial: "",
		denom_patrimonial: ""
	}
	modal;
	Tipobien = {
		codigo: "",
		denominacion: ""
	}

	paramBusqueda = {
		id_entidad: this.Cod_Entidad,
		id_acto: ""
	}

	paramEliminarBien = {
		id_entidad: this.Cod_Entidad,
		id_bien: "",
		id_usuario: ""
	}

	paramAgregar = {
		TipoagregarBien: "",
		id_acto: "",
		id_entidad: this.Cod_Entidad,
		id_forma: undefined,
		nro_documento: "",
		fecha_documento: "",
		usua_creacion: this.Cod_Usuario,
		codigo_patrimonial: "",
		denominacion_bien: "",
		cantidad: "",
		valor_adq: "",
		estado: "",
		tipo_acto: '-1'
	}

	paramCarga = {
		id_entidad: '',
		id_adjuntado: '',
		nombreArchivo: '',
		usua_creacion: ''
	}

	estado_adjuntar: string = "";
	estado_carga: string = "";
	estado_carga_fecha: string = "";
	estado_carga_total: string = "";
	estado_validacion: string = "";
	estado_validacion_fecha: string = "";
	estado_validacion_total: string = "";
	estado_finalizacion: string = "";
	estado_finalizacion_fecha: string = "";
	estado_finalizacion_total: string = "";

	paramBien = {
		id_entidad: this.Cod_Entidad,
		id_bien: "",
		formaAdquis: undefined,
		nroDocAdquis: "",
		fechaAdquis: "",
		codigopatri: "",
		denomBien: "",
		marca: "",
		modelo: "",
		tipo: "",
		color: "",
		nroSerie: "",
		nroChasis: "",
		anioFabric: "",
		otrasCaract: "",
		usoCta: "",
		TipoCta: "",
		CtaContable: "",
		valorAdquis: "",
		porcDeprec: "",
		asegurado: "",
		estadoBien: "",
		observacion: ""
	}

	paramGuardar = {
		id_acto: "",
		id_entidad: "",
		id_forma: "",
		nro_documento: "",
		fecha_documento: "",
		usua_modifica: ""
	}

	paramEliminarActo = {
		id_entidad: "",
		id_acto: ""
	}


	model: any = {};

	id: string = "1";
	urlPDF;


	constructor
	(
		private formBuilder: FormBuilder,
		private SinabipMuebles: SinabipmueblesService,
		private spinner: NgxSpinnerService,
		private modalService: NgbModal,
		public Sinabip : SinabipmueblesService,
		private router: Router
	) {

		let token = sessionStorage.getItem("token");  
		let ruta = sessionStorage.getItem("RutaSinabip")
		if(token === null){
			// window.location.replace(ruta+"SGISBN/System/sinabip.php");
			window.location.replace("./SGISBN/System/sinabip.php");
		}
		this.cargarListadoActos();


		let fechita = new Date();
		let dd = fechita.getDate();
		let mm = fechita.getMonth() + 1;
		let yy = fechita.getFullYear(); 
		let ddd;
		let mmm;

		if (dd < 10) {
			ddd = '0' + dd; 
		}else{
			ddd = dd;
		}

		if (mm < 10) {	
			mmm = '0' + mm;
		}else{
			mmm = mm;
		}

		this.filtro.fechaDesde = yy + '-' + mmm + '-' + ddd;
		this.filtro.fechaHasta = yy + '-' + mmm + '-' + ddd;

		console.log(this.filtro.fechaDesde);
		console.log(this.filtro.fechaHasta);

		let date = new Date();
		date.setDate(date.getDate() - 11);
		let year = date.getFullYear();
		let month = date.getMonth() + 1;
		this.filtro.fecha.month = month;
		this.filtro.fecha.year = year;
		this.afuConfig.uploadAPI.url = this.SinabipMuebles.API_URL + "AdjuntarAltatxt/" + this.Cod_Entidad;

	}


	// fecha_hoy(){
	//   //let today = new Date("10/10/2018");
	//   let today = new Date('12-10-2018').toLocaleDateString('en-US');
	//   today = Date.parseDate(today, 'Y-m-d');

	//   let dd = CDate(today).getDate();
	//   var mm = today.getMonth()+1; 
	//   var yyyy = today.getFullYear();

	//   if(dd<10) 
	//   {
	//     dd=0+dd;
	//   } 

	//   if(mm<10) 
	//   {
	//     mm=0+mm;
	//   } 

	//   today = new Date(mm+'-'+dd+'-'+yyyy);
	//   console.log('a'+today);
	//   // today = mm+'/'+dd+'/'+yyyy;
	//   // console.log(today);
	//   // today = dd+'-'+mm+'-'+yyyy;
	//   // console.log(today);
	//   // today = dd+'/'+mm+'/'+yyyy;
	//   // console.log(today);
	// }


	ngOnInit(): void {



		this.registerForm = this.formBuilder.group({
			documentoActo: ['', Validators.required],
			formaAdquisicion: ['', Validators.required],
			fechaAdquisicion: ['', [Validators.required]],
			//fechaAdquisicion: ['', [Validators.required, Validators.email]],
			//password: ['', [Validators.required, Validators.minLength(6)]]
		});


		this.RegistroTipoBienForm = this.formBuilder.group({
			cantidad: ['', Validators.required],
			valorAdquisicion: ['', Validators.required]
		});

		this.filtro.estado = '-1';
		//this.filtro.fecha.month = -1;
		//this.filtro.forma_adquisicion = -1,

		//this.cargarListadoActos();


	}

	get f() { return this.registerForm.controls; }




	onSubmit() {
		this.submitted = true;

		// stop here if form is invalid
		if (this.registerForm.invalid) {
			swal({
				type: 'error',
				title: 'Datos Incompletosss!!!',
				text: 'No se ha ingresado la información necesaria para continuar',
				// footer: '<a href>Why do I have this issue?</a>'
			})
			return;
		}
		else {
			this.Guardar_Actos();
		}


	}




	onSubmit2() {
		this.submitted = true;

		// stop here if form is invalid
		if (this.registerForm.invalid) {
			swal({
				type: 'error',
				title: 'Datos Incompletosss!!!',
				text: 'No se ha ingresado la información necesaria para continuar',
				// footer: '<a href>Why do I have this issue?</a>'
			})
			return;
		}
		else {
			// this.Guardar_Actos();
		}


	}


	SubirZip() {
		// this.buscar_predio(); 
		// this.mostrar_carga.estado = '5';
		// console.log(this.mostrar_carga);
		// this.nuevo(this.mostrar_carga.predio,this.mostrar_carga.estado);
	}
	AdjuntarAltatxt(event) {

		this.afuConfig.uploadAPI.url = this.SinabipMuebles.API_URL + "AdjuntarAltatxt/" + this.Cod_Entidad;
		//console.log(JSON.parse(event.response));
		this.dataAdjuntado = JSON.parse(event.response);
		console.log(this.dataAdjuntado);
		this.estado_adjuntar = this.dataAdjuntado.data.RESULTADO;
		console.log(this.estado_adjuntar);

	}

	get name() { return this.heroForm.get('name'); }

	get power() { return this.heroForm.get('power'); }

	ventanaTipoRegistro() {
		this.mostrarVentanaTipoRegistro = true;
		this.mostrarVentanaRegistroIndividual = false;
		this.b_ventanaObservacionesValidacion = false;
		//this.paramBien.formaAdquis = undefined;
		this.paramAgregar.id_forma = undefined;

		this.paramAgregar = {
			TipoagregarBien: "",
			id_acto: "",
			id_entidad: "",
			id_forma: "",
			nro_documento: "",
			fecha_documento: "",
			usua_creacion: "",
			codigo_patrimonial: "",
			denominacion_bien: "",
			cantidad: "",
			valor_adq: "",
			estado: "",
			tipo_acto: '-1'
		}

		this.dataEditarActos = [];

		this.id = "-1";

		this.estado_adjuntar = '';
		this.estado_carga = '';
		this.estado_carga_fecha = '';
		this.estado_carga_total = '';
		this.estado_validacion = '';
		this.estado_validacion_fecha = '';
		this.estado_validacion_total = '';
		this.estado_finalizacion = '';
		this.estado_finalizacion_fecha = '';
		this.estado_finalizacion_total = '';


	}

	desabilitar_ventana(){
		this.mostrarVentanaTipoRegistro = null;
		this.mostrarVentanaRegistroIndividual = null;
	}
	
	ventanaRegistroFormaIndividual() {
		this.mostrarVentanaTipoRegistro = false;
		this.mostrarVentanaRegistroIndividual = true;
		this.mostrarVentanaRegistroMasivo = false;
	}

	ventanaRegistroFormaMasiva() {
		this.mostrarVentanaTipoRegistro = false;
		this.mostrarVentanaRegistroIndividual = false;
		this.mostrarVentanaRegistroMasivo = true;

		this.estado_adjuntar = '';
		this.estado_carga = '';
		this.estado_carga_fecha = '';
		this.estado_carga_total = '';
		this.estado_validacion = '';
		this.estado_validacion_fecha = '';
		this.estado_validacion_total = '';
		this.estado_finalizacion = '';
		this.estado_finalizacion_fecha = '';
		this.estado_finalizacion_total = '';

	}


	loadPage(page: number) {
		if (page !== this.previousPage) {
			this.previousPage = page;
			this.cargarListadoActos();
		}
	}

	resetearpag() {
		this.filtro.page = 1;
		this.cargarListadoActos();
	}

	// resetearlimpiarpag(){
	//   this.page = 1;
	//   this.Limpiar_ListadoAsignarPerson();
	// }


	ListadoCatalogoFormaIndividual() {
		this.spinner.show();
		this.SinabipMuebles.postListadoCatalogoFormaIndividual(this.parametros).subscribe((data: any) => {
			this.spinner.hide();
			console.log(data.data);
			this.dataFormaIndividual = data.data;

		});
	}

	ListadoBienesEliminados() {
		this.spinner.show();
		this.SinabipMuebles.postListadoBienesEliminados(this.parametros).subscribe((data: any) => {
			this.spinner.hide();
			console.log(data.data);
			this.databienesEliminados = data.data;
		});
	}

	cargarListadoActos() {
		this.filtro.cod_entidad = this.Cod_Entidad;
		if (this.filtro.nro_documento  == ''){
			this.nro_documento_url = '-';
		}else{
			this.nro_documento_url = this.filtro.nro_documento;
		}

		console.log(this.filtro);
		this.spinner.show();
		this.SinabipMuebles.postListadoActos(this.filtro).subscribe((data: any) => {
			this.spinner.hide();

			if (data.data.error == true){
				if (data.data.reco.hasOwnProperty('cod_entidad')) { 
					this.router.navigate(['error500']);
					return;
				}else if (data.data.reco.hasOwnProperty('tipo_acto')) {
					  this.router.navigate(['error500']);
					  return;
				}else if (data.data.reco.hasOwnProperty('nro_documento')) {
					swal({
						type: 'error',
						title: 'Validacion de Datos',
						text: data.data.reco.nro_documento[0],
					  })
					return;
				}else if (data.data.reco.hasOwnProperty('fechaDesde')) {
					swal({
						type: 'error',
						title: 'Validacion de Datos',
						text: data.data.reco.fechaDesde[0],
					  })
					return;
				}else if (data.data.reco.hasOwnProperty('fechaHasta')) {
					swal({
						type: 'error',
						title: 'Validacion de Datos',
						text: data.data.reco.fechaHasta[0],
					  })
					return;
				}else if (data.data.reco.hasOwnProperty('page')) {
					this.router.navigate(['error500']);
					return;	
				}else if (data.data.reco.hasOwnProperty('records')) {
					this.router.navigate(['error500']);
					return;					
				}
			}

			this.dataActosAdquisicion = data.data;
			console.log(this.dataActosAdquisicion);

			this.total = (this.dataActosAdquisicion.documento.length > 0) ? this.dataActosAdquisicion.documento[0].TOTAL : 0;
			console.log(this.total);
		});
	}

	buscar_actos(){

		if(this.filtro.tipo_acto !== '-1'){
			this.cargarListadoActos();
		}else{
			swal({
				type: 'warning',
				title: 'Datos Incompletos!!!',
				text: 'Ud. no ha indicado el tipo de acto',
				// footer: '<a href>Why do I have this issue?</a>'
			})
		}
	}

	Eliminar_Actos(cod_entidad, id_acto) {

		this.paramEliminarActo = {
			id_entidad: cod_entidad,
			id_acto: id_acto
		}

		if (confirm("¿Está Ud. seguro de eliminar el Acto?")) {
			this.spinner.show();
			this.SinabipMuebles.postEliminar_Actos(this.paramEliminarActo).subscribe((data: any) => {
				this.spinner.hide();
				console.log(cod_entidad, id_acto);
				this.cargarListadoActos();
			});
		}

	}

	BorrarFiltro() {
		this.filtro = {
			fecha:
			{
				month: 1,
				year: 2018
			},
			cod_entidad: this.Cod_Entidad,
			forma_adquisicion: -1,
			tipo_acto: '-1',
			nro_documento: '',
			estado: '-1',
			fechaDesde: null,
			fechaHasta: null,
			page: this.page, records: this.itemsPerPage

		}
		this.dataActosAdquisicion.documento = [];

		console.log(this.filtro);
	}



	traerDatosTipoBien(obj) {
		this.paramAgregar.codigo_patrimonial = obj.NRO_BIEN;
		this.paramAgregar.denominacion_bien = obj.DENOM_BIEN;
		this.TipoagregarBien = "1";
	}

	traerDatosCodPatrimonialesEliminados(obj) {
		this.paramAgregar.codigo_patrimonial = obj.CODIGO_PATRIMONIAL;
		this.paramAgregar.denominacion_bien = obj.DENOMINACION_BIEN;
		this.TipoagregarBien = "2";
	}


	open(content) {
		this.parametros = {
			cod_entidad: this.Cod_Entidad,
			nro_grupo: "-1",
			nro_clase: "-1",
			denom_bien: "",
			nro_bien: "",
			cod_patrimonial: "",
			denom_patrimonial: ""
		}
		this.paramAgregar.codigo_patrimonial = "";
		this.paramAgregar.denominacion_bien = "";
		this.paramAgregar.cantidad = "";
		this.paramAgregar.valor_adq = "";
		this.dataFormaIndividual = [];

		this.onSubmit2();
		if (this.registerForm.valid) {

			this.spinner.show();
			console.log(this.parametros);
			this.modal = this.SinabipMuebles.postListadoCatalogoFormaIndividual(this.parametros).subscribe((data: any) => {
				this.spinner.hide();
				console.log(data.data);
				this.dataFormaIndividual = data.data;
			});


			this.modalService.open(content, {
				ariaLabelledBy: 'modal-basic-title',
				keyboard: false,
				size: 'lg',
				backdrop: 'static'
			}).result.then((result) => {
				this.closeResult = `Closed with: ${result}`;
			}, (reason) => {
				this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
			});
		} else {

			swal({
				type: 'error',
				title: 'Datos Incompletos!!!',
				text: 'No se ha ingresado la información necesaria para continuar',
				// footer: '<a href>Why do I have this issue?</a>'
			})


		}
	}


	open1(content, id_bien) {

		this.paramBien.id_entidad = this.Cod_Entidad;
		this.paramBien.id_bien = id_bien;
		//console.log(this.paramBien);

		this.spinner.show();
		this.modal = this.SinabipMuebles.postDatos_Caracteristicas_Bien(this.paramBien).subscribe((data: any) => {
			this.spinner.hide();

			

			this.dataCaracteristicasBien = data.data;
			let acto = data.data;
			this.paramBien.formaAdquis = acto.CaracteristicasBien[0].NOM_FORM_ADQUIS;
			this.paramBien.nroDocAdquis = acto.CaracteristicasBien[0].NRO_DOCUMENTO_ADQUIS;
			this.paramBien.fechaAdquis = acto.CaracteristicasBien[0].FECHA_DOCUMENTO_ADQUIS;
			this.paramBien.codigopatri = acto.CaracteristicasBien[0].CODIGO_PATRIMONIAL;
			this.paramBien.denomBien = acto.CaracteristicasBien[0].DENOMINACION_BIEN;

			this.paramBien.marca = acto.CaracteristicasBien[0].MARCA;
			this.paramBien.modelo = acto.CaracteristicasBien[0].MODELO;
			this.paramBien.tipo = acto.CaracteristicasBien[0].TIPO;
			this.paramBien.color = acto.CaracteristicasBien[0].COLOR;
			this.paramBien.nroSerie = acto.CaracteristicasBien[0].SERIE;
			this.paramBien.nroChasis = acto.CaracteristicasBien[0].NRO_CHASIS;
			this.paramBien.anioFabric = acto.CaracteristicasBien[0].ANIO_FABRICACION;
			this.paramBien.otrasCaract = acto.CaracteristicasBien[0].OTRAS_CARACT;

			this.paramBien.usoCta = acto.CaracteristicasBien[0].USO_CUENTA;
			this.paramBien.TipoCta = acto.CaracteristicasBien[0].TIP_CUENTA;
			this.paramBien.CtaContable = acto.CaracteristicasBien[0].COD_CTA_CONTABLE;
			this.paramBien.valorAdquis = acto.CaracteristicasBien[0].VALOR_ADQUIS;
			this.paramBien.porcDeprec = acto.CaracteristicasBien[0].PORC_DEPREC;
			this.paramBien.asegurado = acto.CaracteristicasBien[0].OPC_ASEGURADO;
			this.paramBien.estadoBien = acto.CaracteristicasBien[0].COD_ESTADO_BIEN;
			this.paramBien.observacion = acto.CaracteristicasBien[0].OBSERVACION;


			console.log(data.data);

		});


		this.modalService.open(content, {
			ariaLabelledBy: 'modal-basic-title',
			keyboard: false,
			size: 'lg',
			backdrop: 'static'
		}).result.then((result) => {
			this.closeResult = `Closed with: ${result}`;
		}, (reason) => {
			this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
		});
	}



	openValidacion(content, id_adjuntdo) {

		this.paramCarga.id_entidad = this.Cod_Entidad;
		this.paramCarga.id_adjuntado = this.dataAdjuntado.data.ID_ADJUNTADO;
		console.log(this.paramCarga);

		this.spinner.show();
		this.modal = this.SinabipMuebles.postObservaciones_Validacion(this.paramBien).subscribe((data: any) => {
			this.spinner.hide();

			this.dataObservacionValidacion = data.data;
			//let acto = data.data;
			//this.paramBien.formaAdquis = acto.CaracteristicasBien[0].NOM_FORM_ADQUIS;      

			//console.log(data.data);
			//console.log('aaa');
			//console.log(this.dataObservacionValidacion.length);

		});


		this.modalService.open(content, {
			ariaLabelledBy: 'modal-basic-title',
			keyboard: false,
			size: 'lg',
			backdrop: 'static'
		}).result.then((result) => {
			this.closeResult = `Closed with: ${result}`;
		}, (reason) => {
			this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
		});
	}

	Guardar_Caracteristicas_Bien(): void {
		this.spinner.show();
		console.log(this.paramBien);
		this.SinabipMuebles.postGuardar_Caracteristicas_Bien(this.paramBien).subscribe((data: any) => {
			this.spinner.hide();
			console.log(data.data);
			this.modalService.dismissAll();

			swal({
				position: 'center',
				type: 'success',
				title: 'Se Actualizaron las caracteristicas Satisfactoriamente!!!',
				showConfirmButton: false,
				timer: 2000
			})

		});
	}

	cargarTXT() {

		this.paramCarga.id_entidad = this.Cod_Entidad;
		this.paramCarga.id_adjuntado = this.dataAdjuntado.data.ID_ADJUNTADO;
		this.paramCarga.nombreArchivo = this.dataAdjuntado.data.NOMBRE_ARCHIVO;
		this.paramCarga.usua_creacion = this.Cod_Usuario;
		//console.log(this.paramCarga);

		this.spinner.show();
		this.SinabipMuebles.postCargarTXT(this.paramCarga).subscribe((data: any) => {
			this.spinner.hide();
			console.log(data.data);
			this.estado_carga = data.data.ESTADO_CARGA;
			this.estado_carga_fecha = data.data.CARGADO_FECHA;
			this.estado_carga_total = data.data.CARGADO_TOTAL;

			//this.dataGuardarBien = data.data; 

			swal({
				position: 'center',
				type: 'success',
				title: 'Carga Finalizada',
				showConfirmButton: false,
				timer: 2000
			})

		});
	}

	ValidarTXT(content) {

		//console.log(this.dataAdjuntado.data.ID_ADJUNTADO);
		this.paramCarga.id_entidad = this.Cod_Entidad;
		this.paramCarga.id_adjuntado = this.dataAdjuntado.data.ID_ADJUNTADO;
		this.paramCarga.usua_creacion = this.Cod_Usuario;
		//console.log(this.paramCarga);


		this.spinner.show();
		this.SinabipMuebles.postValidarTXT(this.paramCarga).subscribe((data: any) => {
			this.spinner.hide();
			console.log(data.data);
			this.estado_validacion = data.data.ESTADO_VALIDACION;
			this.estado_validacion_fecha = data.data.VALIDADO_FECHA;
			this.estado_validacion_total = data.data.VALIDADO_TOTAL;

		});

		if (this.estado_validacion == "VALIDACION_SATISFACTORIA") {

			swal({
				position: 'center',
				type: 'success',
				title: 'Validación Finalizada',
				showConfirmButton: false,
				timer: 2000
			})
		} else {

			this.spinner.show();
			this.modal = this.SinabipMuebles.postListadoErroresCargaMasiva(this.paramCarga).subscribe((data: any) => {
				this.spinner.hide();
				console.log(data.data);
				//console.log(this.paramCarga);
				this.dataObservacionValidacion = [];
				this.dataObservacionValidacion = data.data;
				console.log('aaa');
				console.log(this.dataObservacionValidacion["duplicado_actos"].length);
				console.log(this.dataObservacionValidacion["duplicado_bienes"].length);
				console.log(this.dataObservacionValidacion["errores_columnas"].length);
				this.cantidadduplicado_actos = this.dataObservacionValidacion["duplicado_actos"].length
				this.cantidadduplicado_bienes = this.dataObservacionValidacion["duplicado_bienes"].length
				this.cantidaderrores_columnas = this.dataObservacionValidacion["errores_columnas"].length


			});

			this.b_ventanaObservacionesValidacion = true;
			this.modalService.open(content, {
				ariaLabelledBy: 'modal-basic-title',
				keyboard: false,
				size: 'lg',
				backdrop: 'static'
			}).result.then((result) => {
				this.closeResult = `Closed with: ${result}`;
			}, (reason) => {
				this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
			});

		}



	}


	FinalizarTXT() {

		//console.log(this.dataAdjuntado.data.ID_ADJUNTADO);
		this.paramCarga.id_entidad = this.Cod_Entidad;
		this.paramCarga.id_adjuntado = this.dataAdjuntado.data.ID_ADJUNTADO;
		this.paramCarga.usua_creacion = this.Cod_Usuario;
		console.log(this.paramCarga);

		this.spinner.show();
		this.SinabipMuebles.postFinalizarTXT(this.paramCarga).subscribe((data: any) => {
			this.spinner.hide();
			console.log(data.data);
			this.estado_finalizacion = data.data.ESTADO_FINALIZACION;
			this.estado_finalizacion_fecha = data.data.FINALIZADO_FECHA;
			this.estado_finalizacion_total = data.data.FINALIZADO_TOTAL;

			swal({
				position: 'center',
				type: 'success',
				title: 'Finalizada',
				showConfirmButton: false,
				timer: 2000
			})

		});

	}



	AgregarBien_Actos(): void {

		this.paramAgregar.TipoagregarBien = this.TipoagregarBien
		this.paramAgregar.id_entidad = this.Cod_Entidad;
		this.paramAgregar.usua_creacion = this.Cod_Usuario;
		if (this.paramAgregar.id_acto == '') {
			this.paramAgregar.id_acto = '-1';
		}

		console.log(this.paramAgregar);

		this.spinner.show();
		this.SinabipMuebles.postAgregarBien_Actos(this.paramAgregar).subscribe((data: any) => {
			this.spinner.hide();
			//console.log(data.data);

			this.id_altas = data.data;
			this.paramAgregar.id_acto = this.id_altas.cabecera.ID;
			console.log(this.paramAgregar.id_acto);

			this.cargarListadoActos();

			this.verDetalleActo(this.paramAgregar.id_acto, '');
			this.modalService.dismissAll();

			//    swal('Registro exitoso...', this.titularAlerta, 'success'); 
			swal({
				position: 'center',
				type: 'success',
				title: 'Se ha Registrado Satisfactoriamente!!!',
				showConfirmButton: false,
				timer: 2000
			})



		});

	}

	Guardar_Actos(): void {

		this.paramAgregar.id_entidad = this.Cod_Entidad;
		this.paramAgregar.usua_creacion = this.Cod_Usuario;
		console.log(this.paramAgregar);

		this.spinner.show();
		this.SinabipMuebles.postGuardar_Actos(this.paramAgregar).subscribe((data: any) => {
			this.spinner.hide();
			console.log(data.data);
			this.dataGuardarBien = data.data;
			this.cargarListadoActos();
			this.mostrarVentanaTipoRegistro = false;
			this.mostrarVentanaRegistroIndividual = false;


			swal({
				type: 'error',
				title: 'Información',
				text: 'Se ha Registrado Satisfactoriamente!!!',
				// footer: '<a href>Why do I have this issue?</a>'
			})

		});

	}

	verDetalleActo(id_acto, tipo_acto) {

		this.paramAgregar.id_entidad = this.Cod_Entidad;
		this.paramAgregar.id_acto = id_acto;
		this.paramAgregar.tipo_acto = tipo_acto;

		this.spinner.show();
		this.SinabipMuebles.postverDetalleActo(this.paramAgregar).subscribe((data: any) => {
			this.spinner.hide();

			if (data.data.error == true){
				if (data.data.reco.hasOwnProperty('id_entidad')) { 
					this.router.navigate(['error500']);
					return;
				}else if (data.data.reco.hasOwnProperty('id_acto')) {
					  this.router.navigate(['error500']);
					  return;
					}else if (data.data.reco.hasOwnProperty('tipo_acto')) {
						this.router.navigate(['error500']);
						return;
				}
				
			}
			
			this.dataverDetalleActos = data.data;
			console.log(this.dataverDetalleActos);

			this.mostrarVentanaTipoRegistro = false;
			this.mostrarVentanaRegistroIndividual = true;

			this.seleccionRows = id_acto;

		});

	}

	



	Imprimir_Detalle_Acto(id_acto, tipo_acto){
		
		
		// this.SinabipMuebles.postListadoFormaActo(this.filtro).subscribe((data: any) => {
		// 	this.spinner.hide();

		// 	this.dataListado_Forma_Acto = data.data;
		// 	this.filtro.forma_adquisicion = -1
		// 	console.log(this.dataListado_Forma_Acto);
		// 	console.log(this.dataListado_Forma_Acto);
		// });

		this.open_Resumen(id_acto, tipo_acto);
	}

	open_Resumen(id_acto, tipo_acto) {
		
		this.urlPDF = this.SinabipMuebles.API_URL+"Pdf_Imprimir_Detalle_Acto/"+  this.Cod_Entidad + "/" + id_acto +"/"+ tipo_acto;
		console.log(this.urlPDF);
	}



	Eliminar_Bien(id_bien) {

		swal({
			title: '¿Esta Ud. seguro de eliminar el Bien?',
			text: "¡No podrás revertir esto!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonText: 'Cancelado',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si, Eliminar!'

		}).then((result) => {
			if (result.value) {

				this.paramEliminarBien.id_entidad = this.Cod_Entidad;
				this.paramEliminarBien.id_usuario = this.Cod_Usuario;
				this.paramEliminarBien.id_bien = id_bien;
				//console.log(this.paramEliminarBien);

				this.spinner.show();
				this.SinabipMuebles.postEliminar_Bien_Proceso(this.paramEliminarBien).subscribe((data: any) => {
					this.spinner.hide();

					this.dataEliminarBien = data.data;
					//console.log(this.dataEliminarBien[0].RESULTADO);
					//console.log(this.paramAgregar.id_acto);

					if (this.dataEliminarBien[0].RESULTADO == 'OK') {

						this.verDetalleActo(this.paramAgregar.id_acto, '');
						this.cargarListadoActos();
						//console.log(this.dataEditarActos);
						swal(
							'Eliminado!',
							'El registro ha sido eliminado.',
							'success'
						)
					} else {
						swal(
							'Error!',
							'Ocurrio un inconveniente',
							'error'
						)
					}


				});



			}
		})

	}


	ListadoFormaActo() {

		console.log(this.filtro);

		this.spinner.show();
		this.SinabipMuebles.postListadoFormaActo(this.filtro).subscribe((data: any) => {
			this.spinner.hide();

			this.dataListado_Forma_Acto = data.data;
			this.filtro.forma_adquisicion = -1
			console.log(this.dataListado_Forma_Acto);

			//this.paramAgregar.id_forma = this.dataEditarActos.cabecera.COD_FORM_ADQUIS;

			console.log(this.dataListado_Forma_Acto);
		});

	}





	private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
			return 'by pressing ESC';
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
			return 'by clicking on a backdrop';
		} else {
			return `with: ${reason}`;
		}
	}




	close() {
		alert('asasd');
		//this.modal.close();

	}

	/*
	buscar(){
	  this.spinner.show();
	  this.SinabipMuebles.getListadoActosAdquisicion(this.filtro).subscribe((data: any) =>{
	  this.spinner.hide();
		this.resultados = data.data;
	  });
	}
	*/


	getLogros(): ILogro[] {
		return [{
			id: 1,
			title: "Logré algo muy interesante",
			description: "Lorem ipsum dolor sit amet"
		}, {
			id: 2,
			title: "Logré otra cosa muy interesante",
			description: "Lorem ipsum dolor sit amet"
		}, {
			id: 3,
			title: "Logré algo aún mas interesante",
			description: "Lorem ipsum dolor sit amet"
		}
		]
	}

}

interface ILogro {
	id: number;
	title: string;
	description?: string;
}