import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-catalogos',
  templateUrl: './catalogos.component.html',
  styleUrls: ['./catalogos.component.css']
})
export class CatalogosComponent implements OnInit {

	RutaSinabip : string = sessionStorage.getItem("RutaSinabip") + "SGISBN/System/sinabip_modulos.php?idm=1&opt=2&menu=0"
  constructor() {
    let token = sessionStorage.getItem("token");  
    let ruta = sessionStorage.getItem("RutaSinabip")
    if(token === null){
      // window.location.replace(ruta+"SGISBN/System/sinabip.php");
      window.location.replace("./SGISBN/System/sinabip.php");
    }
  }

  ngOnInit() {
  }

}
