import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../router.animations';
import { SinabipmueblesService } from '../services/sinabipmuebles.service';
//import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgbModalConfig, NgbModal, ModalDismissReasons, NgbModule} from '@ng-bootstrap/ng-bootstrap';
//import {NgbDatepickerI18n, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
//import { ModalService } from '../_services'; 
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import swal from'sweetalert2';
import { forEach } from '@angular/router/src/utils/collection';
import { AngularFileUploaderComponent } from 'angular-file-uploader';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast'; 
import { DatePipe } from '@angular/common'



@Component({
  selector: 'app-actobaja',
  templateUrl: './actobaja.component.html', 
  styleUrls: ['./actobaja.component.css'],  
  animations: [routerTransition()],
  providers: [NgbModalConfig, NgbModal, DatePipe]
  
})


export class ActosbajaComponent implements OnInit { 
  registerForm: FormGroup;
  RegistroTipoBienForm: FormGroup;
  submitted = false;

  seleccionRows : string = null;
  modificando_registro: string = '';

  Cod_Entidad : string = sessionStorage.getItem("Cod_Entidad"); 
  Cod_Usuario : string = sessionStorage.getItem("Cod_Usuario");
  RutaSinabip : string = sessionStorage.getItem("RutaSinabip");

  mostrarVentanaTipoRegistro : boolean = false; 
  mostrarVentanaRegistroIndividual : boolean = false;
  b_ventanaObservacionesValidacion : boolean = false;
  paramAgregar = {
    TipoagregarBien   : "",
    id_acto           : "",
    id_entidad        : this.Cod_Entidad,
    id_forma          : undefined,
    nro_documento     : "",
    fecha_documento   : "",
    usua_creacion     : this.Cod_Usuario,
    codigo_patrimonial: "", 
    denominacion_bien : "", 
    cantidad          : "", 
    valor_adq         : "", 
    estado            : ""
  }

  paramGuardarActo = {
    id_entidad : this.Cod_Entidad,
    cod_acto : "",
    nro_resolucion : "",
    fecha_resolucion : "",
    id_usuario : "",
    cod_bien : ""

  }

  id2 : string = sessionStorage.getItem("Cod_Entidad");
  id_usuario : string  = sessionStorage.getItem("Cod_Usuario");
  periodo : string  = sessionStorage.getItem("Periodo");
  cod_inv : string  = sessionStorage.getItem("Cod_Inv");

  RegCabInv = { id  : this.id2, cod_inv  : this.cod_inv, anno : this.periodo, idusuario : this.id_usuario }

  Adj_PDFBaja : string  = "";
  RegCabInvNuev : any = [];

  

  afuConfig = {
  uploadAPI: {
      url:this.SinabipMuebles.API_URL+"AdjuntarBajatxt/"+this.Cod_Entidad,
    },
    hideResetBtn: true,
    uploadBtnText:"Adjuntar Archivo",
    uploadMsgText: "",
    formatsAllowed: ".TXT,.txt"
  };

  afuConfig1 = {
    uploadAPI: {
        url:sessionStorage.getItem("Adjuntar_PDFBaja"),
      },
      hideResetBtn: true,
      uploadBtnText:"Adjuntar Archivo",
      uploadMsgText: "",
      formatsAllowed: ".PDF,.pdf"
  };

  // NombArch_Dat = { cod_inventario_ent  : '0', estado  : 0 }

  // NombArch : string  = "";
  // NomArch_arg : any = [];
  mostrar_carga = null;
  resetVar = false;
  closeResult: string;
  urlPDF;  
  // NomArchEstado = -1;
  Adj_ActaConc : string  = "";
  ElimArg_arg : any = [];
  NomArch2_arg : any = [];
  TotalInf : number = 0;
  TotalConc : number = 0;
  NomInf: string  = "";
  NomConc: string  = "";
  FechInfo: string  = "";
  FechConci: string  = ""; 
  informe_cont : any = 0;
  concili_conc : any = 0;
  habilitar_finalizar : string  = "";
  eliminar_cab_bien: boolean = false;
  EstadoPdf_Arg : any = [];
  EstadoPdf: string  = "0"; 
  AdjuntadoPDFBaja: any;
  EstadoValidaGuardar: string= "0"; 
  Actualiza_Arg : any = [];

  dataduplicado_actos = [];
  dataerrores_columnas = [];
  datano_existe_codigos = [];
  datano_disponibles = [];


  @ViewChild('fileUpload1')
  private fileUpload1:  AngularFileUploaderComponent;

    //Paginacion 
    itemsPerPage: number = 10;
    page: any = 1;
    previousPage: any;
    total : any = 0; 

  parametros = { 
    cod_entidad   : this.Cod_Entidad,
    nro_grupo     : "-1",
    nro_clase     : "",
    cod_patrimonial : "",
    denom_bien    : "",
    page : this.page, records : this.itemsPerPage

  } 

  filtro = {
    fecha : 
    {
      month : 10,
      year  : 2018
    },
    cod_entidad       : this.Cod_Entidad,
    forma_adquisicion : 1,
    nro_documento     : '',
    estado            : '1',
    page : this.page, records : this.itemsPerPage
  }

  paramCarga = {
    id_entidad    : '',
    id_adjuntado  : '',
    nombreArchivo : '',
    usua_creacion : ''
  }

    paramBien = {
    id_entidad  : this.Cod_Entidad,      
    id_bien     : "",
    formaAdquis : undefined,
    nroDocAdquis: "",
    fechaAdquis : "",
    codigopatri : "",
    denomBien   : "",
    marca       : "",
    modelo      : "",
    tipo        : "",
    color       : "",
    nroSerie    : "",
    nroChasis   : "",
    anioFabric  : "",
    otrasCaract : "",
    usoCta      : "",
    TipoCta     : "",
    CtaContable : "",
    valorAdquis : "",
    porcDeprec  : "",
    asegurado   : "",
    estadoBien  : "",
    observacion : ""
}

paramEliminarActo = {
  id_entidad   : "",
  id_acto      : ""
}

paramRegistroIndividual = { id_entidad : this.Cod_Entidad,  cod_acto_baja : '', nro_resolucion : '', fecha_resolucion : '', id_usuario : this.Cod_Usuario, cod_patrimonial : '', id_acto : ''}
paramEliminar = { id_entidad : this.Cod_Entidad, id_usuario : this.Cod_Usuario, cod_patrimonial : '', nro_resolucion : '', fecha_resolucion : '', id_acto : ''}

paramPDFvalida = { id_entidad : this.Cod_Entidad,  cod_patrimonial : '',  cod_acto_baja : ''}

actualizarPDFBaja = {id: this.Cod_Entidad, cod_ue_bien_baja :'', nomb_archivo:'', peso: 0 }

  dataEditarActos : any = [];
  dataEditarActos2 : any = [];
  dataFormaIndividual : Array<any> = [];
  dataAdjuntado : any = [];
  dataObservacionValidacion = [];
  dataCaracteristicasBien : Array<any> = [];
  Arrg_patrim = [];
  Arrg_patrim_tmp = [];
  Arrg_patrim_eliminados = [];
  dataActosBaja : any = [];
  dataGuardarBien : any = [];
  dataEliminados : any = [];
  dataConsultaBienesAEliminar = [];
  dataAnio = [];
  dataMes = [];
  dataEliminarBien : any = [];

  
  id : string="1";
  estado_adjuntar : string = "";
  estado_carga : string = "";
  estado_carga_fecha : string = "";
  estado_carga_total : string = "";
  estado_validacion : string ="";
  estado_validacion_fecha : string ="";
  estado_validacion_total : string ="";
  estado_finalizacion : string ="";
  estado_finalizacion_fecha : string ="";
  estado_finalizacion_total : string ="";
  modal;
  //closeResult: string;
  seleccionBien1 : boolean = false;
  mostrarVentanaCaracteristicas : boolean = false;
  seleccionBien2 : boolean = false;
  cantidadduplicado_actos: number = 0;
  cantidadduplicado_bienes: number = 0;
  cantidaderrores_columnas: number = 0;
  cantidadNo_existe_codigos_pat: number = 0;
  cantidadNo_disponible_bienes: number = 0;
  posicion : number;
  contListCodPatrim : number;
  contListselecc : number;
  ListCodPatrim: string;
  ListCodPatrim_Eliminados: string;
  dataRegistroIndividual = [];
  eliminacionDetalleBajas: boolean = false;
  contador : number = 0;
  cod_patrimonial_editar= [];
  
  mostrarVentanaRegistroMasivo : boolean = false;
  

  constructor
  (
    config: NgbModalConfig,
    public datepipe: DatePipe,
    private formBuilder: FormBuilder,
    private SinabipMuebles : SinabipmueblesService, 
    private spinner: NgxSpinnerService, 
    private modalService: NgbModal,
    private router: Router
  ) 

    { 
    
      let date = new Date(); 
      date.setDate( date.getDate() - 11 );
      let year  = date.getFullYear();
      let month = date.getMonth()+1;
      this.filtro.fecha.month = month;
      this.filtro.fecha.year  = year;
      this.afuConfig.uploadAPI.url = this.SinabipMuebles.API_URL+"AdjuntarBajatxt/"+this.Cod_Entidad;
      
      config.backdrop = 'static';
      config.keyboard = false; 
    }
  

  ngOnInit(): void {

 
    this.filtro.estado = '-1';
    this.filtro.fecha.month = -1;
    this.filtro.forma_adquisicion = -1,


    this.registerForm = this.formBuilder.group({ 
      documentoActo: ['', Validators.required],
      formaAdquisicion: ['', Validators.required],
      fechaAdquisicion: ['', Validators.required]
    });
    
    let fechita = new Date();
    let yy = fechita.getFullYear();
    this.filtro.fecha.year = yy;
    this.cargarListadoActosBaja();
  }

  get f() { return this.registerForm.controls; } 

  // Actualiza_InfFinal(){
  //   this.NomArchEstado = 1;
  // }

  ventanaTipoRegistro(){
    this.mostrarVentanaTipoRegistro = true; 
    this.mostrarVentanaRegistroIndividual = false; 
    this.b_ventanaObservacionesValidacion = false;
    this.paramAgregar.id_acto = "";
    this.Arrg_patrim =  [];
    //this.paramBien.formaAdquis = undefined;
    this.paramAgregar.id_forma = undefined;

    this.paramAgregar = {
      TipoagregarBien   : "",
      id_acto           : "",
      id_entidad        : "",
      id_forma          : undefined,
      nro_documento     : "",
      fecha_documento   : "",
      usua_creacion     : "",
      codigo_patrimonial: "",
      denominacion_bien : "",
      cantidad          : "",
      valor_adq         : "",
      estado            : ""
    }

    

    this.dataEditarActos = [];

    this.id= "-1";

    this.estado_adjuntar = '';
    this.estado_carga = '';
    this.estado_carga_fecha = '';
    this.estado_carga_total = '';
    this.estado_validacion = '';
    this.estado_validacion_fecha = '';
    this.estado_validacion_total = '';
    this.estado_finalizacion = '';
    this.estado_finalizacion_fecha = '';
    this.estado_finalizacion_total = '';  

  }


  cargarListadoActosBaja(){ 
    this.filtro.cod_entidad = this.Cod_Entidad;
    this.spinner.show();
    this.SinabipMuebles.postListadoActosBajas(this.filtro).subscribe((data : any) =>{   
    this.spinner.hide();

    if (data.data.error == true){
			if (data.data.reco.hasOwnProperty('cod_entidad')) { 
        this.router.navigate(['error500']);
				return;
			}else if (data.data.reco.hasOwnProperty('nro_documento')) {
        swal({
					type: 'error',
					title: 'Validacion de Datos',
					text: data.data.reco.nro_documento[0],
				  })
				return;
			}else if (data.data.reco.hasOwnProperty('page')) {
        this.router.navigate(['error500']);
        return;
			}else if (data.data.reco.hasOwnProperty('records')) {
        this.router.navigate(['error500']);
        return;
      }
		}

    this.dataActosBaja = data.data;
    this.dataAnio = this.dataActosBaja.anios;
    this.dataMes = this.dataActosBaja.mes;
    this.paramAgregar.id_forma = undefined;
    this.total = ( this.dataActosBaja.documento.length > 0 ) ? this.dataActosBaja.documento[0].TOTAL : 0;
    
    });
  }


  ActualizaPDFBaja(){ 
    this.actualizarPDFBaja = {
      id: this.Cod_Entidad, cod_ue_bien_baja : this.paramAgregar.id_acto, nomb_archivo: this.AdjuntadoPDFBaja.NOMBRE, peso: this.AdjuntadoPDFBaja.PESO 
    }
    console.log(this.actualizarPDFBaja)
    this.spinner.show();
    this.SinabipMuebles.PostActualizaPDFBaja(this.actualizarPDFBaja).subscribe((data : any) =>{   
    this.spinner.hide();
    this.Actualiza_Arg = data.data;
    console.log(this.Actualiza_Arg)
    });
  }

  

  BorrarFiltro()
  { 
    let fechita = new Date();
    let yy = fechita.getFullYear();

    this.filtro = { 
      fecha : 
      {
        month : -1,
        year  : yy
      },
      cod_entidad       : this.Cod_Entidad,
      forma_adquisicion : -1,
      nro_documento     : '',
      estado            : '-1',
      page : this.page, records : this.itemsPerPage
    }

  }

  ventanaRegistroFormaMasiva(){
    this.mostrarVentanaTipoRegistro = false;
    this.mostrarVentanaRegistroIndividual = false;
    this.mostrarVentanaRegistroMasivo = true;

    this.estado_adjuntar = '';
    this.estado_carga = '';
    this.estado_carga_fecha = '';
    this.estado_carga_total = '';
    this.estado_validacion = '';
    this.estado_validacion_fecha = '';
    this.estado_validacion_total = '';
    this.estado_finalizacion = '';
    this.estado_finalizacion_fecha = '';
    this.estado_finalizacion_total = ''; 
  }

  ventanaRegistroFormaIndividual(){
    this.mostrarVentanaTipoRegistro = false;
    this.mostrarVentanaRegistroIndividual = true;
    this.mostrarVentanaRegistroMasivo = false;
    this.modificando_registro = '';
  }

  onSubmit(content3) {
    this.submitted = true;
    
    // stop here if form is invalid
    if (this.registerForm.invalid) { 
      swal({
        type: 'error',
        title: 'Datos Incompletosss!!!',
        text: 'No se ha ingresado la información necesaria para continuar',
        // footer: '<a href>Why do I have this issue?</a>'
      })
        return;
    }
    else
    {
        this.Guardar_Actos(content3);
    }
       
  }

  Guardar_Actos(content3):void{
   
    console.log('this.dataEditarActos2');
    console.log(this.dataEditarActos2);
    let contador = 0;
    this.dataEditarActos2.forEach((data5 :any) =>{
      this.posicion = this.Arrg_patrim.indexOf(this.dataEditarActos2[contador]);
      this.Arrg_patrim.splice(this.posicion,1);
      contador += 1;
      
      
    });

    console.log('this.Arrg_patrim');
    console.log(this.Arrg_patrim);

    this.ListCodPatrim = JSON.stringify(this.Arrg_patrim);
    this.ListCodPatrim = this.ListCodPatrim.substring( 1, (this.ListCodPatrim.length)-1 ); 
    this.ListCodPatrim = this.ListCodPatrim.replace(/['"]+/g, ''); 
    sessionStorage.setItem('Asig_Cod_Patrimonial', this.ListCodPatrim);

    this.paramRegistroIndividual.id_entidad = this.Cod_Entidad;
    this.paramRegistroIndividual.cod_acto_baja = this.registerForm.get('formaAdquisicion').value;
    this.paramRegistroIndividual.nro_resolucion = this.registerForm.get('documentoActo').value;
    this.paramRegistroIndividual.fecha_resolucion = this.registerForm.get('fechaAdquisicion').value;
    this.paramRegistroIndividual.id_usuario = this.Cod_Usuario;
    this.paramRegistroIndividual.cod_patrimonial = sessionStorage.getItem('Asig_Cod_Patrimonial');
    this.paramRegistroIndividual.id_acto = this.paramAgregar.id_acto;

    if(this.EstadoValidaGuardar == '0') {
      this.ValidarPDFBajaCatalogo();
      setTimeout(() => {
        if(this.EstadoPdf == '1') {
          this.modalService.open(content3, {ariaLabelledBy: 'modal-basic-title', 
          keyboard: false,
          size: 'lg',
          backdrop: 'static'}).result.then((result) => { 
            this.closeResult = `Closed with: ${result}`;
          }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`; 
          });
        }else{
          this.spinner.show();
          console.log('parametro de guardar');
          console.log(this.paramRegistroIndividual);
          this.SinabipMuebles.postGuardar_ActosBaja(this.paramRegistroIndividual).subscribe((data : any) =>{
            this.spinner.hide();

            if (data.data.error == true){
              if (data.data.reco.hasOwnProperty('id_entidad')) { 
                 this.router.navigate(['error500']);
                  return;
              }else if (data.data.reco.hasOwnProperty('cod_acto_baja')) {
                  this.router.navigate(['error500']);
                  return;
              }else if (data.data.reco.hasOwnProperty('nro_resolucion')) {
                swal({
                  type: 'error',
                  title: 'Validacion de Datos',
                  text: data.data.reco.nro_resolucion[0],
                  })
                return;
              }else if (data.data.reco.hasOwnProperty('fecha_resolucion')) {
                swal({
                  type: 'error',
                  title: 'Validacion de Datos',
                  text: data.data.reco.fecha_resolucion[0],
                  })
                return;
              }else if (data.data.reco.hasOwnProperty('id_usuario')) {
                this.router.navigate(['error500']);
                return;
              }else if (data.data.reco.hasOwnProperty('id_acto')) {
                this.router.navigate(['error500']);
                return;
              }
            }

          this.dataGuardarBien = data.data;  
          this.cargarListadoActosBaja(); 
          this.mostrarVentanaTipoRegistro = false;
          this.mostrarVentanaRegistroIndividual = false; 
          this.paramAgregar.id_acto = this.dataGuardarBien.ID;
          
          swal({
            position: 'center',
            type: 'success',
            title: 'Se ha Registrado Satisfactoriamente!!!',
          })
          this.Arrg_patrim = [];
          });   
        }
      }, 1000);
    }else{
      this.EstadoValidaGuardar == '0'
      this.spinner.show();
      this.SinabipMuebles.postGuardar_ActosBaja(this.paramRegistroIndividual).subscribe((data : any) =>{
        this.spinner.hide();
      this.dataGuardarBien = data.data;  
      this.cargarListadoActosBaja(); 
      this.mostrarVentanaRegistroIndividual = false; 
      this.paramAgregar.id_acto = this.dataGuardarBien.ID;
      setTimeout(() => {
        this.ActualizaPDFBaja()
      }, 1000);
      this.modalService.dismissAll();

      swal({
        position: 'center',
        type: 'success',
        title: 'Se ha Registrado Satisfactoriamente!!!',
      })
      });   
    }
    this.spinner.hide();
  }

  ValidarPDFBajaCatalogo(){
    this.paramPDFvalida = { id_entidad : this.Cod_Entidad,  cod_patrimonial : sessionStorage.getItem('Asig_Cod_Patrimonial'),  cod_acto_baja :  this.paramRegistroIndividual.cod_acto_baja}
    console.log(this.paramPDFvalida);
    this.spinner.show();
    this.SinabipMuebles.PostValidarPDFBajaCatalogo(this.paramPDFvalida).subscribe((data : any) =>{
      this.spinner.hide();
      this.EstadoPdf_Arg= data.data;  
      this.EstadoPdf = this.EstadoPdf_Arg[0].RESULTADO;
    });
  }


  onSubmit2() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) { 
      swal({
        type: 'error',
        title: 'Datos Incompletosss!!!',
        text: 'No se ha ingresado la información necesaria para continuar',
        // footer: '<a href>Why do I have this issue?</a>'
      })
        return;
    }
}

open(content) { 
  this.parametros = { 
    cod_entidad   : this.Cod_Entidad,
    nro_grupo     : "-1",
    nro_clase     : "-1",
    cod_patrimonial : "",
    denom_bien    : "",
    page : this.page, records : this.itemsPerPage
  } 

  this.paramAgregar.codigo_patrimonial = "";
  this.paramAgregar.denominacion_bien = "";
  this.paramAgregar.cantidad = "";
  this.paramAgregar.valor_adq = "";
  this.dataFormaIndividual = [];
  this.ListCodPatrim = ''; 
  sessionStorage.setItem('Asig_Cod_Patrimonial', '');

  this.onSubmit2();
  if(this.registerForm.valid){
    
    this.spinner.show();
    this.modal=this.SinabipMuebles.postListadoBienesFormaIndividual(this.parametros).subscribe((data : any) =>{ 
      this.spinner.hide();
      this.dataFormaIndividual = data.data;
    });
    

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', 
    keyboard: false,
    size: 'lg',
    backdrop: 'static'}).result.then((result) => { 
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`; 
    });
  }else
  {

    swal({
      type: 'error',
      title: 'Datos Incompletos!!!',
      text: 'No se ha ingresado la información necesaria para continuar', 
      // footer: '<a href>Why do I have this issue?</a>'
    })


  }
}


open2(content2,event) {
  this.Adj_PDFBaja = this.SinabipMuebles.API_URL+"SubirFormatoBaja/"+this.Cod_Entidad; 
  sessionStorage.setItem("Adjuntar_PDFBaja", this.Adj_PDFBaja);
  this.afuConfig1.uploadAPI.url = this.Adj_PDFBaja;
  this.AdjuntadoPDFBaja = JSON.parse(event.response);
  console.log(this.AdjuntadoPDFBaja) ;   

  this.modalService.dismissAll();
  setTimeout(() => {
    this.modalService.open(content2, {size: 'lg', ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
    });

    this.urlPDF = this.SinabipMuebles.API_URL+"VerPDFBaja/"+this.AdjuntadoPDFBaja.NOMBRE;
    this.EstadoValidaGuardar = '1';
  }, 1500);
}

ListadoBienesFormaIndividual(){
  this.spinner.show();
  this.SinabipMuebles.postListadoBienesFormaIndividual(this.parametros).subscribe((data : any) =>{ 
  this.spinner.hide();
  this.dataFormaIndividual = data.data; 
  
});
}


private getDismissReason(reason: any): string {
  if (reason === ModalDismissReasons.ESC) {
    return 'by pressing ESC';
  } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
    return 'by clicking on a backdrop';
  } else {
    return  `with: ${reason}`;
  }
}

loadPage(page: number) {
  if (page !== this.previousPage) {
    this.previousPage = page;
    this.cargarListadoActosBaja();
  }  
}

loadPageRegisterIndividual(page: number) {
  if (page !== this.previousPage) {
    this.previousPage = page;
    this.ListadoBienesFormaIndividual();
  }  
}

resetearpag(){
  this.filtro.page = 1;
  this.cargarListadoActosBaja(); 
}

AdjuntarBajatxt(event){
  
  this.estado_adjuntar == '';
  this.estado_carga = '';
  this.estado_validacion = '';
  this.estado_finalizacion = '';
  this.estado_carga_total = '';
  this.estado_carga_fecha = '';
  this.estado_validacion_total = '';
  this.estado_validacion_fecha = '';
  this.estado_finalizacion_total = '';
  this.estado_finalizacion_fecha = '';

  this.afuConfig.uploadAPI.url = this.SinabipMuebles.API_URL+"AdjuntarBajatxt/"+this.Cod_Entidad; 
  this.dataAdjuntado = JSON.parse(event.response); 
  this.estado_adjuntar = this.dataAdjuntado.data.RESULTADO;
  
}


CargarBajaTXT(){ 
    
  this.paramCarga.id_entidad = this.Cod_Entidad; 
  this.paramCarga.id_adjuntado = this.dataAdjuntado.data.ID_ADJUNTADO;  
  this.paramCarga.nombreArchivo = this.dataAdjuntado.data.NOMBRE_ARCHIVO;
  this.paramCarga.usua_creacion = this.Cod_Usuario; 

  this.spinner.show();
  this.SinabipMuebles.postCargarBajaTXT(this.paramCarga).subscribe((data : any) =>{ 
  this.spinner.hide();

  if (data.data.error == true){
    if (data.data.reco.hasOwnProperty('id_entidad')) { 
        this.router.navigate(['error500']);
        return;
    }else if (data.data.reco.hasOwnProperty('id_adjuntado')) {
        this.router.navigate(['error500']);
        return;
      }else if (data.data.reco.hasOwnProperty('usua_creacion')) {
        this.router.navigate(['error500']);
        return;
      }
    }
    
  


  this.estado_carga = data.data.ESTADO_CARGA;
  this.estado_carga_fecha = data.data.CARGADO_FECHA;
  this.estado_carga_total = data.data.CARGADO_TOTAL;

  swal({
    position: 'center',
    type: 'success',
    title: 'Carga Finalizada',
    showConfirmButton: false,
    timer: 2000
  })

  });
}

ValidarBajaTXT(content)
{
  this.b_ventanaObservacionesValidacion = false;
  this.paramCarga.id_entidad = this.Cod_Entidad; 
  this.paramCarga.id_adjuntado = this.dataAdjuntado.data.ID_ADJUNTADO;  
  this.paramCarga.usua_creacion = this.Cod_Usuario;
  

  this.spinner.show();
  this.SinabipMuebles.postValidarBajaTXT(this.paramCarga).subscribe((data : any) =>{ 
    this.spinner.hide();

    if (data.data.error == true){
      if (data.data.reco.hasOwnProperty('id_entidad')) { 
          this.router.navigate(['error500']);
          return;
      }else if (data.data.reco.hasOwnProperty('id_adjuntado')) {
          this.router.navigate(['error500']);
          return;
        }else if (data.data.reco.hasOwnProperty('usua_creacion')) {
          this.router.navigate(['error500']);
          return;
        }
      }

    this.estado_validacion = data.data.ESTADO_VALIDACION;
    this.estado_validacion_fecha = data.data.VALIDADO_FECHA;
    this.estado_validacion_total = data.data.VALIDADO_TOTAL;


    if (this.estado_validacion == "VALIDACION_SATISFACTORIA" ){

      swal({
        position: 'center', 
        type: 'success',
        title: 'Validación Finalizada',
        showConfirmButton: false,
        timer: 2000 
      })
      this.modalService.dismissAll(); 
    }else{
      
      this.spinner.show();
      this.modal=this.SinabipMuebles.postListadoErroresCargaMasivaBaja(this.paramCarga).subscribe((data : any) =>{   
        this.spinner.hide();
        this.dataObservacionValidacion = [];
        this.dataObservacionValidacion = data.data;

      this.dataduplicado_actos = this.dataObservacionValidacion["duplicado_actos"];
      this.dataerrores_columnas = this.dataObservacionValidacion["errores_columnas"];
      this.datano_existe_codigos = this.dataObservacionValidacion["no_existe_codigos"];
      this.datano_disponibles = this.dataObservacionValidacion["no_disponibles"];
      console.log(this.datano_existe_codigos);
  
        this.cantidadduplicado_actos = this.dataObservacionValidacion["duplicado_actos"].length
        this.cantidaderrores_columnas = this.dataObservacionValidacion["errores_columnas"].length
        this.cantidadNo_existe_codigos_pat = this.dataObservacionValidacion["no_existe_codigos"].length
        this.cantidadNo_disponible_bienes = this.dataObservacionValidacion["no_disponibles"].length

        
  
      });
  
      this.b_ventanaObservacionesValidacion = true;
      this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title',
      keyboard: false,
      size: 'lg',
      backdrop: 'static'}).result.then((result) => { 
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
  
    }

  });

  
}

FinalizarBajaTXT()
{
  this.paramCarga.id_entidad = this.Cod_Entidad; 
  this.paramCarga.id_adjuntado = this.dataAdjuntado.data.ID_ADJUNTADO;  
  this.paramCarga.usua_creacion = this.Cod_Usuario;

  this.spinner.show();
  this.SinabipMuebles.postFinalizarBajaTXT(this.paramCarga).subscribe((data : any) =>{ 
  this.spinner.hide();

  if (data.data.error == true){
    if (data.data.reco.hasOwnProperty('id_entidad')) { 
        this.router.navigate(['error500']);
        return;
    }else if (data.data.reco.hasOwnProperty('id_adjuntado')) {
        this.router.navigate(['error500']);
        return;
      }else if (data.data.reco.hasOwnProperty('usua_creacion')) {
        this.router.navigate(['error500']);
        return;
      }
    }

  this.estado_finalizacion = data.data.ESTADO_FINALIZACION;
  this.estado_finalizacion_fecha = data.data.FINALIZADO_FECHA;
  this.estado_finalizacion_total = data.data.FINALIZADO_TOTAL;

  swal({
    position: 'center',
    type: 'success',
    title: 'Finalizada',
    showConfirmButton: false,
    timer: 2000
  })

    this.cargarListadoActosBaja(); 
    this.mostrarVentanaTipoRegistro = false;
    this.mostrarVentanaRegistroIndividual = false; 
    this.mostrarVentanaRegistroMasivo = false;

  });

}

openValidacion(content, id_adjuntdo) { 

  this.paramCarga.id_entidad = this.Cod_Entidad; 
  this.paramCarga.id_adjuntado = this.dataAdjuntado.data.ID_ADJUNTADO;  

  this.spinner.show();
  this.modal=this.SinabipMuebles.postObservaciones_Validacion(this.paramBien).subscribe((data : any) =>{  
    this.spinner.hide();

    this.dataObservacionValidacion = data.data;

  });

  
  

  this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title',
  keyboard: false,
  size: 'lg',
  backdrop: 'static'}).result.then((result) => { 
    this.closeResult = `Closed with: ${result}`;
  }, (reason) => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
}


editarActoBaja(id_acto){
  this.Arrg_patrim=[];
  this.Arrg_patrim_tmp = [];
  this.Arrg_patrim_eliminados=[];
  this.dataEditarActos2 = [];
  this.paramAgregar.id_acto = id_acto;
  this.paramAgregar.id_entidad = this.Cod_Entidad;

  this.spinner.show();
  this.SinabipMuebles.posteditarActoBaja(this.paramAgregar).subscribe((data : any) =>{   
  this.spinner.hide();
  
  if (data.data.error == true){
    if (data.data.reco.hasOwnProperty('id_entidad')) { 
        this.router.navigate(['error500']);
        return;
    }else if (data.data.reco.hasOwnProperty('id_acto')) {
        this.router.navigate(['error500']);
        return;
    }
    
  }

  this.dataEditarActos = data.data;   

  this.contador = 0;
  this.dataEditarActos.detalles.forEach((data5 :any) =>{
    this.cod_patrimonial_editar = this.dataEditarActos.detalles[this.contador].CODIGO_PATRIMONIAL;
    this.contador += 1;
    this.Arrg_patrim.push(this.cod_patrimonial_editar);
    // this.dataEditarActos2 = [];
    this.dataEditarActos2.push(this.cod_patrimonial_editar);

    this.mostrarVentanaTipoRegistro = false;
    this.mostrarVentanaRegistroIndividual = true;
    this.modificando_registro = 'M';
    this.cantidadduplicado_bienes = 0;
    this.cantidadduplicado_actos = 0;  
    this.cantidaderrores_columnas = 0;
    this.cantidadNo_existe_codigos_pat = 0;
    this.cantidadNo_disponible_bienes = 0;
    
  });
  console.log(this.Arrg_patrim);
  //console.log(this.dataEditarActos2);
  this.seleccionRows = id_acto;

  this.paramAgregar.id_forma = this.dataEditarActos.cabecera.COD_ACTO_BAJA;
  this.paramAgregar.nro_documento = this.dataEditarActos.cabecera.NRO_RESOLUCION_BAJA;
  this.paramAgregar.fecha_documento = this.dataEditarActos.cabecera.FECHA_RESOLUCION_BAJA;
  this.paramAgregar.estado = this.dataEditarActos.cabecera.ESTADO;
  

  });

  this.mostrarVentanaTipoRegistro = false;
  this.mostrarVentanaRegistroIndividual = true;
  
  


}


open1(content, id_bien) { 

  this.paramBien.id_entidad = this.Cod_Entidad;     
  this.paramBien.id_bien = id_bien;   

  this.spinner.show();
  this.modal=this.SinabipMuebles.postDatos_Caracteristicas_Bien(this.paramBien).subscribe((data : any) =>{  
    this.spinner.hide();

    this.dataCaracteristicasBien = data.data;
    let acto = data.data;
    this.paramBien.formaAdquis = acto.CaracteristicasBien[0].NOM_FORM_ADQUIS;      
    this.paramBien.nroDocAdquis = acto.CaracteristicasBien[0].NRO_DOCUMENTO_ADQUIS;   
    this.paramBien.fechaAdquis = acto.CaracteristicasBien[0].FECHA_DOCUMENTO_ADQUIS;   

    this.paramBien.fechaAdquis =this.datepipe.transform(this.paramBien.fechaAdquis, 'dd-MM-yyyy');

    this.paramBien.codigopatri = acto.CaracteristicasBien[0].CODIGO_PATRIMONIAL; 
    this.paramBien.denomBien = acto.CaracteristicasBien[0].DENOMINACION_BIEN; 

    this.paramBien.marca = acto.CaracteristicasBien[0].MARCA; 
    this.paramBien.modelo = acto.CaracteristicasBien[0].MODELO; 
    this.paramBien.tipo = acto.CaracteristicasBien[0].TIPO; 
    this.paramBien.color = acto.CaracteristicasBien[0].COLOR; 
    this.paramBien.nroSerie = acto.CaracteristicasBien[0].SERIE; 
    this.paramBien.nroChasis = acto.CaracteristicasBien[0].NRO_CHASIS; 
    this.paramBien.anioFabric = acto.CaracteristicasBien[0].ANIO_FABRICACION; 
    this.paramBien.otrasCaract = acto.CaracteristicasBien[0].OTRAS_CARACT; 
    
    this.paramBien.usoCta = acto.CaracteristicasBien[0].USO_CUENTA;
    this.paramBien.TipoCta = acto.CaracteristicasBien[0].TIP_CUENTA; 
    this.paramBien.CtaContable = acto.CaracteristicasBien[0].COD_CTA_CONTABLE; 
    this.paramBien.valorAdquis = acto.CaracteristicasBien[0].VALOR_ADQUIS; 
    this.paramBien.porcDeprec = acto.CaracteristicasBien[0].PORC_DEPREC; 
    this.paramBien.asegurado = acto.CaracteristicasBien[0].OPC_ASEGURADO; 
    this.paramBien.estadoBien = acto.CaracteristicasBien[0].COD_ESTADO_BIEN; 
    this.paramBien.observacion = acto.CaracteristicasBien[0].OBSERVACION; 

    this.cantidadduplicado_bienes = 0;
    this.cantidadduplicado_actos = 0;  
    this.cantidaderrores_columnas = 0;
    this.cantidadNo_existe_codigos_pat = 0;
    this.cantidadNo_disponible_bienes = 0;

  });
  

  this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title',
  keyboard: false,
  size: 'lg',
  backdrop: 'static'}).result.then((result) => { 
    this.closeResult = `Closed with: ${result}`;
  }, (reason) => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
}

Eliminar_ActosBaja(cod_entidad, id_acto, content){

  this.eliminar_cab_bien = true;
  this.paramEliminarActo = {
    id_entidad   : cod_entidad,
    id_acto      : id_acto
  }


  swal({
    title: '¿Está Ud. seguro de eliminar el Acto?',
    text: "¡No podrás revertir esto!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonText: 'Cancelado',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si, Eliminar!'
    
  }).then((result) => {
  if (result.value) {

    /**/
    this.eliminacionDetalleBajas = true;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', 
      keyboard: false,
      size: 'lg',
      backdrop: 'static'}).result.then((result) => { 
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`; 
      });
    /**/
    

    
  }
})

  
}




eliminarcodpatrim(estado,cod_patrimonial){
  if(estado == false){
    //this.Arrg_patrim.push(cod_patrimonial);

    this.posicion = this.Arrg_patrim_eliminados.indexOf(cod_patrimonial);
    this.Arrg_patrim_eliminados.splice(this.posicion,1);
  }else{
    //this.posicion = this.Arrg_patrim.indexOf(cod_patrimonial);
    //this.Arrg_patrim.splice(this.posicion,1);

    this.Arrg_patrim_eliminados.push(cod_patrimonial);
  }
  console.log(this.Arrg_patrim_eliminados);

}


almacenarcodpatrim(estado,cod_patrimonial){
  if(estado == true){
    this.Arrg_patrim_tmp.push(cod_patrimonial);
  }else{
    this.posicion = this.Arrg_patrim_tmp.indexOf(cod_patrimonial);
    this.Arrg_patrim_tmp.splice(this.posicion,1);
  }
  console.log(this.Arrg_patrim_tmp);

}


AgregarBien_Actos():void{ 
  this.Arrg_patrim = this.Arrg_patrim_tmp.concat(this.Arrg_patrim);
  console.log('this.Arrg_patrim_tmp');
  console.log(this.Arrg_patrim_tmp);
  console.log('this.Arrg_patrim333');
  console.log(this.Arrg_patrim);

  let contador = 0;
  this.Arrg_patrim_eliminados.forEach((data5 :any) =>{
          
    //console.log(this.Arrg_patrim_eliminados[contador]);
    this.posicion = this.Arrg_patrim.indexOf(this.Arrg_patrim_eliminados[contador]);
    //console.log('posisicion: ' +this.posicion);
    this.Arrg_patrim.splice(this.posicion,1);
    contador += 1;
    
  });

  console.log('this.Arrg_patrim44444');
  console.log(this.Arrg_patrim);


  this.contListCodPatrim = 0;
  this.contListselecc = 0;
  this.ListCodPatrim = ''; 
  
  this.ListCodPatrim = JSON.stringify(this.Arrg_patrim);
  this.ListCodPatrim = this.ListCodPatrim.substring( 1, (this.ListCodPatrim.length)-1 ); 
  this.ListCodPatrim = this.ListCodPatrim.replace(/['"]+/g, ''); 
  sessionStorage.setItem('Asig_Cod_Patrimonial', this.ListCodPatrim);
  this.contListselecc = this.ListCodPatrim.length;

  if(this.contListselecc >= 1)
  {
    this.paramRegistroIndividual.cod_patrimonial = this.ListCodPatrim;
    this.spinner.show();
    console.log(this.paramRegistroIndividual);
    this.modal=this.SinabipMuebles.postAgregar_Bienes_al_Detalle(this.paramRegistroIndividual).subscribe((data : any) =>{  
    this.spinner.hide();
    
    this.dataEditarActos = data.data;
    this.modalService.dismissAll();

  });

  // this.dataEditarActos.forEach((data4 :any) =>{
  //   data4.seleccionado=false;
  // });
  

  }else
  {

    swal({
      position: 'center',
      type: 'error',
      title: 'Ud. no ha seleccionado ningún Bien'
    })

  }
}


Open_Eliminacion_Detalles_Acto(content, id_acto) 
{

    console.log('eliminaaaaa:'+this.Arrg_patrim_eliminados); 
    this.mostrarVentanaCaracteristicas = false;
    this.eliminar_cab_bien = false
    if (Object.keys(this.Arrg_patrim_eliminados).length === 0)
    {
      
      swal({
        type: 'error',
        title: 'Error',
        text: 'Ud. no ha escogido ningún Bien a eliminar!'
      })

    }
    else{
      
      this.eliminacionDetalleBajas = true;
      this.paramAgregar.id_entidad = this.Cod_Entidad;
      
      
      if(id_acto == '')
      {
        console.log('id_acto = ""');
        let contador = 0;
        console.log('this.Arrg_patrim_eliminados');
        console.log(this.Arrg_patrim_eliminados);
        console.log('this.Arrg_patrim');
        console.log(this.Arrg_patrim);

        this.Arrg_patrim_eliminados.forEach((data5 :any) =>{
          
          //console.log(this.Arrg_patrim_eliminados[contador]);
          this.posicion = this.Arrg_patrim.indexOf(this.Arrg_patrim_eliminados[contador]);
          //console.log('posisicion: ' +this.posicion);
          this.Arrg_patrim.splice(this.posicion,1);
          contador += 1;
          
        });

        console.log('this.Arrg_patrim222');
        console.log(this.Arrg_patrim);

        this.AgregarBien_Actos();
        //this.Arrg_patrim_eliminados = [];

      }
      else{
        this.Arrg_patrim_tmp = [];
        this.ListCodPatrim_Eliminados = JSON.stringify(this.Arrg_patrim_eliminados);
        this.ListCodPatrim_Eliminados = this.ListCodPatrim_Eliminados.substring( 1, (this.ListCodPatrim_Eliminados.length)-1 ); 
        this.ListCodPatrim_Eliminados = this.ListCodPatrim_Eliminados.replace(/['"]+/g, ''); 

        this.paramEliminar.cod_patrimonial = this.ListCodPatrim_Eliminados;
        this.paramEliminar.id_acto = id_acto;

        this.spinner.show();
        console.log(this.paramEliminar);
        this.modal=this.SinabipMuebles.postConsultar_Bienes_Baja_a_Eliminar(this.paramEliminar).subscribe((data : any) =>{  
          this.spinner.hide(); 
          this.dataConsultaBienesAEliminar = data.data;
          //let aa = this.dataConsultaBienesAEliminar[0].RESULTADO;

          if(this.dataConsultaBienesAEliminar[0].RESULTADO == 'NO EXISTE'){ 
            let contador = 0;
            this.Arrg_patrim_eliminados.forEach((data5 :any) =>{
              //console.log(this.Arrg_patrim_eliminados);
              //console.log(this.Arrg_patrim_eliminados[contador]);
              this.posicion = this.Arrg_patrim.indexOf(this.Arrg_patrim_eliminados[contador]);
              //console.log('posisicion: ' +this.posicion);
              this.Arrg_patrim.splice(this.posicion,1);
              contador += 1;
              console.log(this.Arrg_patrim);
              
            });

            this.AgregarBien_Actos();
            this.Arrg_patrim_eliminados = [];
          }else{
  
            this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', 
            keyboard: false,
            size: 'lg',
            backdrop: 'static'}).result.then((result) => { 
              this.closeResult = `Closed with: ${result}`;
            }, (reason) => {
              this.closeResult = `Dismissed ${this.getDismissReason(reason)}`; 
            });
  
          }

          //this.modalService.dismissAll();

        });




        


      }
      

    }
  
  
         

}


Eliminacion_Detalles_Acto() 
{
  this.eliminacionDetalleBajas = true;

  if (this.paramEliminar.nro_resolucion == '')
  {
    swal(
    'Aviso!',
    'Debe especificar el Nro. de Resolución',
    'warning'
    )

  }else if(this.paramEliminar.fecha_resolucion == '')
  {
  swal(
  'Aviso!',
  'Debe especificar la fecha de Resolución ',
  'warning'
  )

  }else
  {

    swal({
    title: '¿Esta Ud. seguro de eliminar el Bien?',
    text: "¡No podrás revertir esto!",
    type: 'warning',
    showCancelButton: true, 
    confirmButtonColor: '#3085d6',
    cancelButtonText: 'Cancelado',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si, Eliminar!'

    }).then((result) => 
    {
      if (result.value) 
      {

        console.log("this.eliminar_cab_bien");
        console.log('AA' + this.eliminar_cab_bien);
        if(this.eliminar_cab_bien == false){
          console.log('ELIMINAR ITEMS DE BAJA');

            this.ListCodPatrim = JSON.stringify(this.Arrg_patrim_eliminados);
            this.ListCodPatrim = this.ListCodPatrim.substring( 1, (this.ListCodPatrim.length)-1 ); 
            this.ListCodPatrim = this.ListCodPatrim.replace(/['"]+/g, ''); 
            sessionStorage.setItem('Asig_Cod_Patrimonial', this.ListCodPatrim);
            this.contListselecc = this.ListCodPatrim.length; 

            this.paramEliminar.id_entidad = this.Cod_Entidad;
            this.paramEliminar.id_acto = this.paramAgregar.id_acto;
            this.paramEliminar.cod_patrimonial = this.ListCodPatrim;
            this.paramEliminar.id_usuario = this.Cod_Usuario;



            console.log("this.paramEliminar");
            console.log(this.Cod_Entidad);
            console.log(this.paramEliminar);
            this.spinner.show();
            this.SinabipMuebles.postEliminacion_Detalles_Acto(this.paramEliminar).subscribe((data : any) =>{ 
            this.spinner.hide();

            if (data.data.error == true){
              if (data.data.reco.hasOwnProperty('id_entidad')) { 
                 this.router.navigate(['error500']);
                  return;
              }else if (data.data.reco.hasOwnProperty('id_acto')) {
                  this.router.navigate(['error500']);
                  return;
              }else if (data.data.reco.hasOwnProperty('nro_resolucion')) {
                swal({
                  type: 'error',
                  title: 'Validacion de Datos',
                  text: data.data.reco.nro_resolucion[0],
                  })
                return;
              }else if (data.data.reco.hasOwnProperty('fecha_resolucion')) {
                swal({
                  type: 'error',
                  title: 'Validacion de Datos',
                  text: data.data.reco.fecha_resolucion[0],
                  })
                return;
              }else if (data.data.reco.hasOwnProperty('id_usuario')) {
                this.router.navigate(['error500']);
                return;
              
              }
            }

            this.dataEliminados = data.data;   

            if (this.dataEliminados.datos[0].RESULTADO == 'CORRECTO'){

              this.editarActoBaja(this.paramAgregar.id_acto);
              this.cargarListadoActosBaja();
              swal(
              'Advertencia!',
              'El registro ha sido eliminado.',
              'success'
              )
              this.paramEliminar.cod_patrimonial = '';
              this.paramEliminar.fecha_resolucion = '';
              this.paramEliminar.nro_resolucion = '';
              
              this.modalService.dismissAll();
            }else
            {
            
                swal(
                'Advertencia!',
                'Ocurrio un inconveniente',
                'error'
                )
            }

            });
            
        }
        else
        {
          console.log('ELIMINAR ACTO');
            this.spinner.show();
            console.log(this.paramEliminarActo);
            this.SinabipMuebles.postEliminar_ActosBaja(this.paramEliminarActo).subscribe((data : any) =>{
                this.spinner.hide();

                this.dataEliminarBien = data.data; 
                console.log(this.dataEliminarBien);

                if (this.dataEliminarBien[0].RESULTADO == 'OK'){ 

                this.cargarListadoActosBaja();
                this.mostrarVentanaTipoRegistro = false;
                this.mostrarVentanaRegistroIndividual = false; 

                swal(
                'Eliminado!',
                'El registro ha sido eliminado.',
                'success'
                )
                this.modalService.dismissAll();
                }else{
                    swal(
                    'Error!',
                    'Ocurrio un inconveniente',
                    'error'
                    )
                }

            });
            this.eliminar_cab_bien = false;
        }
      }

    })

  }
}



nuevo(predio,estado){
  this.mostrar_carga = {
    predio : predio, estado : estado
  }
}

NombArchInffActConcUnion(cod_inventario_ent_ini){
  this.SinabipMuebles.getNombArchInffActConcUnion(cod_inventario_ent_ini).subscribe((data : any) =>{
      this.NomArch2_arg = data.data;        
      this.TotalInf = this.NomArch2_arg[0].TOTAL;
      this.TotalConc = this.NomArch2_arg[1].TOTAL;
      this.NomInf = this.NomArch2_arg[0].NOM_ARCHIVO;
      this.NomConc = this.NomArch2_arg[1].NOM_ARCHIVO;
      this.FechInfo = this.NomArch2_arg[0].FECHA_ARCHIVO;
      this.FechConci = this.NomArch2_arg[1].FECHA_ARCHIVO;
      
      this.informe_cont = this.NomInf.length;
      this.concili_conc = this.NomConc.length;
      if (this.informe_cont == 0){
        //SIN INFORME
        this.habilitar_finalizar = '1'
      }else{
        this.habilitar_finalizar = '0'
        if (this.concili_conc == 0){
          //SIN CONCILIACION
          this.habilitar_finalizar = '1'
        }else{
          this.habilitar_finalizar = '0'
        }
      }
      
  });
}



// RegistrarcabInventario(id_ini,cod_inv_ini,anno_ini,idusuario_ini, event){
//   swal({
//     title: 'Confirme si el archivo adjuntado es el correcto para continuar..',
//     text: "",
//     type: 'warning',
//     showCancelButton: true,
//     confirmButtonColor: '#3085d6',
//     cancelButtonColor: '#d33',
//     confirmButtonText: 'Acepta, Confirmar Resumen!'
//   }).then((result) => {
//     if (result.value) {

//       this.RegCabInv = { id  : id_ini, cod_inv  : cod_inv_ini, anno : anno_ini, idusuario : idusuario_ini }
//       this.SinabipMuebles.PostRegistrarcabInventario(this.RegCabInv).subscribe((data : any) =>{
//           this.RegCabInvNuev = data.data;   
//           this.Adj_PDFBaja = this.SinabipMuebles.API_URL+"SubirFormatoBaja/"+this.Cod_Entidad; 
//           this.dataAdjuntado = JSON.parse(event.response); 
//           this.estado_adjuntar = this.dataAdjuntado.data.RESULTADO;

//           sessionStorage.setItem("Adjuntar_PDFBaja", this.Adj_PDFBaja);
//           this.afuConfig.uploadAPI.url = this.Adj_PDFBaja;
//           this.fileUpload1.uploadAPI = this.Adj_PDFBaja;
     

//       });
//       this.nuevo(1,2);
//     }
//   })

// }


EliminarArchivo(){
  // this.SinabipMuebles.getEliminarArchivo(this.NomArchEstado,this.NombArch).subscribe((data : any) =>{
  //     this.ElimArg_arg = data.data;
  // });
  // if ( this.NomArchEstado == 1){
    // this.fileUpload1.resetFileUpload();
  // }else{

  // }
}



}

interface ILogro{
  id : number;
  title : string;
  description ?: string;
}