import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActobajaComponent } from './actobaja.component';

describe('ActobajaComponent', () => {
  let component: ActobajaComponent;
  let fixture: ComponentFixture<ActobajaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActobajaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActobajaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
