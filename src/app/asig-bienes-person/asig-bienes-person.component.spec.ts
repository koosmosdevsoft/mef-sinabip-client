import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsigBienesPersonComponent } from './asig-bienes-person.component';

describe('AsigBienesPersonComponent', () => {
  let component: AsigBienesPersonComponent;
  let fixture: ComponentFixture<AsigBienesPersonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsigBienesPersonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsigBienesPersonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
