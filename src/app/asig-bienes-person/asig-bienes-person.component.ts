import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { SinabipmueblesService } from '../services/sinabipmuebles.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgbModalConfig, NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';


@Component({
	selector: 'app-asig-bienes-person',
	templateUrl: './asig-bienes-person.component.html',
	styleUrls: ['./asig-bienes-person.component.css'],
	animations: [routerTransition()],
	providers: [NgbModalConfig, NgbModal]
})
export class AsigBienesPersonComponent implements OnInit {
	ListPers_Arg: Array<any> = [];
	ListtipoBienEnt_Arg: Array<any> = [];
	ListBienAsignar_Arg: Array<any> = [];
	ListComboBienAsignar_Arg: Array<any> = [];
	Listfechapecosa_Arg: Array<any> = [];
	Listfechapecosacont_Arg: Array<any> = [];
	ActFechaPecosa_Arg: Array<any> = [];
	ListaObtenerTodo_Arg: Array<any> = [];
	ArgError: Array<any> = [];
	dataAdjuntado: any = [];
	// id : string = '2550';
	// idusuario : string = '9610';
	id: string = sessionStorage.getItem("Cod_Entidad");
	idusuario: string = sessionStorage.getItem("Cod_Usuario");
	RutaSinabip : string = sessionStorage.getItem("RutaSinabip");
	seleccionados = [];
	fechpecosa = [];
	Arrg_patrim = [];
	dataObservacionValidacion: Array<any> = [];
	closeResult: string;
	ListCodPatrim: string;
	IngPecosa: string;
	Ingcod_patr: string;
	contListCodPatrim: number;
	contListselecc: number;
	ContActPecosaTotal: number;
	ContActPecosa: number;
	posicion: number;
	contcheck: number;
	contcheck2: number;
	checkcod_patr: string;
	checkcod_patr2: string;
	asigTodoEstado: string;
	mostrar_carga = null;
	estado_adjuntar: string = "";
	estado_carga: string = "";
	estado_carga_fecha: string = "";
	estado_carga_total: string = "";
	estado_validacion: string = "";
	estado_validacion_fecha: string = "";
	estado_validacion_total: string = "";
	estado_finalizacion: string = "";
	estado_finalizacion_fecha: string = "";
	estado_finalizacion_total: string = "";
	estado_Validacion: string = "";
	estado_Duplicados: string = "";
	estado_Localarea: string = "";
	habilitar_adjuntar: string = "";
	b_ventanaObservacionesValidacion: boolean = false;

	afuConfig = {
		uploadAPI: {
			url: this.Sinabip.API_URL + "AdjuntarAsignartxt/" + this.id + "-" + this.idusuario,
		},
		hideResetBtn: true,
		uploadBtnText: "Adjuntar Archivo",
		uploadMsgText: "Archivo TXT Adjuntado",
		formatsAllowed: ".txt,.TXT"
	};

	//Paginacion 
	itemsPerPage: number = 20;
	page: any = 1;
	previousPage: any;
	total: any = 0;

	filtro = {
		id: this.id, cod_patrimonial: '', denominacion: '', fecha_adquis: '', cod_pers: '',
		nrodocument: '', nrobien: '', flagesta: '', page: this.page, records: this.itemsPerPage
	}
	Listfechapecosa = { id: this.id, cod_patrimonial: '', estado: 0 }
	Listfechapecosacont = { id: this.id, cod_patrimonial: '', estado: 0 }
	ActFechaPecosa = { id: this.id, cod_patrimonial: '', fecha_pecosa: '' }
	paramCarga = { id_entidad: '', id_adjuntado: '', nombreArchivo: '', usua_creacion: '' }
	modal;

	constructor(config: NgbModalConfig, private Sinabip: SinabipmueblesService,
		private route: ActivatedRoute,
		private router: Router,
		private spinner: NgxSpinnerService,
		private modalService: NgbModal) {
		sessionStorage.setItem("Cod_Entidad", this.id);
		sessionStorage.setItem("Cod_Usuario", this.idusuario);
		let token = sessionStorage.getItem("token");  
		if(token === null){
			// window.location.replace(this.RutaSinabip+"SGISBN/System/sinabip.php");
			window.location.replace("./SGISBN/System/sinabip.php");
		}
		this.Listadopersonal();
		this.ListadoTipoBienEntidad();
		this.ListadoAsignarPerson(1);
		this.ListadoComboAsig();
		config.backdrop = 'static';
		config.keyboard = false;
		this.afuConfig.uploadAPI.url = this.Sinabip.API_URL + "AdjuntarAsignartxt/" + this.id + "-" + this.idusuario;
		this.habilitar_adjuntar = "";
	}

	ngOnInit() {
	}

	AdjuntarAsigtxt(event) {
		this.afuConfig.uploadAPI.url = this.Sinabip.API_URL + "AdjuntarAsignartxt/" + this.id + "-" + this.idusuario;
		this.dataAdjuntado = JSON.parse(event.response);
		this.estado_adjuntar = this.dataAdjuntado.data.RESULTADO;
	}

	LimpiaAsigMasiva() {
		this.estado_adjuntar = '';
		this.estado_carga = '';
		this.estado_carga_fecha = '';
		this.estado_carga_total = '';
		this.estado_validacion = '';
		this.estado_validacion_fecha = '';
		this.estado_validacion_total = '';
		this.estado_finalizacion = '';
		this.estado_finalizacion_fecha = '';
		this.estado_finalizacion_total = '';
	}

	cargarTXT() {
		this.paramCarga.id_entidad = this.id;
		this.paramCarga.id_adjuntado = this.dataAdjuntado.data.ID_ADJUNTADO;
		this.paramCarga.nombreArchivo = this.dataAdjuntado.data.NOMBRE_ARCHIVO;
		this.paramCarga.usua_creacion = this.idusuario;
		this.spinner.show();
		this.Sinabip.PostCargarAsigTXT(this.paramCarga).subscribe((data: any) => {
			this.spinner.hide();
			this.estado_carga = data.data.ESTADO_CARGA;
			this.estado_carga_fecha = data.data.CARGADO_FECHA;
			this.estado_carga_total = data.data.CARGADO_TOTAL;
			swal({
				position: 'center',
				type: 'success',
				title: 'Carga Finalizada',
				showConfirmButton: false,
				timer: 2000
			})
			this.habilitar_adjuntar = '1';
		});
	}

	ValidarTXT(content2) {
		this.paramCarga.id_entidad = this.id;
		this.paramCarga.id_adjuntado = this.dataAdjuntado.data.ID_ADJUNTADO;
		this.paramCarga.usua_creacion = this.idusuario;
		this.spinner.show();
		this.Sinabip.postValidarAsigTXT(this.paramCarga).subscribe((data: any) => {
			this.spinner.hide();
			this.estado_validacion = data.data.ESTADO_VALIDACION;
			this.estado_validacion_fecha = data.data.VALIDADO_FECHA;
			this.estado_validacion_total = data.data.VALIDADO_TOTAL;

			if (this.estado_validacion == "VALIDACION_SATISFACTORIA") {
				swal({
					position: 'center',
					type: 'success',
					title: 'Validación Finalizada',
					showConfirmButton: false,
					timer: 2000
				})
			} else {
				this.spinner.show();
				this.modal = this.Sinabip.postListadoErroresAsigCargaMasiva(this.paramCarga).subscribe((data: any) => {
					this.spinner.hide();
					this.dataObservacionValidacion = [];
					this.dataObservacionValidacion = data.data;
					this.ArgError = data.data.cant_error;
					this.estado_Validacion = this.ArgError[0].VALIDACION;
					this.estado_Duplicados = this.ArgError[0].DUPLICADOS;
					this.estado_Localarea = this.ArgError[0].LOCALAREA;

					if (this.ArgError[0].ESTADO != '0') {
						this.b_ventanaObservacionesValidacion = true;
						this.modalService.open(content2, {
							ariaLabelledBy: 'modal-basic-title',
							keyboard: false,
							size: 'lg',
							backdrop: 'static'
						}).result.then((result) => {
							this.closeResult = `Closed with: ${result}`;
						}, (reason) => {
							this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
						});
					}
				});

			}
		});
	}

	FinalizarTXT() {
		this.paramCarga.id_entidad = this.id;
		this.paramCarga.id_adjuntado = this.dataAdjuntado.data.ID_ADJUNTADO;
		this.paramCarga.usua_creacion = this.idusuario;
		this.spinner.show();
		this.Sinabip.postFinalizarAsigTXT(this.paramCarga).subscribe((data: any) => {
			this.spinner.hide();
			this.estado_finalizacion = data.data.ESTADO_FINALIZACION;
			this.estado_finalizacion_fecha = data.data.FINALIZADO_FECHA;
			this.estado_finalizacion_total = data.data.FINALIZADO_TOTAL;
			if (this.estado_finalizacion == 'FINALIZACION_SATISFACTORIA') {
				swal({
					position: 'center',
					type: 'success',
					title: 'Finalizada',
					showConfirmButton: false,
					timer: 2000
				})
				this.desabilitar_ventana();
			} else {
				swal({
					position: 'center',
					type: 'success',
					title: 'Error al Finalizar',
					showConfirmButton: false,
					timer: 2000
				})
			}
		});
	}

	private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
			return 'by pressing ESC';
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
			return 'by clicking on a backdrop';
		} else {
			return `with: ${reason}`;
		}
	}

	desabilitar_ventana() {
		this.mostrar_carga = null;
		this.estado_adjuntar = null;
		this.LimpiaAsigMasiva();
		// this.estado_carga = null;
		// this.estado_validacion = null;
		// this.estado_finalizacion = null;
	}

	nuevo(estado) {
		this.mostrar_carga = {
			personal: '', estado: estado
		}
		this.habilitar_adjuntar = '';
		this.estado_adjuntar = null;
		this.LimpiaAsigMasiva();
	}

	open(content, estado) {
		if (estado === 1) {
			this.asignartodo();
			this.asigTodoEstado = sessionStorage.getItem("Asig_Cod_Patrimonial");
			if (this.asigTodoEstado == null) {
				this.asigTodoEstado = '0';
			}
			if (this.asigTodoEstado != '0') {

				swal({
					title: 'Confirmar la Asignación de Todos los Bienes?',
					text: "",
					type: 'question',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Acepta, Asignación Total!'
				}).then((result) => {
					if (result.value) {
						this.Listfechapecosacont.estado = 2;
						this.spinner.show();
						this.Sinabip.PostListadoFechaPecosa(this.Listfechapecosacont).subscribe((data: any) => {
							this.spinner.hide();
							this.Listfechapecosacont_Arg = data.data;
							if (this.Listfechapecosacont_Arg[0].CANT >= 1) {
								this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
									this.closeResult = `Closed with: ${result}`;
								}, (reason) => {
								});
							} else {
								this.ver();
							}
						});
					} else {
						this.Limpiar_ListadoAsignarPerson();
					}
				})
			} else {
				swal({
					position: 'center',
					type: 'warning',
					title: 'No Cuenta con Bienes Asignar!!!',
					showConfirmButton: false,
					timer: 4000
				})
			}
		}

		if (estado === 2) {
			this.asignarseleccionado();
			if (this.contListselecc >= 1) {
				this.Listfechapecosacont.estado = 4;
				this.spinner.show();
				this.Sinabip.PostListadoFechaPecosa(this.Listfechapecosacont).subscribe((data: any) => {
					this.spinner.hide();
					this.Listfechapecosacont_Arg = data.data;
					if (this.Listfechapecosacont_Arg[0].CANT >= 1) {
						this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
							this.closeResult = `Closed with: ${result}`;
						}, (reason) => {
						});
					} else {
						this.ver();
					}
				});
			} else {
				swal({
					position: 'center',
					type: 'warning',
					title: 'Debe seleccionar un bien para continuar!!!',
					showConfirmButton: false,
					timer: 4000
				})
			}
		}
	}

	Listadopersonal() {
		this.spinner.show();
		this.Sinabip.getListadopersonal(this.id).subscribe((data: any) => {
			this.spinner.hide();
			this.ListPers_Arg = data.data;
		});
	}

	ListadoTipoBienEntidad() {
		this.spinner.show();
		this.Sinabip.getListadoTipoBienEntidad(this.id).subscribe((data: any) => {
			this.spinner.hide();
			this.ListtipoBienEnt_Arg = data.data;
		});
	}

	ListadoComboAsig() {
		this.spinner.show();
		this.Sinabip.getListadoComboAsig().subscribe((data: any) => {
			this.spinner.hide();
			this.ListComboBienAsignar_Arg = data.data;
		});
	}

	ListadoAsignarPerson(estado) {
		this.spinner.show();
		this.Sinabip.PostListadoAsignarPerson(this.filtro).subscribe((data: any) => {
			this.spinner.hide();

			if (data.data.error == true){
				if (data.data.reco.hasOwnProperty('id')) { 
					this.router.navigate(['error500']);
					return;
				}else if (data.data.reco.hasOwnProperty('fecha_adquis')) {
					  swal({
						type: 'error',
						title: 'Validacion de Datos',
						text: data.data.reco.fecha_adquis[0],
					  })
					  return;
				}else if (data.data.reco.hasOwnProperty('nrodocument')) {
					swal({
						type: 'error',
						title: 'Validacion de Datos',
						text: data.data.reco.nrodocument[0],
					  })
					return;
				}else if (data.data.reco.hasOwnProperty('page')) {
					this.router.navigate(['error500']);
					return;
				}else if (data.data.reco.hasOwnProperty('records')) {
					this.router.navigate(['error500']);
					return;
				}
				
			}

			this.ListBienAsignar_Arg = data.data;
			this.total = (this.ListBienAsignar_Arg.length > 0) ? this.ListBienAsignar_Arg[0].Total : 0;
			if (estado == 1) {
				this.contcheck = 0;
				this.Arrg_patrim.forEach((data3: any) => {
					this.checkcod_patr = this.Arrg_patrim[this.contcheck];
					this.contcheck = this.contcheck + 1;
					this.contcheck2 = 0;
					this.ListBienAsignar_Arg.forEach((data2: any) => {
						this.checkcod_patr2 = this.ListBienAsignar_Arg[this.contcheck2].CODIGO_PATRIMONIAL;
						this.contcheck2 = this.contcheck2 + 1;
						if (this.checkcod_patr == this.checkcod_patr2) {
							data2.seleccionado = true;
						}
					});
				});
			}
			if (estado == 2) {
				this.Arrg_patrim = [];
				sessionStorage.setItem('Asig_Cod_Patrimonial', '');
				this.ListCodPatrim = '';
				this.ListBienAsignar_Arg.forEach((data4: any) => {
					data4.seleccionado = false;
				});
			}

		});
	}

	loadPage(page: number) {
		if (page !== this.previousPage) {
			this.previousPage = page;
			this.ListadoAsignarPerson(1);
		}
	}

	resetearpag() {
		this.filtro.page = 1;
		this.ListadoAsignarPerson(1);
	}

	resetearlimpiarpag() {
		this.page = 1;
		this.Limpiar_ListadoAsignarPerson();
	}

	Limpiar_ListadoAsignarPerson() {
		this.filtro = {
			id: this.id, cod_patrimonial: '', denominacion: '', fecha_adquis: '', cod_pers: '',
			nrodocument: '', nrobien: '', flagesta: '', page: this.page, records: this.itemsPerPage
		}
		this.Arrg_patrim = [];
		sessionStorage.setItem('Asig_Cod_Patrimonial', '');
		this.ListCodPatrim = '';
		this.spinner.show();
		this.Sinabip.PostListadoAsignarPerson(this.filtro).subscribe((data: any) => {
			this.spinner.hide();
			this.ListBienAsignar_Arg = data.data;
			this.total = (this.ListBienAsignar_Arg.length > 0) ? this.ListBienAsignar_Arg[0].Total : 0;
			this.ListBienAsignar_Arg.forEach((data2: any) => {
				data2.seleccionado = false;
			});
		});
	}

	asignartodo() {
		sessionStorage.setItem('Asig_Cod_Patrimonial', '');
		this.seleccionados = [];
		this.contListselecc = 0;
		this.ListBienAsignar_Arg.forEach((data: any) => {
			data.seleccionado = true;
			if (data.FLAGPERS != 1 && data.seleccionado == true) {
				this.seleccionados.push(data);
			}
		});
		this.spinner.show();
		this.Sinabip.getListaobtTodosCP(this.id).subscribe((data: any) => {
			this.spinner.hide();
			this.ListaObtenerTodo_Arg = data.data;
			this.asigTodoEstado = this.ListaObtenerTodo_Arg[0].LISTA_CP;
			sessionStorage.setItem('Asig_Cod_Patrimonial', this.asigTodoEstado);
			this.contListselecc = this.asigTodoEstado.length;
		});
		this.Listfechapecosa.cod_patrimonial = '';
		this.Listfechapecosa.estado = 1;
		this.ListadoFechaPecosa();
	}

	asignarseleccionado() {
		sessionStorage.setItem('Asig_Cod_Patrimonial', '');
		this.seleccionados = [];
		this.contListCodPatrim = 0;
		this.contListselecc = 0;
		this.ListCodPatrim = '';

		this.ListCodPatrim = JSON.stringify(this.Arrg_patrim);
		this.ListCodPatrim = this.ListCodPatrim.substring(1, (this.ListCodPatrim.length) - 1);
		this.ListCodPatrim = this.ListCodPatrim.replace(/["]+/g, '');
		sessionStorage.setItem('Asig_Cod_Patrimonial', this.ListCodPatrim);
		this.contListselecc = this.ListCodPatrim.length;

		if (this.contListselecc >= 1) {
			this.Listfechapecosa.cod_patrimonial = this.ListCodPatrim;
			this.Listfechapecosacont.cod_patrimonial = this.ListCodPatrim;
			this.Listfechapecosa.estado = 3;
			this.ListadoFechaPecosa();
		}
	}

	ListadoFechaPecosa() {
		this.spinner.show();
		this.Sinabip.PostListadoFechaPecosa(this.Listfechapecosa).subscribe((data: any) => {
			this.spinner.hide();
			this.Listfechapecosa_Arg = data.data;
		});
	}

	ActualizaFechaPecosa() {
		this.fechpecosa = [];
		this.ContActPecosa = 0;
		this.ContActPecosaTotal = 0;
		this.Listfechapecosa_Arg.forEach((data: any) => {
			this.fechpecosa.push(data);
		});
		this.fechpecosa.forEach((data: any) => {
			this.IngPecosa = this.fechpecosa[this.ContActPecosa].fechpecosa;
			this.Ingcod_patr = this.fechpecosa[this.ContActPecosa].CODIGO_PATRIMONIAL;
			if (this.IngPecosa != undefined || this.IngPecosa != null) {
				this.ActFechaPecosa.cod_patrimonial = this.Ingcod_patr;
				this.ActFechaPecosa.fecha_pecosa = this.IngPecosa;
				this.Sinabip.PostActualizaPecosa(this.ActFechaPecosa).subscribe((data: any) => {
					this.ActFechaPecosa_Arg = data.data;
				});
			} else {
				this.ContActPecosaTotal = this.ContActPecosaTotal + 1;
			}
			this.ContActPecosa = this.ContActPecosa + 1;
		});
		if (this.ContActPecosaTotal >= 1) {
			swal('Aviso: Falta Modificar ' + this.ContActPecosaTotal + ' Fechas')
			// alert('Aviso falta Modificar :'+this.ContActPecosaTotal);
		} else {
			this.modalService.dismissAll();
			this.ver();
		}
	}

	ver() {
		this.router.navigate(["asig-bienes-det"]);
	}

	almacenarcodpatrim(estado, cod_patrimonial) {
		if (estado == true) {
			this.Arrg_patrim.push(cod_patrimonial);
		} else {
			this.posicion = this.Arrg_patrim.indexOf(cod_patrimonial);
			this.Arrg_patrim.splice(this.posicion, 1);
		}
	}
	

}
