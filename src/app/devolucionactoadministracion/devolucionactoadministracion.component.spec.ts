import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevolucionactoadministracionComponent } from './devolucionactoadministracion.component';

describe('DevolucionactoadministracionComponent', () => {
  let component: DevolucionactoadministracionComponent;
  let fixture: ComponentFixture<DevolucionactoadministracionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevolucionactoadministracionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevolucionactoadministracionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
