import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { SinabipmueblesService } from '../services/sinabipmuebles.service';
import {NgbModalConfig, NgbModal, ModalDismissReasons, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import swal from'sweetalert2';
// import { forEach } from '@angular/router/src/utils/collection';
import { DatePipe } from '@angular/common'
// import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-devolucionactoadministracion',
  templateUrl: './devolucionactoadministracion.component.html',
  styleUrls: ['./devolucionactoadministracion.component.css'],  
  animations: [routerTransition()],
  providers: [NgbModalConfig, NgbModal, DatePipe]
})
export class DevolucionactoadministracionComponent implements OnInit {
  registerForm: FormGroup;
  RegistroTipoBienForm: FormGroup;
  submitted = false;
  seleccionRows : string = null;
  
  Cod_Entidad : string = sessionStorage.getItem("Cod_Entidad"); 
  Cod_Usuario : string = sessionStorage.getItem("Cod_Usuario");
	RutaSinabip : string = sessionStorage.getItem("RutaSinabip");
  
  mostrarVentanaTipoRegistro : boolean = false; 
  mostrarVentanaRegistroIndividual : boolean = false;
  b_ventanaObservacionesValidacion : boolean = false;
  paramAgregar = {
    TipoagregarBien   : "",
    id_acto           : "",
    id_entidad        : this.Cod_Entidad,
    nro_documento     : "",
    fecha_documento   : "",
    usua_creacion     : this.Cod_Usuario,
    codigo_patrimonial: "", 
    denominacion_bien : "", 
    cantidad          : "", 
    valor_adq         : "", 
    estado            : ""
  }

  paramGuardarActo = {
    id_entidad : this.Cod_Entidad,
    cod_acto : "",
    nro_resolucion : "",
    fecha_resolucion : "",
    id_usuario : "",
    cod_bien : ""
  }

  afuConfig = {
    uploadAPI: {
      url:this.SinabipMuebles.API_URL+"SubirAdministracionTxt/"+this.Cod_Entidad,
      },
      hideResetBtn: true,
      uploadBtnText:"Adjuntar Archivo",
      uploadMsgText: "",
      formatsAllowed: ".TXT,.txt"
  };

    //Paginacion 
    itemsPerPage: number = 10;
    page: any = 1;
    previousPage: any;
    total : any = 0; 

    //Paginacion 
    itemsPerPage2: number = 10;
    page2: any = 1;
    previousPage2: any;
    total2 : any = 0; 

  parametros = { 
    cod_entidad   : this.Cod_Entidad,
    nro_grupo     : "-1",
    nro_clase     : "",
    cod_patrimonial : "",
    denom_bien    : "",
    page2 : this.page2, 
    records : this.itemsPerPage2
  } 

  filtro = {
    fecha : 
    {
      month : 10,
      year  : 2018
    },
    cod_entidad       : this.Cod_Entidad,
    nro_documento     : '',
    estado            : '1',
    page : this.page, 
    records : this.itemsPerPage
  }

  paramCarga = {
    id_entidad    : '',
    id_adjuntado  : '',
    nombreArchivo : '',
    usua_creacion : ''
  }

  paramBien = {
    id_entidad  : this.Cod_Entidad,      
    id_bien     : "",
    formaAdquis : undefined,
    nroDocAdquis: "",
    fechaAdquis : "",
    codigopatri : "",
    denomBien   : "",
    marca       : "",
    modelo      : "",
    tipo        : "",
    color       : "",
    nroSerie    : "",
    nroChasis   : "",
    anioFabric  : "",
    otrasCaract : "",
    usoCta      : "",
    TipoCta     : "",
    CtaContable : "",
    valorAdquis : "",
    porcDeprec  : "",
    asegurado   : "",
    estadoBien  : "",
    observacion : ""
}

paramEliminarActo = {
  id_entidad   : "",
  id_acto      : ""
}

paramRegistroIndividual = { id_entidad : this.Cod_Entidad,   nro_resolucion : '', fecha_resolucion : '',
                            id_usuario : this.Cod_Usuario, 
                            cod_patrimonial : '', id_acto : '', mod_registro :''}

paramEliminar = { id_entidad : this.Cod_Entidad, id_usuario : this.Cod_Usuario, id_acto : '', id_bien : ''}

  dataAnio = [];
  dataMes = [];
  dataEditarActos : any = [];
  dataFormaIndividual : Array<any> = [];
  dataFormaIndividual2 : Array<any> = [];
  dataAdjuntado : any = [];
  dataObservacionValidacion : Array<any> = [];
  dataCaracteristicasBien : Array<any> = [];
  Arrg_patrim_previo = [];
  Arrg_patrim_editar = [];
  Arrg_patrim_Confirmado = [];
  Arrg_patrim_eliminados = [];
  dataActosBaja : any = [];
  dataGuardarBien : any = [];
  dataEliminados : any = [];
  dataListadosFI = [];
    
  id : string="1";
  estado_adjuntar : string = "";
  estado_carga : string = "";
  estado_carga_fecha : string = "";
  estado_carga_total : string = "";
  estado_validacion : string ="";
  estado_validacion_fecha : string ="";
  estado_validacion_total : string ="";
  estado_finalizacion : string ="";
  estado_finalizacion_fecha : string ="";
  estado_finalizacion_total : string ="";
  modal;
  closeResult: string;
  seleccionBien1 : boolean = false;
  seleccionBien2 : boolean = false;
  cantidadduplicado_actos: number = 0;
  cantidadduplicado_bienes: number = 0;
  cantidaderrores_columnas: number = 0;
  cantidadNo_existe_codigos_pat: number = 0;
  cantidadNo_disponible_bienes: number = 0;
  posicion : number;
  contListCodPatrim : number;
  contListselecc : number;
  ListCodPatrim: string;
  ListCodPatrimPrevio: string;
  ListCodPatrimConfirmado: string;
  dataRegistroIndividual = [];
  eliminacionDetalleBajas: boolean = false;
  contador : number = 0;
  cod_patrimonial_editar= [];
  modificando_registro: string = '';
  mostrarVentanaRegistroMasivo : boolean = false;
  Devest_mostr: string = '';

  constructor(
    public datepipe: DatePipe,
    private formBuilder: FormBuilder,
    private SinabipMuebles : SinabipmueblesService, 
    private spinner: NgxSpinnerService, 
    private modalService: NgbModal,
    private router: Router
    ) 
    { 
      let token = sessionStorage.getItem("token");  
      let ruta = sessionStorage.getItem("RutaSinabip")
      if(token === null){
        // window.location.replace(ruta+"SGISBN/System/sinabip.php");
        window.location.replace("./SGISBN/System/sinabip.php");
      }
      let date = new Date(); 
      date.setDate( date.getDate() - 11 );
      let year  = date.getFullYear();
      let month = date.getMonth()+1;
      this.filtro.fecha.month = month;
      this.filtro.fecha.year  = year;
      this.afuConfig.uploadAPI.url = this.SinabipMuebles.API_URL+"AdjuntarDevAdministraciontxt/"+this.Cod_Entidad;
    }

    ngOnInit(): void {
      this.filtro.estado = '-1';
      this.filtro.fecha.month = -1;
  
      this.registerForm = this.formBuilder.group({ 
        documentoActo: ['', Validators.required],
        fechaAdministracion: ['', Validators.required]
      });
      
      let fechita = new Date();
      let yy = fechita.getFullYear();
      this.filtro.fecha.year = yy;
      this.cargarListadoActosAdministracion();
    }

    get f() { return this.registerForm.controls; } 

    ventanaTipoRegistro(){
      this.mostrarVentanaTipoRegistro = true; 
      this.mostrarVentanaRegistroIndividual = false; 
      this.b_ventanaObservacionesValidacion = false;
  
      this.paramAgregar.id_acto = "";
      this.Arrg_patrim_previo = [];
      this.Arrg_patrim_editar = [];
      this.Arrg_patrim_Confirmado = [];
      sessionStorage.setItem('Asig_Cod_Patrimonial', '');
  
      this.paramAgregar = {
        TipoagregarBien   : "",
        id_acto           : "",
        id_entidad        : this.Cod_Entidad,
        nro_documento     : "",
        fecha_documento   : "",
        usua_creacion     : this.Cod_Usuario,
        codigo_patrimonial: "", 
        denominacion_bien : "", 
        cantidad          : "", 
        valor_adq         : "", 
        estado            : ""
      }
    
      this.dataEditarActos = [];
      this.id= "-1";
      this.estado_adjuntar = '';
      this.estado_carga = '';
      this.estado_carga_fecha = '';
      this.estado_carga_total = '';
      this.estado_validacion = '';
      this.estado_validacion_fecha = '';
      this.estado_validacion_total = '';
      this.estado_finalizacion = '';
      this.estado_finalizacion_fecha = '';
      this.estado_finalizacion_total = '';  
      this.submitted=false;
    }
  
  
    cargarListadoActosAdministracion(){ 
      this.filtro.cod_entidad = this.Cod_Entidad;
      this.spinner.show();
      this.SinabipMuebles.postListadoDevolAdministracion(this.filtro).subscribe((data : any) =>{   
      this.spinner.hide();

      if (data.data.error == true){
        if (data.data.reco.hasOwnProperty('cod_entidad')) { 
          this.router.navigate(['error500']);
          return;
        }else if (data.data.reco.hasOwnProperty('nro_documento')) {
          swal({
            type: 'error',
            title: 'Validacion de Datos',
            text: data.data.reco.nro_documento[0],
            })
            return;
        }else if (data.data.reco.hasOwnProperty('page')) {
            this.router.navigate(['error500']);
          return;
        }else if (data.data.reco.hasOwnProperty('records')) {
            this.router.navigate(['error500']);
            return;
        }
      }

      this.dataActosBaja = data.data;
      this.dataAnio = this.dataActosBaja.anios;
      this.dataMes = this.dataActosBaja.mes;  
      this.total = ( this.dataActosBaja.documento.length > 0 ) ? this.dataActosBaja.documento[0].TOTAL : 0;
      });
    }
  
    BorrarFiltro()
    { 
      let fechita = new Date();
      let yy = fechita.getFullYear();
  
      this.filtro = { 
        fecha : 
        {
          month : -1,
          year  : yy
        },
        cod_entidad       : this.Cod_Entidad,
        nro_documento     : '',
        estado            : '-1',
        page : this.page, records : this.itemsPerPage
      }
      this.cargarListadoActosAdministracion();
    }
  
    ventanaRegistroFormaMasiva(){
      this.mostrarVentanaTipoRegistro = false;
      this.mostrarVentanaRegistroIndividual = false;
      this.mostrarVentanaRegistroMasivo = true;
  
      this.estado_adjuntar = '';
      this.estado_carga = '';
      this.estado_carga_fecha = '';
      this.estado_carga_total = '';
      this.estado_validacion = '';
      this.estado_validacion_fecha = '';
      this.estado_validacion_total = '';
      this.estado_finalizacion = '';
      this.estado_finalizacion_fecha = '';
      this.estado_finalizacion_total = ''; 
    }
  
    ventanaRegistroFormaIndividual(){
      this.mostrarVentanaTipoRegistro = false;
      this.mostrarVentanaRegistroIndividual = true;
      this.mostrarVentanaRegistroMasivo = false;
      this.modificando_registro = '';
      this.spinner.show();
      this.modal=this.SinabipMuebles.postDatosListadosFormaIndividual(this.paramAgregar).subscribe((data : any) =>{ 
        this.spinner.hide();
        this.dataListadosFI = data.data;
      });
    }
  
    cambiarFormaAdministracion(){ 
      this.spinner.show();
      this.modal=this.SinabipMuebles.postDatosListadosFormaIndividual(this.paramAgregar).subscribe((data : any) =>{ 
        this.spinner.hide();
        this.dataListadosFI = data.data;
      });
    }
  
    onSubmit() {
      this.submitted = true;
      if (this.registerForm.invalid) { 
        swal({
          type: 'error',
          title: 'Datos Incompletosss!!!',
          text: 'No se ha ingresado la información necesaria para continuar',
        })
          return;
      }
      else
      {
        this.Guardar_Actos();
      }
         
    }
  
    Guardar_Actos():void{
      for (var i=0; i < this.Arrg_patrim_editar.length; i++){
        this.posicion = this.Arrg_patrim_Confirmado.indexOf(this.Arrg_patrim_editar[i]);
        this.Arrg_patrim_Confirmado.splice(this.posicion,1);
      }  
      this.ListCodPatrimConfirmado = JSON.stringify(this.Arrg_patrim_Confirmado);
      this.ListCodPatrimConfirmado = this.ListCodPatrimConfirmado.substring( 1, (this.ListCodPatrimConfirmado.length)-1 ); 
      this.ListCodPatrimConfirmado = this.ListCodPatrimConfirmado.replace(/['"]+/g, '');
      sessionStorage.setItem('Asig_Cod_Patrimonial', this.ListCodPatrimConfirmado);
  
      this.paramRegistroIndividual.id_entidad = this.Cod_Entidad;
      this.paramRegistroIndividual.nro_resolucion = this.registerForm.get('documentoActo').value;
      this.paramRegistroIndividual.fecha_resolucion = this.registerForm.get('fechaAdministracion').value;
      this.paramRegistroIndividual.id_usuario = this.Cod_Usuario;
      this.paramRegistroIndividual.cod_patrimonial = sessionStorage.getItem('Asig_Cod_Patrimonial'); 
      this.paramRegistroIndividual.mod_registro = this.modificando_registro; 
      this.paramRegistroIndividual.id_acto = this.paramAgregar.id_acto;      
      // console.log(this.paramRegistroIndividual);
      this.spinner.show();
      this.SinabipMuebles.postGuardar_DevolAdministracion(this.paramRegistroIndividual).subscribe((data : any) =>{
        this.spinner.hide();

        if (data.data.error == true){
          if (data.data.reco.hasOwnProperty('id_entidad')) { 
              this.router.navigate(['error500']); 
              return;
          }else if (data.data.reco.hasOwnProperty('nro_resolucion')) {
            swal({
              type: 'error',
              title: 'Validacion de Datos',
              text: data.data.reco.nro_resolucion[0],
              })
              return;
          }else if (data.data.reco.hasOwnProperty('fecha_resolucion')) {
            swal({
              type: 'error',
              title: 'Validacion de Datos',
              text: data.data.reco.fecha_resolucion[0],
              })
            return;
          }else if (data.data.reco.hasOwnProperty('id_usuario')) {
            this.router.navigate(['error500']);
            return;
          }else if (data.data.reco.hasOwnProperty('id_acto')) {
            this.router.navigate(['error500']);
            return;
          }
          
        }

      this.dataGuardarBien = data.data;  
      this.cargarListadoActosAdministracion(); 
  
      this.Arrg_patrim_previo = [];
      this.Arrg_patrim_editar = [];
      this.Arrg_patrim_Confirmado = [];
      this.mostrarVentanaTipoRegistro = false; 
      this.mostrarVentanaRegistroIndividual = false; 
      this.b_ventanaObservacionesValidacion = false;
      this.paramAgregar.id_acto = this.dataGuardarBien.ID;
      
      swal({
        position: 'center',
        type: 'success',
        title: 'Se ha Registrado Satisfactoriamente!!!',
      })
      });
      
    }
  
    onSubmit2() {
      this.submitted = true;
      if (this.registerForm.invalid) { 
        swal({
          type: 'error',
          title: 'Datos Incompletosss!!!',
          text: 'No se ha ingresado la información necesaria para continuar',
        })
          return;
      }
      else
      {
        // this.Guardar_Actos();
      }
    }

  desabilitar_ventana(){
    this.mostrarVentanaTipoRegistro = false;  
    this.mostrarVentanaRegistroIndividual = false;
  }
    
  
  open(content) { 
    this.parametros = { 
      cod_entidad   : this.Cod_Entidad,
      nro_grupo     : "-1",
      nro_clase     : "-1",
      cod_patrimonial : "",
      denom_bien    : "",
      page2 : this.page2, records : this.itemsPerPage2
    } 
  
    this.paramAgregar.codigo_patrimonial = "";
    this.paramAgregar.denominacion_bien = "";
    this.paramAgregar.cantidad = "";
    this.paramAgregar.valor_adq = "";
    this.dataFormaIndividual = [];
    this.ListCodPatrim = '';   
    this.Devest_mostr = '';
  
    this.onSubmit2();
    if(this.registerForm.valid){
      
      this.spinner.show();
      this.modal=this.SinabipMuebles.postListadoBienesDevolFormaIndividual_Administracion(this.parametros).subscribe((data : any) =>{ 
        this.spinner.hide();
        this.dataFormaIndividual = data.data;
      });      
  
      this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', 
      keyboard: false,
      size: 'lg',
      backdrop: 'static'}).result.then((result) => { 
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`; 
      });
    }else
    {
      swal({
        type: 'error',
        title: 'Datos Incompletos!!!',
        text: 'No se ha ingresado la información necesaria para continuar', 
      })
    }
  }
  
  ListadoBienesFormaIndividual_Administracion(){
    this.spinner.show();
    this.SinabipMuebles.postListadoBienesDevolFormaIndividual_Administracion(this.parametros).subscribe((data : any) =>{ 
    this.spinner.hide();
    this.dataFormaIndividual = data.data; 
    this.dataFormaIndividual2 = data.data.bienespatrimoniales; 
    this.total = ( this.dataFormaIndividual2.length > 0 ) ? this.dataFormaIndividual2[0].TOTAL : 0;
  });
  }
    
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  
  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.cargarListadoActosAdministracion();
    }  
  }
  
  loadPageRegisterIndividual(page2: number) {
    if (page2 !== this.previousPage2) {
      this.previousPage2 = page2;
      this.ListadoBienesFormaIndividual_Administracion();
    }  
  }
  
  resetearpag(){
    this.filtro.page = 1;
    this.cargarListadoActosAdministracion(); 
  }
  
  AdjuntarDevAdministraciontxt(event){
    this.estado_adjuntar == '';
    this.estado_carga = '';
    this.estado_validacion = '';
    this.estado_finalizacion = '';
    this.estado_carga_total = '';
    this.estado_carga_fecha = '';
    this.estado_validacion_total = '';
    this.estado_validacion_fecha = '';
    this.estado_finalizacion_total = '';
    this.estado_finalizacion_fecha = '';
    this.afuConfig.uploadAPI.url = this.SinabipMuebles.API_URL+"AdjuntarDevAdministraciontxt/"+this.Cod_Entidad; 
    this.dataAdjuntado = JSON.parse(event.response); 
    this.estado_adjuntar = this.dataAdjuntado.data.RESULTADO;
  }
  
  CargarAdministracionTXT(){ 
    this.paramCarga.id_entidad = this.Cod_Entidad; 
    this.paramCarga.id_adjuntado = this.dataAdjuntado.data.ID_ADJUNTADO;  
    this.paramCarga.nombreArchivo = this.dataAdjuntado.data.NOMBRE_ARCHIVO;
    this.paramCarga.usua_creacion = this.Cod_Usuario; 
    this.spinner.show();
    this.SinabipMuebles.postCargarDevAdministracionTXT(this.paramCarga).subscribe((data : any) =>{  
    this.spinner.hide();

    if (data.data.error == true){
      if (data.data.reco.hasOwnProperty('id_entidad')) { 
          this.router.navigate(['error500']);
          return;
      }else if (data.data.reco.hasOwnProperty('id_adjuntado')) {
          this.router.navigate(['error500']);
          return;
        }else if (data.data.reco.hasOwnProperty('usua_creacion')) {
          this.router.navigate(['error500']);
          return;
        }
      }

    this.estado_carga = data.data.ESTADO_CARGA;
    this.estado_carga_fecha = data.data.CARGADO_FECHA;
    this.estado_carga_total = data.data.CARGADO_TOTAL;
    swal({
      position: 'center',
      type: 'success',
      title: 'Carga Finalizada',
      showConfirmButton: false,
      timer: 2000
    })
  
    });
  }
  
  ValidarAdministracionTXT(content)
  {
   this.Devest_mostr = '1';
    // this.b_ventanaObservacionesValidacion = false;
    this.paramCarga.id_entidad = this.Cod_Entidad; 
    this.paramCarga.id_adjuntado = this.dataAdjuntado.data.ID_ADJUNTADO;  
    this.paramCarga.usua_creacion = this.Cod_Usuario;
    this.spinner.show();
    this.SinabipMuebles.postValidarDevAdministracionTXT(this.paramCarga).subscribe((data : any) =>{ 
      this.spinner.hide();

      if (data.data.error == true){
        if (data.data.reco.hasOwnProperty('id_entidad')) { 
            this.router.navigate(['error500']);
            return;
        }else if (data.data.reco.hasOwnProperty('id_adjuntado')) {
            this.router.navigate(['error500']);
            return;
          }else if (data.data.reco.hasOwnProperty('usua_creacion')) {
            this.router.navigate(['error500']);
            return;
          }
        }

      this.estado_validacion = data.data.ESTADO_VALIDACION;
      this.estado_validacion_fecha = data.data.VALIDADO_FECHA;
      this.estado_validacion_total = data.data.VALIDADO_TOTAL;
  
    if (this.estado_validacion == "VALIDACION_SATISFACTORIA" ){
      swal({
        position: 'center', 
        type: 'success',
        title: 'Validación Finalizada',
        showConfirmButton: false,
        timer: 2000 
      })
      this.modalService.dismissAll();
    }else{
      this.spinner.show();
      this.modal=this.SinabipMuebles.postListadoErroresCargaMasivaAdministracion(this.paramCarga).subscribe((data : any) =>{   
        this.spinner.hide();
        this.dataObservacionValidacion = [];
        this.dataObservacionValidacion = data.data;
        this.cantidadduplicado_actos = this.dataObservacionValidacion["duplicado_actos"].length 
        this.cantidaderrores_columnas = this.dataObservacionValidacion["errores_columnas"].length
        this.cantidadNo_existe_codigos_pat = this.dataObservacionValidacion["no_existe_codigos"].length
        this.cantidadNo_disponible_bienes = this.dataObservacionValidacion["no_disponibles"].length
      });
  
      console.log(this.Devest_mostr);
      this.b_ventanaObservacionesValidacion = true;
      this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title',
      keyboard: false,
      size: 'lg',
      backdrop: 'static'}).result.then((result) => { 
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    }
    });
  }
  
  FinalizarAdministracionTXT()
  {
    this.paramCarga.id_entidad = this.Cod_Entidad; 
    this.paramCarga.id_adjuntado = this.dataAdjuntado.data.ID_ADJUNTADO;  
    this.paramCarga.usua_creacion = this.Cod_Usuario;
    this.spinner.show();
    this.SinabipMuebles.postFinalizarDevAdministracionTXT(this.paramCarga).subscribe((data : any) =>{ 
    this.spinner.hide();

    if (data.data.error == true){
      if (data.data.reco.hasOwnProperty('id_entidad')) { 
          this.router.navigate(['error500']);
          return;
      }else if (data.data.reco.hasOwnProperty('id_adjuntado')) {
          this.router.navigate(['error500']);
          return;
        }else if (data.data.reco.hasOwnProperty('usua_creacion')) {
          this.router.navigate(['error500']);
          return;
        }
      }

    this.estado_finalizacion = data.data.ESTADO_FINALIZACION;
    this.estado_finalizacion_fecha = data.data.FINALIZADO_FECHA;
    this.estado_finalizacion_total = data.data.FINALIZADO_TOTAL;
    swal({
      position: 'center',
      type: 'success',
      title: 'Finalizada',
      showConfirmButton: false,
      timer: 2000
    })
    this.cargarListadoActosAdministracion(); 
    this.mostrarVentanaTipoRegistro = false;
    this.mostrarVentanaRegistroIndividual = false; 
    this.mostrarVentanaRegistroMasivo = false;
    });
  }
  
  openValidacion(content, id_adjuntdo) { 
    this.paramCarga.id_entidad = this.Cod_Entidad; 
    this.paramCarga.id_adjuntado = this.dataAdjuntado.data.ID_ADJUNTADO;  
    this.spinner.show();
    this.modal=this.SinabipMuebles.postObservaciones_Validacion(this.paramBien).subscribe((data : any) =>{  
      this.spinner.hide();
      this.dataObservacionValidacion = data.data;
    });
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title',
    keyboard: false,
    size: 'lg',
    backdrop: 'static'}).result.then((result) => { 
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
    
  editarActoAdministracion(id_acto){
    this.Arrg_patrim_eliminados=[];
    this.paramAgregar.id_acto = id_acto;
    this.paramAgregar.id_entidad = this.Cod_Entidad;
    this.Arrg_patrim_previo = [];
    this.Arrg_patrim_editar = [];
    this.Arrg_patrim_Confirmado = [];
    sessionStorage.setItem('Asig_Cod_Patrimonial', '');
    this.spinner.show();
    this.SinabipMuebles.posteditarDevolAdministracion(this.paramAgregar).subscribe((data : any) =>{   
    this.spinner.hide();

    if (data.data.error == true){
			if (data.data.reco.hasOwnProperty('id_entidad')) { 
				this.router.navigate(['error500']);
				return;
			}else if (data.data.reco.hasOwnProperty('id_acto')) {
				  this.router.navigate(['error500']);
				  return;
			}
			
		}

    this.dataEditarActos = data.data;   
    this.contador = 0;
    this.dataEditarActos.detalles.forEach((data5 :any) =>{
      this.cod_patrimonial_editar = this.dataEditarActos.detalles[this.contador].CODIGO_PATRIMONIAL;
      this.contador += 1;
      this.Arrg_patrim_editar.push(this.cod_patrimonial_editar);
      this.mostrarVentanaTipoRegistro = false;
      this.mostrarVentanaRegistroIndividual = true;
  
      this.modificando_registro = 'M';
      this.cantidadduplicado_bienes = 0;
      this.cantidadduplicado_actos = 0;  
      this.cantidaderrores_columnas = 0;
      this.cantidadNo_existe_codigos_pat = 0;
      this.cantidadNo_disponible_bienes = 0;
      
    });
    this.seleccionRows = id_acto;
    this.registerForm.get('documentoActo').setValue(this.dataEditarActos.cabecera.NRO_DOCUMENTO);
    this.paramAgregar.nro_documento = this.dataEditarActos.cabecera.NRO_DOCUMENTO;
    this.registerForm.get('fechaAdministracion').setValue(this.dataEditarActos.cabecera.FECHA_DOCUMENTO);
    this.paramAgregar.fecha_documento = this.dataEditarActos.cabecera.FECHA_DOCUMENTO;
    this.cambiarFormaAdministracion();
    });
    this.mostrarVentanaTipoRegistro = false;
    this.mostrarVentanaRegistroIndividual = true;
  }
    
  open1(content, id_bien) { 
    this.paramBien.id_entidad = this.Cod_Entidad;     
    this.paramBien.id_bien = id_bien;   
    this.Devest_mostr = '';
    this.spinner.show();
    this.modal=this.SinabipMuebles.postDatos_Caracteristicas_Bien(this.paramBien).subscribe((data : any) =>{  
      this.spinner.hide();
  
      this.dataCaracteristicasBien = data.data;
      let acto = data.data;
      this.paramBien.formaAdquis = acto.CaracteristicasBien[0].NOM_FORM_ADQUIS;      
      this.paramBien.nroDocAdquis = acto.CaracteristicasBien[0].NRO_DOCUMENTO_ADQUIS;   
      this.paramBien.fechaAdquis = acto.CaracteristicasBien[0].FECHA_DOCUMENTO_ADQUIS;   
      this.paramBien.fechaAdquis =this.datepipe.transform(this.paramBien.fechaAdquis, 'dd-MM-yyyy');
      this.paramBien.codigopatri = acto.CaracteristicasBien[0].CODIGO_PATRIMONIAL; 
      this.paramBien.denomBien = acto.CaracteristicasBien[0].DENOMINACION_BIEN; 
      this.paramBien.marca = acto.CaracteristicasBien[0].MARCA; 
      this.paramBien.modelo = acto.CaracteristicasBien[0].MODELO; 
      this.paramBien.tipo = acto.CaracteristicasBien[0].TIPO; 
      this.paramBien.color = acto.CaracteristicasBien[0].COLOR; 
      this.paramBien.nroSerie = acto.CaracteristicasBien[0].SERIE; 
      this.paramBien.nroChasis = acto.CaracteristicasBien[0].NRO_CHASIS; 
      this.paramBien.anioFabric = acto.CaracteristicasBien[0].ANIO_FABRICACION; 
      this.paramBien.otrasCaract = acto.CaracteristicasBien[0].OTRAS_CARACT; 
      this.paramBien.usoCta = acto.CaracteristicasBien[0].USO_CUENTA;
      this.paramBien.TipoCta = acto.CaracteristicasBien[0].TIP_CUENTA; 
      this.paramBien.CtaContable = acto.CaracteristicasBien[0].COD_CTA_CONTABLE; 
      this.paramBien.valorAdquis = acto.CaracteristicasBien[0].VALOR_ADQUIS; 
      this.paramBien.porcDeprec = acto.CaracteristicasBien[0].PORC_DEPREC; 
      this.paramBien.asegurado = acto.CaracteristicasBien[0].OPC_ASEGURADO; 
      this.paramBien.estadoBien = acto.CaracteristicasBien[0].COD_ESTADO_BIEN; 
      this.paramBien.observacion = acto.CaracteristicasBien[0].OBSERVACION; 
      this.cantidadduplicado_bienes = 0;
      this.cantidadduplicado_actos = 0;  
      this.cantidaderrores_columnas = 0;
      this.cantidadNo_existe_codigos_pat = 0;
      this.cantidadNo_disponible_bienes = 0;
      // console.log(data.data);
    });
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title',
    keyboard: false,
    size: 'lg',
    backdrop: 'static'}).result.then((result) => { 
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  
  Eliminar_ActosdevlAdministracion(id_acto){
    swal({
      title: '¿Esta Ud. seguro de eliminar el Acta de Administración?',
      text: "¡No podrás revertir esto!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonText: 'Cancelado',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Eliminar!'
      
    }).then((result) => {
      if (result.value) {
      
      this.paramEliminar.id_entidad = this.Cod_Entidad;
      this.paramEliminar.id_acto = id_acto;
      this.paramEliminar.id_usuario = this.Cod_Usuario;
      this.spinner.show();
      this.SinabipMuebles.postEliminacion_Devol_Administracion(this.paramEliminar).subscribe((data : any) =>{
        this.spinner.hide();

        if (data.data.error == true){
          if (data.data.reco.hasOwnProperty('id_entidad')) { 
            this.router.navigate(['error500']);
            return;
          }else if (data.data.reco.hasOwnProperty('id_acto')) {
              this.router.navigate(['error500']);
              return;
          }else if (data.data.reco.hasOwnProperty('id_usuario')) {
            this.router.navigate(['error500']);
            return;
          }
          
        }


        this.dataEliminados = data.data;   
        if (this.dataEliminados.datos[0].RESULTADO == 'CORRECTO'){
          this.cargarListadoActosAdministracion();
          this.mostrarVentanaTipoRegistro = false; 
          this.mostrarVentanaRegistroIndividual = false; 
          this.b_ventanaObservacionesValidacion = false;
          swal(
            'Eliminado!',
            'El registro ha sido eliminado.',
            'success' 
          )
          this.modalService.dismissAll();
        }else{
          swal(
          'Error!',
          'Ocurrio un inconveniente',
          'error'
          )
        }
      });
      }
    })
  }
  
  almacenarcodpatrim(estado,cod_patrimonial){
    if(estado == true){
      this.Arrg_patrim_previo.push(cod_patrimonial);
    }else{
      this.posicion = this.Arrg_patrim_previo.indexOf(cod_patrimonial);
      this.Arrg_patrim_previo.splice(this.posicion,1);
    }
  }
  
  AgregarBien_Actos():void{
    this.contListCodPatrim = 0; 
    this.contListselecc = 0;
    this.ListCodPatrim = ''; 
    this.Arrg_patrim_Confirmado = this.Arrg_patrim_editar.concat(this.Arrg_patrim_previo, this.Arrg_patrim_Confirmado); 
    this.ListCodPatrimPrevio = JSON.stringify(this.Arrg_patrim_previo);
    this.ListCodPatrimConfirmado = JSON.stringify(this.Arrg_patrim_Confirmado);
    this.ListCodPatrimPrevio = this.ListCodPatrimPrevio.substring( 1, (this.ListCodPatrimPrevio.length)-1 ); 
    this.ListCodPatrimPrevio = this.ListCodPatrimPrevio.replace(/['"]+/g, '');
    this.ListCodPatrimConfirmado = this.ListCodPatrimConfirmado.substring( 1, (this.ListCodPatrimConfirmado.length)-1 ); 
    this.ListCodPatrimConfirmado = this.ListCodPatrimConfirmado.replace(/['"]+/g, '');
  
    sessionStorage.setItem('Asig_Cod_PatrimonialPrevio', this.ListCodPatrimPrevio);
    sessionStorage.setItem('Asig_Cod_Patrimonial', this.ListCodPatrimConfirmado);
  
    this.contListselecc = this.ListCodPatrimConfirmado.length;
    if(this.contListselecc >= 1)
    {
      this.paramRegistroIndividual.cod_patrimonial = this.ListCodPatrimConfirmado; 
      this.spinner.show();
      this.modal=this.SinabipMuebles.postAgregar_Bienes_al_Detalle_DevAdministracion(this.paramRegistroIndividual).subscribe((data : any) =>{  
      this.spinner.hide();
      this.dataEditarActos = data.data;
      this.modalService.dismissAll();
    });
    }else
    {
      swal(
        'Información',
        'Ud. no ha seleccionado ningún Bien',
        'info'
      )
    }
  }
  
  Eliminacion_Detalles_DevAdministracion(id_bien) 
  {
    this.eliminacionDetalleBajas = false;
    swal({
      title: '¿Esta Ud. seguro de eliminar el Bien?',
      text: "¡No podrás revertir esto!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonText: 'Cancelado',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Eliminar!'
      
    }).then((result) => {
      if (result.value) {
      
      this.paramEliminar.id_entidad = this.Cod_Entidad;
      this.paramEliminar.id_acto = this.paramAgregar.id_acto;
      this.paramEliminar.id_bien = id_bien;
      this.paramEliminar.id_usuario = this.Cod_Usuario;
      this.spinner.show();
      this.SinabipMuebles.postEliminacion_Detalles_DevAdministracion(this.paramEliminar).subscribe((data : any) =>{
        this.spinner.hide();
        this.dataEliminados = data.data;   
        // console.log(this.dataEliminados); 
        if (this.dataEliminados.datos[0].RESULTADO == 'CORRECTO'){
  
          this.editarActoAdministracion(this.paramAgregar.id_acto);
          this.cargarListadoActosAdministracion();
          swal(
            'Eliminado!',
            'El registro ha sido eliminado.',
            'success' 
          )
          this.modalService.dismissAll();
        }else{
          swal(
          'Error!',
          'Ocurrio un inconveniente',
          'error'
          )
        }
      });
      }
    })
  }
}

interface ILogro{
  id : number;
  title : string;
  description ?: string;
}