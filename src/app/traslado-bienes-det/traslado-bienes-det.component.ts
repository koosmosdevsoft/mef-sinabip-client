import { Component, OnInit } from '@angular/core';

import { routerTransition } from '../router.animations';
import { SinabipmueblesService } from '../services/sinabipmuebles.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
// import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbModalConfig, NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
	selector: 'app-traslado-bienes-det',
	templateUrl: './traslado-bienes-det.component.html',
	styleUrls: ['./traslado-bienes-det.component.css'],
	animations: [routerTransition()],
	providers: [NgbModalConfig, NgbModal]
})
export class TrasladoBienesDetComponent implements OnInit {

	id: string = sessionStorage.getItem("Cod_Entidad");
	idusuario: string = sessionStorage.getItem("Cod_Usuario");
	NomPers_Arg: Array<any> = [];
	TipoTraslado_Arg: Array<any> = [];
	ListBienTrasl_Arg: Array<any> = [];
	ListaObtenerTodo_Arg: Array<any> = [];
	Arrg_patrim = [];
	ListPred_Arg: Array<any> = [];
	ListArea_Arg: Array<any> = [];
	ListPers_Arg: Array<any> = [];
	NomAreaPred_Arg: Array<any> = [];
	AgregarCabTrasl_Arg: Array<any> = [];
	NomPersonal: string;
	NroDocumento: string;
	contcheck: number;
	contcheck2: number;
	checkcod_patr: string;
	checkcod_patr2: string;
	ListCodPatrim: string;
	posicion: number;
	seleccionados = [];
	contListCodPatrim: number;
	contListselecc: number;
	asigTodoEstado: string;
	closeResult: string;
	NomPredio: string;
	NomArea: string;
	registerForm: FormGroup;
	submitted = false;

	//Paginacion 
	itemsPerPage: number = 20;
	page: any = 1;
	previousPage: any;
	total: any = 0;

	ListadoAreaPredio = { id: this.id, id_predio: '', id_area: '', estado: '0', Tipotraslado: '', id_personal: '' }

	AgregarCabTrasl = {
		id: this.id, id_predio_dest: '', id_area_dest: '', id_personal_dest: '',
		id_personal_orig: '', tipo_traslado: '', id_usuario: ''
	}

	filtro = {
		id: this.id, cod_pers: sessionStorage.getItem("Traslado_Personal_Origen"), cod_patrimonial: '', denominacion: '', page: this.page, records: this.itemsPerPage
	}  

	constructor(private formBuilder: FormBuilder, config: NgbModalConfig, private Sinabip: SinabipmueblesService,private route: ActivatedRoute,private router: Router,private spinner: NgxSpinnerService,private modalService: NgbModal) {
		this.ListadoTrasladoPerson(1);
		this.PersonalNomb(sessionStorage.getItem("Traslado_Personal_Origen"));
		this.Listadopredarea('1');
		config.backdrop = 'static';
		config.keyboard = false;
	}

	ngOnInit() {
		this.registerForm = this.formBuilder.group({
			formapredio: ['', Validators.required],
			formaarea: ['', Validators.required],
			//fechaAdquisicion: ['', [Validators.required, Validators.email]],
			//password: ['', [Validators.required, Validators.minLength(6)]]
		});
	}


	PersonalNomb(id_personal) {
		this.spinner.show();
		this.Sinabip.getPersonalNomb(this.id, id_personal).subscribe((data: any) => {
			this.spinner.hide();
			this.NomPers_Arg = data.data;
			this.NomPersonal = this.NomPers_Arg[0].PERSONAL;
			this.NroDocumento = this.NomPers_Arg[0].NRO_DOCUMENTO;
		});
	}

	ListadoTrasladoPerson(estado) {
		this.spinner.show();
		this.Sinabip.PostListadoTrasladoPerson(this.filtro).subscribe((data: any) => {
			this.spinner.hide();
			this.ListBienTrasl_Arg = data.data;
			this.total = (this.ListBienTrasl_Arg.length > 0) ? this.ListBienTrasl_Arg[0].Total : 0;
			if (estado == 1) {
				this.contcheck = 0;
				this.Arrg_patrim.forEach((data3: any) => {
					this.checkcod_patr = this.Arrg_patrim[this.contcheck];
					this.contcheck = this.contcheck + 1;
					this.contcheck2 = 0;
					this.ListBienTrasl_Arg.forEach((data2: any) => {
						this.checkcod_patr2 = this.ListBienTrasl_Arg[this.contcheck2].CODIGO_PATRIMONIAL;
						this.contcheck2 = this.contcheck2 + 1;
						if (this.checkcod_patr == this.checkcod_patr2) {
							data2.seleccionado = true;
						}
					});
				});
			}
			if (estado == 2) {
				this.Arrg_patrim = [];
				sessionStorage.setItem('Traslado_Cod_Patrimonial', '');
				this.ListCodPatrim = '';
				this.ListBienTrasl_Arg.forEach((data4: any) => {
					data4.seleccionado = false;
				});
			}
		});
	}

	loadPage(page: number) {
		if (page !== this.previousPage) {
			this.previousPage = page;
			this.ListadoTrasladoPerson(1);
		}
	}

	resetearpag() {
		this.filtro.page = 1;
		this.ListadoTrasladoPerson(1);
	}

	resetearlimpiarpag() {
		this.page = 1;
		this.Limpiar_ListadoTrasladoPerson();
	}

	Limpiar_ListadoTrasladoPerson() {
		this.filtro = {
			id: this.id, cod_pers: sessionStorage.getItem("Traslado_Personal_Origen"), cod_patrimonial: '', denominacion: '', page: this.page, records: this.itemsPerPage
		}
		this.Arrg_patrim = [];
		sessionStorage.setItem('Traslado_Cod_Patrimonial', '');
		this.ListCodPatrim = '';
		this.spinner.show();
		this.Sinabip.PostListadoTrasladoPerson(this.filtro).subscribe((data: any) => {
			this.spinner.hide();
			this.ListBienTrasl_Arg = data.data;
			this.ListBienTrasl_Arg.forEach((data2: any) => {
				data2.seleccionado = false;
			});
		});
	}


	almacenarcodpatrim(estado, cod_patrimonial) {
		if (estado == true) {
			this.Arrg_patrim.push(cod_patrimonial);
		} else {
			this.posicion = this.Arrg_patrim.indexOf(cod_patrimonial);
			this.Arrg_patrim.splice(this.posicion, 1);
		}
	}

	Trasladarseleccionado() {
		sessionStorage.setItem('Traslado_Cod_Patrimonial', '');
		this.seleccionados = [];
		this.contListCodPatrim = 0;
		this.contListselecc = 0;
		this.ListCodPatrim = '';

		this.ListCodPatrim = JSON.stringify(this.Arrg_patrim);
		this.ListCodPatrim = this.ListCodPatrim.substring(1, (this.ListCodPatrim.length) - 1);
		this.ListCodPatrim = this.ListCodPatrim.replace(/['"]+/g, '');
		sessionStorage.setItem('Traslado_Cod_Patrimonial', this.ListCodPatrim);
		this.contListselecc = this.ListCodPatrim.length;
	}

	Trasladartodo() {
		sessionStorage.setItem('Traslado_Cod_Patrimonial', '');
		this.seleccionados = [];
		this.contListselecc = 0;
		this.ListBienTrasl_Arg.forEach((data: any) => {
			data.seleccionado = true;
			if (data.FLAGPERS != 1 && data.seleccionado == true) {
				this.seleccionados.push(data);
			}
		});

		this.spinner.show();
		this.Sinabip.getListaobtTodosCPDevol(this.id, sessionStorage.getItem("Traslado_Personal_Origen")).subscribe((data: any) => {
			this.spinner.hide();
			this.ListaObtenerTodo_Arg = data.data;
			this.asigTodoEstado = this.ListaObtenerTodo_Arg[0].LISTA_CP;
			sessionStorage.setItem('Traslado_Cod_Patrimonial', this.asigTodoEstado);
			this.contListselecc = this.asigTodoEstado.length;
		});
	}



	ListadoTipoTraslado() {
		this.spinner.show();
		this.Sinabip.GetListadoTipoTraslado().subscribe((data: any) => {
			this.spinner.hide();
			this.TipoTraslado_Arg = data.data;
		});
	}

	open(content, estado) {
		if (estado === 1) {
			this.Trasladartodo();
			this.asigTodoEstado = sessionStorage.getItem("Traslado_Cod_Patrimonial");
			if (this.asigTodoEstado == null) {
				this.asigTodoEstado = '0';
			}
			if (this.asigTodoEstado != '0') {

				swal({
					title: 'Confirmar el Traslado de Todos los Bienes?',
					text: "",
					type: 'question',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Acepta, Traslado Total!'
				}).then((result) => {
					if (result.value) {
						this.modalService.open(content, {  ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
							this.closeResult = `Closed with: ${result}`;
						}, (reason) => {
						});
						this.ListadoAreaPredio.Tipotraslado = '';
						this.ListadoAreaPredio.id_personal = '';
						this.ListadoAreaPredio.id_predio = '';
						this.ListadoAreaPredio.id_area = '';
						this.Listadopersonal();
						this.ListadoTipoTraslado();
						this.Listadopredarea('4');
					} else {
						this.Limpiar_ListadoTrasladoPerson();
					}
				})
			} else {
				swal({
					position: 'center',
					type: 'warning',
					title: 'No Cuenta con Bienes Seleccionados!!!',
					showConfirmButton: false,
					timer: 4000
				})
			}
		}

		if (estado === 2) {
			this.Trasladarseleccionado();
			if (this.contListselecc >= 1) {
				this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
					this.closeResult = `Closed with: ${result}`;
				}, (reason) => {
				});
				this.ListadoAreaPredio.Tipotraslado = '';
				this.ListadoAreaPredio.id_personal = '';
				this.ListadoAreaPredio.id_predio = '';
				this.ListadoAreaPredio.id_area = '';
				this.Listadopersonal();
				this.ListadoTipoTraslado();
				this.Listadopredarea('4');
			} else {
				swal({
					position: 'center',
					type: 'warning',
					title: 'Debe seleccionar un bien para continuar!!!',
					showConfirmButton: false,
					timer: 4000
				})
			}
		}
	}

	Listadopredarea(estado) {
		this.ListadoAreaPredio.estado = estado;
		if (estado === '1') {
			this.Sinabip.PostListadopredarea(this.ListadoAreaPredio).subscribe((data: any) => {
				this.ListPred_Arg = data.data;
			});
		}
		if (estado === '2') {
			this.Sinabip.PostListadopredarea(this.ListadoAreaPredio).subscribe((data: any) => {
				this.ListArea_Arg = data.data;
			});
		}
		if (estado === '4') {
			this.Sinabip.PostListadopredarea(this.ListadoAreaPredio).subscribe((data: any) => {
				this.NomAreaPred_Arg = data.data;
			});
		}
	}

	ActualizaArea() {
		this.ListadoAreaPredio.id_predio = this.registerForm.get('formapredio').value;
		this.ListadoAreaPredio.id_area = '';
		this.Listadopredarea('2');
	}

	Actualizatipotrasl() {
		this.registerForm = this.formBuilder.group({
			formapredio: ['', Validators.required],
			formaarea: ['', Validators.required],
		});
		this.ListadoAreaPredio.id_personal = '';
		this.ListadoAreaPredio.id_predio = '';
		this.ListadoAreaPredio.id_area = '';
	}

	Actualizaperson() {
		this.registerForm = this.formBuilder.group({
			formapredio: ['', Validators.required],
			formaarea: ['', Validators.required],
		});
		this.ListadoAreaPredio.id_predio = '';
		this.ListadoAreaPredio.id_area = '';
	}

	Listadopersonal() {
		this.spinner.show();
		this.Sinabip.getListadopersonal(this.id).subscribe((data: any) => {
			this.spinner.hide();
			this.ListPers_Arg = data.data;
		});
	}

	AgregarCabTraslado() {
		sessionStorage.setItem("Trasl_Predio", this.ListadoAreaPredio.id_predio);
		sessionStorage.setItem("Trasl_Area", this.ListadoAreaPredio.id_area);
		sessionStorage.setItem("Trasl_Personal", this.ListadoAreaPredio.id_personal);
		sessionStorage.setItem("Trasl_TipoTrasl", this.ListadoAreaPredio.Tipotraslado);
		this.AgregarCabTrasl = {
			id: this.id,
			id_predio_dest: sessionStorage.getItem("Trasl_Predio"),
			id_area_dest: sessionStorage.getItem("Trasl_Area"),
			id_personal_dest: sessionStorage.getItem("Trasl_Personal"),
			id_personal_orig: sessionStorage.getItem("Traslado_Personal_Origen"),
			tipo_traslado: sessionStorage.getItem("Trasl_TipoTrasl"),
			id_usuario: sessionStorage.getItem("Cod_Usuario")
		}
		this.spinner.show();
		this.Sinabip.PostAgregarCabTraslado(this.AgregarCabTrasl).subscribe((data: any) => {
			this.spinner.hide();

			if (data.data.error == true){
				if (data.data.reco.hasOwnProperty('id')) { 
					this.router.navigate(['error500']);
					return;
				}else if (data.data.reco.hasOwnProperty('id_predio_dest')) {
					  this.router.navigate(['error500']);
					  return;
				}else if (data.data.reco.hasOwnProperty('id_area_dest')) {
					  this.router.navigate(['error500']);
					  return;
				}else if (data.data.reco.hasOwnProperty('id_personal_dest')) {
					this.router.navigate(['error500']);
					return;
				}else if (data.data.reco.hasOwnProperty('id_personal_orig')) {
					this.router.navigate(['error500']);
					return;
				}else if (data.data.reco.hasOwnProperty('id_usuario')) {
					this.router.navigate(['error500']);
					return;
				}
			}

			this.AgregarCabTrasl_Arg = data.data;
			sessionStorage.setItem("NrTrasl_Act", this.AgregarCabTrasl_Arg[0].NROTRASL);
			sessionStorage.setItem("NrDev_Act", this.AgregarCabTrasl_Arg[0].NRODEV);
			sessionStorage.setItem("NrAsig_Act", this.AgregarCabTrasl_Arg[0].NROASIG);
			sessionStorage.setItem("Fecha_Traslado", this.AgregarCabTrasl_Arg[0].FECHA_TRASL);
		});
		this.modalService.dismissAll();
		setTimeout(() => {
			this.ver();
		}, 1500);
	}

	ver() {
		this.router.navigate(["traslados-bienes-resumen"]);
	}

	get f() { return this.registerForm.controls; }


	onSubmit() {
		this.submitted = true;
		// stop here if form is invalid
		if (this.registerForm.invalid) {
			swal({
				type: 'error',
				title: 'Datos Incompletosss!!!',
				text: 'No se ha ingresado la información necesaria para continuar',
				// footer: '<a href>Why do I have this issue?</a>'
			})
			return;
		}
		else {
			this.ListadoAreaPredio.id_predio = this.registerForm.get('formapredio').value;
			this.ListadoAreaPredio.id_area = this.registerForm.get('formaarea').value;
			this.AgregarCabTraslado();
		}
	}

}
