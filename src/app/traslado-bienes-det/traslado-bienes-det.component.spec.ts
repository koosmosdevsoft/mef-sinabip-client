import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrasladoBienesDetComponent } from './traslado-bienes-det.component';

describe('TrasladoBienesDetComponent', () => {
  let component: TrasladoBienesDetComponent;
  let fixture: ComponentFixture<TrasladoBienesDetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrasladoBienesDetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrasladoBienesDetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
