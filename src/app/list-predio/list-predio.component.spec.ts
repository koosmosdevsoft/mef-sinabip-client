import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPredioComponent } from './list-predio.component';

describe('ListPredioComponent', () => {
  let component: ListPredioComponent;
  let fixture: ComponentFixture<ListPredioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPredioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPredioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
