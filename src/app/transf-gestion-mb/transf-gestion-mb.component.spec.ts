import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransfGestionMbComponent } from './transf-gestion-mb.component';

describe('TransfGestionMbComponent', () => {
  let component: TransfGestionMbComponent;
  let fixture: ComponentFixture<TransfGestionMbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransfGestionMbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransfGestionMbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
