import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-transf-gestion-mb',
  templateUrl: './transf-gestion-mb.component.html',
  styleUrls: ['./transf-gestion-mb.component.css']
})
export class TransfGestionMbComponent implements OnInit {

	RutaSinabip : string = sessionStorage.getItem("RutaSinabip") + "SGISBN/System/sinabip_modulos.php?idm=1&opt=35&menu=0"
  constructor() {
    let token = sessionStorage.getItem("token");  
    let ruta = sessionStorage.getItem("RutaSinabip")
    if(token === null){
      // window.location.replace(ruta+"SGISBN/System/sinabip.php");
      window.location.replace("./SGISBN/System/sinabip.php");
    }
   }

  ngOnInit() {
  }

}
