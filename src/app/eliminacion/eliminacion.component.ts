import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { SinabipmueblesService } from '../services/sinabipmuebles.service';
//import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgbModalConfig, NgbModal, ModalDismissReasons, NgbModule} from '@ng-bootstrap/ng-bootstrap';
//import {NgbDatepickerI18n, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
//import { ModalService } from '../_services'; 
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import swal from'sweetalert2';

@Component({
  selector: 'app-eliminacion',
  templateUrl: './eliminacion.component.html', 
  styleUrls: ['./eliminacion.component.css'],  
  animations: [routerTransition()],
  providers: [NgbModalConfig, NgbModal]
})


export class EliminacionComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
titularAlerta:string='';
dataEliminacion : any = [];
 Arrg_bien = [];
 posicion : number;
 mostrarVentanaEliminarSeleccionados : boolean = false;
 mostrarVentanaResumenEliminacion : boolean = false;
 dataDetalleEliminacion : any = [];
 estado_eliminacion : string = "";
 codigo_eliminacion : number = -1;
 n_motivos : string = "";
 Cod_Entidad : string = sessionStorage.getItem("Cod_Entidad");
 Cod_Usuario : string = sessionStorage.getItem("Cod_Usuario");
 RutaSinabip : string = sessionStorage.getItem("RutaSinabip");
 urlPDF;
 cod_entidad_recod;

 afuConfig = {
  uploadAPI: {
      url:this.SinabipMuebles.API_URL+"SubirAltaTxt/"+this.Cod_Entidad,
    },
    hideResetBtn: true,
    uploadBtnText:"Adjuntar Archivo",
    uploadMsgText: "",
    formatsAllowed: ".TXT,.txt"
};

dataAdjuntado: any = [];
estado_adjuntar: string = "";
estado_carga : string = "";
estado_carga_fecha : string = "";
estado_carga_total : string = "";
estado_validacion : string ="";
estado_validacion_fecha : string ="";
estado_validacion_total : string ="";
estado_finalizacion : string ="";
estado_finalizacion_fecha : string ="";
estado_finalizacion_total : string ="";

b_ventanaObservacionesValidacion : boolean = false;
modal;
dataObservacionValidacion : Array<any> = [];
cantidaderrores_columnas: number = 0;
cantidadNo_existe_codigos_pat: number = 0;
closeResult: string;

paramCarga = {
  id_entidad    : '',
  id_adjuntado  : '',
  nombreArchivo : '',
  usua_creacion : ''
  }

afuConfig2 = {
  uploadAPI: {
    url: this.SinabipMuebles.API_URL + "Adjuntareliminartxt/" + this.Cod_Entidad + "-" + this.Cod_Usuario,
  },
  hideResetBtn: true,
  uploadBtnText: "Adjuntar Archivo",
  uploadMsgText: "Archivo TXT Adjuntado",
  formatsAllowed: ".txt,.TXT"
};

heroForm : any = [];

flag_carga_masiva : number;

//Paginacion 
itemsPerPage: number = 20;
page: any = 1;
previousPage: any;
total : any = 0;

filtro = {
  COD_ENTIDAD : this.Cod_Entidad,
  CODIGO_PATRIMONIAL : '',
  DENOMINACION : '',
  NRO_DOCUMENTO_ADQUIS : '',
  FECHA_DOCUMENTO_ADQUIS : '',
  FECHA_DOCUMENTO_ADQUISHASTA : '',
  bienes : '',
  page : this.page, records : this.itemsPerPage
}

paramRecod = {
  cod_entidad  : this.Cod_Entidad,      
  bienes : '',
  oficio :'',
  resolucion : '',
  informe : '',
  motivos : '',
  fecha : ''
}

oficio_impr : string ="";
resolucion_impr : string ="";
informe_impr : string ="";
motivos_impr : string ="";

  constructor(config: NgbModalConfig,
    private formBuilder: FormBuilder,
    private SinabipMuebles : SinabipmueblesService, 
    private spinner: NgxSpinnerService,
    private modalService: NgbModal,
    private router: Router
    ) 

    {   
      let token = sessionStorage.getItem("token");  
      let ruta = sessionStorage.getItem("RutaSinabip")
      if(token === null){
        // window.location.replace(ruta+"SGISBN/System/sinabip.php");
        window.location.replace("./SGISBN/System/sinabip.php");
      }
      config.backdrop = 'static';
      config.keyboard = false;
      this.afuConfig.uploadAPI.url = this.SinabipMuebles.API_URL+"AdjuntarAltatxt/"+this.Cod_Entidad;
      this.afuConfig2.uploadAPI.url = this.SinabipMuebles.API_URL + "Adjuntareliminartxt/" + this.Cod_Entidad + "-" + this.Cod_Usuario;
    }

  ngOnInit(): void {

    this.registerForm = this.formBuilder.group({ 
      oficio: ['', Validators.required],
      resolucion: ['', Validators.required],
      informe: ['', Validators.required],
      motivos: ['', Validators.required],
      fecha: ['', [Validators.required]],
    });
    this.filtro.CODIGO_PATRIMONIAL = '';
    this.filtro.DENOMINACION = '';
    this.filtro.NRO_DOCUMENTO_ADQUIS = '',
    this.filtro.FECHA_DOCUMENTO_ADQUIS = '',
    this.filtro.bienes = '',
    this.cargarListadoEliminacion();
  }
  
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) { 
      swal({
        type: 'error',
        title: 'Datos Incompletos!!!',
        text: 'No se ha ingresado la información necesaria para continuar',
      })
      return;
    }
    this.ProcesarEliminacion();
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.cargarListadoEliminacion();
    }
  }

  ventanaEliminacionSeleccionados(){
    if (this.Arrg_bien.length > 0) {
      this.mostrarVentanaEliminarSeleccionados = true;
      this.mostrarVentanaResumenEliminacion = false;
      this.desabilitar_ventana();
    }else{
      swal({
        type: 'error',
        title: '¡ERROR!',
        text: 'No se ha seleccionado ningún bien para eliminar',
      })
      this.mostrarVentanaEliminarSeleccionados = false;
      this.mostrarVentanaResumenEliminacion = false;
    }
  }

BusquedaEliminados(){

  if (this.filtro.FECHA_DOCUMENTO_ADQUIS == '' || this.filtro.FECHA_DOCUMENTO_ADQUISHASTA == ''){ 
    swal({
      type: 'error',
      title: '¡ERROR!',
      text: 'No se ha especificado el rango de fecha para la búsqueda',
    })
  }else{
    this.cargarListadoEliminacion();
  }
}

  cargarListadoEliminacion(){

    this.filtro.COD_ENTIDAD = this.Cod_Entidad;
    this.spinner.show();
    this.SinabipMuebles.postListadoEliminacion(this.filtro).subscribe((data : any) =>{  
      this.spinner.hide();

      if (data.data.error == true){
        if (data.data.reco.hasOwnProperty('COD_ENTIDAD')) { 
          this.router.navigate(['error500']);
          return;
        }else if (data.data.reco.hasOwnProperty('NRO_DOCUMENTO_ADQUIS')) {
          swal({
            type: 'error',
            title: 'Validacion de Datos',
            text: data.data.reco.NRO_DOCUMENTO_ADQUIS[0],
            })
            return;
        }else if (data.data.reco.hasOwnProperty('FECHA_DOCUMENTO_ADQUIS')) {
          swal({
            type: 'error',
            title: 'Validacion de Datos',
            text: data.data.reco.FECHA_DOCUMENTO_ADQUIS[0],
            })
          return;
        }else if (data.data.reco.hasOwnProperty('FECHA_DOCUMENTO_ADQUISHASTA')) {
          swal({
            type: 'error',
            title: 'Validacion de Datos',
            text: data.data.reco.FECHA_DOCUMENTO_ADQUISHASTA[0],
            })
          return;
        }else if (data.data.reco.hasOwnProperty('page')) {
          this.router.navigate(['error500']);
          return;
        }else if (data.data.reco.hasOwnProperty('records')) {
          this.router.navigate(['error500']);
          return;
        }
      }

      this.dataEliminacion = data.data;
      console.log(this.filtro);
      this.total = (this.dataEliminacion.elim.length > 0 ) ? this.dataEliminacion.elim[0].TOTAL : 0;
    });

 
  }

  almacenarBienes(estado,denom){
    if(estado == true){
      this.Arrg_bien.push(denom);
    }else{
      this.posicion = this.Arrg_bien.indexOf(denom);
      this.Arrg_bien.splice(this.posicion,1);
    }
  }

  BorrarFiltro(){
    this.filtro.CODIGO_PATRIMONIAL = '';
    this.filtro.DENOMINACION = '';
    this.filtro.NRO_DOCUMENTO_ADQUIS = '',
    this.filtro.FECHA_DOCUMENTO_ADQUIS = ''
    this.filtro.FECHA_DOCUMENTO_ADQUISHASTA = ''
  }

  ProcesarEliminacion():void{
    this.paramRecod.cod_entidad = this.Cod_Entidad;
    this.paramRecod.oficio = this.registerForm.get('oficio').value;
    this.paramRecod.resolucion = this.registerForm.get('resolucion').value;
    this.paramRecod.informe = this.registerForm.get('informe').value;
    this.paramRecod.motivos = this.registerForm.get('motivos').value;
    this.paramRecod.fecha = this.registerForm.get('fecha').value;
    this.paramRecod.bienes = this.Arrg_bien.toString();
    console.log(this.paramRecod);
    this.spinner.show();
    this.SinabipMuebles.postProcesarEliminacion(this.paramRecod).subscribe((data : any) =>{
      this.spinner.hide();
      this.estado_eliminacion = data.data.Resultado.estado_eliminacion;
      this.codigo_eliminacion = data.data.Resultado.codigo_eliminacion;
      this.n_motivos = data.data.n_motivos.DESCRIPCION;
      this.dataDetalleEliminacion = data.data.ElimDetalle;
      if (this.estado_eliminacion == 'ELIMINACION_SATISFACTORIA'){
        swal({
          position: 'center', 
          type: 'success',
          title: 'Proceso de eliminación finalizado correctamente',
          showConfirmButton: false,
          timer: 3000 
        })
        this.mostrarVentanaEliminarSeleccionados = false;
        this.mostrarVentanaResumenEliminacion = true;
        this.Arrg_bien = [];
        this.registerForm.reset();
        this.cargarListadoEliminacion();
      }else{
        swal({
          type: 'error',
          title: '¡ERROR!',
          text: 'Ocurrió un error al procesar la eliminación',
        })
      }
    })
  }

  FinalizarEliminacion(){
    this.mostrarVentanaEliminarSeleccionados = false;
    this.mostrarVentanaResumenEliminacion = false;
    this.Arrg_bien = [];
    this.registerForm.reset();
    this.cargarListadoEliminacion();

    swal({
      position: 'center', 
      type: 'success',
      title: 'Eliminacion Finalizada',
      showConfirmButton: false,
      timer: 3000 
    })
  }

  ImprimirReporteEliminacion(){
    this.paramRecod.cod_entidad = this.Cod_Entidad;
    this.oficio_impr = this.paramRecod.oficio.replace('/', '-');
    this.resolucion_impr = this.paramRecod.resolucion.replace('/', '-');
    this.informe_impr = this.paramRecod.informe.replace('/', '-');
    this.motivos_impr = this.paramRecod.motivos.replace('/', '-');

    this.paramRecod.bienes = this.Arrg_bien.toString();
    this.spinner.show();
    window.open(this.SinabipMuebles.API_URL+"ImprimirResumenEliminacion/"+
        this.paramRecod.cod_entidad+"/"+
        this.codigo_eliminacion+"/"+
        this.oficio_impr+"/"+
        this.resolucion_impr+"/"+
        this.informe_impr+"/"+
        this.motivos_impr+"/"+
        this.paramRecod.fecha,'_blank');
    this.spinner.hide();
  }

  carga_masiva() {
    this.desabilitar_ventana();
    this.flag_carga_masiva = 1;
    this.mostrarVentanaEliminarSeleccionados = false;
    this.mostrarVentanaResumenEliminacion = false;
    this.estado_adjuntar  ='';
    this.estado_carga = '';
    this.estado_validacion = '';
    this.estado_finalizacion = '';

  }
  
	desabilitar_ventana() {
		this.flag_carga_masiva = 0;
  }
  
  
	Adjuntareliminaciontxt(event) {
		this.estado_adjuntar == '';
		this.estado_carga = '';
		this.estado_validacion = '';
		this.estado_finalizacion = '';
		this.estado_carga_total = '';
		this.estado_carga_fecha = '';
		this.estado_validacion_total = '';
		this.estado_validacion_fecha = '';
		this.estado_finalizacion_total = '';
    this.estado_finalizacion_fecha = '';

    this.afuConfig2.uploadAPI.url = this.SinabipMuebles.API_URL + "Adjuntareliminartxt/" + this.Cod_Entidad + "-" + this.Cod_Usuario;
		this.dataAdjuntado = JSON.parse(event.response);
    this.estado_adjuntar = this.dataAdjuntado.data.RESULTADO;
    console.log(this.estado_adjuntar);
  }
  
	CargarEliminacionTXT(){ 
    
		this.paramCarga.id_entidad = this.Cod_Entidad; 
		this.paramCarga.id_adjuntado = this.dataAdjuntado.data.ID_ADJUNTADO;  
		this.paramCarga.nombreArchivo = this.dataAdjuntado.data.NOMBRE_ARCHIVO;
		this.paramCarga.usua_creacion = this.Cod_Usuario; 
		console.log(this.paramCarga);
	  
		this.spinner.show();
		this.SinabipMuebles.postCargareliminartxt(this.paramCarga).subscribe((data : any) =>{  
    this.spinner.hide();
    
    if (data.data.error == true){
      if (data.data.reco.hasOwnProperty('id_entidad')) { 
          this.router.navigate(['error500']);
          return;
      }else if (data.data.reco.hasOwnProperty('id_adjuntado')) {
          this.router.navigate(['error500']);
          return;
        }else if (data.data.reco.hasOwnProperty('usua_creacion')) {
          this.router.navigate(['error500']);
          return;
        }
      }

		this.estado_carga = data.data.ESTADO_CARGA;
		this.estado_carga_fecha = data.data.CARGADO_FECHA;
		this.estado_carga_total = data.data.CARGADO_TOTAL;
	  
		//this.dataGuardarBien = data.data; 
	  
		swal({
		  position: 'center',
		  type: 'success',
		  title: 'Carga Finalizada',
		  showConfirmButton: false,
		  timer: 2000
		})
	  
		});
    }
    


	  ValidarEliminacionTXT(content)
	  {
		this.b_ventanaObservacionesValidacion = false;
		this.paramCarga.id_entidad = this.Cod_Entidad; 
		this.paramCarga.id_adjuntado = this.dataAdjuntado.data.ID_ADJUNTADO;  
		this.paramCarga.usua_creacion = this.Cod_Usuario;
		  
		this.spinner.show();
		this.SinabipMuebles.postValidarEliminarTXT(this.paramCarga).subscribe((data : any) =>{ 
		  this.spinner.hide();
      
      if (data.data.error == true){
        if (data.data.reco.hasOwnProperty('id_entidad')) { 
            this.router.navigate(['error500']);
            return;
        }else if (data.data.reco.hasOwnProperty('id_adjuntado')) {
            this.router.navigate(['error500']);
            return;
          }else if (data.data.reco.hasOwnProperty('usua_creacion')) {
            this.router.navigate(['error500']);
            return;
          }
        }

		  this.estado_validacion = data.data.ESTADO_VALIDACION;
		  this.estado_validacion_fecha = data.data.VALIDADO_FECHA;
		  this.estado_validacion_total = data.data.VALIDADO_TOTAL;
	  
		
	  
		if (this.estado_validacion == "VALIDACION_SATISFACTORIA" ){
	  
		  swal({
			position: 'center', 
			type: 'success',
			title: 'Validación Finalizada',
			showConfirmButton: false,
			timer: 2000 
		  })
		  this.modalService.dismissAll();
		  
		}else{
		  
		  this.spinner.show();
		  this.modal=this.SinabipMuebles.postListadoErroresCargaMasivaEliminar(this.paramCarga).subscribe((data : any) =>{   
			this.spinner.hide();
			// console.log(data.data);
			this.dataObservacionValidacion = [];
			this.dataObservacionValidacion = data.data;
			// console.log(this.dataObservacionValidacion["errores_columnas"].length);
			// console.log(this.dataObservacionValidacion["no_existe_codigos"].length);
			this.cantidaderrores_columnas = this.dataObservacionValidacion["errores_columnas"].length
			this.cantidadNo_existe_codigos_pat = this.dataObservacionValidacion["no_existe_codigos"].length
		  });
		  this.b_ventanaObservacionesValidacion = true;
		  this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title',
		  keyboard: false,
		  size: 'lg',
		  backdrop: 'static'}).result.then((result) => { 
			this.closeResult = `Closed with: ${result}`;
		  }, (reason) => {
			this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
		  });
		}
		});
	  }
	  
	  
	  FinalizarEliminacionTXT()
	  {
		this.paramCarga.id_entidad = this.Cod_Entidad; 
		this.paramCarga.id_adjuntado = this.dataAdjuntado.data.ID_ADJUNTADO;  
		this.paramCarga.usua_creacion = this.Cod_Usuario;
		console.log(this.paramCarga);
	  
		this.spinner.show();
		this.SinabipMuebles.postFinalizarEliminacionTXT(this.paramCarga).subscribe((data : any) =>{ 
    this.spinner.hide();
    
    if (data.data.error == true){
      if (data.data.reco.hasOwnProperty('id_entidad')) { 
          this.router.navigate(['error500']);
          return;
      }else if (data.data.reco.hasOwnProperty('id_adjuntado')) {
          this.router.navigate(['error500']);
          return;
        }else if (data.data.reco.hasOwnProperty('usua_creacion')) {
          this.router.navigate(['error500']);
          return;
        }
      }

		console.log(data.data);
		this.estado_finalizacion = data.data.ESTADO_FINALIZACION;
		this.estado_finalizacion_fecha = data.data.FINALIZADO_FECHA;
		this.estado_finalizacion_total = data.data.FINALIZADO_TOTAL;
	  
		swal({
		  position: 'center',
		  type: 'success',
		  title: 'Finalizada',
		  showConfirmButton: false,
		  timer: 2000
    })
    
		this.estado_carga =  "";
		this.estado_validacion = "";
		this.estado_finalizacion = "";
		this.estado_adjuntar = "";

		this.desabilitar_ventana();
	  
		});
	  
	  }
	  



}