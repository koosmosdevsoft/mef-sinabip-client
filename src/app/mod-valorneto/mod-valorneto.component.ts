import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { SinabipmueblesService } from '../services/sinabipmuebles.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgbModalConfig, NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';

@Component({
	selector: 'app-mod-valorneto',
	templateUrl: './mod-valorneto.component.html',
	styleUrls: ['./mod-valorneto.component.css'],
	animations: [routerTransition()],
	providers: [NgbModalConfig, NgbModal]
})
export class ModValornetoComponent implements OnInit {
	ListaModValorNeto_Arg: Array<any> = [];
	ActualizarDepreciacion_Arg: Array<any> = [];
	ValidaFechaDepr_Arg: Array<any> = [];
	flag_carga_masiva : number;

	
	id: string = sessionStorage.getItem("Cod_Entidad");
	idusuario: string = sessionStorage.getItem("Cod_Usuario");
	RutaSinabip : string = sessionStorage.getItem("RutaSinabip");
	closeResult: string;
	valida_fecha: string;
	total_long: number;
	dataAdjuntado: any = [];
	estado_adjuntar: string = "";
	grupo:number = 1;

	Cod_Entidad : string = this.id;
	Cod_Usuario : string = this.idusuario;

	estado_carga : string = "";
	estado_carga_fecha : string = "";
	estado_carga_total : string = "";
	estado_validacion : string ="";
	estado_validacion_fecha : string ="";
	estado_validacion_total : string ="";
	estado_finalizacion : string ="";
	estado_finalizacion_fecha : string ="";
	estado_finalizacion_total : string ="";
	flag_ImportarFormatoExcel : number;

	b_ventanaObservacionesValidacion : boolean = false;
	modal;
	dataObservacionValidacion : Array<any> = [];
	dataDescargas : Array<any> = [];
	dataDescargasXGrupo : Array<any> = [];

	cantidadduplicado_actos: number = 0;
	cantidadduplicado_bienes: number = 0;
	cantidaderrores_columnas: number = 0;
	cantidadNo_existe_codigos_pat: number = 0;
	cantidadNo_disponible_bienes: number = 0;


	//Paginacion 
	itemsPerPage: number = 20;
	page: any = 1;
	previousPage: any;
	total: any = 0;


	afuConfig = {
		uploadAPI: {
			url: this.Sinabip.API_URL + "AdjuntarDepreciaciontxt/" + this.id + "-" + this.idusuario,
		},
		hideResetBtn: true,
		uploadBtnText: "Adjuntar Archivo",
		uploadMsgText: "Archivo TXT Adjuntado",
		formatsAllowed: ".txt,.TXT"
	};

	filtro = {
		id: this.id, cod_patrimonial: '', denominacion: '', page: this.page, records: this.itemsPerPage
	}

	filtro2 = {
		id: this.id, valordeprecacumuladosi: 0.00, porcentajedeprecsi: 0, valornetosi: 0.00, cod_ue_bien_patri: '', valordepreciacionEjerciciosi: 0, cuentaContable: ''
	}

	filtro3 = {
		id: this.id, cod_ue_bien_patri: ''
	}  


	paramCarga = {
		id_entidad    : '',
		id_adjuntado  : '',
		nombreArchivo : '',
		usua_creacion : ''
	  }

	  paramDescarga = {
		  id_entidad : this.Cod_Entidad,
		  nro_grupo: 1
	  }

	constructor(config: NgbModalConfig, private Sinabip: SinabipmueblesService,
		private route: ActivatedRoute,
		private router: Router,
		private spinner: NgxSpinnerService,
		private modalService: NgbModal,
		private SinabipMuebles : SinabipmueblesService, 
	) {

		let token = sessionStorage.getItem("token");  
		let ruta = sessionStorage.getItem("RutaSinabip")
		if(token === null){
			// window.location.replace(ruta+"SGISBN/System/sinabip.php");
			window.location.replace("./SGISBN/System/sinabip.php");
		}
		this.ListadoModificacionValorNeto();
		config.backdrop = 'static';
		config.keyboard = false;
		this.afuConfig.uploadAPI.url = this.Sinabip.API_URL + "AdjuntarDepreciaciontxt/" + this.id + "-" + this.idusuario;
	}

	ngOnInit() {
	}



	private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
		  return 'by pressing ESC';
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
		  return 'by clicking on a backdrop';
		} else {
		  return  `with: ${reason}`;
		}
	}


	AdjuntarDepreciaciontxt(event) {
		this.estado_adjuntar == '';
		this.estado_carga = '';
		this.estado_validacion = '';
		this.estado_finalizacion = '';
		this.estado_carga_total = '';
		this.estado_carga_fecha = '';
		this.estado_validacion_total = '';
		this.estado_validacion_fecha = '';
		this.estado_finalizacion_total = '';
		this.estado_finalizacion_fecha = '';

		this.afuConfig.uploadAPI.url = this.Sinabip.API_URL + "AdjuntarDepreciaciontxt/" + this.id + "-" + this.idusuario;
		this.dataAdjuntado = JSON.parse(event.response);
		this.estado_adjuntar = this.dataAdjuntado.data.RESULTADO;
	}


	CargarDepreciacionTXT(){ 
    
		this.paramCarga.id_entidad = this.Cod_Entidad; 
		this.paramCarga.id_adjuntado = this.dataAdjuntado.data.ID_ADJUNTADO;  
		this.paramCarga.nombreArchivo = this.dataAdjuntado.data.NOMBRE_ARCHIVO;
		this.paramCarga.usua_creacion = this.Cod_Usuario; 
		//console.log(this.paramCarga);
	  
		this.spinner.show();
		this.SinabipMuebles.postCargarDepreciacionTXT(this.paramCarga).subscribe((data : any) =>{  
		this.spinner.hide();

		if (data.data.error == true){
			if (data.data.reco.hasOwnProperty('id_entidad')) { 
				this.router.navigate(['error500']);
				return;
			}else if (data.data.reco.hasOwnProperty('id_adjuntado')) {
				this.router.navigate(['error500']);
				return;
			}
			
		}

		this.estado_carga = data.data.ESTADO_CARGA;
		this.estado_carga_fecha = data.data.CARGADO_FECHA;
		this.estado_carga_total = data.data.CARGADO_TOTAL;
	  
		//this.dataGuardarBien = data.data; 
	  
		swal({
		  position: 'center',
		  type: 'success',
		  title: 'Carga Finalizada',
		  showConfirmButton: false,
		  timer: 2000
		})
	  
		});
	  }
	  
	  ValidarDepreciacionTXT(content)
	  {
		this.b_ventanaObservacionesValidacion = false;
		this.paramCarga.id_entidad = this.Cod_Entidad; 
		this.paramCarga.id_adjuntado = this.dataAdjuntado.data.ID_ADJUNTADO;  
		this.paramCarga.usua_creacion = this.Cod_Usuario;
		console.log(this.paramCarga);
		
	  
		this.spinner.show();
		this.SinabipMuebles.postValidarDepreciacionTXT(this.paramCarga).subscribe((data : any) =>{ 
		  this.spinner.hide();

		if (data.data.error == true){
			if (data.data.reco.hasOwnProperty('id_entidad')) { 
				this.router.navigate(['error500']);
				return;
			}else if (data.data.reco.hasOwnProperty('id_adjuntado')) {
				this.router.navigate(['error500']);
				return;
			}
			
		}

		 // console.log(data.data);
		  this.estado_validacion = data.data.ESTADO_VALIDACION;
		  this.estado_validacion_fecha = data.data.VALIDADO_FECHA;
		  this.estado_validacion_total = data.data.VALIDADO_TOTAL;
	  
		
	  
		if (this.estado_validacion == "VALIDACION_SATISFACTORIA" ){
	  
		  swal({
			position: 'center', 
			type: 'success',
			title: 'Validación Finalizada',
			showConfirmButton: false,
			timer: 2000 
		  })
		  this.modalService.dismissAll();
		  
		}else{
		  
		  this.spinner.show();
		  this.modal=this.SinabipMuebles.postListadoErroresCargaMasivaDepreciacion(this.paramCarga).subscribe((data : any) =>{   
			this.spinner.hide();
			console.log(data.data);
			this.dataObservacionValidacion = [];
			this.dataObservacionValidacion = data.data;
			
			console.log(this.dataObservacionValidacion["errores_columnas"].length);
			console.log(this.dataObservacionValidacion["no_existe_codigos"].length);
	  
			this.cantidaderrores_columnas = this.dataObservacionValidacion["errores_columnas"].length
			this.cantidadNo_existe_codigos_pat = this.dataObservacionValidacion["no_existe_codigos"].length
	  
		  });
	  
		  this.b_ventanaObservacionesValidacion = true;
		  this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title',
		  keyboard: false,
		  size: 'lg',
		  backdrop: 'static'}).result.then((result) => { 
			this.closeResult = `Closed with: ${result}`;
		  }, (reason) => {
			this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
		  });
	  
		  
	  
		}
		
		});
	  
	  }
	  
	  FinalizarDepreciacionTXT()
	  {
		//console.log(this.dataAdjuntado.data.ID_ADJUNTADO);
		this.paramCarga.id_entidad = this.Cod_Entidad; 
		this.paramCarga.id_adjuntado = this.dataAdjuntado.data.ID_ADJUNTADO;  
		this.paramCarga.usua_creacion = this.Cod_Usuario;
		console.log(this.paramCarga);
	  
		this.spinner.show();
		this.SinabipMuebles.postFinalizarDepreciacionTXT(this.paramCarga).subscribe((data : any) =>{ 
		this.spinner.hide();

		if (data.data.error == true){
			if (data.data.reco.hasOwnProperty('id_entidad')) { 
				this.router.navigate(['error500']);
				return;
			}else if (data.data.reco.hasOwnProperty('id_adjuntado')) {
				this.router.navigate(['error500']);
				return;
			}
			
		}

		//console.log(data.data);
		this.estado_finalizacion = data.data.ESTADO_FINALIZACION;
		this.estado_finalizacion_fecha = data.data.FINALIZADO_FECHA;
		this.estado_finalizacion_total = data.data.FINALIZADO_TOTAL;
	  
		swal({
		  position: 'center',
		  type: 'success',
		  title: 'Finalizada',
		  showConfirmButton: false,
		  timer: 2000
		})
	  
		//this.cargarListadoActosAdministracion(); 
		// this.mostrarVentanaTipoRegistro = false;
		// this.mostrarVentanaRegistroIndividual = false; 
		// this.mostrarVentanaRegistroMasivo = false;
		//this.flag_carga_masiva ==0;

		this.estado_carga =  "";
		this.estado_validacion = "";
		this.estado_finalizacion = "";
		this.estado_adjuntar = "";

		this.desabilitar_ventana();
	  
		});
	  
	  }
	  







	ListadoModificacionValorNeto() {
		this.spinner.show();
		this.Sinabip.PostListadoModificacionValorNeto(this.filtro).subscribe((data: any) => {
			this.spinner.hide();
			if (data.data.error == true){
				if (data.data.reco.hasOwnProperty('id')) { 
					this.router.navigate(['error500']);
					return;
				}else if (data.data.reco.hasOwnProperty('page')) {
					  this.router.navigate(['error500']);
					  return;
				}else if (data.data.reco.hasOwnProperty('records')) {
					this.router.navigate(['error500']);
					return;
				}
			}

			this.ListaModValorNeto_Arg = data.data;
			console.log(this.ListaModValorNeto_Arg);
			this.total = (this.ListaModValorNeto_Arg.length > 0) ? this.ListaModValorNeto_Arg[0].Total : 0;
		});
	}

	loadPage(page: number) {
		if (page !== this.previousPage) {
			this.previousPage = page;
			this.ListadoModificacionValorNeto();
		}
	}

	resetearpag() {
		this.filtro.page = 1;
		this.ListadoModificacionValorNeto();
	}

	resetearlimpiarpag() {
		this.page = 1;
		this.Limpiar_ListadoModificacionValorNeto();
	}

	Limpiar_ListadoModificacionValorNeto() {
		this.filtro = {
			id: this.id, cod_patrimonial: '', denominacion: '', page: this.page, records: this.itemsPerPage
		}
		this.spinner.show();
		this.Sinabip.PostListadoModificacionValorNeto(this.filtro).subscribe((data: any) => {
			this.spinner.hide();
			this.ListaModValorNeto_Arg = data.data;
			this.total = (this.ListaModValorNeto_Arg.length > 0) ? this.ListaModValorNeto_Arg[0].Total : 0;
		});
	}

	open(content, PORC_DEPREC, VALOR_DEPREC_ACUM_SI, VALOR_NETO_SI, COD_UE_BIEN_PATRI, VALOR_DEPREC_EJERCICIO, NRO_CTA_CONTABLE) {
		
		this.desabilitar_ventana();
		this.flag_ImportarFormatoExcel = 0;
		this.filtro2 = {
			id: this.id, valordeprecacumuladosi: VALOR_DEPREC_ACUM_SI, porcentajedeprecsi: PORC_DEPREC, valornetosi: VALOR_NETO_SI, cod_ue_bien_patri: COD_UE_BIEN_PATRI, valordepreciacionEjerciciosi: VALOR_DEPREC_EJERCICIO, cuentaContable: NRO_CTA_CONTABLE
		}
		console.log(this.filtro2);

		this.filtro3 = {
			id: this.id, cod_ue_bien_patri: COD_UE_BIEN_PATRI
		}

		this.spinner.show();
		this.Sinabip.PostValidaFechaDepreciacion(this.filtro3).subscribe((data: any) => {
			this.spinner.hide();

			if (data.data.error == true){
				if (data.data.reco.hasOwnProperty('id')) { 
					this.router.navigate(['error500']);
					  return;
				}else if (data.data.reco.hasOwnProperty('cod_ue_bien_patri')) {
					  this.router.navigate(['error500']);
					  return;
				}
				
			}

			this.ValidaFechaDepr_Arg = data.data;
			this.valida_fecha = this.ValidaFechaDepr_Arg[0].RESULTADO;
		});

		this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
			this.closeResult = `Closed with: ${result}`;
		}, (reason) => {
		});
	}

	carga_masiva() {
		this.flag_carga_masiva = 1;
		
	}

	desabilitar_ventana() {
		this.flag_carga_masiva = 0;
	}


	keyPress(event: any) {
		const pattern = /[0-9.]/;
		const inputChar = String.fromCharCode(event.charCode);

		if (!pattern.test(inputChar)) {
			// invalid character, prevent input
			event.preventDefault();
		}
	}

	ActualizarDepreciacion(depre_ejercicio, depre_acumulada, valorneto, porcentaje, cuentaContable) {
		//console.log(porcentaje);
		// console.log(this.filtro2);
		if (porcentaje >= 0 && porcentaje <= 100) {
			if (this.valida_fecha == '1') {
				swal({
					title: 'Confirmar Actualización de Valor?',
					text: "",
					type: 'question',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Acepta, Actualización!'
				}).then((result) => {
					if (result.value) {
						this.spinner.show();
						this.Sinabip.PostActualizarDepreciacion(this.filtro2).subscribe((data: any) => {
							this.spinner.hide();

							if (data.data.error == true){
								if (data.data.reco.hasOwnProperty('id')) { 
									  this.router.navigate(['error500']);
									  return;
								}else if (data.data.reco.hasOwnProperty('cod_ue_bien_patri')) {
									  this.router.navigate(['error500']);
									  return;
								}else if (data.data.reco.hasOwnProperty('valordepreciacionEjerciciosi')) {
									swal({
										type: 'error',
										title: 'Validacion de Datos',
										text: data.data.reco.valordepreciacionEjerciciosi[0],
									  })
									return;
								}else if (data.data.reco.hasOwnProperty('valordeprecacumuladosi')) {
									swal({
										type: 'error',
										title: 'Validacion de Datos',
										text: data.data.reco.valordeprecacumuladosi[0],
									  })
									return;
								}else if (data.data.reco.hasOwnProperty('valornetosi')) {
									swal({
										type: 'error',
										title: 'Validacion de Datos',
										text: data.data.reco.valornetosi[0],
									  })
									return;
								}else if (data.data.reco.hasOwnProperty('porcentajedeprecsi')) {
									swal({
										type: 'error',
										title: 'Validacion de Datos',
										text: data.data.reco.porcentajedeprecsi[0],
									  })
									return;
								}
							}

							this.ActualizarDepreciacion_Arg = data.data;
							setTimeout(() => {
								this.ListadoModificacionValorNeto();
								this.modalService.dismissAll();
							}, 500);
						});
					}
				})
			} else {
				// if (acumulado < 1) {
				// 	swal('Aviso falta Ingresar Valor Acumulado');
				// } else if (valorneto < 1) {
				// 	swal('Aviso falta Ingresar Valor Neto');
				// } else {
					swal({
						title: 'Confirmar Actualización de Valor?',
						text: "",
						type: 'question',
						showCancelButton: true,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'Acepta, Actualización!'
					}).then((result) => {
						if (result.value) {
							this.spinner.show();
							this.Sinabip.PostActualizarDepreciacion(this.filtro2).subscribe((data: any) => {
								this.spinner.hide();
								this.ActualizarDepreciacion_Arg = data.data;
								setTimeout(() => {
									this.ListadoModificacionValorNeto();
									this.modalService.dismissAll();
								}, 500);
							});
						}
					})
				// }
			}
		} else {
			swal('Aviso: Valor Incorrecto')
		} 
	}
	
	ImportarFormatoExcel(content){
		
		/* parametros */
		this.paramDescarga.id_entidad = this.Cod_Entidad;
		//this.paramDescarga.nro_grupo = nro_grupo;
		console.log('entidad: ' +  this.paramDescarga.id_entidad);
		

		this.spinner.show();
		this.modal=this.SinabipMuebles.postImportarFormatoExcel(this.paramDescarga).subscribe((data : any) =>{  
		this.spinner.hide();

		let reg = data.data.GrupoDescargas[0].NRO_GRUPO;
		this.dataDescargas = [];
		for (let index = 0; index < reg; index++) {
			this.dataDescargas.push({
				index : index
			});
		}
		let acto = data.data;
		

		// this.cantidadduplicado_bienes = 0;
		// this.cantidadduplicado_actos = 0;  
		// this.cantidaderrores_columnas = 0;
		// this.cantidadNo_existe_codigos_pat = 0;
		// this.cantidadNo_disponible_bienes = 0;

		});



		this.flag_ImportarFormatoExcel = 1;

		//this.eliminacionDetalleBajas = true;
		this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', 
		keyboard: false,
		size: 'lg',
		backdrop: 'static'}).result.then((result) => { 
			this.closeResult = `Closed with: ${result}`;
		}, (reason) => {
			this.closeResult = `Dismissed ${this.getDismissReason(reason)}`; 
		});

	}


	ImportarFormatoExcelxGrupo(nro_grupo){

		/* parametros */
		this.paramDescarga.id_entidad = this.Cod_Entidad;
		this.paramDescarga.nro_grupo = nro_grupo;
		this.grupo = nro_grupo;
		console.log('entidad: ' +  this.paramDescarga.id_entidad);
		console.log('grupo: ' +  this.paramDescarga.nro_grupo);
		

		this.spinner.show();
		this.modal=this.SinabipMuebles.postImportarFormatoExcelxGrupo(this.paramDescarga).subscribe((data : any) =>{
		this.spinner.hide();

		this.dataDescargasXGrupo = data.data;
		console.log(data.data);

		});

	}

}

