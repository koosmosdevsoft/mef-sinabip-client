import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModValornetoComponent } from './mod-valorneto.component';

describe('ModValornetoComponent', () => {
  let component: ModValornetoComponent;
  let fixture: ComponentFixture<ModValornetoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModValornetoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModValornetoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
