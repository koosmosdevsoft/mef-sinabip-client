import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrasladoBienesComponent } from './traslado-bienes.component';

describe('TrasladoBienesComponent', () => {
  let component: TrasladoBienesComponent;
  let fixture: ComponentFixture<TrasladoBienesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrasladoBienesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrasladoBienesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
