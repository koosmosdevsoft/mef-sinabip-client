import { Component, OnInit, Directive } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { SinabipmueblesService } from '../../services/sinabipmuebles.service';
import {NgbModalConfig, NgbModal, ModalDismissReasons, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators, NgControl } from '@angular/forms';
import swal from'sweetalert2';
import { DatePipe } from '@angular/common'; 

@Component({
  selector: 'app-forma-masivaReingreso',
  templateUrl: './forma-masivaReingreso.component.html',
  styleUrls: ['./forma-masivaReingreso.component.css'],
  animations: [routerTransition()],
  providers: [NgbModalConfig, NgbModal, DatePipe]
})
export class FormaMasivaReingresoComponent implements OnInit {

  /* Declaracion de variables */

  Cod_Entidad : string = sessionStorage.getItem("Cod_Entidad");
  Cod_Usuario : string = sessionStorage.getItem("Cod_Usuario");
  RutaSinabip : string = sessionStorage.getItem("RutaSinabip");

  afuConfig = {
    uploadAPI: {
        url:this.SinabipMuebles.API_URL+"SubirAltaTxt/"+this.Cod_Entidad,
      },
      hideResetBtn: true,
      uploadBtnText:"Adjuntar Archivo",
      uploadMsgText: "",
      formatsAllowed: ".TXT,.txt"
  };

  estado_adjuntar : string = "";
  estado_carga : string = "";
  estado_carga_fecha : string = "";
  estado_carga_total : string = "";
  estado_validacion : string ="";
  estado_validacion_fecha : string ="";
  estado_validacion_total : string ="";
  estado_finalizacion : string ="";
  estado_finalizacion_fecha : string ="";
  estado_finalizacion_total : string ="";

  dataAdjuntado : any = [];

  paramCarga = {
    id_entidad    : '',
    id_adjuntado  : '',
    nombreArchivo : '',
    usua_creacion : ''
  }

  b_ventanaObservacionesValidacion : boolean = false;
  modal;

  dataObservacionValidacion : Array<any> = [];
  dataresultado : Array<any> = [];
  cantidadduplicado_actos: number = 0;
  cantidadduplicado_bienes: number = 0;
  cantidaderrores_columnas: number = 0;
  cantidadNo_existe_codigos_pat: number = 0;
  cantidadNo_disponible_bienes: number = 0;
  modificando_registro: string = '';

  closeResult: string;

  

  /* Fin de Declaracion de variables */

  constructor
  (
    public datepipe: DatePipe,
    private formBuilder: FormBuilder,
    private SinabipMuebles : SinabipmueblesService, 
    private spinner: NgxSpinnerService,
    private modalService: NgbModal,
    private router: Router
  ) 
  {
    this.afuConfig.uploadAPI.url = this.SinabipMuebles.API_URL+"AdjuntarReingresotxt/"+this.Cod_Entidad;
  }

  ngOnInit() {
  }


  /* Declaracion de Funciones */

  AdjuntarReingresotxt(event){
    
    this.estado_adjuntar == '';
    this.estado_carga = '';
    this.estado_validacion = '';
    this.estado_finalizacion = '';
    this.estado_carga_total = '';
    this.estado_carga_fecha = '';
    this.estado_validacion_total = '';
    this.estado_validacion_fecha = '';
    this.estado_finalizacion_total = '';
    this.estado_finalizacion_fecha = '';

    this.afuConfig.uploadAPI.url = this.SinabipMuebles.API_URL+"AdjuntarReingresotxt/"+this.Cod_Entidad;
    this.dataAdjuntado = JSON.parse(event.response); 
    this.estado_adjuntar = this.dataAdjuntado.data.RESULTADO;
 
  }


  CargarEntregaTXT(){ 
    this.paramCarga.id_entidad = this.Cod_Entidad; 
    this.paramCarga.id_adjuntado = this.dataAdjuntado.data.ID_ADJUNTADO;  
    this.paramCarga.nombreArchivo = this.dataAdjuntado.data.NOMBRE_ARCHIVO;
    this.paramCarga.usua_creacion = this.Cod_Usuario; 

    console.log(this.paramCarga);

    this.spinner.show();
    this.SinabipMuebles.postCargarEntregaTXT(this.paramCarga).subscribe((data : any) =>{ 
    this.spinner.hide();

    if (data.data.error == true){
      if (data.data.reco.hasOwnProperty('id_entidad')) { 
          this.router.navigate(['error500']);
          return;
      }else if (data.data.reco.hasOwnProperty('id_adjuntado')) {
          this.router.navigate(['error500']);
          return;
        }else if (data.data.reco.hasOwnProperty('usua_creacion')) {
          this.router.navigate(['error500']);
          return;
        }
      }

    this.estado_carga = data.data.ESTADO_CARGA;
    this.estado_carga_fecha = data.data.CARGADO_FECHA;
    this.estado_carga_total = data.data.CARGADO_TOTAL;

    swal({
      position: 'center',
      type: 'success',
      title: 'Carga Finalizada',
      showConfirmButton: false,
      timer: 2000
    })

    });
  }


  ValidarEntregaTXT(content){
    this.b_ventanaObservacionesValidacion = false;
    this.paramCarga.id_entidad = this.Cod_Entidad; 
    this.paramCarga.id_adjuntado = this.dataAdjuntado.data.ID_ADJUNTADO;  
    this.paramCarga.usua_creacion = this.Cod_Usuario;

    this.spinner.show();
    this.SinabipMuebles.postValidarEntregaTXT(this.paramCarga).subscribe((data : any) =>{ 
      this.spinner.hide();

      if (data.data.error == true){
        if (data.data.reco.hasOwnProperty('id_entidad')) { 
            this.router.navigate(['error500']);
            return;
        }else if (data.data.reco.hasOwnProperty('id_adjuntado')) {
            this.router.navigate(['error500']);
            return;
          }else if (data.data.reco.hasOwnProperty('usua_creacion')) {
            this.router.navigate(['error500']);
            return;
          }
        }

      this.estado_validacion = data.data.ESTADO_VALIDACION;
      this.estado_validacion_fecha = data.data.VALIDADO_FECHA;
      this.estado_validacion_total = data.data.VALIDADO_TOTAL;

      console.log(this.estado_validacion);
      
      if (this.estado_validacion == "VALIDACION_SATISFACTORIA" ){

        swal({
          position: 'center', 
          type: 'success',
          title: 'Validación Finalizada',
          showConfirmButton: false,
          timer: 2000 
        })
        this.modalService.dismissAll();
  
      }else{
        
        this.spinner.show();
        this.modal=this.SinabipMuebles.postListadoErroresCargaMasivarEntrega(this.paramCarga).subscribe((data : any) =>{   
          this.spinner.hide();
          this.dataObservacionValidacion = [];
          this.dataObservacionValidacion = data.data;


          this.cantidadduplicado_actos = this.dataObservacionValidacion["duplicado_actos"].length 
          this.cantidaderrores_columnas = this.dataObservacionValidacion["errores_columnas"].length
          this.cantidadNo_existe_codigos_pat = this.dataObservacionValidacion["no_existe_codigos"].length
          this.cantidadNo_disponible_bienes = this.dataObservacionValidacion["no_disponibles"].length


          this.b_ventanaObservacionesValidacion = true;
        });
  
        // setTimeout(() => {
        //   this.b_ventanaObservacionesValidacion = true;   
        // }, 1000);
        
        this.b_ventanaObservacionesValidacion = true;
        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title',
        keyboard: false,
        size: 'lg',
        backdrop: 'static'}).result.then((result) => { 
          this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
  
  
  
      }
    });
    
    

  }


  FinalizarEntregaTXT(){

    this.paramCarga.id_entidad = this.Cod_Entidad; 
    this.paramCarga.id_adjuntado = this.dataAdjuntado.data.ID_ADJUNTADO;  
    this.paramCarga.usua_creacion = this.Cod_Usuario;

    this.spinner.show();
    this.SinabipMuebles.postFinalizarEntregaTXT(this.paramCarga).subscribe((data : any) =>{ 
    this.spinner.hide();

    if (data.data.error == true){
      if (data.data.reco.hasOwnProperty('id_entidad')) { 
          this.router.navigate(['error500']);
          return;
      }else if (data.data.reco.hasOwnProperty('id_adjuntado')) {
          this.router.navigate(['error500']);
          return;
        }else if (data.data.reco.hasOwnProperty('usua_creacion')) {
          this.router.navigate(['error500']);
          return;
        }
      }

    this.estado_finalizacion = data.data.ESTADO_FINALIZACION;
    this.estado_finalizacion_fecha = data.data.FINALIZADO_FECHA;
    this.estado_finalizacion_total = data.data.FINALIZADO_TOTAL;

    swal({
      position: 'center',
      type: 'success',
      title: 'Finalizada'
      
    })

    

    //this.cargarListadoActosAdquisicion(); 
    // this.mostrarVentanaTipoRegistro = false;
    // this.mostrarVentanaRegistroIndividual = false; 
    // this.mostrarVentanaRegistroMasivo = false;

    });

  }


  

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  

  /* Fin de Fuciones */

}
