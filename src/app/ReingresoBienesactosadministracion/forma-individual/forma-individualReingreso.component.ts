import { Component, OnInit, HostListener, Input, Output, EventEmitter} from '@angular/core';
import { routerTransition } from '../../router.animations';
import { SinabipmueblesService } from '../../services/sinabipmuebles.service';
import {NgbModalConfig, NgbModal, ModalDismissReasons, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators, NgControl } from '@angular/forms';
import swal from'sweetalert2';
import { DatePipe } from '@angular/common'; 

@Component({
  selector: 'app-forma-individualReingreso',
  templateUrl: './forma-individualReingreso.component.html',
  styleUrls: ['./forma-individualReingreso.component.css'],
  animations: [routerTransition()],
  providers: [NgbModalConfig, NgbModal, DatePipe]
})

export class FormaIndividualReingresoComponent implements OnInit {
  @Input("registro_consultado_desdePadre") registro_Consultado = null;
  @Output("escuchaGuardadodeHijo") header1 = new EventEmitter();
  @Output("registro_Consultado") header2 = new EventEmitter();
  @Input("tipo") tipoDoc = null;

  @Output("_eventoGuardar1") eventoGuardar = new EventEmitter();
  @Output("_eventoEliminarItem") eventoEliminar = new EventEmitter();
  @Input("id_acto") id_Acto = "";

  evento = null;


  //Paginacion 
  itemsPerPage: number = 20;
  page: any = 1;
  previousPage: any;
  total : any = 0; 

  /* Declaracion de variables */

  Cod_Entidad : string = sessionStorage.getItem("Cod_Entidad");
  Cod_Usuario : string = sessionStorage.getItem("Cod_Usuario");
  registerForm: FormGroup;
  submitted = false;
  seleccionRows : string = null;

  parametros = { 
    cod_entidad   : this.Cod_Entidad,
    nro_grupo     : "-1",
    nro_clase     : "",
    denom_bien    : "",
    nro_bien      : "",
    cod_patrimonial : "",
    denom_patrimonial : "",
    nro_resoluc : "",
    page : this.page, records : this.itemsPerPage
  } 

  paramAgregar = {
    id_acto           : "",
    id_entidad        : this.Cod_Entidad,
    id_forma          : undefined,
    nro_documento     : "",
    fecha_documento   : "",
    fecha_inicio      : "",
    fecha_vencimiento : "",
    tipo_benef        : "",
    ruc_beneficiario  : "",
    motivo            : "",
    usua_creacion     : this.Cod_Usuario,
    codigo_patrimonial: "",
    denominacion_bien : "",
    cantidad          : "",
    valor_adq         : "",
    estado            : ""
  }
  modal;

  mostrarVentanaRegistroIndividual : boolean = false;
  mostrarVentanaRegistroMasivo : boolean = false;
  flag_listado_catalogo : boolean = false;

  dataActosRecepcion : any = [];
  dataFormaIndividual : any = [];
  closeResult: string;
  dataListadoTipoAdministracion : any = [];
  datatipoBenef : any = [];
  dataEditarActos : any = [];
  dataEditarActos2 : any = [];
  flag_SeleccionCatalogo : boolean = false;
  TipoagregarBien    : string = "";
  id_altas : any = [];
  COD_UE_BIEN_ENTREGA : number;

  nro_documento : string = "";

  Arrg_patrim = [];
  Arrg_patrim_tmp = [];
  posicion : number;
  Arrg_patrim_eliminados = [];
  contListCodPatrim : number;
  contListselecc : number;
  ListCodPatrim: string;
  id_acto: number;

  paramRegistroIndividual = { 
    id_entidad        : this.Cod_Entidad,  
    cod_acto     : '', 
    nro_documento    : '', 
    fecha_documento  : '', 
    tipo_benef       : '',
    ruc_benef        : '',
    id_usuario       : this.Cod_Usuario, 
    cod_patrimonial  : '', 
    id_acto          : ''
  }
  paramEliminar = { id_entidad : this.Cod_Entidad, id_usuario : this.Cod_Usuario, cod_patrimonial : '', nro_resolucion : '', fecha_resolucion : '', id_acto : '', cod_ue_bien_patri : ''}
  dataGuardarBien : any = [];
  dataEliminarBien : any = [];

  modificando_registro : string = "";
  

  /* Fin de Declaracion de Variables */

  constructor
  (
    public datepipe: DatePipe,
    private formBuilder: FormBuilder,
    private SinabipMuebles : SinabipmueblesService, 
    private spinner: NgxSpinnerService,
    private modalService: NgbModal,
    private router: Router
  ) 
  { 
    let date = new Date();
    //this.ListadoTipoAdministracion_Recepcion();
    
    
  }

  ngOnChanges(){
    if(this.id_Acto != ""){
      this.EditarEntrega(this.id_Acto);
    }
  }

 
  ngOnInit() {
  
    
    this.registerForm = this.formBuilder.group({ 
      formaAdquisicion: ['', Validators.required],
      documentoActo: ['', Validators.required],
      fechaAdministracion: ['', [Validators.required]],
      tipoBeneficiario: ['', [Validators.required]],
      rucBeneficiario: ['', [Validators.required]],

    });

    this.ListadoTipoAdministracion_Entrega();

    // this.nro_documento = sessionStorage.getItem("nro_resolucion");
    // alert(this.nro_documento);

  }

  get f() { return this.registerForm.controls; } 


  /* Funciones */
  CargarventanaRegistroFormaIndividual(){
    alert('aaa');
    this.mostrarVentanaRegistroIndividual = true;
    this.mostrarVentanaRegistroMasivo = false;
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  ListadoBienesXActoAdministracion(){
    this.spinner.show();
    console.log(this.parametros);
    this.SinabipMuebles.postListadoBienesXActoAdministracion(this.parametros).subscribe((data : any) =>{ 
    this.spinner.hide();
    this.dataFormaIndividual = data.data; 
    console.log(this.dataFormaIndividual);
    //console.log(this.dataFormaIndividual)
    this.total = ( this.dataFormaIndividual.catalogo.length > 0 ) ? this.dataFormaIndividual.catalogo[0].TOTAL : 0;
    //console.log('cc');
    console.log(this.total);
    
  });
  }


  abrirVentaSeleccionCatalogo(obj, content) {
    
    this.flag_SeleccionCatalogo = true;
    this.paramAgregar.codigo_patrimonial = obj.NRO_BIEN;
    this.paramAgregar.denominacion_bien = obj.DENOM_BIEN;
    
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title',
    keyboard: false,
    size: 'lg',
    backdrop: 'static'}).result.then((result) => { 
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }


  AgregarBien_para_Entrega():void{
    

      this.paramAgregar.id_entidad = this.Cod_Entidad;
      this.paramAgregar.id_forma = this.registerForm.get('formaAdquisicion').value;
      this.paramAgregar.nro_documento = this.registerForm.get('documentoActo').value;
      this.paramAgregar.fecha_documento = this.registerForm.get('fechaAdministracion').value;
      this.paramAgregar.tipo_benef = this.registerForm.get('tipoBeneficiario').value;
      this.paramAgregar.ruc_beneficiario = this.registerForm.get('rucBeneficiario').value;
      this.paramAgregar.motivo = 'Prueba de Motivo';
      this.paramAgregar.usua_creacion = this.Cod_Usuario;

      this.header1.emit('OK');

      console.log("this.paramAgregar.id_acto");
      console.log(this.paramAgregar.id_acto);
    if(this.paramAgregar.id_acto == ''){
      this.paramAgregar.id_acto = '-1';
    }
  
    this.spinner.show();
    console.log(this.paramAgregar);
    this.SinabipMuebles.postAgregarBien_para_Entrega(this.paramAgregar).subscribe((data : any) =>{
    this.spinner.hide();

    this.id_altas = data.data;
    this.paramAgregar.id_acto = this.id_altas.cabecera.ID;

    if (this.id_altas.cabecera.RESULTADO == 'OK'){

      //this.cargarListadoActosAdquisicion(); 
      //this.editarActoAdquisicion(this.paramAgregar.id_acto);
      this.modalService.dismissAll();
      swal({
        position: 'center',
        type: 'success',
        title: 'Se ha Registrado Satisfactoriamente!!!',
      })

    }else{
      swal({
        position: 'center',

        type: 'error',
        title: 'Ha ocurrido un error y no se ha registrado, vuelva a intentar nuevamente!!!',
      })
    }
 
     });
 
  }


  onSubmit() {
    //alert('guardando...');
    this.submitted = true;
    
    // stop here if form is invalid
    if (this.registerForm.invalid) { 
      swal({
        type: 'error',
        title: '!!Datos Incompletos!!',
        text: 'No se ha ingresado la información necesaria para continuar',
        // footer: '<a href>Why do I have this issue?</a>'
      })
        return;
    }
    else
    {
        this.Guardar_Actos();
    }
       
  }

  Guardar_Actos():void{
   
    console.log('this.dataEditarActos2');
    console.log(this.dataEditarActos2);
    let contador = 0;
    this.dataEditarActos2.forEach((data5 :any) =>{
      this.posicion = this.Arrg_patrim.indexOf(this.dataEditarActos2[contador]);
      this.Arrg_patrim.splice(this.posicion,1);
      contador += 1;
    });

    console.log('this.Arrg_patrim');
    console.log(this.Arrg_patrim);

    this.ListCodPatrim = JSON.stringify(this.Arrg_patrim);
    this.ListCodPatrim = this.ListCodPatrim.substring( 1, (this.ListCodPatrim.length)-1 ); 
    this.ListCodPatrim = this.ListCodPatrim.replace(/['"]+/g, ''); 
    sessionStorage.setItem('Asig_Cod_Patrimonial', this.ListCodPatrim);

    this.paramRegistroIndividual.id_entidad = this.Cod_Entidad;
    this.paramRegistroIndividual.cod_acto = this.registerForm.get('formaAdquisicion').value;
    this.paramRegistroIndividual.nro_documento = this.registerForm.get('documentoActo').value;
    this.paramRegistroIndividual.fecha_documento = this.registerForm.get('fechaAdministracion').value;
    this.paramRegistroIndividual.tipo_benef = this.registerForm.get('tipoBeneficiario').value;
    this.paramRegistroIndividual.ruc_benef = this.registerForm.get('rucBeneficiario').value;
    this.paramRegistroIndividual.id_usuario = this.Cod_Usuario;
    this.paramRegistroIndividual.cod_patrimonial = sessionStorage.getItem('Asig_Cod_Patrimonial');
    this.paramRegistroIndividual.id_acto = this.paramAgregar.id_acto;

    this.spinner.show();
      this.SinabipMuebles.postGuardar_Entrega_Reingreso(this.paramRegistroIndividual).subscribe((data : any) =>{
      this.spinner.hide();

      if (data.data.error == true){
        if (data.data.reco.hasOwnProperty('id_entidad')) { 
          this.router.navigate(['error500']);
          return;
        }else if (data.data.reco.hasOwnProperty('cod_acto')) {
            this.router.navigate(['error500']);
            return;
        }else if (data.data.reco.hasOwnProperty('nro_documento')) {
          swal({
            type: 'error',
            title: 'Validacion de Datos',
            text: data.data.reco.nro_documento[0],
            })
          return;
        }else if (data.data.reco.hasOwnProperty('fecha_documento')) {
          swal({
            type: 'error',
            title: 'Validacion de Datos',
            text: data.data.reco.fecha_documento[0],
            })
          return;
        }else if (data.data.reco.hasOwnProperty('id_usuario')) {
          this.router.navigate(['error500']);
          return;
        }else if (data.data.reco.hasOwnProperty('id_acto')) {
          this.router.navigate(['error500']);
          return;
        }
      }
      
      this.dataGuardarBien = data.data;  
      this.ListadoTipoAdministracion_Entrega();
      this.mostrarVentanaRegistroIndividual = false;
      this.paramAgregar.id_acto = this.dataGuardarBien.ID;

      swal({
        position: 'center',
        type: 'success',
        title: 'Se ha Registrado Satisfactoriamente!!!',
      })
      this.eventoGuardar.emit('');
      }); 
      

  }



  EditarEntrega(id_acto){
    //this.mostrarVentanaTipoRegistro = true;
    //this.title = "individual";

    this.paramAgregar.id_acto = id_acto;
    this.paramAgregar.id_entidad = this.Cod_Entidad;

    this.spinner.show();
    this.SinabipMuebles.postEditarEntrega(this.paramAgregar).subscribe((data : any) =>{  
    this.spinner.hide();
    
    this.dataEditarActos = data.data;  
    console.log("this.dataEditarActos"); 
    console.log(this.dataEditarActos);

    // this.paramAgregar.id_forma = this.dataEditarActos.cabecera.COD_TIPO_ADMIN;
    // this.paramAgregar.nro_documento = this.dataEditarActos.cabecera.NRO_DOCUMENTO;
    // this.paramAgregar.fecha_documento = this.dataEditarActos.cabecera.FECHA_DOCUMENTO;
    this.paramAgregar.estado = this.dataEditarActos.cabecera.ESTADO;

    this.seleccionRows = id_acto;

    sessionStorage.setItem("nro_resolucion","zzz");

    this.registerForm.get('formaAdquisicion').setValue(this.dataEditarActos.cabecera.COD_TIPO_ADMIN);
    this.registerForm.get('documentoActo').setValue(this.dataEditarActos.cabecera.NRO_DOCUMENTO);
    this.registerForm.get('fechaAdministracion').setValue(this.dataEditarActos.cabecera.FECHA_DOCUMENTO);
    this.registerForm.get('tipoBeneficiario').setValue(this.dataEditarActos.cabecera.COD_TIPO_BENEF);
    this.registerForm.get('rucBeneficiario').setValue(this.dataEditarActos.cabecera.RUC_BENEF_ADMINIS);
    
    });
    

    // this.mostrarVentanaTipoRegistro = true;
    // this.mostrarVentanaRegistroIndividual = true;
    // this.modificando_registro = 'M';
    // this.cantidadduplicado_bienes = 0;
    // this.cantidadduplicado_actos = 0;  
    // this.cantidaderrores_columnas = 0;
        
     
  }


  


  keyPress(event: any) {
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {    
        // invalid character, prevent input
        event.preventDefault();
    }
  }

  keyPresspuntos(event: any) {
    const pattern = /[0-9.]/;
    const inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {    
        // invalid character, prevent input
        event.preventDefault();
    }
  }

  


  onSubmit2() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) { 
      swal({
        type: 'error',
        title: 'Datos Incompletosss!!!',
        text: 'No se ha ingresado la información necesaria para continuar',
        // footer: '<a href>Why do I have this issue?</a>'
      })
        return;
    }
    else
    {
      // this.Guardar_Actos();
    }
    
    
  }


  ListadoTipoAdministracion_Entrega(){
    this.spinner.show();
    this.SinabipMuebles.postListadoTipoAdministracion_Entrega('').subscribe((data : any) =>{ 
    this.spinner.hide();
    this.dataListadoTipoAdministracion = data.data;
    this.registerForm.get('formaAdquisicion').setValue(undefined);
    this.registerForm.get('tipoBeneficiario').setValue(undefined);
    //this.submitted=false;
    console.log(this.dataListadoTipoAdministracion);
    
    });
  }


  abrirListaCatalogoBienes(content) { 

    this.flag_listado_catalogo = true;
    
    this.parametros = { 
      cod_entidad   : this.Cod_Entidad,
      nro_grupo     : "-1",
      nro_clase     : "-1",
      denom_bien    : "",
      nro_bien      : "",
      cod_patrimonial : "",
      denom_patrimonial : "",
      nro_resoluc : "",
      page : this.page, records : this.itemsPerPage 
    } 
    this.paramAgregar.codigo_patrimonial = "";
    this.paramAgregar.denominacion_bien = "";
    this.paramAgregar.cantidad = "";
    this.paramAgregar.valor_adq = "";
    this.dataFormaIndividual = [];

    this.onSubmit2();
    if(this.registerForm.valid){
      
      this.spinner.show();
      this.modal=this.SinabipMuebles.postListadoCatalogoBienes_Reingreso(this.parametros).subscribe((data : any) =>{ 
        this.spinner.hide();
        this.dataFormaIndividual = data.data;
        console.log(this.dataFormaIndividual);
      });
      
  
      this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', 
      keyboard: false,
      size: 'lg',
      backdrop: 'static'}).result.then((result) => { 
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`; 
      });
    }else
    {

      swal({
        type: 'error',
        title: 'Datos Incompletos!!!',
        text: 'No se ha ingresado la información necesaria para continuar',
        // footer: '<a href>Why do I have this issue?</a>'
      })


    }

  }


  mostrarVentana(ventana){
    this.header1.emit(ventana);
    if (ventana == 'individual'){
      //this.ListadoTipoAdministracion_Recepcion();
    }
    // this.evento = ventana;
    // this.header.emit(this.evento);
  }


  almacenarcodpatrim(estado,cod_patrimonial){
    if(estado == true){
      this.Arrg_patrim_tmp.push(cod_patrimonial);
    }else{
      this.posicion = this.Arrg_patrim_tmp.indexOf(cod_patrimonial);
      this.Arrg_patrim_tmp.splice(this.posicion,1);
    }
    console.log(this.Arrg_patrim_tmp);
  
  }


  AgregarBien_Actos():void{ 
    this.Arrg_patrim = this.Arrg_patrim_tmp.concat(this.Arrg_patrim);
    console.log('this.Arrg_patrim_tmp');
    console.log(this.Arrg_patrim_tmp);
    console.log('this.Arrg_patrim');
    console.log(this.Arrg_patrim);
  
    let contador = 0;
    this.Arrg_patrim_eliminados.forEach((data5 :any) =>{
            
      //console.log(this.Arrg_patrim_eliminados[contador]);
      this.posicion = this.Arrg_patrim.indexOf(this.Arrg_patrim_eliminados[contador]);
      //console.log('posisicion: ' +this.posicion);
      this.Arrg_patrim.splice(this.posicion,1);
      contador += 1;
      
    });
  
    console.log('this.Arrg_patrim44444');
    console.log(this.Arrg_patrim);
  
  
    this.contListCodPatrim = 0;
    this.contListselecc = 0;
    this.ListCodPatrim = ''; 
    
    this.ListCodPatrim = JSON.stringify(this.Arrg_patrim);
    this.ListCodPatrim = this.ListCodPatrim.substring( 1, (this.ListCodPatrim.length)-1 ); 
    this.ListCodPatrim = this.ListCodPatrim.replace(/['"]+/g, ''); 
    sessionStorage.setItem('Asig_Cod_Patrimonial', this.ListCodPatrim);
    this.contListselecc = this.ListCodPatrim.length;
  
    if(this.contListselecc >= 1)
    {
      this.paramRegistroIndividual.cod_patrimonial = this.ListCodPatrim;
      this.spinner.show();
      console.log("this.paramRegistroIndividual");
      console.log(this.paramRegistroIndividual);
      this.modal=this.SinabipMuebles.postAgregar_Bienes_al_Detalle_Reingreso(this.paramRegistroIndividual).subscribe((data : any) =>{  
      this.spinner.hide();
      
      console.log("this.dataEditarActos");
      console.log(this.dataEditarActos);
      this.dataEditarActos = data.data;
      this.modalService.dismissAll();
  
    });
  
    // this.dataEditarActos.forEach((data4 :any) =>{
    //   data4.seleccionado=false;
    // });
    
  
    }else
    {
  
      swal({
        position: 'center',
        type: 'error',
        title: 'Ud. no ha seleccionado ningún Bien'
      })
  
    }
  }


  EliminarItemReingreso(COD_UE_BIEN_PATRI){
    this.paramEliminar.cod_ue_bien_patri = COD_UE_BIEN_PATRI;
    this.paramEliminar.id_entidad = this.Cod_Entidad;
    
    swal({
      title: '¿Está Ud. seguro de eliminar un Item?',
      text: "¡No podrás revertir esto!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonText: 'Cancelado',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Eliminar!'
      
    }).then((result) => {
    if (result.value) {
      this.spinner.show();
      console.log(this.paramEliminar);
      this.SinabipMuebles.postEliminarItemReingreso(this.paramEliminar).subscribe((data : any) =>{
        this.spinner.hide();
        this.dataEliminarBien = data.data; 
        
        if (this.dataEliminarBien[0].RESULTADO == 'CORRECTO'){ 
          this.COD_UE_BIEN_ENTREGA =  this.dataEliminarBien[0].COD_UE_BIEN_ENTREGA;
          console.log(this.COD_UE_BIEN_ENTREGA);
         
          this.EditarEntrega(this.COD_UE_BIEN_ENTREGA);
          //this.ListadoTipoAdministracion_Entrega();
          
          
          swal(
            'Eliminado!',
            'El registro ha sido eliminado.',
            'success'
          )
          this.eventoEliminar.emit('');
      }else{
        swal(
          'Error!',
          'Ocurrio un inconveniente',
          'error'
        )
      }

      
      });
      
    }
  })
  }

  DetalleTecnico(){
    alert('Detalle tecnico');
  }

  loading(){
    
  }


  


  

  /* Fin de Funciones */

}
