import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResponsablePatrComponent } from './responsable-patr.component';

describe('ResponsablePatrComponent', () => {
  let component: ResponsablePatrComponent;
  let fixture: ComponentFixture<ResponsablePatrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResponsablePatrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResponsablePatrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
