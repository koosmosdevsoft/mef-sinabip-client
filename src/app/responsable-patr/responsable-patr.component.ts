import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-responsable-patr',
  templateUrl: './responsable-patr.component.html',
  styleUrls: ['./responsable-patr.component.css']
})
export class ResponsablePatrComponent implements OnInit {

	RutaSinabip : string ;
  estado: string = sessionStorage.getItem("Est_Menu");
  constructor() { 
    let token = sessionStorage.getItem("token");  
    let ruta = sessionStorage.getItem("RutaSinabip")
    if(token === null){
      // window.location.replace(ruta+"SGISBN/System/sinabip.php");
      window.location.replace("./SGISBN/System/sinabip.php");
    }
    this.cargarmenu()
  }
  ngOnInit() {
  }

  cargarmenu(){
    this.estado = sessionStorage.getItem("Est_Menu");
    if(this.estado === "1"){
      this.RutaSinabip = sessionStorage.getItem("RutaSinabip") + "SGISBN/System/sinabip_modulos.php?idm=1&opt=5&menu=0"
    }

    if(this.estado === "2"){
      this.RutaSinabip = sessionStorage.getItem("RutaSinabip") + "SGISBN/System/sinabip_modulos.php?idm=1&opt=7&menu=0"
    }

  }

}
