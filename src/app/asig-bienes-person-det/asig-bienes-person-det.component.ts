import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { SinabipmueblesService } from '../services/sinabipmuebles.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import swal from'sweetalert2';

@Component({
	selector: 'app-asig-bienes-person-det',
	templateUrl: './asig-bienes-person-det.component.html',
	styleUrls: ['./asig-bienes-person-det.component.css'],
	animations: [routerTransition()],
	providers: [NgbModalConfig, NgbModal]
})

export class AsigBienesPersonDetComponent implements OnInit {
  ListPers_Arg : Array<any> = [];
  ListPred_Arg : Array<any> = [];
  ListArea_Arg : Array<any> = [];
  NomAreaPred_Arg : Array<any> = [];
  NomPers_Arg : Array<any> = [];
  ResumenPatrim_Arg : Array<any> = [];
  AgregarCabAsig_Arg : Array<any> = [];
  PDF_FIN_ASIG_Arg : Array<any> = [];
  ElimCabAsig_Arg : Array<any> = [];
  AgregarDetAsig_Arg : Array<any> = [];
  id : string = sessionStorage.getItem("Cod_Entidad");
  idusuario : string = sessionStorage.getItem("Cod_Usuario");
  NomPersonal : string;  
  NomPredio : string;
  NomArea : string;
  NroDocumento: string;
  closeResult: string;
  mostrar_carga = null;
  
  //Paginacion 
  itemsPerPage: number = 20;
  page: any = 1;
  previousPage: any;
  total : any = 0;

  //Paginacion2
  itemsPerPage2: number = 6;
  page2: any = 1;
  previousPage2: any;
  total2 : any = 0;
  
  nro_asignacion: string = '';
  urlPDF;  
  filtro = {  
    id  : this.id, nrodocument  : '', appaterpers  : '', apmaterpers  : '', nombre  : '', page : this.page, records : this.itemsPerPage 
  }
  ListadoAreaPredio = { id : this.id, id_predio : '', id_area : '', estado  : '0' }
  ResumenPatrim = { id : this.id, cod_patrimonial : sessionStorage.getItem("Asig_Cod_Patrimonial"), page2 : this.page2, records2 : this.itemsPerPage2 }
  AgregarCabAsig = { 
    id : this.id, id_predio : sessionStorage.getItem("Asig_Predio"), id_area : sessionStorage.getItem("Asig_Area"), 
    id_personal : sessionStorage.getItem("Asig_Personal") , id_usuario : sessionStorage.getItem("Cod_Usuario") 
  }
  ElimCabAsig = { id : this.id, num_asig : sessionStorage.getItem("NrAsig_Act") }
  AgregarDetAsig = { 
    id : this.id, Listcod_patrimonial : sessionStorage.getItem("Asig_Cod_Patrimonial"), num_asig :sessionStorage.getItem("NrAsig_Act"),
    id_predio : sessionStorage.getItem("Asig_Predio"), id_area : sessionStorage.getItem("Asig_Area"), 
    id_personal : sessionStorage.getItem("Asig_Personal") , id_usuario : sessionStorage.getItem("Cod_Usuario") 
  }
  PDF_FIN_ASIG = { 
    id : this.id, 
    nrasig_act :sessionStorage.getItem("NrAsig_Act"),
    cod_patrimonial : sessionStorage.getItem("Asig_Cod_Patrimonial")
  }

  constructor(config: NgbModalConfig,private Sinabip : SinabipmueblesService,
    private route: ActivatedRoute,
    private router: Router,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal) {
      this.ListadoPersonalAsignar(); 
      this.Listadopredarea('1');
      config.backdrop = 'static';
      config.keyboard = false;
    }

  ngOnInit() {
  }

  desabilitar_ventana(){
    this.mostrar_carga=null;
  }

  ActualizaArea(){
    this.ListadoAreaPredio.id_area = '';
    this.Listadopredarea('2');
  }

  ListadoPersonalAsignar(){
    this.spinner.show();
    this.Sinabip.PostListadoPersonalAsignar(this.filtro).subscribe((data : any) =>{
    this.spinner.hide();

    if (data.data.error == true){
			if (data.data.reco.hasOwnProperty('id')) { 
				this.router.navigate(['error500']);
				return;
			}else if (data.data.reco.hasOwnProperty('nrodocument')) {
          swal({
            type: 'error',
            title: 'Validacion de Datos',
            text: data.data.reco.nrodocument[0],
            })
				  return;
			}else if (data.data.reco.hasOwnProperty('page')) {
        this.router.navigate(['error500']);
        return;
			}else if (data.data.reco.hasOwnProperty('records')) {
        this.router.navigate(['error500']);
        return;
      }
    }
    
        this.ListPers_Arg = data.data;
        this.total = ( this.ListPers_Arg.length > 0 ) ? this.ListPers_Arg[0].Total : 0;
    });
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.ListadoPersonalAsignar();
    }
  }

  resetearpag(){
    this.filtro.page = 1;
    this.ListadoPersonalAsignar();
  }

  resetearlimpiarpag(){
    this.page = 1;
    this.Limpiar_ListadoAsignarPerson();
  }

  Limpiar_ListadoAsignarPerson(){
    this.filtro = {  
    id  : this.id, nrodocument  : '', appaterpers  : '', apmaterpers  : '', nombre  : '', page : this.page, records : this.itemsPerPage 
    }
    this.Sinabip.PostListadoPersonalAsignar(this.filtro).subscribe((data : any) =>{
      this.ListPers_Arg = data.data;
    });
  }

  nuevo(id_personal,estado){
    this.mostrar_carga = {
      personal : id_personal, estado : estado
    }
    this.ListadoAreaPredio = { id : this.id, id_predio : '', id_area : '', estado  : '1' }
    this.Listadopredarea('2');
    sessionStorage.setItem("Asig_Personal", id_personal); 
    this.PersonalNomb(id_personal);
  }
  
  PersonalNomb(idpredio){
    this.spinner.show();
    this.Sinabip.getPersonalNomb(this.id,idpredio).subscribe((data : any) =>{
    this.spinner.hide();
        this.NomPers_Arg = data.data;
        this.NomPersonal = this.NomPers_Arg[0].PERSONAL;
        this.NroDocumento = this.NomPers_Arg[0].NRO_DOCUMENTO;
    });
  }

  open(content){    
    if( this.ListadoAreaPredio.id_predio == '' ){
      swal({
        position: 'center',
        type: 'success',
        title: 'Falta seleccionar el local',
        showConfirmButton: false,
        timer: 2000
      })
    }else{
      if( this.ListadoAreaPredio.id_area == '' ){
        swal({
          position: 'center',
          type: 'success',
          title: 'Falta seleccionar el área',
          showConfirmButton: false,
          timer: 2000
        })
      }else{
        sessionStorage.setItem("Asig_Predio", this.ListadoAreaPredio.id_predio);
        sessionStorage.setItem("Asig_Area", this.ListadoAreaPredio.id_area);
        this.Listadopredarea('4');
        this.ResumenPatrimonialAsig();
        this.AgregarCabAsignacion();
        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
          this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
        });

      }
    }
  }

  Listadopredarea(estado){
    this.ListadoAreaPredio.estado =  estado;
    if(estado === '1'){
      this.Sinabip.PostListadopredarea(this.ListadoAreaPredio).subscribe((data : any) =>{
        this.ListPred_Arg = data.data;
      });
    } 
    if(estado === '2'){
      this.Sinabip.PostListadopredarea(this.ListadoAreaPredio).subscribe((data : any) =>{
        this.ListArea_Arg = data.data;
      });
    }     
    if(estado === '4'){
      this.Sinabip.PostListadopredarea(this.ListadoAreaPredio).subscribe((data : any) =>{
        this.NomAreaPred_Arg = data.data;
        this.NomPredio = this.NomAreaPred_Arg[0].DENOMINACION_PREDIO;
        this.NomArea = this.NomAreaPred_Arg[0].DESC_AREA;
      });
    } 
  }


  ResumenPatrimonialAsig(){    
    this.ResumenPatrim.cod_patrimonial = sessionStorage.getItem("Asig_Cod_Patrimonial");
    this.spinner.show();
    this.Sinabip.PostResumenPatrimonialAsig(this.ResumenPatrim).subscribe((data : any) =>{
    this.spinner.hide();
        this.ResumenPatrim_Arg = data.data;
        this.total2 = ( this.ResumenPatrim_Arg.length > 0 ) ? this.ResumenPatrim_Arg[0].Total : 0;
    });
  }

  loadPage2(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.ResumenPatrimonialAsig();
    }
  }

  AgregarCabAsignacion(){    
    this.AgregarCabAsig = { 
      id : this.id, id_predio : sessionStorage.getItem("Asig_Predio"), id_area : sessionStorage.getItem("Asig_Area"), 
      id_personal : sessionStorage.getItem("Asig_Personal") , id_usuario : sessionStorage.getItem("Cod_Usuario") 
    }
    
    this.spinner.show();
    this.Sinabip.PostAgregarCabAsignacion(this.AgregarCabAsig).subscribe((data : any) =>{
    this.spinner.hide();

    if (data.data.error == true){
			if (data.data.reco.hasOwnProperty('id')) { 
				  this.router.navigate(['error500']);
				  return;
			}else if (data.data.reco.hasOwnProperty('id_predio')) {
				  this.router.navigate(['error500']);
				  return;
			}else if (data.data.reco.hasOwnProperty('id_area')) {
        this.router.navigate(['error500']);
        return;
      }else if (data.data.reco.hasOwnProperty('id_personal')) {
        this.router.navigate(['error500']);
        return;
      }else if (data.data.reco.hasOwnProperty('id_usuario')) {
        this.router.navigate(['error500']);
        return;
			}
    }
    
        this.AgregarCabAsig_Arg = data.data;
        sessionStorage.setItem("NrAsig_Act", this.AgregarCabAsig_Arg[0].NROASIG); 
    });
  }
  
  EliminarCabAsignacion(){   
    this.ElimCabAsig = { id : this.id, num_asig : sessionStorage.getItem("NrAsig_Act") }
    swal({
      title: 'Confirma si cancelas el registro?',
      text: "",
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Acepta, Cancelar Registro!'
    }).then((result) => {
      if (result.value) {
        this.spinner.show();
        this.Sinabip.PostEliminarCabAsignacion(this.ElimCabAsig).subscribe((data : any) =>{
        this.spinner.hide();
            this.ElimCabAsig_Arg = data.data;
        });
        this.modalService.dismissAll();
      }
    })
  }
  
  AgregarDetAsignacion(content2){    
    this.AgregarDetAsig = { 
      id : this.id, Listcod_patrimonial : sessionStorage.getItem("Asig_Cod_Patrimonial"), num_asig :sessionStorage.getItem("NrAsig_Act"),
      id_predio : sessionStorage.getItem("Asig_Predio"), id_area : sessionStorage.getItem("Asig_Area"), 
      id_personal : sessionStorage.getItem("Asig_Personal") , id_usuario : sessionStorage.getItem("Cod_Usuario") 
    }
    swal({
      title: 'Confirma Guardar el Registro de Asignación?',
      text: "",
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Acepta, Guardar!'
    }).then((result) => {
      if (result.value) {
        this.modalService.dismissAll();
        this.nro_asignacion=sessionStorage.getItem("NrAsig_Act")
        this.spinner.show();
        this.Sinabip.PostAgregarDetAsignacion(this.AgregarDetAsig).subscribe((data : any) =>{
        this.spinner.hide();

        if (data.data.error == true){
          if (data.data.reco.hasOwnProperty('id')) { 
              this.router.navigate(['error500']);
              return;
          }else if (data.data.reco.hasOwnProperty('id_predio')) {
              this.router.navigate(['error500']);
              return;
          }else if (data.data.reco.hasOwnProperty('id_area')) {
            this.router.navigate(['error500']);
            return;
          }else if (data.data.reco.hasOwnProperty('id_personal')) {
            this.router.navigate(['error500']);
            return;
          }else if (data.data.reco.hasOwnProperty('id_usuario')) {
            this.router.navigate(['error500']);
            return;
          }
        }

            this.AgregarDetAsig_Arg = data.data;
            this.Iniciar_Asign();
        });
        setTimeout(() => {
          this.open_Resumen(content2,this.nro_asignacion);
          }, 1000);
      }
    })
  }

  open_Resumen(content2,nroasig) {
    this.modalService.open(content2, {size: 'lg', ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
    });
    this.urlPDF = this.Sinabip.API_URL+"Pdf_ResumenAsignacion/"+this.id +"/"+nroasig;
  }


  
  Iniciar_Asign(){
    sessionStorage.setItem('Asig_Cod_Patrimonial', '');
    sessionStorage.setItem('NrAsig_Act', '');
    sessionStorage.setItem('Asig_Predio', '');
    sessionStorage.setItem('Asig_Area', '');
    sessionStorage.setItem('Asig_Personal', '');
    
    this.router.navigate(["asig-bienes"]);
  }

}
