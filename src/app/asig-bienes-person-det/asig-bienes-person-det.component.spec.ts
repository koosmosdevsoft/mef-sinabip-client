import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsigBienesPersonDetComponent } from './asig-bienes-person-det.component';

describe('AsigBienesPersonDetComponent', () => {
  let component: AsigBienesPersonDetComponent;
  let fixture: ComponentFixture<AsigBienesPersonDetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsigBienesPersonDetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsigBienesPersonDetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
