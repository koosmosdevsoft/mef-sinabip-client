import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrasladoBienesResumenComponent } from './traslado-bienes-resumen.component';

describe('TrasladoBienesResumenComponent', () => {
  let component: TrasladoBienesResumenComponent;
  let fixture: ComponentFixture<TrasladoBienesResumenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrasladoBienesResumenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrasladoBienesResumenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
