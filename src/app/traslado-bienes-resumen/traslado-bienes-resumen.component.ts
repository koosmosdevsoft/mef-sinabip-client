import { Component, OnInit } from '@angular/core';

import { routerTransition } from '../router.animations';
import { SinabipmueblesService } from '../services/sinabipmuebles.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgbModalConfig, NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';


@Component({
	selector: 'app-traslado-bienes-resumen',
	templateUrl: './traslado-bienes-resumen.component.html',
	styleUrls: ['./traslado-bienes-resumen.component.css'],
	animations: [routerTransition()],
	providers: [NgbModalConfig, NgbModal]
})
export class TrasladoBienesResumenComponent implements OnInit {

	id: string = sessionStorage.getItem("Cod_Entidad");
	idusuario: string = sessionStorage.getItem("Cod_Usuario");
	nro_traslado: string = sessionStorage.getItem("NrTrasl_Act");
	fecha_traslado: string = sessionStorage.getItem("Fecha_Traslado");
	tipo_traslado: string = sessionStorage.getItem("Trasl_TipoTrasl");

	ResumenPatrim_Arg: Array<any> = [];
	ResumenPatrim2_Arg: Array<any> = [];
	TipoTrans_Arg: Array<any> = [];
	ElimCabTraslado_Arg: Array<any> = [];
	AgregarDetTras_Arg: Array<any> = [];

	NomAreaPred_Arg: Array<any> = [];
	NomPredio: string;
	NomArea: string;

	NomPers_Arg: Array<any> = [];
	NomPers2_Arg: Array<any> = [];
	NomPersonal: string;
	NomPersonal2: string;
	NroDocumento: string;
	NroDocumento2: string;
	des_tipo_tras: string;

	closeResult: string;
	urlPDF;

	//Paginacion 
	itemsPerPage: number = 20;
	page: any = 1;
	previousPage: any;
	total: any = 0;

	//Paginacion 
	itemsPerPage2: number = 20;
	page2: any = 1;
	previousPage2: any;
	total2: any = 0;

	ResumenPatrim = { id: this.id, cod_patrimonial: sessionStorage.getItem("Traslado_Cod_Patrimonial"), page2: this.page, records2: this.itemsPerPage }

	ResumenPatrim2 = { id: this.id, cod_patrimonial: sessionStorage.getItem("Traslado_Cod_Patrimonial"), page2: this.page2, records2: this.itemsPerPage2 }

	ListadoAreaPredio = { id: this.id, id_predio: '', id_area: '', estado: '0' }

	ElimCabTraslado = {
		id: this.id, num_asignacion: sessionStorage.getItem("NrAsig_Act"),
		num_devolucion: sessionStorage.getItem("NrDev_Act"), num_traslado: sessionStorage.getItem("NrTrasl_Act")
	}

	AgregarDetTras = {
		id: this.id,
		Listcod_patrimonial: sessionStorage.getItem("Traslado_Cod_Patrimonial"),
		num_asignacion: sessionStorage.getItem("NrAsig_Act"),
		num_devolucion: sessionStorage.getItem("NrDev_Act"),
		num_traslado: sessionStorage.getItem("NrTrasl_Act"),
		id_predio_dest: sessionStorage.getItem("Trasl_Predio"),
		id_area_dest: sessionStorage.getItem("Trasl_Area"),
		id_personal_dest: sessionStorage.getItem("Trasl_Personal"),
		id_personal_orig: sessionStorage.getItem("Traslado_Personal_Origen"),
		id_usuario: sessionStorage.getItem("Cod_Usuario")
	}
	constructor(config: NgbModalConfig, private Sinabip: SinabipmueblesService,private route: ActivatedRoute,private router: Router,private spinner: NgxSpinnerService,private modalService: NgbModal) {
		this.ResumenPatrimonialDev2(1);
		this.ResumenPatrimonialDev2(2);
		this.Listadopredarea(4);
		this.ListadoTipoTrasladoxid();
		config.backdrop = 'static';
		config.keyboard = false;

		this.nro_traslado = sessionStorage.getItem("NrTrasl_Act");
		this.fecha_traslado = sessionStorage.getItem("Fecha_Traslado");

		this.PersonalNomb(sessionStorage.getItem("Traslado_Personal_Origen"));
		if (this.tipo_traslado == '2') {
			this.PersonalNomb2(sessionStorage.getItem("Trasl_Personal"));
		} else {
			this.PersonalNomb2(sessionStorage.getItem("Traslado_Personal_Origen"));
		}


	}

	ngOnInit() {
	}

	ResumenPatrimonialDev2(estado) {
		if (estado == 1) {
			this.ResumenPatrim.cod_patrimonial = sessionStorage.getItem("Traslado_Cod_Patrimonial");
			this.spinner.show();
			this.Sinabip.PostResumenPatrimonialAsig(this.ResumenPatrim).subscribe((data: any) => {
				this.spinner.hide();
				this.ResumenPatrim_Arg = data.data;
				this.total = (this.ResumenPatrim_Arg.length > 0) ? this.ResumenPatrim_Arg[0].Total : 0;
			});
		}

		if (estado == 2) {
			this.ResumenPatrim2.cod_patrimonial = sessionStorage.getItem("Traslado_Cod_Patrimonial");
			this.spinner.show();
			this.Sinabip.PostResumenPatrimonialAsig(this.ResumenPatrim2).subscribe((data: any) => {
				this.spinner.hide();
				this.ResumenPatrim2_Arg = data.data;
				this.total2 = (this.ResumenPatrim2_Arg.length > 0) ? this.ResumenPatrim2_Arg[0].Total : 0;
			});
		}
	}

	loadPage(page: number) {
		if (page !== this.previousPage) {
			this.previousPage = page;
			this.ResumenPatrimonialDev2(1);
		}
	}

	loadPage2(page: number) {
		if (page !== this.previousPage2) {
			this.previousPage2 = page;
			this.ResumenPatrimonialDev2(2);
		}
	}

	Listadopredarea(estado) {
		this.ListadoAreaPredio.id_predio = sessionStorage.getItem("Trasl_Predio");
		this.ListadoAreaPredio.id_area = sessionStorage.getItem("Trasl_Area");
		this.ListadoAreaPredio.estado = estado;

		this.Sinabip.PostListadopredarea(this.ListadoAreaPredio).subscribe((data: any) => {
			this.NomAreaPred_Arg = data.data;
			this.NomPredio = this.NomAreaPred_Arg[0].DENOMINACION_PREDIO;
			this.NomArea = this.NomAreaPred_Arg[0].DESC_AREA;
		});
	}

	PersonalNomb(idpredio) {
		this.spinner.show();
		this.Sinabip.getPersonalNomb(this.id, idpredio).subscribe((data: any) => {
			this.spinner.hide();
			this.NomPers_Arg = data.data;
			this.NomPersonal = this.NomPers_Arg[0].PERSONAL;
			this.NroDocumento = this.NomPers_Arg[0].NRO_DOCUMENTO;
		});
	}

	PersonalNomb2(idpredio) {
		this.spinner.show();
		this.Sinabip.getPersonalNomb(this.id, idpredio).subscribe((data: any) => {
			this.spinner.hide();
			this.NomPers2_Arg = data.data;
			this.NomPersonal2 = this.NomPers2_Arg[0].PERSONAL;
			this.NroDocumento2 = this.NomPers2_Arg[0].NRO_DOCUMENTO;
		});
	}

	ListadoTipoTrasladoxid() {
		this.spinner.show();
		this.Sinabip.GetListadoTipoTrasladoxid(sessionStorage.getItem("Trasl_TipoTrasl")).subscribe((data: any) => {
			this.spinner.hide();
			this.TipoTrans_Arg = data.data;
			this.des_tipo_tras = this.TipoTrans_Arg[0].DESCRIPCION_ESTADO;
		});
	}

	EliminarCabTraslado() {
		this.ElimCabTraslado = {
			id: this.id, num_asignacion: sessionStorage.getItem("NrAsig_Act"),
			num_devolucion: sessionStorage.getItem("NrDev_Act"), num_traslado: sessionStorage.getItem("NrTrasl_Act")
		}

		swal({
			title: 'Confirma si se cancela el registro?',
			text: "",
			type: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Acepta, Cancelar Registro!'
		}).then((result) => {
			if (result.value) {
				this.spinner.show();
				this.Sinabip.PostEliminarCabTraslado(this.ElimCabTraslado).subscribe((data: any) => {
					this.spinner.hide();
					this.ElimCabTraslado_Arg = data.data;
				});
				this.Retorno_trasl();
			}
		})
	}

	Retorno_trasl() {
		this.router.navigate(["traslados-bienes"]);
		this.Limpiartemporal();
	}

	Limpiartemporal() {
		sessionStorage.setItem("Traslado_Personal_Origen", '');
		sessionStorage.setItem("Traslado_Cod_Patrimonial", '');
		sessionStorage.setItem("Trasl_TipoTrasl", '');
		sessionStorage.setItem("Trasl_Predio", '');
		sessionStorage.setItem("Trasl_Personal", '');
		sessionStorage.setItem("Trasl_Area", '');
		sessionStorage.setItem("NrTrasl_Act", '');
		sessionStorage.setItem("NrDev_Act", '');
		sessionStorage.setItem("NrAsig_Act", '');
		sessionStorage.setItem("Fecha_Traslado", '');
	}

	AgregarDetTraslado(content) {
		this.AgregarDetTras = {
			id: this.id,
			Listcod_patrimonial: sessionStorage.getItem("Traslado_Cod_Patrimonial"),
			num_asignacion: sessionStorage.getItem("NrAsig_Act"),
			num_devolucion: sessionStorage.getItem("NrDev_Act"),
			num_traslado: sessionStorage.getItem("NrTrasl_Act"),
			id_predio_dest: sessionStorage.getItem("Trasl_Predio"),
			id_area_dest: sessionStorage.getItem("Trasl_Area"),
			id_personal_dest: sessionStorage.getItem("Trasl_Personal"),
			id_personal_orig: sessionStorage.getItem("Traslado_Personal_Origen"),
			id_usuario: sessionStorage.getItem("Cod_Usuario")
		}
		swal({
			title: 'Confirma Guardar el Registro de Traslado?',
			text: "",
			type: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Acepta, Guardar!'
		}).then((result) => {
			if (result.value) {
				this.spinner.show();
				this.Sinabip.PostAgregarDetTraslado(this.AgregarDetTras).subscribe((data: any) => {
					this.spinner.hide();
					this.AgregarDetTras_Arg = data.data;
				});
				setTimeout(() => {
					this.open_Resumen(content, sessionStorage.getItem("NrTrasl_Act"));
				}, 1000);

				setTimeout(() => {
					this.Retorno_trasl();
				}, 1500)
			}
		})
	}

	open_Resumen(content, nr_traslado) {
		this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
			this.closeResult = `Closed with: ${result}`;
		}, (reason) => {
		});
		this.urlPDF = this.Sinabip.API_URL + "Pdf_ResumenTraslados/" + this.id + "/" + nr_traslado;

	}

}
