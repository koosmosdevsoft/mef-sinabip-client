import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModdatosbienComponent } from './moddatosbien.component';

describe('ModdatosbienComponent', () => {
  let component: ModdatosbienComponent;
  let fixture: ComponentFixture<ModdatosbienComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModdatosbienComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModdatosbienComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
