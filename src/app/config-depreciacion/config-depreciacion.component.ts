import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { routerTransition } from '../router.animations';
import { SinabipmueblesService } from '../services/sinabipmuebles.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
// import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbModalConfig, NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
	selector: 'app-config-depreciacion',
	templateUrl: './config-depreciacion.component.html',
	styleUrls: ['./config-depreciacion.component.css'],
	animations: [routerTransition()],
	providers: [NgbModalConfig, NgbModal]
})
export class ConfigDepreciacionComponent implements OnInit {

	@ViewChild('divClick') divClick: ElementRef;
	ListFechasDep_Arg: Array<any> = [];
	Listfechapecosa_Arg: Array<any> = [];
	ActFechaPecosa_Arg: Array<any> = [];
	Cantfechapecosa_Arg: Array<any> = [];
	GuardartipoDepr_Arg: Array<any> = [];
	id: string = sessionStorage.getItem("Cod_Entidad");
	idusuario: string = sessionStorage.getItem("Cod_Usuario");
	closeResult: string;
	fechpecosa = [];
	ContActPecosaTotal: number;
	ContActPecosa: number;
	IngPecosa: string;
	Ingcod_patr: string;
	cantpecosa: number;

	filtro = { id: this.id, cod_fecha: '' }
	Listfechapecosa = { id: this.id, cod_patrimonial: '', estado: 5 }
	ActFechaPecosa = { id: this.id, cod_patrimonial: '', fecha_pecosa: '' }
	GuardarTipoFecha = { id: this.id, tipo: '', cod_user: '' }

	constructor(
		private formBuilder: FormBuilder, config: NgbModalConfig, private Sinabip: SinabipmueblesService,
		private route: ActivatedRoute,
		private router: Router,
		private spinner: NgxSpinnerService,
		private modalService: NgbModal) {

		this.ListaFechaDepreciacionSelec();

		config.backdrop = 'static';
		config.keyboard = false;
	}

	ngOnInit() {
		setTimeout(() => {
			this.divClick.nativeElement.click();
		}, 200);
	}

	open(content) {
		this.modalService.dismissAll();
		this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
			this.closeResult = `Closed with: ${result}`;
		}, (reason) => {
		});
	}

	open2(content2) {
		if (this.filtro.cod_fecha == '') {
			swal('Aviso Seleccione el tipo de Fecha');
		} else if (this.filtro.cod_fecha == '2') {
			this.CantFechaPecosa();
			setTimeout(() => {
				if (this.cantpecosa > 0) {
					this.modalService.dismissAll();
					this.ListadoFechaPecosa();
					this.modalService.open(content2, {  ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
						this.closeResult = `Closed with: ${result}`;
					}, (reason) => {
					});
				} else {
					this.GuardarTipoDepreciacion(this.filtro.cod_fecha);
				}
			}, 1000);
		} else {
			this.GuardarTipoDepreciacion(this.filtro.cod_fecha);
		}
	}

	ListaFechaDepreciacionSelec() {
		this.spinner.show();
		this.Sinabip.GetListaFechaDepreciacionSelec().subscribe((data: any) => {
			this.spinner.hide();
			this.ListFechasDep_Arg = data.data;
		});
	}

	ListadoFechaPecosa() {
		this.Listfechapecosa.estado = 5;
		this.spinner.show();
		this.Sinabip.PostListadoFechaPecosa(this.Listfechapecosa).subscribe((data: any) => {
			this.spinner.hide();
			this.Listfechapecosa_Arg = data.data;
		});
	}

	CantFechaPecosa() {
		this.Listfechapecosa.estado = 6;
		this.spinner.show();
		this.Sinabip.PostListadoFechaPecosa(this.Listfechapecosa).subscribe((data: any) => {
			this.spinner.hide();
			this.Cantfechapecosa_Arg = data.data;
			this.cantpecosa = this.Cantfechapecosa_Arg[0].CANT;
		});
	}

	GuardarTipoDepreciacion(TipoFecha) {
		swal({
			title: 'Confirmar si guarda el Tipo de Fecha seleccionada?',
			text: "",
			type: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Acepta, Tipo Fecha!'
		}).then((result) => {
			if (result.value) {
				this.GuardarTipoFecha = { id: this.id, tipo: TipoFecha, cod_user: this.idusuario }
				this.spinner.show();
				this.Sinabip.PostGuardarTipoDepreciacion(this.GuardarTipoFecha).subscribe((data: any) => {
					this.spinner.hide();
					this.GuardartipoDepr_Arg = data.data;
					this.modalService.dismissAll();
				});
			}
		})
	}

	ActualizaFechaPecosa() {
		this.fechpecosa = [];
		this.ContActPecosa = 0;
		this.ContActPecosaTotal = 0;
		this.Listfechapecosa_Arg.forEach((data: any) => {
			this.fechpecosa.push(data);
		});
		this.fechpecosa.forEach((data: any) => {
			this.IngPecosa = this.fechpecosa[this.ContActPecosa].fechpecosa;
			this.Ingcod_patr = this.fechpecosa[this.ContActPecosa].CODIGO_PATRIMONIAL;
			if (this.IngPecosa != undefined || this.IngPecosa != null) {
				this.ActFechaPecosa.cod_patrimonial = this.Ingcod_patr;
				this.ActFechaPecosa.fecha_pecosa = this.IngPecosa;
				this.spinner.show();
				this.Sinabip.PostActualizaPecosa(this.ActFechaPecosa).subscribe((data: any) => {
					this.spinner.hide();
					this.ActFechaPecosa_Arg = data.data;
				});
			} else {
				this.ContActPecosaTotal = this.ContActPecosaTotal + 1;
			}
			this.ContActPecosa = this.ContActPecosa + 1;
		});
		if (this.ContActPecosaTotal >= 1) {
			swal('Aviso: Falta Modificar ' + this.ContActPecosaTotal + ' Fechas')
		} else {
			this.modalService.dismissAll();
			this.GuardarTipoDepreciacion(this.filtro.cod_fecha);
		}
	}

}
