import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigDepreciacionComponent } from './config-depreciacion.component';

describe('ConfigDepreciacionComponent', () => {
  let component: ConfigDepreciacionComponent;
  let fixture: ComponentFixture<ConfigDepreciacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigDepreciacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigDepreciacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
