import { Component, OnInit, ɵConsole } from '@angular/core';
import { routerTransition } from '../router.animations';
import { SinabipmueblesService } from '../services/sinabipmuebles.service';
//import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgbModalConfig, NgbModal, ModalDismissReasons, NgbModule} from '@ng-bootstrap/ng-bootstrap';
//import {NgbDatepickerI18n, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
//import { ModalService } from '../_services'; 
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import swal from'sweetalert2';


@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html', 
  styleUrls: ['./personal.component.css'],  
  animations: [routerTransition()],
  providers: [NgbModalConfig, NgbModal]
})

export class PersonalComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
titularAlerta:string='';
 dataPersonal : any = [];
 Arrg_denom = [];
 posicion : number;
 maxCaract : number = 8;
 mostrarVentanaNuevoPersonal : boolean = false;
 mostrarVentanaEditarPersonal : boolean = false;
 mostrarVentanaRecodificarSeleccionados : boolean = false;
 mostrarVentanaResumenRecodificacion : boolean = false;
 dataDetalleRecodificacion : any = [];
 estado_recodificacion : string = "";
 estado_bajapersonal : string = "";
 estado_eliminarpersonal : string = "";
 estado_registrarpersonal : string = "";
 codigo_recodificacion : number = -1;
 cod_ue_personal : number = -1;
 codigo_registrarpersonal : number = -1;
 cod_ue_personal_bd : number = -1;
 Cod_Entidad : string = sessionStorage.getItem("Cod_Entidad");
 Cod_Usuario : string = sessionStorage.getItem("Cod_Usuario");
 RutaSinabip : string = sessionStorage.getItem("RutaSinabip");
 urlPDF;
 cod_entidad_recod;
 afuConfig = {
  uploadAPI: {
      url:this.SinabipMuebles.API_URL+"SubirAltaTxt/"+this.Cod_Entidad,
    },
    hideResetBtn: true,
    uploadBtnText:"Adjuntar Archivo",
    uploadMsgText: "",
    formatsAllowed: ".TXT,.txt"
};

heroForm : any = [];

//Paginacion 
itemsPerPage: number = 20;
page: any = 1;
previousPage: any;
total : any = 0;

filtro = {
  documento : '',
  nombres : '',
  apellidopat : '',
  apellidomat : '',  
  tipo : -1,  
  cod_entidad       : this.Cod_Entidad,
  page : this.page, records : this.itemsPerPage
}

paramPers = {
  cod_entidad  : this.Cod_Entidad,      
  documento : '',
  apellidopat :'',
  apellidomat :'',
  nombres : '',
  modalidad : -1,
  tipo : -1
}

paramPersE = {
	cod_entidad  : this.Cod_Entidad,      
	documento : '',
	apellidopat :'',
	apellidomat :'',
	nombres : '',
	modalidad : -1,
	tipo : -1,
	cod_ue_pers : 0
  }

  constructor(
    private formBuilder: FormBuilder,
    private SinabipMuebles : SinabipmueblesService, 
    private spinner: NgxSpinnerService,
	private modalService: NgbModal,
	private router: Router) 

    { 
		let token = sessionStorage.getItem("token");  
		let ruta = sessionStorage.getItem("RutaSinabip")
		if(token === null){
			// window.location.replace(ruta+"SGISBN/System/sinabip.php");
			window.location.replace("./SGISBN/System/sinabip.php");
			return;
		}
		this.afuConfig.uploadAPI.url = this.SinabipMuebles.API_URL+"AdjuntarAltatxt/"+this.Cod_Entidad+"/"+token;
    }

  ngOnInit(): void {

    this.registerForm = this.formBuilder.group({ 
      documento: ['', Validators.required],
      nombres: ['', Validators.required],
      apellidopat: ['', [Validators.required]],
      apellidomat: ['', [Validators.required]],
      modalidad: [-1, [Validators.required]],
      tipo: [-1, [Validators.required]],
    });

    this.filtro.documento = '';
    this.filtro.nombres = '';
    this.filtro.apellidopat = '',
    this.filtro.apellidomat = '',
    this.filtro.tipo = -1,
    this.cargarListadoPersonal();
  }

  get f() { return this.registerForm.controls; }

  // onSubmit() {
  //   this.submitted = true;
  //   if (this.registerForm.invalid || this.registerForm.get('modalidad').value == -1 || this.registerForm.get('tipo').value == -1) { 
  //     swal({
  //       type: 'error',
  //       title: 'Datos Incompletos!!!',
  //       text: 'No se ha ingresado la información necesaria para continuar',
  //     })
  //     return;
	// }
  //   this.ProcesarRegistroPersonal();
	// }
	


	onSubmit() {
    this.submitted = true;
    
    // stop here if form is invalid
    if (this.registerForm.invalid) { 
      swal({
        type: 'error',
        title: 'Datos Incompletosss!!!',
        text: 'No se ha ingresado la información necesaria para continuar',
        // footer: '<a href>Why do I have this issue?</a>'
      })
        return;
    }
    else
    {
        this.ProcesarRegistroPersonal();
    }
       
  }


  Caract(){
	  if (this.registerForm.get('tipo').value == 1){
		this.maxCaract = 8;
	  }else{
		this.maxCaract = 12;
	  }
	  this.registerForm.get('documento').setValue('');
  }

	ProcesarRegistroPersonal(){
		this.paramPers.cod_entidad = this.Cod_Entidad;
		this.paramPers.documento = this.registerForm.get('documento').value;
		this.paramPers.apellidopat = this.registerForm.get('apellidopat').value;
		this.paramPers.apellidomat = this.registerForm.get('apellidomat').value;
		this.paramPers.nombres = this.registerForm.get('nombres').value;
		this.paramPers.modalidad = this.registerForm.get('modalidad').value;
		this.paramPers.tipo = this.registerForm.get('tipo').value;
		this.spinner.show();
		
		this.SinabipMuebles.postRegistrarPersonal(this.paramPers).subscribe((data : any) =>{
			this.spinner.hide();

			if (data.data.error == true){
				if (data.data.reco.hasOwnProperty('cod_entidad')) { 
					this.router.navigate(['error500']);
					return;
				}else if (data.data.reco.hasOwnProperty('documento')) {
					swal({
						type: 'error',
						title: 'Validacion de Datos',
						text: data.data.reco.documento[0],
					  })
					  return;
				}else if (data.data.reco.hasOwnProperty('modalidad')) {
					this.router.navigate(['error500']);
					return;
				}else if (data.data.reco.hasOwnProperty('tipo')) {
					this.router.navigate(['error500']);
					return;
				}
				
			}
			
			this.estado_registrarpersonal = data.data[0].estado_registropersonal;
			this.codigo_registrarpersonal = data.data[0].codigo_registropersonal;
			if(this.estado_registrarpersonal == 'REGISTROPERSONAL_SATISFACTORIA'){
				swal({
					position: 'center', 
					type: 'success',
					title: 'Proceso de registro de personal finalizado correctamente',
					showConfirmButton: false,
					timer: 3000 
				})
				this.mostrarVentanaNuevoPersonal = false;
				this.registerForm.reset();
				this.cargarListadoPersonal();
			}else{
				swal({
				  type: 'error',
				  title: '¡ERROR!',
				  text: 'Ocurrió un error al procesar el registro de personal',
				})
			}
		})
	}

  ventanaNuevoPersonal(){
	this.mostrarVentanaNuevoPersonal = true;
  }

	loadPage(page: number) {
		if (page !== this.previousPage) {
			this.previousPage = page;
			this.cargarListadoPersonal();
		}
	}

  cargarListadoPersonal(){
	this.spinner.show();
	//this.filtro.cod_entidad = "2550";
	//console.log(this.filtro);
    this.SinabipMuebles.postListadoPersonal(this.filtro).subscribe((data : any) =>{  
		  this.spinner.hide();
		  //console.log(data.data.error);
		if (data.data.error == true){
			if (data.data.reco.hasOwnProperty('documento')) { 
				swal({
					type: 'error',
					title: 'Validacion de Datos',
					text: data.data.reco.documento[0],
					// footer: '<a href>Why do I have this issue?</a>'
				  })
				  return;
			}else if (data.data.reco.hasOwnProperty('cod_entidad')) {
				  this.router.navigate(['error500']);
				  return;
				  //this.router.navigate(['actosadministracion']);
			}
			
		}
		this.dataPersonal = data.data;
		this.total = ( this.dataPersonal.reco.length > 0 ) ? this.dataPersonal.reco[0].TOTAL : 0;
    });
  }

  BorrarFiltro(){
    this.filtro.documento = '';
    this.filtro.nombres = '';
    this.filtro.apellidomat = '';
    this.filtro.apellidopat = '';
    this.filtro.tipo = -1;
  }

  editarPersonal(cod_ue_pers){
	
	this.cod_ue_personal = cod_ue_pers;
	this.spinner.show();
	this.SinabipMuebles.postObtenerDataPersonal({paramEditar: this.cod_ue_personal}).subscribe((data : any) =>{
		this.spinner.hide();
		
		if (data.data.error == true){
			
			if (data.data.reco.hasOwnProperty('paramEditar')) { 
				this.router.navigate(['error500']);
				return;
			}
		}

		this.paramPersE.tipo = data.data[0].TIP_DOCUMENTO;
		this.paramPersE.documento = data.data[0].NRO_DOCUMENTO;
		this.paramPersE.nombres = data.data[0].UE_NOMBRES_PERS;
		this.paramPersE.apellidopat = data.data[0].UE_APEPAT_PERS;
		this.paramPersE.apellidomat = data.data[0].UE_APEMAT_PERS;
		this.paramPersE.modalidad = data.data[0].COD_MODALIDAD;
		this.paramPersE.cod_ue_pers = data.data[0].COD_UE_PERS;
		this.mostrarVentanaNuevoPersonal = false;
		this.mostrarVentanaEditarPersonal = true;

		this.registerForm.get('documento').setValue(this.paramPersE.documento);
		this.registerForm.get('nombres').setValue(this.paramPersE.nombres);
		this.registerForm.get('apellidopat').setValue(this.paramPersE.apellidopat);
		this.registerForm.get('apellidomat').setValue(this.paramPersE.apellidomat);
		this.registerForm.get('apellidomat').setValue(this.paramPersE.apellidomat);
		this.registerForm.get('modalidad').setValue(this.paramPersE.modalidad);
		this.registerForm.get('tipo').setValue(this.paramPersE.tipo);

		//console.log(this.paramPersE);
		//this.submitted = true;
	})
  }

  // onSubmitEdicion(){


	// this.submitted = true;
	// if (this.registerForm.invalid || this.registerForm.get('modalidad').value == -1 || this.registerForm.get('tipo').value == -1) { 
	// 	swal({
	// 	  type: 'error',
	// 	  title: 'Datos Incompletos!!!',
	// 	  text: 'No se ha ingresado la información necesaria para continuar',
	// 	})
	// 	return;
	// }
	// this.ProcesarEditarPersonal();
	// }
	


	onSubmitEdicion() {
		this.submitted = true;
		
    
    // stop here if form is invalid
    if (this.registerForm.invalid) { 
      swal({
        type: 'error',
        title: 'Datos Incompletosss!!!',
        text: 'No se ha ingresado la información necesaria para continuar',
        // footer: '<a href>Why do I have this issue?</a>'
      })
        return;
    }
    else
    {
        this.ProcesarEditarPersonal();
    }
       
	}
	

  ProcesarEditarPersonal(){
	this.paramPersE.cod_entidad = this.Cod_Entidad;
	this.paramPersE.documento = this.registerForm.get('documento').value;
	this.paramPersE.apellidopat = this.registerForm.get('apellidopat').value;
	this.paramPersE.apellidomat = this.registerForm.get('apellidomat').value;
	this.paramPersE.nombres = this.registerForm.get('nombres').value;
	this.paramPersE.modalidad = this.registerForm.get('modalidad').value;
	this.paramPersE.tipo = this.registerForm.get('tipo').value;
	this.spinner.show();
	this.SinabipMuebles.postEditarPersonal(this.paramPersE).subscribe((data : any) =>{
		this.spinner.hide();
		this.estado_registrarpersonal = data.data.estado_editarpersonal;
		this.codigo_registrarpersonal = data.data.codigo_editarpersonal;
		if(this.estado_registrarpersonal == 'EDITARPERSONAL_SATISFACTORIA'){
			swal({
				position: 'center', 
				type: 'success',
				title: 'Proceso de edición de personal finalizado correctamente',
				showConfirmButton: false,
				timer: 3000 
			})
			this.mostrarVentanaEditarPersonal = false;
			this.registerForm.reset();
			this.cargarListadoPersonal();
		}else{
			swal({
			  type: 'error',
			  title: '¡ERROR!',
			  text: 'Ocurrió un error al procesar la edición del personal',
			})
		}
	})
	}

  bajaPersonal(cod_ue_pers){
	this.cod_ue_personal = cod_ue_pers;
    swal({
      title: '¿Está Ud. seguro de dar de baja al personal?',
      text: "¡No podrás revertir esto!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonText: 'Cancelado',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Dar de baja!'
    }).then((result) => {
		if (result.value) {
			this.spinner.show();
			this.SinabipMuebles.postProcesarBajaPersonal({paramBaja: this.cod_ue_personal}).subscribe((data : any) =>{
			this.spinner.hide();

			if (data.data.error == true){
				if (data.data.reco.hasOwnProperty('paramBaja')) { 
					this.router.navigate(['error500']);
					return;
				}
				
			}

			this.estado_bajapersonal = data.data[0].estado_baja_personal;
			this.cod_ue_personal_bd = data.data[0].cod_ue_pers;
			if (this.estado_bajapersonal == 'BAJAPERSONAL_SATISFACTORIA'){
				swal({
					position: 'center', 
					type: 'success',
					title: 'Proceso de baja de personal finalizado correctamente',
					showConfirmButton: false,
					timer: 3000 
				})
			}else if (this.estado_bajapersonal == 'BAJAPERSONAL_ASIGNADO'){
				swal(
					'Aviso!!',
					'El personal cuenta con bienes Asignados, primero realizar Devolucion!',
					'info'
					)
			}else{
				swal({
					type: 'error',
					title: '¡ERROR!',
					text: 'Ocurrió un error al procesar la baja del personal',
				})
			}
			this.cargarListadoPersonal();
			})
		}
	})
  }

  eliminarPersonal(cod_ue_pers){
	  console.log(cod_ue_pers);
    this.cod_ue_personal = cod_ue_pers;
    swal({
      title: '¿Está Ud. seguro de eliminar al personal?',
      text: "¡No podrás revertir esto!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonText: 'Cancelado',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'
    }).then((result) => {
		if (result.value) {
			this.spinner.show();
			this.SinabipMuebles.postProcesarEliminarPersonal(this.cod_ue_personal).subscribe((data : any) =>{
			this.spinner.hide();

			if (data.data.error == true){
				if (data.data.reco.hasOwnProperty('paramEliminar')) { 
					this.router.navigate(['error500']);
					return;
				}
			}

			this.estado_eliminarpersonal = data.data[0].estado_eliminar_personal;
			this.cod_ue_personal_bd = data.data[0].cod_ue_pers;

			console.log(this.estado_eliminarpersonal);

			if (this.estado_eliminarpersonal == 'ELIMINARPERSONAL_SATISFACTORIA'){
				swal({
					position: 'center', 
					type: 'success',
					title: 'Proceso de eliminación de personal finalizado correctamente',
					showConfirmButton: false,
					timer: 3000 
				})
			}else if (this.estado_eliminarpersonal == 'ELIMINARPERSONAL_ASIGNADO'){
				swal(
					'Aviso!!',
					'El personal cuenta con bienes Asignados, primero realizar Devolucion!',
					'info'
					)
			}else{
				swal({
					type: 'error',
					title: '¡ERROR!',
					text: 'Ocurrió un error al procesar la eliminación del personal',
				})
			}
			this.cargarListadoPersonal();
			})
		}
	})
  }
}