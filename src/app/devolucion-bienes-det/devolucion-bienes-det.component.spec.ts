import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevolucionBienesDetComponent } from './devolucion-bienes-det.component';

describe('DevolucionBienesDetComponent', () => {
  let component: DevolucionBienesDetComponent;
  let fixture: ComponentFixture<DevolucionBienesDetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevolucionBienesDetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevolucionBienesDetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
