import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { SinabipmueblesService } from '../services/sinabipmuebles.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
// import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbModalConfig, NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';


@Component({
	selector: 'app-devolucion-bienes-det',
	templateUrl: './devolucion-bienes-det.component.html',
	styleUrls: ['./devolucion-bienes-det.component.css'],
	animations: [routerTransition()],
	providers: [NgbModalConfig, NgbModal]
})
export class DevolucionBienesDetComponent implements OnInit {

	id: string = sessionStorage.getItem("Cod_Entidad");
	idusuario: string = sessionStorage.getItem("Cod_Usuario");

	ListBienDevol_Arg: Array<any> = [];
	ListBienDevolucion_Arg: Array<any> = [];
	ListaObtenerTodo_Arg: Array<any> = [];
	AgregarCabDev_Arg: Array<any> = [];
	NomPers_Arg: Array<any> = [];
	Arrg_patrim = [];
	seleccionados = [];
	ElimCabDevol_Arg: Array<any> = [];
	ResumenPatrim_Arg: Array<any> = [];
	AgregarDetDevol_Arg: Array<any> = [];
	nro_devolucion: string = '';
	NomPersonal: string;
	NroDocumento: string;
	contListCodPatrim: number;
	ListCodPatrim: string;
	contcheck: number;
	contcheck2: number;
	checkcod_patr: string;
	checkcod_patr2: string;
	posicion: number;
	contListselecc: number;
	asigTodoEstado: string;
	closeResult: string;
	urlPDF;
	mostrar_carga = null;

	//Paginacion 
	itemsPerPage: number = 20;
	page: any = 1;
	previousPage: any;
	total: any = 0;

	//Paginacion2
	itemsPerPage2: number = 6;
	page2: any = 1;
	previousPage2: any;
	total2: any = 0;

	filtro = {
		id: this.id, cod_pers: sessionStorage.getItem("Devolucion_Personal"), cod_patrimonial: '', denominacion: '', page: this.page, records: this.itemsPerPage
	}

	AgregarCabDevolucion = {
		id: this.id, id_personal: sessionStorage.getItem("Devolucion_Personal"), motivo: '', id_usuario: sessionStorage.getItem("Cod_Usuario")
	}

	ResumenPatrim = { id: this.id, cod_patrimonial: sessionStorage.getItem("Devol_Cod_Patrimonial"), page2: this.page2, records2: this.itemsPerPage2 }

	ElimCabDevol = { id: this.id, num_asig: sessionStorage.getItem("NrDev_Act") }

	AgregarDetDevol = {
		id: this.id, Listcod_patrimonial: sessionStorage.getItem("Devol_Cod_Patrimonial"), num_devol: sessionStorage.getItem("NrDev_Act"),
		id_personal: sessionStorage.getItem("Devolucion_Personal"), id_usuario: sessionStorage.getItem("Cod_Usuario")
	}

	constructor(config: NgbModalConfig, private Sinabip: SinabipmueblesService,
		private route: ActivatedRoute,
		private router: Router,
		private spinner: NgxSpinnerService,
		private modalService: NgbModal) {
			sessionStorage.setItem("Cod_Entidad", this.id);
			sessionStorage.setItem("Cod_Usuario", this.idusuario);
			this.ListadoDevolucionPerson(1);
			this.PersonalNomb(sessionStorage.getItem("Devolucion_Personal"))
			config.backdrop = 'static';
			config.keyboard = false;
		}

	ngOnInit() {
	}


	ListadoDevolucionPerson(estado) {
		// console.log(this.filtro);
		this.spinner.show();
		this.Sinabip.PostListadoDevolucionPerson(this.filtro).subscribe((data: any) => {
			this.spinner.hide();
			this.ListBienDevol_Arg = data.data;
			// console.log(this.ListBienDevol_Arg);
			this.total = (this.ListBienDevol_Arg.length > 0) ? this.ListBienDevol_Arg[0].Total : 0;
			if (estado == 1) {
				this.contcheck = 0;
				this.Arrg_patrim.forEach((data3: any) => {
					this.checkcod_patr = this.Arrg_patrim[this.contcheck];
					this.contcheck = this.contcheck + 1;
					this.contcheck2 = 0;
					this.ListBienDevol_Arg.forEach((data2: any) => {
						this.checkcod_patr2 = this.ListBienDevol_Arg[this.contcheck2].CODIGO_PATRIMONIAL;
						this.contcheck2 = this.contcheck2 + 1;
						if (this.checkcod_patr == this.checkcod_patr2) {
							data2.seleccionado = true;
						}
					});
				});
			}
			if (estado == 2) {
				this.Arrg_patrim = [];
				sessionStorage.setItem('Devol_Cod_Patrimonial', '');
				this.ListCodPatrim = '';
				this.ListBienDevol_Arg.forEach((data4: any) => {
					data4.seleccionado = false;
				});
			}
		});
	}

	loadPage(page: number) {
		if (page !== this.previousPage) {
			this.previousPage = page;
			this.ListadoDevolucionPerson(1);
		}
	}

	loadPage2(page2: number) {
		if (page2 !== this.previousPage2) {
			this.previousPage2 = page2;
			this.ResumenPatrimonialDev();
		}
	}

	resetearpag() {
		this.filtro.page = 1;
		this.ListadoDevolucionPerson(1);
	}

	resetearlimpiarpag() {
		this.page = 1;
		this.Limpiar_ListadoDevolverPerson();
	}

	Limpiar_ListadoDevolverPerson() {
		this.filtro = {
			id: this.id, cod_pers: sessionStorage.getItem("Devolucion_Personal"), cod_patrimonial: '', denominacion: '', page: this.page, records: this.itemsPerPage
		}
		this.Arrg_patrim = [];
		sessionStorage.setItem('Devol_Cod_Patrimonial', '');
		this.ListCodPatrim = '';
		this.spinner.show();
		this.Sinabip.PostListadoDevolucionPerson(this.filtro).subscribe((data: any) => {
			this.spinner.hide();
			this.ListBienDevol_Arg = data.data;
			this.ListBienDevol_Arg.forEach((data2: any) => {
				data2.seleccionado = false;
			});
		});
	}

	almacenarcodpatrim(estado, cod_patrimonial) {
		if (estado == true) {
			this.Arrg_patrim.push(cod_patrimonial);
		} else {
			this.posicion = this.Arrg_patrim.indexOf(cod_patrimonial);
			this.Arrg_patrim.splice(this.posicion, 1);
		}
	}

	open(content, estado) {
		if (estado === 1) {
			this.Devolvertodo();
			this.asigTodoEstado = sessionStorage.getItem("Devol_Cod_Patrimonial");
			// console.log(this.asigTodoEstado);
			if (this.asigTodoEstado == null) {
				this.asigTodoEstado = '0';
			}
			if (this.asigTodoEstado != '0') {

				swal({
					title: 'Confirmar la Devolución de Todos los Bienes?',
					text: "",
					type: 'question',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Acepta, Devolución Total!'
				}).then((result) => {
					if (result.value) {
						this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
							this.closeResult = `Closed with: ${result}`;
						}, (reason) => {
						});
					} else {
						this.Limpiar_ListadoDevolverPerson();
					}
				})
			} else {
				swal({
					position: 'center',
					type: 'warning',
					title: 'No Cuenta con Bienes Seleccionados!!!',
					showConfirmButton: false,
					timer: 4000
				})
			}
		}

		if (estado === 2) {
			this.Devolverseleccionado();
			if (this.contListselecc >= 1) {
				this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
					this.closeResult = `Closed with: ${result}`;
				}, (reason) => {
				});
			} else {
				swal({
					position: 'center',
					type: 'warning',
					title: 'Debe seleccionar un bien para continuar!!!',
					showConfirmButton: false,
					timer: 4000
				})
			}
		}
	}

	Devolverseleccionado() {
		sessionStorage.setItem('Devol_Cod_Patrimonial', '');
		this.seleccionados = [];
		this.contListCodPatrim = 0;
		this.contListselecc = 0;
		this.ListCodPatrim = '';

		this.ListCodPatrim = JSON.stringify(this.Arrg_patrim);
		this.ListCodPatrim = this.ListCodPatrim.substring(1, (this.ListCodPatrim.length) - 1);
		this.ListCodPatrim = this.ListCodPatrim.replace(/['"]+/g, '');
		sessionStorage.setItem('Devol_Cod_Patrimonial', this.ListCodPatrim);
		this.contListselecc = this.ListCodPatrim.length;
	}

	Devolvertodo() {
		sessionStorage.setItem('Devol_Cod_Patrimonial', '');
		this.seleccionados = [];
		this.contListselecc = 0;
		this.ListBienDevol_Arg.forEach((data: any) => {
			data.seleccionado = true;
			if (data.FLAGPERS != 1 && data.seleccionado == true) {
				this.seleccionados.push(data);
			}
		});

		this.spinner.show();
		this.Sinabip.getListaobtTodosCPDevol(this.id, sessionStorage.getItem("Devolucion_Personal")).subscribe((data: any) => {
			this.spinner.hide();

			if (data.data.error == true){
				if (data.data.reco.hasOwnProperty('id')) { 
					this.router.navigate(['error500']);
					 return;
				}else if (data.data.reco.hasOwnProperty('idpers')) {
					this.router.navigate(['error500']);
					return;
				}
				
			}

			this.ListaObtenerTodo_Arg = data.data;
			this.asigTodoEstado = this.ListaObtenerTodo_Arg[0].LISTA_CP;
			sessionStorage.setItem('Devol_Cod_Patrimonial', this.asigTodoEstado);
			this.contListselecc = this.asigTodoEstado.length;
		});
	}

	PersonalNomb(id_personal) {
		this.spinner.show();
		this.Sinabip.getPersonalNomb(this.id, id_personal).subscribe((data: any) => {
			this.spinner.hide();
			this.NomPers_Arg = data.data;
			this.NomPersonal = this.NomPers_Arg[0].PERSONAL;
			this.NroDocumento = this.NomPers_Arg[0].NRO_DOCUMENTO;
		});
	}

	ResumenPatrimonialDev() {
		this.ResumenPatrim.cod_patrimonial = sessionStorage.getItem("Devol_Cod_Patrimonial");
		this.spinner.show();
		this.Sinabip.PostResumenPatrimonialAsig(this.ResumenPatrim).subscribe((data: any) => {
			this.spinner.hide();
			this.ResumenPatrim_Arg = data.data;
			this.total2 = (this.ResumenPatrim_Arg.length > 0) ? this.ResumenPatrim_Arg[0].Total : 0;
		});
	}

	OpenReumen(content2) {
		this.modalService.dismissAll();
		this.AgregarCabDevol();
		setTimeout(() => {
			this.PersonalNomb(sessionStorage.getItem("Devolucion_Personal"));
			this.ResumenPatrimonialDev();
			this.modalService.open(content2, {  ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
				this.closeResult = `Closed with: ${result}`;
			}, (reason) => {
			});
		}, 500);
	}

	AgregarCabDevol() {
		// console.log(this.AgregarCabDevolucion);    
		this.spinner.show();
		this.Sinabip.PostAgregarCabDevolucion(this.AgregarCabDevolucion).subscribe((data: any) => {
			this.spinner.hide();

			if (data.data.error == true){
				if (data.data.reco.hasOwnProperty('id')) { 
					this.router.navigate(['error500']);
					  return;
				}else if (data.data.reco.hasOwnProperty('id_personal')) {
					  this.router.navigate(['error500']);
					  return;
				}else if (data.data.reco.hasOwnProperty('id_usuario')) {
					this.router.navigate(['error500']);
					return;
				}
			}

			this.AgregarCabDev_Arg = data.data;
			// console.log(this.AgregarCabDev_Arg);  
			sessionStorage.setItem("NrDev_Act", this.AgregarCabDev_Arg[0].NRODEVOL);
		});
	}

	EliminarCabDevolucion() {
		this.ElimCabDevol = { id: this.id, num_asig: sessionStorage.getItem("NrDev_Act") }
		swal({
			title: 'Confirma si se cancela el registro?',
			text: "",
			type: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Acepta, Cancelar Registro!'
		}).then((result) => {
			if (result.value) {
				this.spinner.show();
				this.Sinabip.PostEliminarCabDevolucion(this.ElimCabDevol).subscribe((data: any) => {
					this.spinner.hide();
					this.ElimCabDevol_Arg = data.data;
				});
				this.modalService.dismissAll();
			}
		})
	}

	AgregarDetDevolucion(content3) {
		this.AgregarDetDevol = {
			id: this.id, Listcod_patrimonial: sessionStorage.getItem("Devol_Cod_Patrimonial"), num_devol: sessionStorage.getItem("NrDev_Act"),
			id_personal: sessionStorage.getItem("Devolucion_Personal"), id_usuario: sessionStorage.getItem("Cod_Usuario")
		}
		swal({
			title: 'Confirma Guardar el Registro de Devolución?',
			text: "",
			type: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Acepta, Guardar!'
		}).then((result) => {
			if (result.value) {
				this.modalService.dismissAll();
				this.nro_devolucion = sessionStorage.getItem("NrDev_Act")
				this.spinner.show();
				
				this.Sinabip.PostAgregarDetDevolucion(this.AgregarDetDevol).subscribe((data: any) => {
					this.spinner.hide();

					if (data.data.error == true){
						if (data.data.reco.hasOwnProperty('id')) { 
							this.router.navigate(['error500']);
							return;
						}else if (data.data.reco.hasOwnProperty('id_personal')) {
							  this.router.navigate(['error500']);
							  return;
						}else if (data.data.reco.hasOwnProperty('id_usuario')) {
							this.router.navigate(['error500']);
							return;
						}
					}

					this.AgregarDetDevol_Arg = data.data;
					// console.log(this.AgregarDetDevol_Arg );
					this.Iniciar_Devol();
				});
				setTimeout(() => {
					this.open_Resumen(content3, this.nro_devolucion);
				}, 1000);
			}
		})
	}

	open_Resumen(content3, nrdevolu_act) {
		this.modalService.open(content3, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
			this.closeResult = `Closed with: ${result}`;
		}, (reason) => {
		});
		this.urlPDF = this.Sinabip.API_URL + "Pdf_ResumenDevolucion/" + this.id + "/" + nrdevolu_act;
	}

	Iniciar_Devol() {
		sessionStorage.setItem('Devol_Cod_Patrimonial', '');
		sessionStorage.setItem('NrDev_Act', '');
		sessionStorage.setItem('Devolucion_Personal', '');

		this.router.navigate(["devolucion-bienes"]);
	}

}
