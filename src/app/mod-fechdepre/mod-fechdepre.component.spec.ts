import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModFechdepreComponent } from './mod-fechdepre.component';

describe('ModFechdepreComponent', () => {
  let component: ModFechdepreComponent;
  let fixture: ComponentFixture<ModFechdepreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModFechdepreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModFechdepreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
