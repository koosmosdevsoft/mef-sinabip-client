import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { SinabipmueblesService } from '../services/sinabipmuebles.service';
import { NgxSpinnerService } from 'ngx-spinner';
// import { NgbModalConfig, NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-mod-fechdepre',
  templateUrl: './mod-fechdepre.component.html',
  styleUrls: ['./mod-fechdepre.component.css']
  ,animations: [routerTransition()]
	// ,providers: [NgbModalConfig, NgbModal]
})
export class ModFechdepreComponent implements OnInit {

	id: string = sessionStorage.getItem("Cod_Entidad");
	idusuario: string = sessionStorage.getItem("Cod_Usuario");
	Listfechapecosa_Arg: Array<any> = [];
	Actualizafechapecosa_Arg: Array<any> = [];
	// ActFechaPecosa_Arg: Array<any> = [];
	// Cantfechapecosa_Arg: Array<any> = [];
	// GuardartipoDepr_Arg: Array<any> = [];
	// closeResult: string;
	// fechpecosa = [];
	// ContActPecosaTotal: number;
	// ContActPecosa: number;
	// IngPecosa: string;
	// Ingcod_patr: string;
	// cantpecosa: number;

  //Paginacion 
	itemsPerPage: number = 20;
	page: any = 1;
	previousPage: any;
  total: any = 0;
  
  filtro = {
		id: this.id, cod_patrimonial: '', denominacion: '', page: this.page, records: this.itemsPerPage
  }  

  actualizadata ={
    id : this.id, cod_patrimonial: '', fecha_pecosa: ''
  }

  constructor(
    private Sinabip: SinabipmueblesService,
	private spinner: NgxSpinnerService,
	private router: Router
    ) {

		let token = sessionStorage.getItem("token");  
		let ruta = sessionStorage.getItem("RutaSinabip")
		if(token === null){
			// window.location.replace(ruta+"SGISBN/System/sinabip.php");
			window.location.replace("./SGISBN/System/sinabip.php");
		}
		this.ListaFechaPecosaActualiza();
    }

  ngOnInit() {
  }

  resetearpag() {
		this.filtro.page = 1;
		this.ListaFechaPecosaActualiza();
  }
  
  resetearlimpiarpag() {
		this.page = 1;
		this.Limpiar_ListadoAsignarPerson();
  }
  
  loadPage(page: number) {
		if (page !== this.previousPage) {
			this.previousPage = page;
			this.ListaFechaPecosaActualiza();
		}
	}

  Limpiar_ListadoAsignarPerson() {
		this.filtro = {
			id: this.id, cod_patrimonial: '', denominacion: '', page: this.page, records: this.itemsPerPage
		}
		this.spinner.show();
		this.Sinabip.PostListaFechaPecosaActualiza(this.filtro).subscribe((data: any) => {
			this.spinner.hide();
			this.Listfechapecosa_Arg = data.data;
			this.total = (this.Listfechapecosa_Arg.length > 0) ? this.Listfechapecosa_Arg[0].TOTAL : 0;
		});
	}

	ListaFechaPecosaActualiza() {
		this.spinner.show();
		this.Sinabip.PostListaFechaPecosaActualiza(this.filtro).subscribe((data: any) => {
			  this.spinner.hide();
			
			if (data.data.error == true){
				if (data.data.reco.hasOwnProperty('id')) { 
					this.router.navigate(['error500']);
					return;
				}else if (data.data.reco.hasOwnProperty('page')) {
					this.router.navigate(['error500']);
					return;
				}else if (data.data.reco.hasOwnProperty('records')) {
					this.router.navigate(['error500']);
					return;
				}
			}
			
			this.Listfechapecosa_Arg = data.data;
			this.total = (this.Listfechapecosa_Arg.length > 0) ? this.Listfechapecosa_Arg[0].TOTAL : 0;
		});
	}

  ActualizaPecosa(CODIGO_PATRIMONIAL, FECHA_1RA_PECOSA){
		swal({
			title: 'Confirmar si se Actualiza la Fecha Pecosa?',
			text: "",
			type: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Acepta, Fecha Pecosa!'
		}).then((result) => {
			if (result.value) {
				FECHA_1RA_PECOSA = '10-05-2020L'
				console.log(FECHA_1RA_PECOSA);
				this.actualizadata ={
				id : this.id, cod_patrimonial: CODIGO_PATRIMONIAL, fecha_pecosa: FECHA_1RA_PECOSA
				}
				this.spinner.show();
				this.Sinabip.PostActualizaPecosa(this.actualizadata).subscribe((data: any) => {
					this.spinner.hide();

					if (data.data.error == true){
						if (data.data.reco.hasOwnProperty('id')) { 
							this.router.navigate(['error500']);
							return;
						}else if (data.data.reco.hasOwnProperty('fecha_pecosa')) {
							swal({
								type: 'error',
								title: 'Validacion de Datos',
								text: data.data.reco.fecha_pecosa[0],
							  })
							  return;
						}
						
					}
					this.Actualizafechapecosa_Arg = data.data;
				});
        	}
      })
    }



}
