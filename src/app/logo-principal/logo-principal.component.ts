import { Component, OnInit,  ElementRef, ViewChild  } from '@angular/core';
import { routerTransition } from '../router.animations';
import { SinabipmueblesService } from '../services/sinabipmuebles.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import {NgbModalConfig, NgbModal, ModalDismissReasons, NgbModule} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-logo-principal', 
  templateUrl: './logo-principal.component.html',   
  styleUrls: ['./logo-principal.component.css'],
  animations: [routerTransition()],
  providers: [NgbModalConfig, NgbModal]  
})
export class LogoPrincipalComponent implements OnInit {   

  //PARA PRUEBAS EN LOCAL COMENTAR 
	 //id: string = '2550'; 
   //id_usuario: string = '9845';  
   RutaSinabip : string;

  //INICIO @1 PARA PRUEBAS EN LOCAL DESCOMENTAR 
	id: string = sessionStorage.getItem("Cod_Entidad");   
	id_usuario: string = sessionStorage.getItem("Cod_Usuario");
  // RutaSinabip : string = sessionStorage.getItem("RutaSinabip");
  //FIN @1

  @ViewChild('divClick') divClick: ElementRef; 
  ValidarEstado_Arg : Array<any> = [];
  closeResult: string; 
  ValidarEstado: string; 
  filtroid = { id : this.id  } 

  constructor(
		config: NgbModalConfig,private Sinabip : SinabipmueblesService,
		private route: ActivatedRoute,
		private router: Router,
		private spinner: NgxSpinnerService,
		private modalService: NgbModal) {

    //INICIO @1 PARA PRUEBAS EN LOCAL DESCOMENTAR  
     //sessionStorage.setItem("Cod_Entidad", this.id);    
     //sessionStorage.setItem("Cod_Usuario", this.id_usuario);   
     
     sessionStorage.setItem("RutaSinabip", 'https://wstest.mineco.gob.pe/');
     this.RutaSinabip = sessionStorage.getItem("RutaSinabip");
    //FIN @1

    let token = sessionStorage.getItem("token");  
    console.log(token)
    //debugger
    //return
    if(token === null){
      //window.location.replace(this.RutaSinabip+"SGISBN/System/sinabip.php");
     // location.href=this.RutaSinabip+"/SGISBN/sinabip.php";
    }

		this.PostValidaDepreciacionestado();
    config.backdrop = 'static';
    config.keyboard = false; 
  }
  
	ngOnInit() {
    setTimeout(() => {
      this.divClick.nativeElement.click();
      }, 1500);
	}

  PostValidaDepreciacionestado(){
    this.spinner.show();
    this.Sinabip.PostValidaDepreciacionestado(this.filtroid).subscribe((data : any) =>{
    this.spinner.hide();
				this.ValidarEstado_Arg = data.data;
				console.log(this.ValidarEstado_Arg)
        this.ValidarEstado = this.ValidarEstado_Arg[0].ESTADO;
    });
  }

  open(content) {
    if(this.ValidarEstado == '1' || this.ValidarEstado == '3') {
      this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
      });
    }else{
      this.modalService.dismissAll();
    }
  }

  ruta_fecha(){
    this.modalService.dismissAll();
    this.router.navigate(["config-depreciacion"]);
  }
  
  ruta_valores(){
    this.modalService.dismissAll();
    this.router.navigate(["mod-valorneto"]);
	}
	
}