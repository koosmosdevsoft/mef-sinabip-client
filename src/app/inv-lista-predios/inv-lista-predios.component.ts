import { Component, OnInit } from '@angular/core';

import { routerTransition } from '../router.animations';
import { SinabipmueblesService } from '../services/sinabipmuebles.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import swal from'sweetalert2';

@Component({
	selector: 'app-inv-lista-predios',
	templateUrl: './inv-lista-predios.component.html',
	styleUrls: ['./inv-lista-predios.component.css'],
	animations: [routerTransition()]
})
export class InvListaPrediosComponent implements OnInit {

	resultados: Array<any> = [];
	habboton;
	BuscarIdpredio: any = [];
	BuscarIdpredio2: any = [];
	regsinbien: Array<any> = [];
	elimAdj: Array<any> = [];
	ListNomAdj_Ar: Array<any> = [];
	id: string = sessionStorage.getItem("Cod_Entidad");
	id_usuario: string = sessionStorage.getItem("Cod_Usuario");
	RutaSinabip : string = sessionStorage.getItem("RutaSinabip");
	EntPredUsuario: string = "";

	afuConfig = {
		uploadAPI: {
			url: sessionStorage.getItem("Adjuntar"),
		},
		hideResetBtn: true,
		uploadBtnText: "Adjuntar Archivo ZIP",
		uploadMsgText: "Archivo ZIP Adjuntado",
		formatsAllowed: ".zip,.ZIP",
		maxSize: 20
	};

	//Paginacion 
	itemsPerPage: number = 10;
	page: any = 1;
	previousPage: any;
	total: any = 0;

	filtro = { id: this.id, predio: '', page: this.page, records: this.itemsPerPage }
	RSBIEN = { id: this.id, idusuario: this.id_usuario, idpredio: '' }
	ListNomAdj = { id: this.id, idpredio: '', idusuario: this.id_usuario }
	mostrar_carga = null;

	constructor(private Sinabip2: SinabipmueblesService,
		private route: ActivatedRoute,
		private router: Router,
		private spinner: NgxSpinnerService) {

		this.id = sessionStorage.getItem("Cod_Entidad");
		this.id_usuario = sessionStorage.getItem("Cod_Usuario");

		this.buscar_predio();
		this.HabilitarBotonInvFinal(this.id);
		// subir zip
		this.afuConfig.uploadAPI.url = sessionStorage.getItem("Adjuntar");
	}

	ngOnInit() {
	}

	SubirZip(event) {
		console.log(event.response);
		if(event.response === "true"){
			this.buscar_predio();
			this.mostrar_carga.estado = '5';
			this.nuevo(this.mostrar_carga.predio, this.mostrar_carga.estado);
		}else{
			swal({
				title: 'Aviso!! : Archivo ZIP no corresponde al formato actual, favor de descarga Excel actualizado para subir archivo ZIP.',
				text: "",
				type: 'info'
			});
			this.desabilitar_ventana();
		}
	}

	buscar_predio() {
		this.spinner.show();
		this.Sinabip2.PostListadoPrediosInvEntidad(this.filtro).subscribe((data: any) => {
			this.spinner.hide();
			this.resultados = data.data;
			this.total = (this.resultados.length > 0) ? this.resultados[0].Total : 0; // this.resultados[0].Total;
		});
	}

	loadPage(page: number) {
		if (page !== this.previousPage) {
			this.previousPage = page;
			this.buscar_predio();
		}
	}

	Quitar_Filtro_Busc_Local(id_ini) {
		this.filtro = { id: id_ini, predio: '', page: this.page, records: this.itemsPerPage }
		this.spinner.show();
		this.Sinabip2.PostListadoPrediosInvEntidad(this.filtro).subscribe((data: any) => {
			this.spinner.hide();
			this.resultados = data.data;
			this.total = (this.resultados.length > 0) ? this.resultados[0].Total : 0; // this.resultados[0].Total;
		});
		this.desabilitar_ventana();
	}


	resetearpag() {
		this.filtro.page = 1;
		this.buscar_predio();
	}

	resetearlimpiarpag() {
		this.page = 1;
		this.Quitar_Filtro_Busc_Local(this.id);
	}

	


	desabilitar_ventana() {
		this.mostrar_carga = null;
	}

	async nuevo(predio, estado) {
		this.mostrar_carga = {
			predio: predio, estado: estado
		}
		this.ListadoPadronPredios_Id(predio).then((data: any) => {
			this.BuscarIdpredio = data.data;
			// EN PROCESO - CARGADOS - ADJUNTADO
			if (estado == 1 || estado == 3 || estado == 5) {
				this.ListadoNombreAdjunto(this.BuscarIdpredio.COD_ENTIDAD, this.BuscarIdpredio.ID_PREDIO_SBN, this.id_usuario);
			}
		});
	}

	adjuntar(predio, cod_entidad) {
		this.EntPredUsuario = this.Sinabip2.API_URL + "SubirZip/" + cod_entidad + "-" + predio + "-" + this.id_usuario;
		sessionStorage.setItem("Adjuntar", this.EntPredUsuario);
		this.afuConfig.uploadAPI.url = this.EntPredUsuario;
	}

	HabilitarBotonInvFinal(id) {
		this.Sinabip2.getHabilitarBotonInv(id).subscribe((data: any) => {
			this.habboton = data.data;
		});
	}

	ListadoPadronPredios_Id(idpredio) {
		return this.Sinabip2.getListadoPadronPredios_Id(idpredio).toPromise();
	}

	RegistroInvSinBienes(id_ini, idusuario_ini, idpredio_ini) {
		swal({
			title: 'Confirma Registrar este Local y/o Predio sin bienes muebles?',
			text: "",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Acepta, Confirmación!'
		}).then((result) => {
			if (result.value) {
				this.RSBIEN = {
					id: id_ini,
					idusuario: idusuario_ini,
					idpredio: idpredio_ini
				}
				this.resultados.forEach(element => {
					if (this.mostrar_carga.predio == element.ID_PREDIO_SBN) {
						element.ID_ESTADO = 3;
						element.ESTADO = "CARGADOS";
					}
				});
				this.desabilitar_ventana();
				this.Sinabip2.PostRegistroInvSinBienes(this.RSBIEN).subscribe((data: any) => {
					this.regsinbien = data.data;
				});
			}
		})
	}

	ListadoNombreAdjunto(id_ini, idpredio_ini, idusuario_ini) {
		this.ListNomAdj = {
			id: id_ini, idpredio: idpredio_ini, idusuario: idusuario_ini
		}
		this.Sinabip2.PostListadoNombreAdjunto(this.ListNomAdj).subscribe((data: any) => {
			this.ListNomAdj_Ar = data.data;
		});
	}

	EliminarAdjunto() {
		swal({
			title: 'Confirmar si se desea eliminar archivo Adjunto?',
			text: "",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Acepta, Confirmación!'
		}).then((result) => {
			this.resultados.forEach(element => {
				if (this.mostrar_carga.predio == element.ID_PREDIO_SBN) {
					element.ID_ESTADO = 0;
					element.TOTAL_REG_FINALIZADOS = 0;
					element.ESTADO = "PENDIENTE";
				}
			});
			this.desabilitar_ventana();
			this.Sinabip2.PostEliminarAdjunto(this.ListNomAdj).subscribe((data: any) => {
				this.elimAdj = data.data;
			});
		})

	}

	EliminarAdjuntoFinal(idpredio) {
		swal({
			title: 'Confirmar si se desea eliminar archivo Adjunto?',
			text: "",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Acepta, Confirmación!'
		}).then((result) => {
			if (result.value) {
				this.resultados.forEach(element => {
					if (idpredio == element.ID_PREDIO_SBN) {
						element.ID_ESTADO = 0;
						element.TOTAL_REG_FINALIZADOS = 0;
						element.ESTADO = "PENDIENTE";
					}
				});
				this.desabilitar_ventana();
				this.ListNomAdj.idpredio = idpredio;
				this.Sinabip2.PostEliminarAdjunto(this.ListNomAdj).subscribe((data: any) => {
					this.elimAdj = data.data;
				});
			}
		})
	}

	EnviarAdjunto() {
		swal({
			title: 'Confirmar si se desea Enviar archivo Adjunto?',
			text: "",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Acepta, Confirmación!'
		}).then((result) => {
			if (result.value) {
				this.resultados.forEach(element => {
					if (this.mostrar_carga.predio == element.ID_PREDIO_SBN) {
						element.ID_ESTADO = 1;
						element.ESTADO = "EN PROCESO";
					}
				});
				this.Sinabip2.PostEnviarAdjunto(this.ListNomAdj).subscribe((data: any) => {
					this.elimAdj = data.data;
				});
				this.desabilitar_ventana();
				swal(
					'Aviso!!',
					'EL PROCESO DE CARGA TIENE UN TIEMPO APROX. DE 30 MINUTOS A MAS PARA PROCESAR LA INFORMACIÓN!',
					'info'
				)
			}
		})
	}

	ver() {
		this.router.navigate(["inv-resumen"]);
	}

	// verificaNombre(event){
	// 	console.log(event);
	// }
}
