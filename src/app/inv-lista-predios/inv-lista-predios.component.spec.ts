import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvListaPrediosComponent } from './inv-lista-predios.component';

describe('InvListaPrediosComponent', () => {
  let component: InvListaPrediosComponent;
  let fixture: ComponentFixture<InvListaPrediosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvListaPrediosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvListaPrediosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
