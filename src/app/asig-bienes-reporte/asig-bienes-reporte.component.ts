import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { SinabipmueblesService } from '../services/sinabipmuebles.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgbModalConfig, NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';


@Component({
  selector: 'app-asig-bienes-reporte',
  templateUrl: './asig-bienes-reporte.component.html',
  styleUrls: ['./asig-bienes-reporte.component.css'],
	animations: [routerTransition()],
	providers: [NgbModalConfig, NgbModal]
})
export class AsigBienesReporteComponent implements OnInit {
  ListBienAsignar_Arg: Array<any> = [];
  ListBienAsignardet_Arg: Array<any> = [];
	// id : string = '2550';
	// idusuario : string = '9610';
  id: string = sessionStorage.getItem("Cod_Entidad");
  idusuario: string = sessionStorage.getItem("Cod_Usuario");
  NomPersonal: string;

	mostrar_carga = null;
  //Paginacion 
	itemsPerPage: number = 20;
	page: any = 1;
	previousPage: any;
  total: any = 0;
  
  filtro = {
		id: this.id, cod_patrimonial: '', denominacion: '', fecha_adquis: '', 
		nrodocument: '', nombre: '', apellidoP: '', apellidoM: '', predio: '', area: '', page: this.page, records: this.itemsPerPage
  }
  
  filtro2 = {
		id: this.id, cod_pers: '', page: this.page, records: this.itemsPerPage
	}

  constructor(config: NgbModalConfig, private Sinabip: SinabipmueblesService,
		private route: ActivatedRoute,
		private router: Router,
		private spinner: NgxSpinnerService,
		private modalService: NgbModal) { 
			let token = sessionStorage.getItem("token");  
			let ruta = sessionStorage.getItem("RutaSinabip")
			if(token === null){
				// window.location.replace(ruta+"SGISBN/System/sinabip.php");
				window.location.replace("./SGISBN/System/sinabip.php");
			}
      sessionStorage.setItem("Cod_Entidad", this.id);
      sessionStorage.setItem("Cod_Usuario", this.idusuario);
      this.ListadoAsignarPersonRep()
    }

  ngOnInit() {

  }

  ListadoAsignarPersonRep() {
    this.mostrar_carga = null;
		this.spinner.show();
		this.Sinabip.postListadoAsignarPersonReporte(this.filtro).subscribe((data: any) => {
			this.spinner.hide();

			if (data.data.error == true){
				if (data.data.reco.hasOwnProperty('id')) { 
					this.router.navigate(['error500']);
					return;
				}else if (data.data.reco.hasOwnProperty('fecha_adquis')) {
					swal({
						type: 'error',
						title: 'Validacion de Datos',
						text: data.data.reco.fecha_adquis[0],
					  })
					  return;
				}else if (data.data.reco.hasOwnProperty('page')) {
					this.router.navigate(['error500']);
					return;
				}else if (data.data.reco.hasOwnProperty('records')) {
					this.router.navigate(['error500']);
					return;
				}
				
			}

			this.ListBienAsignar_Arg = data.data;
			this.total = (this.ListBienAsignar_Arg.length > 0) ? this.ListBienAsignar_Arg[0].Total : 0;
		});
  }
  
  resetearpag() {
		this.filtro.page = 1;
		this.ListadoAsignarPersonRep();
  }
  
  loadPage(page: number) {
		if (page !== this.previousPage) {
			this.previousPage = page;
			this.ListadoAsignarPersonRep();
		}
  }
  
  resetearlimpiarpag() {
	this.page = 1;
	this.Limpiar_AsignarPersonRep();
  }

  Limpiar_AsignarPersonRep() {
    this.mostrar_carga = null;
		this. filtro = {
      id: this.id, cod_patrimonial: '', denominacion: '', fecha_adquis: '', 
      nrodocument: '', nombre: '', apellidoP: '', apellidoM: '', predio: '', area: '', page: this.page, records: this.itemsPerPage
    }
		this.spinner.show();
		this.Sinabip.postListadoAsignarPersonReporte(this.filtro).subscribe((data: any) => {
			this.spinner.hide();
			this.ListBienAsignar_Arg = data.data;
		});
  }
  
	nuevo(estado) {
		this.mostrar_carga = {
			personal: '', estado: estado
		}
	}

  ListadoAsignarPersonRepDet(cod_personal, nom_personal) {
    this.NomPersonal = nom_personal;
    this.filtro2 = {
      id: this.id, cod_pers: cod_personal, page: this.page, records: this.itemsPerPage
    }    
		this.spinner.show();
		this.Sinabip.postListadoAsignarPersonReporteDet(this.filtro2).subscribe((data: any) => {
			this.spinner.hide();
			this.ListBienAsignardet_Arg = data.data;
			//this.total = (this.ListBienAsignardet_Arg.length > 0) ? this.ListBienAsignardet_Arg[0].Total : 0;
		});
  }

  desabilitar_ventana() {
		this.mostrar_carga = null;
  }
  
 

}
