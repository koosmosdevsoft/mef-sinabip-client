import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsigBienesReporteComponent } from './asig-bienes-reporte.component';

describe('AsigBienesReporteComponent', () => {
  let component: AsigBienesReporteComponent;
  let fixture: ComponentFixture<AsigBienesReporteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsigBienesReporteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsigBienesReporteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
