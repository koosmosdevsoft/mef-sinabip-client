import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { SinabipmueblesService } from '../services/sinabipmuebles.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgbModalConfig, NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import swal from 'sweetalert2';


@Component({
  selector: 'app-ficha-datos-reporte',
  templateUrl: './ficha-datos-reporte.component.html',
  styleUrls: ['./ficha-datos-reporte.component.css'],
	animations: [routerTransition()],
	providers: [NgbModalConfig, NgbModal]
})
export class FichaDatosReporteComponent implements OnInit {

	id: string = sessionStorage.getItem("Cod_Entidad");
	idusuario: string = sessionStorage.getItem("Cod_Usuario");
	RutaSinabip : string = sessionStorage.getItem("RutaSinabip");

	Cod_Entidad : string = this.id;
	Cod_Usuario : string = this.idusuario;
	ListGrupo_Arg: Array<any> = [];
	ListClase_Arg: Array<any> = [];
	Listsituacionactual_Arg: Array<any> = [];
	Listestadobien_Arg: Array<any> = [];
	Listtipobien_Arg: Array<any> = [];
	Listfichatecnica_Arg: Array<any> = [];

	//Paginacion 
	// itemsPerPage2: number = 20;
	// page: any = 1;
	// previousPage: any;
	// total: any = 0;

	cod_patrimonial: string;
	denominacion: string;
	cod_grupo: string;
	cod_clase: string;
	estado_bien: string;
	cod_tipo_mov: string;
	itemsPerPage: string= '200';

	Pdf_descarga: string;
	dataDescargas : Array<any> = [];

	filtro = {
		id: this.id, cod_patrimonial: '', denominacion: '', cod_grupo: '', cod_clase: '',
		estado_bien: '', cod_tipo_mov: ''
		// , page: this.page, records: this.itemsPerPage
	}

	// filtro2 = {
	// 	id: this.id, cod_patrimonial: '', denominacion: '', cod_grupo: '', cod_clase: '',
	// 	estado_bien: '', cod_tipo_mov: ''
	// 	// , page: this.page, records: this.itemsPerPage
	// }

  constructor(
	  	public Sinabip: SinabipmueblesService,
		private spinner: NgxSpinnerService,
		private router: Router
		) { 
			let token = sessionStorage.getItem("token");  
			let ruta = sessionStorage.getItem("RutaSinabip")
			if(token === null){
				// window.location.replace(ruta+"SGISBN/System/sinabip.php");
				window.location.replace("./SGISBN/System/sinabip.php");
			}
		this.ListadoGrupo();
		this.ListadoEstadoBien();
		this.ListadoTipoMotivo();
		this.Actualizar_PDF();
		}

  ngOnInit() {
	}
	
	ListadoGrupo() {
		this.spinner.show();
		this.Sinabip.GetListadoGrupo().subscribe((data: any) => {
			this.spinner.hide();
			this.ListGrupo_Arg = data.data;
		});
	}
	
	ListadoClase(COD_GRUPO) {
		this.filtro.cod_clase = '';
		if (COD_GRUPO == '') {
			COD_GRUPO = 0;
		}
		this.ListadoGrupo();
		this.spinner.show();
		this.Sinabip.GetListadoClase(COD_GRUPO).subscribe((data: any) => {
			this.spinner.hide();
			this.ListClase_Arg = data.data;
		});
	}
	
	Actualizar_PDF() {
		this.cod_patrimonial = this.filtro.cod_patrimonial;
		this.denominacion = this.filtro.denominacion;
		this.cod_grupo = this.filtro.cod_grupo;
		this.cod_clase = this.filtro.cod_clase;
		this.estado_bien = this.filtro.estado_bien;
		this.cod_tipo_mov = this.filtro.cod_tipo_mov;

		if (this.cod_patrimonial == '') { this.cod_patrimonial = '-1' }
		if (this.denominacion == '') { this.denominacion = '-1' }
		if (this.cod_grupo == '') { this.cod_grupo = '-1' }
		if (this.cod_clase == '') { this.cod_clase = '-1' }
		if (this.estado_bien == '') { this.estado_bien = '-1' }
		if (this.cod_tipo_mov == '') { this.cod_tipo_mov = '-1' }

		this.Pdf_descarga = this.cod_patrimonial + '.' + this.denominacion + '.' + this.cod_grupo + '.' + this.cod_clase + '.' + this.estado_bien + '.' + this.cod_tipo_mov + '.' + this.itemsPerPage ;
		this.Pdf_descarga = btoa(this.Pdf_descarga);
		
		this.spinner.show();
		this.Sinabip.GetFichaTecnicaContar(this.id,this.Pdf_descarga).subscribe((data: any) => {
			this.spinner.hide();

			if (data.data.error == true){
				if (data.data.reco.hasOwnProperty('id')) { 
					this.router.navigate(['error500']);
					return;
				}
			}
			
			let reg = data.data.GrupoDescargas[0].NRO_GRUPO;
			this.dataDescargas = [];
			for (let index = 0; index < reg; index++) {
				this.dataDescargas.push({
					index : index
				});
			}
		});
	}

	ListadoSituacionActual() {
		this.Actualizar_PDF();
	}
	
	Quitar_Filtro() {
		this.filtro = {
			id: this.id, cod_patrimonial: '', denominacion: '', cod_grupo: '', cod_clase: '',
			estado_bien: '', cod_tipo_mov: ''
		}
		this.Actualizar_PDF();
	}

	ListadoEstadoBien() {
		this.spinner.show();
		this.Sinabip.GetListadoEstadoBien().subscribe((data: any) => {
			this.spinner.hide();
			this.Listestadobien_Arg = data.data;
		});
	}

	ListadoTipoMotivo() {
		this.spinner.show();
		this.Sinabip.GetListadoTipoMotivo().subscribe((data: any) => {
			this.spinner.hide();
			this.Listtipobien_Arg = data.data;
		});
	}


}
