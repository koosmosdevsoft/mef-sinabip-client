import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FichaDatosReporteComponent } from './ficha-datos-reporte.component';

describe('FichaDatosReporteComponent', () => {
  let component: FichaDatosReporteComponent;
  let fixture: ComponentFixture<FichaDatosReporteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FichaDatosReporteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FichaDatosReporteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
