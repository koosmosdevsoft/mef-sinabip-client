import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvPrincipalComponent } from './inv-principal.component';

describe('InvPrincipalComponent', () => {
  let component: InvPrincipalComponent;
  let fixture: ComponentFixture<InvPrincipalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvPrincipalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvPrincipalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
