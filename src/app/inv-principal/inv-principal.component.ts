import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { SinabipmueblesService } from '../services/sinabipmuebles.service';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';

@Component({
	selector: 'app-inv-principal',
	templateUrl: './inv-principal.component.html',
	styleUrls: ['./inv-principal.component.css'],
	animations: [routerTransition()]
})
export class InvPrincipalComponent implements OnInit {
	public alerts: Array<any> = [];
	public sliders: Array<any> = [];
	historicos: Array<any> = [];
	InvActual: Array<any> = [];
	TotalActivo: Array<any> = [];
	ValidaTotalActivo: Array<any> = [];
	EstFinal_Arg;
	//   id : string = '2550';
	//   idusuario : string = '464890';
	id: string = sessionStorage.getItem("Cod_Entidad");
	idusuario: string = sessionStorage.getItem("Cod_Usuario");
	RutaSinabip : string = sessionStorage.getItem("RutaSinabip");
	Periodo: string;
	Cod_Inv: string;

	EstFinal = { id: this.id, cod_inv: this.Cod_Inv }
	id_valida = { id: this.id }

	constructor(private Sinabip: SinabipmueblesService,
		private router: Router) {

			let token = sessionStorage.getItem("token");  
			let ruta = sessionStorage.getItem("RutaSinabip")
			if(token === null){
				// window.location.replace(ruta+"SGISBN/System/sinabip.php");
				window.location.replace("./SGISBN/System/sinabip.php");
			}
		this.sliders.push(
			{
				imagePath: 'assets/images/slider1.jpg',
				label: 'First slide label',
				text:
					'Nulla vitae elit libero, a pharetra augue mollis interdum.'
			},
			{
				imagePath: 'assets/images/slider2.jpg',
				label: 'Second slide label',
				text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
			},
			{
				imagePath: 'assets/images/slider3.jpg',
				label: 'Third slide label',
				text:
					'Praesent commodo cursus magna, vel scelerisque nisl consectetur.'
			}
		);

		this.alerts.push(
			{
				id: 1,
				type: 'success',
				message: `Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            Voluptates est animi quibusdam praesentium quam, et perspiciatis,
            consectetur velit culpa molestias dignissimos
            voluptatum veritatis quod aliquam! Rerum placeat necessitatibus, vitae dolorum`
			},
			{
				id: 2,
				type: 'warning',
				message: `Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            Voluptates est animi quibusdam praesentium quam, et perspiciatis,
            consectetur velit culpa molestias dignissimos
            voluptatum veritatis quod aliquam! Rerum placeat necessitatibus, vitae dolorum`
			}
		);

		// this.id = '256';
		// this.idusuario = '8267';
		// sessionStorage.setItem("Cod_Entidad", this.id);
		// sessionStorage.setItem("Cod_Usuario", this.idusuario);
		
		this.cargarDatos(this.id);
		this.ListadoHabilitadoInv(this.id);
		this.TotalInvActivo(this.id);
	}
 
	ngOnInit() {
		this.EstadoFinalInv();
	}

	cargarDatos(id) {
		this.Sinabip.getListadoHistoricoInv(id).subscribe((data: any) => {

			if (data.data.error == true){
				if (data.data.reco.hasOwnProperty('id')) { 
					this.router.navigate(['error500']);
					  return;
				}
			}

			this.historicos = data.data;
		});
	}

	ListadoHabilitadoInv(id) {
		this.Sinabip.getListadoHabilitadoInv(id).subscribe((data: any) => {
			this.InvActual = data.data;
			this.Periodo = this.InvActual[0].PERIODO;
			this.Cod_Inv = this.InvActual[0].COD_INVENTARIO;
			sessionStorage.setItem("Periodo", this.Periodo);
			sessionStorage.setItem("Cod_Inv", this.Cod_Inv);
		});
	}

	EstadoFinalInv() {
		this.Cod_Inv = sessionStorage.getItem("Cod_Inv");
		this.EstFinal = { id: this.id, cod_inv: this.Cod_Inv }
		this.Sinabip.PostEstadoFinalInv(this.EstFinal).subscribe((data: any) => {

			if (data.data.error == true){
				
				if (data.data.reco.hasOwnProperty('id')) { 
					this.router.navigate(['error500']);
					return;
				}else if (data.data.reco.hasOwnProperty('cod_inv')) {
					  this.router.navigate(['error500']);
					  return;
				}
				
			}
			this.EstFinal_Arg = data.data;
		});
	}

	public closeAlert(alert: any) {
		const index: number = this.alerts.indexOf(alert);
		this.alerts.splice(index, 1);
	}

	TotalInvActivo(id) {
		this.Sinabip.getTotalInvActivos(id).subscribe((data: any) => {
			this.TotalActivo = data.data;
		});
	}

	ValidaResponsablePredio(TotalActivo) {
		this.id_valida = { id: this.id }
		this.Sinabip.PostValidaResponsablePredio(this.id_valida).subscribe((data: any) => {

			if (data.data.error == true){
				if (data.data.reco.hasOwnProperty('id')) { 
					this.router.navigate(['error500']);
					return;
				}
			}

			this.ValidaTotalActivo = data.data;
			if (this.ValidaTotalActivo[0].ESTADO == '1' || this.ValidaTotalActivo[0].ESTADO == '2' || this.ValidaTotalActivo[0].ESTADO == '3') {
				swal({
					position: 'center',
					type: 'warning',
					title: this.ValidaTotalActivo[0].MENSJ + '!!!',
					showConfirmButton: false,
					timer: 4000
				})
			} else if (this.ValidaTotalActivo[0].ESTADO == '4') {
				this.ver(TotalActivo)
			}
		});

	}

	ver(TotalActivo) {
		switch (TotalActivo.Ruta) {
			case 'inv-lista-predios':
				this.router.navigate(["inv-lista-predios"]);
				break;
			case 'inv-resumen':
				this.router.navigate(["inv-resumen"]);
				break;
		}
	}

}
