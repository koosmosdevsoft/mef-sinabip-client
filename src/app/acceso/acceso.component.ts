import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SinabipmueblesService } from '../services/sinabipmuebles.service';

@Component({
  selector: 'app-acceso',
  templateUrl: './acceso.component.html',
  styleUrls: ['./acceso.component.css']
})
export class AccesoComponent implements OnInit {

  RutaSinabip : string; 
  constructor(
    private route: ActivatedRoute
    ,private Sinabip : SinabipmueblesService,
    private router: Router) {
      
    }
    
    ngOnInit() {
      if(sessionStorage.getItem("Cod_Entidad")!==null ){
        sessionStorage.setItem("Cod_Entidad", null); 
      }
      if(sessionStorage.getItem("Cod_Usuario")!==null ){
        sessionStorage.setItem("Cod_Usuario", null);
      }
      
      
      let id = this.route.snapshot.paramMap.get('id'); 
      //let token = this.route.snapshot.paramMap.get('token'); 
      console.log(id);
      let token =  id.split('__');
      console.log(token);
      console.log(token[0]);
      console.log(token[1]);
      let cods = this.b64_to_utf8(token[1]);
      let variables = cods.split('-');
      console.log(variables);
       
      //return;
      ////alert('llegojhjh...');
      sessionStorage.setItem("RutaSinabip", 'https://wstest.mineco.gob.pe/');  
      if(variables.length> 0 && variables[2] == 'SINASINA' || token != null){
        
        sessionStorage.setItem("Cod_Entidad", variables[0]); 
        sessionStorage.setItem("Cod_Usuario", variables[1]);
        sessionStorage.setItem("token", token[0]); 
        //debugger
        this.router.navigate(['/']); 
        /*this.Sinabip.validaToken(token).subscribe((data : any) =>{
          if(data.data.status){
            this.router.navigate(['/']); 
          }else{
            this.RutaSinabip = sessionStorage.getItem("RutaSinabip");
            //location.href="./SGISBN/sinabip.php";
            //window.location.replace(this.RutaSinabip+"SGISBN/System/sinabip.php");
          }
        });*/

      }else{
        this.RutaSinabip = sessionStorage.getItem("RutaSinabip");
       // window.location.replace(this.RutaSinabip+"SGISBN/System/sinabip.php");
      }

    }
    


  b64_to_utf8( str ) {
    let b = window.atob( str );
    let a = escape(b);
    return decodeURIComponent(a);
  }

}
