import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActosadquisicionComponent } from './actosadquisicion.component';

describe('ActosadquisicionComponent', () => {
  let component: ActosadquisicionComponent;
  let fixture: ComponentFixture<ActosadquisicionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActosadquisicionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActosadquisicionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
