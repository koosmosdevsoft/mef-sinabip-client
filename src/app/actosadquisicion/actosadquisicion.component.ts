import { Component, OnInit, Directive } from '@angular/core';
import { routerTransition } from '../router.animations';
import { SinabipmueblesService } from '../services/sinabipmuebles.service';
//import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgbModalConfig, NgbModal, ModalDismissReasons, NgbModule} from '@ng-bootstrap/ng-bootstrap';
//import {NgbDatepickerI18n, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
//import { ModalService } from '../_services'; 
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators, NgControl } from '@angular/forms';
import swal from'sweetalert2';
import { DatePipe } from '@angular/common'; 

//import { parse } from 'path';


@Component({  
  selector: 'app-actosadquisicion', 
  templateUrl: './actosadquisicion.component.html', 
  styleUrls: ['./actosadquisicion.component.css'],  
  animations: [routerTransition()],
  providers: [NgbModalConfig, NgbModal, DatePipe]
  
})



export class ActosadquisicionComponent implements OnInit {
  registerForm: FormGroup;
  RegistroTipoBienForm: FormGroup;
  submitted = false;

  seleccionRows : string = null;
  
  
resultado : void;
titularAlerta:string='';
 titulo  : string = 'Bienvenidos';   
 dataActosAdquisicion : any = [];
 dataAnio = [];
 dataMes = [];
 dataFormaIndividual : any = [];
 databienesEliminados : Array<any> = [];
 dataAdjuntado : any = [];
 dataEditarActos : any = [];
 dataEliminarBien : any = [];
 dataAgregarBien : any = [];
 dataGuardarBien : any = [];
 dataValidacionCaracter : any = [];
 mostrarVentanaTipoRegistro : boolean = false;
 mostrarVentanaRegistroIndividual : boolean = false;
 mostrarVentanaRegistroMasivo : boolean = false;
 closeResult: string;
 seleccionBien1 : boolean = false;
 seleccionBien2 : boolean = false;
 b_ventanaObservacionesValidacion : boolean = false;
 mostrarVentanaCaracteristicas : boolean = false;
 ResultadoGrabar : number = 0;
 TipoagregarBien    : string = "";
 id_altas : any = [];
 dataCaracteristicasBien : Array<any> = [];
 dataObservacionValidacion : Array<any> = [];
 dataresultado : Array<any> = [];
 cantidadduplicado_actos: number = 0;
 cantidadduplicado_bienes: number = 0;
 cantidaderrores_columnas: number = 0;
 modificando_registro: string = '';

 Cod_Entidad : string = sessionStorage.getItem("Cod_Entidad");
 Cod_Usuario : string = sessionStorage.getItem("Cod_Usuario");
 RutaSinabip : string = sessionStorage.getItem("RutaSinabip");

 afuConfig = {
  uploadAPI: {
      url:this.SinabipMuebles.API_URL+"SubirAltaTxt/"+this.Cod_Entidad,
    },
    hideResetBtn: true,
    uploadBtnText:"Adjuntar Archivo",
    uploadMsgText: "",
    formatsAllowed: ".TXT,.txt"
};

heroForm : any = [];

//Paginacion 
itemsPerPage: number = 20;
page: any = 1;
previousPage: any;
total : any = 0; 
 
V_COD_UE_BIEN_PATRI: string;


 filtro = {
    fecha : 
    {
      month : 10,
      year  : 2018
    },
  cod_entidad       : this.Cod_Entidad,
  forma_adquisicion : 1,
  nro_documento     : '',
  estado            : '1',
  page : this.page, records : this.itemsPerPage
}

parametros = { 
  cod_entidad   : this.Cod_Entidad,
  nro_grupo     : "-1",
  nro_clase     : "",
  denom_bien    : "",
  nro_bien      : "",
  cod_patrimonial : "",
  denom_patrimonial : "",
  page : this.page, records : this.itemsPerPage
} 
modal;
Tipobien = {
  codigo        : "",
  denominacion  : ""
}

paramBusqueda = { 
  id_entidad : this.Cod_Entidad,
  id_acto    : ""
}


Busquedaserie_Arg: any = [];

paramBusquedaserie = { 
  id_entidad : this.Cod_Entidad,
  nroSerie   : "",
  cod_bien   : ""
}

paramEliminarBien = { 
  id_entidad  : this.Cod_Entidad,
  id_bien     : "",
  id_usuario  : ""
}

paramAgregar = {
        TipoagregarBien   : "",
        id_acto           : "",
        id_entidad        : this.Cod_Entidad,
        id_forma          : undefined,
        nro_documento     : "",
        fecha_documento   : "",
        usua_creacion     : this.Cod_Usuario,
        codigo_patrimonial: "",
        denominacion_bien : "",
        cantidad          : "",
        valor_adq         : "",
        estado            : ""
}

paramCarga = {
      id_entidad    : '',
      id_adjuntado  : '',
      nombreArchivo : '',
      usua_creacion : ''
}

estado_adjuntar : string = "";
estado_carga : string = "";
estado_carga_fecha : string = "";
estado_carga_total : string = "";
estado_validacion : string ="";
estado_validacion_fecha : string ="";
estado_validacion_total : string ="";
estado_finalizacion : string ="";
estado_finalizacion_fecha : string ="";
estado_finalizacion_total : string ="";

flag_SeleccionCatalogo : number = 0;

paramBien = {
        id_entidad  : this.Cod_Entidad,      
        id_bien     : "",
        formaAdquis : undefined,
        nroDocAdquis: "",
        fechaAdquis : "",
        codigopatri : "",
        denomBien   : "",
        marca       : "",
        modelo      : "",
        tipo        : "",
        color       : "",
        nroSerie    : "",
        dimension   : "",
        placa_matri : "",
        nroMotor    : "",
        nroChasis   : "",
        anioFabric  : "",
        raza        : "",
        especie     : "",
        edad        : "",
        otrasCaract : "",
        usoCta      : "P",
        TipoCta     : "",
        CtaContable : "",
        valorAdquis : "",
        porcDeprec  : "",
        asegurado   : "",
        estadoBien  : "",
        observacion : "",
        DEPRECIACION : "",
        XMARCA: "",
        XMODELO: "",
        XTIPO: "",
        XCOLOR: "",
        XSERIE: "",
        XDIMENSION: "",
        XPLACA: "",
        XMOTOR: "",
        XCHASIS: "",
        XANIO_FABRICACION: "",
        XRAZA: "",
        XESPECIE: "",
        XEDAD: "",
        XOTRAS_CARACTERISTICAS: ""

}

paramGuardar = {
        id_acto         : "",
        id_entidad      : "",
        id_forma        : "",
        nro_documento   : "",
        fecha_documento : "",
        usua_modifica   : ""
}

paramEliminarActo = {
    id_entidad   : "",
    id_acto      : ""
}


model:any = {};

id : string="1";

  constructor(
    public datepipe: DatePipe,
    private formBuilder: FormBuilder,
    private SinabipMuebles : SinabipmueblesService, 
    private spinner: NgxSpinnerService,
    private modalService: NgbModal,
    private router: Router


    ) 

    { 
      let token = sessionStorage.getItem("token");  
      if(token === null){
        // window.location.replace(this.RutaSinabip+"SGISBN/System/sinabip.php");
        window.location.replace("./SGISBN/System/sinabip.php");
      }
      let date = new Date(); 
      date.setDate( date.getDate() - 11 );
      let year  = date.getFullYear();
      let month = date.getMonth()+1;

      this.filtro.fecha.month = month;
      this.filtro.fecha.year  = year;
      this.afuConfig.uploadAPI.url = this.SinabipMuebles.API_URL+"AdjuntarAltatxt/"+this.Cod_Entidad;


    }

  ngOnInit(): void {

    this.registerForm = this.formBuilder.group({ 
      documentoActo: ['', Validators.required],
      formaAdquisicion: ['', Validators.required],
      fechaAdquisicion: ['', [Validators.required]],

      
      
      //fechaAdquisicion: ['', [Validators.required, Validators.email]],
      //password: ['', [Validators.required, Validators.minLength(6)]]
    });

  

    
    


    this.RegistroTipoBienForm = this.formBuilder.group({ 
      cantidad: ['', Validators.required],
      valorAdquisicion: ['', Validators.required]
    });

    this.filtro.estado = '-1';
    this.filtro.fecha.month = -1;
    this.filtro.forma_adquisicion = -1;

    let fechita = new Date();
    let yy = fechita.getFullYear();
    this.filtro.fecha.year = yy;
    this.cargarListadoActosAdquisicion();
    

  }

  get f() { return this.registerForm.controls; } 

  
  onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.registerForm.invalid) { 
          swal({
            type: 'error',
            title: 'Datos Incompletos!!!',
            text: 'No se ha ingresado la información necesaria para continuar',
            // footer: '<a href>Why do I have this issue?</a>'
          })
            return;
        }
        else
        {
          this.Guardar_Actos();
        }
        
        
    }


    
  
  onSubmit2() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) { 
      swal({
        type: 'error',
        title: 'Datos Incompletosss!!!',
        text: 'No se ha ingresado la información necesaria para continuar',
        // footer: '<a href>Why do I have this issue?</a>'
      })
        return;
    }
    else
    {
      // this.Guardar_Actos();
    }
    
    
}


  SubirZip(){
    // this.buscar_predio(); 
    // this.mostrar_carga.estado = '5';
    // this.nuevo(this.mostrar_carga.predio,this.mostrar_carga.estado);
  }
  AdjuntarAltatxt(event){
    
    this.estado_adjuntar == '';
    this.estado_carga = '';
    this.estado_validacion = '';
    this.estado_finalizacion = '';
    this.estado_carga_total = '';
    this.estado_carga_fecha = '';
    this.estado_validacion_total = '';
    this.estado_validacion_fecha = '';
    this.estado_finalizacion_total = '';
    this.estado_finalizacion_fecha = '';

    this.afuConfig.uploadAPI.url = this.SinabipMuebles.API_URL+"AdjuntarAltatxt/"+this.Cod_Entidad;
    this.dataAdjuntado = JSON.parse(event.response); 
    this.estado_adjuntar = this.dataAdjuntado.data.RESULTADO;

    
    
  }

  get name() { return this.heroForm.get('name'); }

  get power() { return this.heroForm.get('power'); }

  ventanaTipoRegistro(){
    this.mostrarVentanaTipoRegistro = true; 
    this.mostrarVentanaRegistroIndividual = false; 
    this.b_ventanaObservacionesValidacion = false;
    this.paramAgregar.id_forma = undefined;

    this.paramAgregar = {
      TipoagregarBien   : "",
      id_acto           : "",
      id_entidad        : "",
      id_forma          : undefined,
      nro_documento     : "",
      fecha_documento   : "",
      usua_creacion     : "",
      codigo_patrimonial: "",
      denominacion_bien : "",
      cantidad          : "",
      valor_adq         : "",
      estado            : ""
    }

    this.dataEditarActos = [];

    this.id= "-1";

    this.estado_adjuntar = '';
    this.estado_carga = '';
    this.estado_carga_fecha = '';
    this.estado_carga_total = '';
    this.estado_validacion = '';
    this.estado_validacion_fecha = '';
    this.estado_validacion_total = '';
    this.estado_finalizacion = '';
    this.estado_finalizacion_fecha = '';
    this.estado_finalizacion_total = '';

    this.registerForm.get('formaAdquisicion').setValue(undefined);
    this.submitted=false;

    

  }

  ventanaRegistroFormaIndividual(){
    this.mostrarVentanaTipoRegistro = false;
    this.mostrarVentanaRegistroIndividual = true;
    this.mostrarVentanaRegistroMasivo = false;
    this.modificando_registro = '';
  }

  ventanaRegistroFormaMasiva(){
    this.mostrarVentanaTipoRegistro = false;
    this.mostrarVentanaRegistroIndividual = false;
    this.mostrarVentanaRegistroMasivo = true;

    this.estado_adjuntar = '';
    this.estado_carga = '';
    this.estado_carga_fecha = '';
    this.estado_carga_total = '';
    this.estado_validacion = '';
    this.estado_validacion_fecha = '';
    this.estado_validacion_total = '';
    this.estado_finalizacion = '';
    this.estado_finalizacion_fecha = '';
    this.estado_finalizacion_total = '';
    
  }


  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.cargarListadoActosAdquisicion();
    }  
  }
  
  resetearpag(){
    this.filtro.page = 1;
    this.cargarListadoActosAdquisicion(); 
  }


  loadPageCatalogo(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.ListadoCatalogoFormaIndividual();
    }  
  }

  resetearpagCatalogo(){
    this.filtro.page = 1;
    this.ListadoCatalogoFormaIndividual(); 
  }

  // resetearlimpiarpag(){
  //   this.page = 1;
  //   this.Limpiar_ListadoAsignarPerson();
  // }


  ListadoCatalogoFormaIndividual(){ 
    this.spinner.show();
    console.log(this.parametros);
    this.SinabipMuebles.postListadoCatalogoFormaIndividual(this.parametros).subscribe((data : any) =>{ 
    this.spinner.hide();
    this.dataFormaIndividual = data.data; 
    //console.log(this.dataFormaIndividual)
    this.total = ( this.dataFormaIndividual.catalogo.length > 0 ) ? this.dataFormaIndividual.catalogo[0].TOTAL : 0;
    //console.log('cc');
    console.log(this.total);
    
  });
  }

  ListadoBienesEliminados(){
    this.spinner.show();
    this.SinabipMuebles.postListadoBienesEliminados(this.parametros).subscribe((data : any) =>{ 
    this.spinner.hide();
    this.databienesEliminados = data.data; 
  });
}

  cargarListadoActosAdquisicion(){ 
    this.filtro.cod_entidad = this.Cod_Entidad;
    this.spinner.show();
    this.SinabipMuebles.getListadoActosAdquisicion(this.filtro).subscribe((data : any) =>{  
    this.spinner.hide();

    if (data.data.error == true){
			if (data.data.reco.hasOwnProperty('cod_entidad')) { 
        this.router.navigate(['error500']);
				  return;
			}else if (data.data.reco.hasOwnProperty('nro_documento')) {
        swal({
					type: 'error',
					title: 'Validacion de Datos',
					text: data.data.reco.nro_documento[0],
				  })
				return;
			}else if (data.data.reco.hasOwnProperty('page')) {
        this.router.navigate(['error500']);
        return;
			}else if (data.data.reco.hasOwnProperty('records')) {
        this.router.navigate(['error500']);
        return;
      }
		}

    this.dataActosAdquisicion = data.data;
    //console.log(this.dataActosAdquisicion);
    this.dataAnio = this.dataActosAdquisicion.anios;
    this.dataMes = this.dataActosAdquisicion.mes;
    this.paramAgregar.id_forma = undefined;
    this.total = ( this.dataActosAdquisicion.documento.length > 0 ) ? this.dataActosAdquisicion.documento[0].TOTAL : 0;
    console.log(this.total);
    
    });
  }

  Eliminar_Actos(cod_entidad, id_acto){
    
    this.paramEliminarActo = {
      id_entidad   : cod_entidad,
      id_acto      : id_acto
    }

    swal({
      title: '¿Está Ud. seguro de eliminar el Acto?',
      text: "¡No podrás revertir esto!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonText: 'Cancelado',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Eliminar!'
      
    }).then((result) => {
    if (result.value) {
      this.spinner.show();
      console.log(this.paramEliminarActo);
      this.SinabipMuebles.postEliminar_Actos(this.paramEliminarActo).subscribe((data : any) =>{
        this.spinner.hide();

        if (data.data.error == true){
          if (data.data.reco.hasOwnProperty('id_entidad')) { 
              this.router.navigate(['error500']);
              return;
          }else if (data.data.reco.hasOwnProperty('id_acto')) {
              this.router.navigate(['error500']);
              return;
          }
          
        }

        this.dataEliminarBien = data.data; 
        //console.log(this.dataEliminarBien);
        
        if (this.dataEliminarBien[0].RESULTADO == 'OK'){ 

          this.cargarListadoActosAdquisicion();
          this.mostrarVentanaTipoRegistro = false;
          this.mostrarVentanaRegistroIndividual = false; 
          
          swal(
            'Eliminado!',
            'El registro ha sido eliminado.',
            'success'
          )
      }else{
        swal(
          'Error!',
          'Ocurrio un inconveniente',
          'error'
        )
      }

      
      });
    
      
      
    }
  })
    
  }

  BorrarFiltro(){ 
    let fechita = new Date();
    let yy = fechita.getFullYear();

    this.filtro = { 
      fecha : 
      {
        month : -1,
        year  : yy
      },
    cod_entidad       : this.Cod_Entidad,
    forma_adquisicion : -1,
    nro_documento     : '',
    estado            : '-1',
    page : this.page, records : this.itemsPerPage

  }
  }



traerDatosTipoBien(obj){
  this.paramAgregar.codigo_patrimonial = obj.NRO_BIEN;
  this.paramAgregar.denominacion_bien = obj.DENOM_BIEN;
  this.TipoagregarBien = "1";
}

traerDatosCodPatrimonialesEliminados(obj){
  this.paramAgregar.codigo_patrimonial = obj.CODIGO_PATRIMONIAL;
  this.paramAgregar.denominacion_bien = obj.DENOMINACION_BIEN;
  this.TipoagregarBien = "2";
}


  open(content) { 
    
    this.parametros = { 
      cod_entidad   : this.Cod_Entidad,
      nro_grupo     : "-1",
      nro_clase     : "-1",
      denom_bien    : "",
      nro_bien      : "",
      cod_patrimonial : "",
      denom_patrimonial : "",
      page : this.page, records : this.itemsPerPage 
    } 
    this.paramAgregar.codigo_patrimonial = "";
    this.paramAgregar.denominacion_bien = "";
    this.paramAgregar.cantidad = "";
    this.paramAgregar.valor_adq = "";
    this.dataFormaIndividual = [];

    this.onSubmit2();
    if(this.registerForm.valid){
      
      this.spinner.show();
      this.modal=this.SinabipMuebles.postListadoCatalogoFormaIndividual(this.parametros).subscribe((data : any) =>{ 
        this.spinner.hide();
        this.dataFormaIndividual = data.data;
      });
      
  
      this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', 
      keyboard: false,
      size: 'lg',
      backdrop: 'static'}).result.then((result) => { 
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`; 
      });
    }else
    {

      swal({
        type: 'error',
        title: 'Datos Incompletos!!!',
        text: 'No se ha ingresado la información necesaria para continuar',
        // footer: '<a href>Why do I have this issue?</a>'
      })


    }
  }


  open1(content, id_bien) { 

    this.V_COD_UE_BIEN_PATRI = id_bien;

    this.paramBien.id_entidad = this.Cod_Entidad;     
    this.paramBien.id_bien = id_bien;   

    this.spinner.show();
    this.modal=this.SinabipMuebles.postDatos_Caracteristicas_Bien(this.paramBien).subscribe((data : any) =>{  
      this.spinner.hide();

      this.dataCaracteristicasBien = data.data;
      console.log(data.data);
      
      let acto = data.data; 
      this.paramBien.formaAdquis = acto.CaracteristicasBien[0].NOM_FORM_ADQUIS;      
      this.paramBien.nroDocAdquis = acto.CaracteristicasBien[0].NRO_DOCUMENTO_ADQUIS;   
      this.paramBien.fechaAdquis = acto.CaracteristicasBien[0].FECHA_DOCUMENTO_ADQUIS;
      
      this.paramBien.fechaAdquis =this.datepipe.transform(this.paramBien.fechaAdquis, 'dd-MM-yyyy');
      
      this.paramBien.codigopatri = acto.CaracteristicasBien[0].CODIGO_PATRIMONIAL; 
      this.paramBien.denomBien = acto.CaracteristicasBien[0].DENOMINACION_BIEN; 
      

      this.paramBien.marca = acto.CaracteristicasBien[0].MARCA; 
      this.paramBien.modelo = acto.CaracteristicasBien[0].MODELO; 
      this.paramBien.tipo = acto.CaracteristicasBien[0].TIPO; 
      this.paramBien.color = acto.CaracteristicasBien[0].COLOR; 
      this.paramBien.nroSerie = acto.CaracteristicasBien[0].SERIE; 
      this.paramBien.dimension = acto.CaracteristicasBien[0].DIMENSION; 
      this.paramBien.placa_matri = acto.CaracteristicasBien[0].PLACA; 
      this.paramBien.nroMotor = acto.CaracteristicasBien[0].NRO_MOTOR; 
      this.paramBien.nroChasis = acto.CaracteristicasBien[0].NRO_CHASIS; 
      this.paramBien.anioFabric = acto.CaracteristicasBien[0].ANIO_FABRICACION; 
      this.paramBien.raza = acto.CaracteristicasBien[0].RAZA; 
      this.paramBien.especie = acto.CaracteristicasBien[0].ESPECIE; 
      this.paramBien.edad = acto.CaracteristicasBien[0].EDAD; 
      this.paramBien.otrasCaract = acto.CaracteristicasBien[0].OTRAS_CARACT; 
      this.paramBien.DEPRECIACION = acto.CaracteristicasBien[0].DEPRECIACION; 
      
      this.paramBien.usoCta = (acto.CaracteristicasBien[0].USO_CUENTA == "0") ? "P" : acto.CaracteristicasBien[0].USO_CUENTA;
      this.paramBien.TipoCta = (acto.CaracteristicasBien[0].TIP_CUENTA == "0") ? "" : acto.CaracteristicasBien[0].TIP_CUENTA;
      this.paramBien.CtaContable = (acto.CaracteristicasBien[0].COD_CTA_CONTABLE == "0") ? "" : acto.CaracteristicasBien[0].COD_CTA_CONTABLE;

      this.paramBien.valorAdquis = (Math.round(acto.CaracteristicasBien[0].VALOR_ADQUIS * 100) / 100) + '';
 
      this.paramBien.porcDeprec = acto.CaracteristicasBien[0].PORC_DEPREC; 

      this.paramBien.asegurado = (acto.CaracteristicasBien[0].OPC_ASEGURADO == "0") ? "" : acto.CaracteristicasBien[0].OPC_ASEGURADO;
      this.paramBien.estadoBien = (acto.CaracteristicasBien[0].COD_ESTADO_BIEN == "0") ? "" : acto.CaracteristicasBien[0].COD_ESTADO_BIEN;

      this.paramBien.XMARCA = (acto.CaracteristicasBien[0].XMARCA == null) ? "" : acto.CaracteristicasBien[0].XMARCA;
      this.paramBien.XMODELO = (acto.CaracteristicasBien[0].XMODELO == null) ? "" : acto.CaracteristicasBien[0].XMODELO;
      this.paramBien.XTIPO = (acto.CaracteristicasBien[0].XTIPO == null) ? "" : acto.CaracteristicasBien[0].XTIPO;
      this.paramBien.XCOLOR = (acto.CaracteristicasBien[0].XCOLOR == null) ? "" : acto.CaracteristicasBien[0].XCOLOR;
      this.paramBien.XSERIE = (acto.CaracteristicasBien[0].XSERIE == null) ? "" : acto.CaracteristicasBien[0].XSERIE;
      this.paramBien.XDIMENSION = (acto.CaracteristicasBien[0].XDIMENSION == null) ? "" : acto.CaracteristicasBien[0].XDIMENSION;
      this.paramBien.XPLACA = (acto.CaracteristicasBien[0].XPLACA == null) ? "" : acto.CaracteristicasBien[0].XPLACA;
      this.paramBien.XMOTOR = (acto.CaracteristicasBien[0].XMOTOR == null) ? "" : acto.CaracteristicasBien[0].XMOTOR;
      this.paramBien.XCHASIS = (acto.CaracteristicasBien[0].XCHASIS == null) ? "" : acto.CaracteristicasBien[0].XCHASIS;
      this.paramBien.XANIO_FABRICACION = (acto.CaracteristicasBien[0].XANIO_FABRICACION == null) ? "" : acto.CaracteristicasBien[0].XANIO_FABRICACION;
      this.paramBien.XRAZA = (acto.CaracteristicasBien[0].XRAZA == null) ? "" : acto.CaracteristicasBien[0].XRAZA;
      this.paramBien.XESPECIE = (acto.CaracteristicasBien[0].XESPECIE == null) ? "" : acto.CaracteristicasBien[0].XESPECIE;
      this.paramBien.XEDAD = (acto.CaracteristicasBien[0].XEDAD == null) ? "" : acto.CaracteristicasBien[0].XEDAD;
      this.paramBien.XOTRAS_CARACTERISTICAS = (acto.CaracteristicasBien[0].XOTRAS_CARACTERISTICAS == null) ? "" : acto.CaracteristicasBien[0].XOTRAS_CARACTERISTICAS;

      this.cantidadduplicado_bienes = 0;
      this.cantidadduplicado_actos = 0;  
      this.cantidaderrores_columnas = 0;
      this.paramBien.observacion = acto.CaracteristicasBien[0].OBSERVACION; 
  
    });
    

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title',
    keyboard: false,
    size: 'lg',
    backdrop: 'static'}).result.then((result) => { 
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }


  openSeleccionCatalogo(content) {

    // this.paramBien.id_entidad = this.Cod_Entidad;     
    // this.paramBien.id_bien = id_bien;   

    // this.spinner.show();
    // this.modal=this.SinabipMuebles.postDatos_Caracteristicas_Bien(this.paramBien).subscribe((data : any) =>{  
    //   this.spinner.hide();

    //   this.dataCaracteristicasBien = data.data;
    //   console.log(data.data);
       
  
    // });
    
    this.flag_SeleccionCatalogo = 1;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title',
    keyboard: false,
    size: 'lg',
    backdrop: 'static'}).result.then((result) => { 
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }



  openValidacion(content, id_adjuntdo) { 

    this.paramCarga.id_entidad = this.Cod_Entidad; 
    this.paramCarga.id_adjuntado = this.dataAdjuntado.data.ID_ADJUNTADO;  

    this.spinner.show();
    this.modal=this.SinabipMuebles.postObservaciones_Validacion(this.paramBien).subscribe((data : any) =>{  
    this.spinner.hide();

    this.dataObservacionValidacion = data.data;
  
    });
    

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title',
    keyboard: false,
    size: 'lg',
    backdrop: 'static'}).result.then((result) => { 
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }


  Guardar_Caracteristicas_Bien():void{

    this.paramBusquedaserie = { 
      id_entidad : this.Cod_Entidad,
      nroSerie    : this.paramBien.nroSerie,
      cod_bien : this.V_COD_UE_BIEN_PATRI
    }

		this.spinner.show();
		this.SinabipMuebles.postBqserieExiste(this.paramBusquedaserie).subscribe((data: any) => {
			this.spinner.hide();
      this.Busquedaserie_Arg = data.data;
      console.log(this.Busquedaserie_Arg);
      console.log(this.Busquedaserie_Arg.ESTADO );

    if (this.Busquedaserie_Arg.ESTADO == '1'){
      swal({
        position: 'center',
        type: 'warning',
        title: 'La serie ya existe en otro Codigo Patrimonial',
        showConfirmButton: true
        //timer: 2000
      })
    }else
      if (this.paramBien.usoCta == ''){
        swal({
          position: 'center',
          type: 'warning',
          title: 'Debe especificar el uso de la cuenta',
          showConfirmButton: true
          //timer: 2000
        })
      }else
        if (this.paramBien.TipoCta == ''){
          swal({
            position: 'center',
            type: 'warning',
            title: 'Debe especificar el Tipo de cuenta',
            showConfirmButton: true
            //timer: 2000
          })
        }else
          if (this.paramBien.CtaContable == ''){
            swal({
              position: 'center',
              type: 'warning',
              title: 'Debe especificar la Cuenta Contable',
              showConfirmButton: true
              //timer: 2000
            })
          }else
            if (this.paramBien.valorAdquis == '' || this.paramBien.valorAdquis <= '0'){
              swal({
                position: 'center',
                type: 'warning',
                title: 'Debe ingresar el Valor de Adquisición',
                showConfirmButton: true
                //timer: 2000
              })
                }else
                  if (this.paramBien.asegurado == ''){
                    swal({
                      position: 'center',
                      type: 'warning',
                      title: 'Debe especificar si se encuentra asegurado',
                      showConfirmButton: true
                      //timer: 2000
                    })
                  }else
                    if (this.paramBien.estadoBien == ''){
                      swal({
                        position: 'center',
                        type: 'warning',
                        title: 'Debe especificar estado del bien',
                        showConfirmButton: true
                        //timer: 2000
                      })
                    }else
                    {

                      this.spinner.show();
                      this.SinabipMuebles.postGuardar_Caracteristicas_Bien(this.paramBien).subscribe((data : any) =>{
                      this.spinner.hide();
                      
                      console.log(this.paramBien);
                      this.dataresultado = data.data;
                      if(this.dataresultado[0].RESULTADO == "ERROR DE VALIDACION"){
                  
                        swal({
                          position: 'center',
                          type: 'success',
                          title: 'Debe ingresar las caracteristicas que exige el Tipo del Bien',
                          showConfirmButton: true
                          //timer: 2000
                        })
                  
                      }
                      
                      
                      else
                      {
                                    
                          this.modalService.dismissAll();
                          swal({
                            position: 'center',
                            type: 'success',
                            title: 'Se Actualizaron las caracteristicas Satisfactoriamente!!!',
                            showConfirmButton: false,
                            timer: 2000
                          })
                  
                      }
                    });

                    }
    });
  }

  cargarTXT(){ 
    
    this.paramCarga.id_entidad = this.Cod_Entidad; 
    this.paramCarga.id_adjuntado = this.dataAdjuntado.data.ID_ADJUNTADO;  
    this.paramCarga.nombreArchivo = this.dataAdjuntado.data.NOMBRE_ARCHIVO;
    this.paramCarga.usua_creacion = this.Cod_Usuario; 

    this.spinner.show();
    this.SinabipMuebles.postCargarTXT(this.paramCarga).subscribe((data : any) =>{ 
    this.spinner.hide();

    if (data.data.error == true){
			if (data.data.reco.hasOwnProperty('id_entidad')) { 
          this.router.navigate(['error500']);
				  return;
			}else if (data.data.reco.hasOwnProperty('id_adjuntado')) {
				  this.router.navigate(['error500']);
				  return;
        }else if (data.data.reco.hasOwnProperty('usua_creacion')) {
				  this.router.navigate(['error500']);
				  return;
			}
			
		}

    this.estado_carga = data.data.ESTADO_CARGA;
    this.estado_carga_fecha = data.data.CARGADO_FECHA;
    this.estado_carga_total = data.data.CARGADO_TOTAL;

    swal({
      position: 'center',
      type: 'success',
      title: 'Carga Finalizada',
      showConfirmButton: false,
      timer: 2000
    })

    });
  }

  ValidarTXT(content){
    this.b_ventanaObservacionesValidacion = false;
    this.paramCarga.id_entidad = this.Cod_Entidad; 
    this.paramCarga.id_adjuntado = this.dataAdjuntado.data.ID_ADJUNTADO;  
    this.paramCarga.usua_creacion = this.Cod_Usuario;

    this.spinner.show();
    this.SinabipMuebles.postValidarTXT(this.paramCarga).subscribe((data : any) =>{ 
      this.spinner.hide();

      if (data.data.error == true){
        if (data.data.reco.hasOwnProperty('id_entidad')) { 
          this.router.navigate(['error500']);
          return;
        }else if (data.data.reco.hasOwnProperty('id_adjuntado')) {
            this.router.navigate(['error500']);
            return;
        }else if (data.data.reco.hasOwnProperty('usua_creacion')) {
          this.router.navigate(['error500']);
          return;
        }
        
      }

      this.estado_validacion = data.data.ESTADO_VALIDACION;
      this.estado_validacion_fecha = data.data.VALIDADO_FECHA;
      this.estado_validacion_total = data.data.VALIDADO_TOTAL;

      
      
      if (this.estado_validacion == "VALIDACION_SATISFACTORIA" ){

        swal({
          position: 'center', 
          type: 'success',
          title: 'Validación Finalizada',
          showConfirmButton: false,
          timer: 2000 
        })
        this.modalService.dismissAll();
  
      }else{
        
        this.spinner.show();
        this.modal=this.SinabipMuebles.postListadoErroresCargaMasiva(this.paramCarga).subscribe((data : any) =>{   
          this.spinner.hide();
          this.dataObservacionValidacion = [];
          this.dataObservacionValidacion = data.data;
          this.cantidadduplicado_actos = this.dataObservacionValidacion["duplicado_actos"].length
          this.cantidadduplicado_bienes = this.dataObservacionValidacion["duplicado_bienes"].length
          this.cantidaderrores_columnas = this.dataObservacionValidacion["errores_columnas"].length
        });
  
        // setTimeout(() => {
        //   this.b_ventanaObservacionesValidacion = true;   
        // }, 1000);
        
        this.b_ventanaObservacionesValidacion = true;
        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title',
        keyboard: false,
        size: 'lg',
        backdrop: 'static'}).result.then((result) => { 
          this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
  
  
  
      }
    });
    
    
    
    
    

  }



  FinalizarTXT(){

    this.paramCarga.id_entidad = this.Cod_Entidad; 
    this.paramCarga.id_adjuntado = this.dataAdjuntado.data.ID_ADJUNTADO;  
    this.paramCarga.usua_creacion = this.Cod_Usuario;

    this.spinner.show();
    this.SinabipMuebles.postFinalizarTXT(this.paramCarga).subscribe((data : any) =>{ 
    this.spinner.hide();

    if (data.data.error == true){
      if (data.data.reco.hasOwnProperty('id_entidad')) { 
        this.router.navigate(['error500']);
        return;
      }else if (data.data.reco.hasOwnProperty('id_adjuntado')) {
          this.router.navigate(['error500']);
          return;
      }else if (data.data.reco.hasOwnProperty('usua_creacion')) {
        this.router.navigate(['error500']);
        return;
      }
      
    }

    this.estado_finalizacion = data.data.ESTADO_FINALIZACION;
    this.estado_finalizacion_fecha = data.data.FINALIZADO_FECHA;
    this.estado_finalizacion_total = data.data.FINALIZADO_TOTAL;

    swal({
      position: 'center',
      type: 'success',
      title: 'Finalizada'
      
    })

    

    this.cargarListadoActosAdquisicion(); 
    this.mostrarVentanaTipoRegistro = false;
    this.mostrarVentanaRegistroIndividual = false; 
    this.mostrarVentanaRegistroMasivo = false;

    });

  }

  

  AgregarBien_Actos():void{
    
    console.log(this.paramAgregar.cantidad);
    if(this.paramAgregar.codigo_patrimonial == '' && this.paramAgregar.denominacion_bien == '' ){
      swal(
        'Debe escoger un Bien Patrimonial', 
        '',
        'question'
      )
      return;
    }else if(this.paramAgregar.cantidad == '' || this.paramAgregar.valor_adq == '' ){
      swal(
        'Debe ingresar la cantidad y Valor de Adquisición',
        '',
        'question'
      )
      return;
    
      
    }else if(parseInt(this.paramAgregar.cantidad) > 50 || parseInt(this.paramAgregar.cantidad) < 1 ){
      swal(
        'Se debe registrar como mínimo 1 Bien y como máximo 50 Bienes',
        '',
        'question'
      )
      return;
    
    }else{

      this.paramAgregar.TipoagregarBien = this.TipoagregarBien
      this.paramAgregar.id_entidad = this.Cod_Entidad;
      this.paramAgregar.usua_creacion = this.Cod_Usuario;
      this.paramAgregar.id_forma = this.registerForm.get('formaAdquisicion').value;


    if(this.paramAgregar.id_acto == ''){
      this.paramAgregar.id_acto = '-1';
    }

  
    this.spinner.show();
    this.SinabipMuebles.postAgregarBien_Actos(this.paramAgregar).subscribe((data : any) =>{ 
    this.spinner.hide();

    this.id_altas = data.data;
    this.paramAgregar.id_acto = this.id_altas.cabecera.ID;
    // console.log('AAA');
    // console.log(this.id_altas);
    // console.log('BBB');
    // console.log(this.paramAgregar);

    if (this.id_altas.cabecera.RESULTADO == 'OK'){

      this.cargarListadoActosAdquisicion();
      this.editarActoAdquisicion(this.paramAgregar.id_acto);
      this.modalService.dismissAll();
      swal({
        position: 'center',
        type: 'success',
        title: 'Se ha Registrado Satisfactoriamente!!!',
      })

    }else{
      swal({
        position: 'center',
        type: 'error',
        title: 'Ha ocurrido un error y no se ha registrado, vuelva a intentar nuevamente!!!',
      })
    }
 
     });

     
    }   

    
  }


  AgregarBien_Eliminados_Actos():void{

    if(this.paramAgregar.codigo_patrimonial == '' && this.paramAgregar.denominacion_bien == '' ){ 
      swal(
        'Debe escoger un Bien Patrimonial', 
        '',
        'question'
      )
    }else if(this.paramAgregar.valor_adq == '' ){
      swal(
        'Debe ingresar el Valor de Adquisición',
        '',
        'question'
      )
    
    }else{

      this.paramAgregar.TipoagregarBien = this.TipoagregarBien
      this.paramAgregar.id_entidad = this.Cod_Entidad;
      this.paramAgregar.usua_creacion = this.Cod_Usuario;
      this.paramAgregar.id_forma = this.registerForm.get('formaAdquisicion').value;

    if(this.paramAgregar.id_acto == ''){
      this.paramAgregar.id_acto = '-1';
    }

  
    this.spinner.show();
    this.SinabipMuebles.postAgregarBien_Actos(this.paramAgregar).subscribe((data : any) =>{ 
    this.spinner.hide();

    this.id_altas = data.data;
    this.paramAgregar.id_acto = this.id_altas.cabecera.ID;

    this.cargarListadoActosAdquisicion(); 

    this.editarActoAdquisicion(this.paramAgregar.id_acto);
    this.modalService.dismissAll();

    swal({
      position: 'center',
      type: 'success',
      title: 'Se ha Registrado Satisfactoriamente!!!',
    })
    
    
 
     });

    }

    
    
  }

  Guardar_Actos():void{

    this.paramAgregar.id_entidad = this.Cod_Entidad; 
    this.spinner.show();
    console.log(this.paramAgregar);

    this.SinabipMuebles.postValidar_Caracteritica_Bienes(this.paramAgregar).subscribe((data : any) =>{
      this.spinner.hide();

      if (data.data.error == true){
        if (data.data.reco.hasOwnProperty('id_entidad')) { 
            this.router.navigate(['error500']);
            return;
        }else if (data.data.reco.hasOwnProperty('id_acto')) {
            this.router.navigate(['error500']);
            return;
        }
        
      }

      this.dataValidacionCaracter = data.data; 
      
      console.log(this.dataValidacionCaracter.length); 
      
      if(this.dataValidacionCaracter.length > 0){

        swal({
          position: 'center',
          type: 'error',
          title: 'Falta ingresar las caracteristicas a los Bienes',
          
        })

      }else{


        /**/ 

        swal({
          title: 'Una vez que guarde, ya no podrá modificar los Bienes del Acto',
          text: "¡No podrás revertir esto!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonText: 'Cancelar',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, quiero Guardar!'
          
        }).then((result) => {
        if (result.value) {
          this.spinner.show();
          
          this.paramAgregar.id_forma = this.registerForm.get('formaAdquisicion').value;
          this.SinabipMuebles.postGuardar_Actos(this.paramAgregar).subscribe((data : any) =>{ 
          this.spinner.hide();

          if (data.data.error == true){
            if (data.data.reco.hasOwnProperty('id_acto')) { 
                this.router.navigate(['error500']);
                return;
            }else if (data.data.reco.hasOwnProperty('id_entidad')) {
                this.router.navigate(['error500']);
                return;
            }else if (data.data.reco.hasOwnProperty('id_forma')) {
              this.router.navigate(['error500']);
              return;
            }else if (data.data.reco.hasOwnProperty('nro_documento')) {
              swal({
                type: 'error',
                title: 'Validacion de Datos',
                text: data.data.reco.nro_documento[0],
                })
              return;
            }else if (data.data.reco.hasOwnProperty('fecha_documento')) {
              swal({
                type: 'error',
                title: 'Validacion de Datos',
                text: data.data.reco.fecha_documento[0],
                })
              return;
            }else if (data.data.reco.hasOwnProperty('usua_creacion')) {
              this.router.navigate(['error500']);
              return;
            }
          }
          this.dataGuardarBien = data.data; 
          this.cargarListadoActosAdquisicion(); 
          this.mostrarVentanaTipoRegistro = false;
          this.mostrarVentanaRegistroIndividual = false; 

            swal({
              position: 'center',
              type: 'success',
              title: 'Se ha Registrado Satisfactoriamente!!!',

            })

          });
          
          
        }
    })

        /**/ 

    }
      
  });

    
  }

  editarActoAdquisicion(id_acto){
    
    this.paramAgregar.id_acto = id_acto;
    this.paramAgregar.id_entidad = this.Cod_Entidad;
    this.spinner.show();
    this.SinabipMuebles.postEditarActoAdquisicion(this.paramAgregar).subscribe((data : any) =>{  
    this.spinner.hide();
    
    if (data.data.error == true){
			if (data.data.reco.hasOwnProperty('id_entidad')) { 
          this.router.navigate(['error500']);
          return;
			}else if (data.data.reco.hasOwnProperty('id_acto')) {
				  this.router.navigate(['error500']);
				  return;
			}
			
		}

    this.dataEditarActos = data.data;   

    this.paramAgregar.id_forma = this.dataEditarActos.cabecera.COD_FORM_ADQUIS;
    this.paramAgregar.nro_documento = this.dataEditarActos.cabecera.NRO_DOCUMENTO_ADQUIS;
    this.paramAgregar.fecha_documento = this.dataEditarActos.cabecera.FECHA_DOCUMENTO_ADQUIS; 
    //this.paramAgregar.fecha_documento = this.dataEditarActos.cabecera.FECHA_DOCUMENTO_ADQUIS.toLocaleDateString('en-US');
    // alert(Date.parse(this.dataEditarActos.cabecera.FECHA_DOCUMENTO_ADQUIS)); 
    // alert(this.dataEditarActos.cabecera.FECHA_DOCUMENTO_ADQUIS.toLocaleDateString());  
    // alert(this.paramAgregar.fecha_documento);
    this.paramAgregar.estado = this.dataEditarActos.cabecera.ESTADO;

    this.seleccionRows = id_acto;

    this.registerForm.get('formaAdquisicion').setValue(this.dataEditarActos.cabecera.COD_FORM_ADQUIS);   
    this.paramAgregar.id_forma = this.dataEditarActos.cabecera.COD_FORM_ADQUIS;


    var startDate = this.dataEditarActos.cabecera.COD_FORM_ADQUIS;
    var convertedStartDate = new Date(startDate);
    var month = convertedStartDate.getMonth() + 1
    var day = convertedStartDate.getDay();
    var year = convertedStartDate.getFullYear();
    var shortStartDate = month + "/" + day + "/" + year;

    this.paramAgregar.fecha_documento = shortStartDate;

    console.log(this.dataEditarActos);
    
    });
    

    this.mostrarVentanaTipoRegistro = false;
    this.mostrarVentanaRegistroIndividual = true;
    this.modificando_registro = 'M';
    this.cantidadduplicado_bienes = 0;
    this.cantidadduplicado_actos = 0;  
    this.cantidaderrores_columnas = 0;
        
     
  }

  Eliminar_Bien(id_bien){

    swal({
      title: '¿Esta Ud. seguro de eliminar el Bien?',
      text: "¡No podrás revertir esto!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonText: 'Cancelado',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Eliminar!'
      
    }).then((result) => {
    if (result.value) {
    
      this.paramEliminarBien.id_entidad = this.Cod_Entidad; 
      this.paramEliminarBien.id_usuario = this.Cod_Usuario;
      this.paramEliminarBien.id_bien = id_bien;

      this.spinner.show();
      this.SinabipMuebles.postEliminar_Bien_Proceso(this.paramEliminarBien).subscribe((data : any) =>{
        this.spinner.hide();
        
        this.dataEliminarBien = data.data; 
        
        if (this.dataEliminarBien[0].RESULTADO == 'OK'){ 

          this.cargarListadoActosAdquisicion();
          this.editarActoAdquisicion(this.paramAgregar.id_acto);
          
          swal(
            'Eliminado!',
            'El registro ha sido eliminado.',
            'success'
          )
      }else{
        swal(
          'Error!',
          'Ocurrio un inconveniente',
          'error'
        )
      }

      
      });
    
      
      
    }
})

  }

  keyPress(event: any) {
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {    
        // invalid character, prevent input
        event.preventDefault();
    }
  }

  keyPresspuntos(event: any) {
    const pattern = /[0-9.]/;
    const inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {    
        // invalid character, prevent input
        event.preventDefault();
    }
  }
  


  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }


}
