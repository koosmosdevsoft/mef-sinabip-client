import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { SinabipmueblesService } from '../services/sinabipmuebles.service';
//import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgbModalConfig, NgbModal, ModalDismissReasons, NgbModule} from '@ng-bootstrap/ng-bootstrap';
//import {NgbDatepickerI18n, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
//import { ModalService } from '../_services'; 
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import swal from'sweetalert2';

@Component({
  selector: 'app-recodificacion',
  templateUrl: './recodificacion.component.html', 
  styleUrls: ['./recodificacion.component.css'],  
  animations: [routerTransition()],
  providers: [NgbModalConfig, NgbModal]
})


export class RecodificacionComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
titularAlerta:string='';
 dataRecodificacion : any = [];
 Arrg_denom = [];
 posicion : number;
 mostrarVentanaRecodificarSeleccionados : boolean = false;
 mostrarVentanaResumenRecodificacion : boolean = false;
 dataDetalleRecodificacion : any = [];
 estado_recodificacion : string = "";
 codigo_recodificacion : number = -1;
 Cod_Entidad : string = sessionStorage.getItem("Cod_Entidad");
 Cod_Usuario : string = sessionStorage.getItem("Cod_Usuario");
 RutaSinabip : string = sessionStorage.getItem("RutaSinabip");
 urlPDF;
 cod_entidad_recod;
 afuConfig = {
  uploadAPI: {
      url:this.SinabipMuebles.API_URL+"SubirAltaTxt/"+this.Cod_Entidad,
    },
    hideResetBtn: true,
    uploadBtnText:"Adjuntar Archivo",
    uploadMsgText: "",
    formatsAllowed: ".TXT,.txt"
};

heroForm : any = [];

//Paginacion 
itemsPerPage: number = 20;
page: any = 1;
previousPage: any;
total : any = 0;

filtro = {
  cod_entidad       : this.Cod_Entidad,
  tipo_bien         : '',
  nombre_bien       : '',
  grupo             : -1,
  clase             : -1,
  denominaciones    : '',
  page : this.page, records : this.itemsPerPage
}

paramRecod = {
  cod_entidad  : this.Cod_Entidad,      
  denominaciones : '',
  oficio :'',
  informe : '',
  fecha : ''
}

  constructor(
    private formBuilder: FormBuilder,
    private SinabipMuebles : SinabipmueblesService, 
    private spinner: NgxSpinnerService,
    private modalService: NgbModal,
    private router: Router
    ) 

    { 
      let token = sessionStorage.getItem("token");  
      let ruta = sessionStorage.getItem("RutaSinabip")
      if(token === null){
        // window.location.replace(ruta+"SGISBN/System/sinabip.php");
        window.location.replace("./SGISBN/System/sinabip.php");
      }
      this.afuConfig.uploadAPI.url = this.SinabipMuebles.API_URL+"AdjuntarAltatxt/"+this.Cod_Entidad;

    }

  ngOnInit(): void {

    this.registerForm = this.formBuilder.group({ 
      oficio: ['', Validators.required],
      informe: ['', Validators.required],
      fecha: ['', [Validators.required]],
    });
    this.filtro.tipo_bien = '';
    this.filtro.nombre_bien = '';
    this.filtro.grupo = -1,
    this.filtro.clase = -1,
    this.filtro.denominaciones = '',
    this.cargarListadoRecodificacion();
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) { 
      swal({
        type: 'error',
        title: 'Datos Incompletos!!!',
        text: 'No se ha ingresado la información necesaria para continuar',
      })
      return;
    }
    this.ProcesarRecodificacion();
  }

  ventanaRecodificacionSeleccionados(){
    if (this.Arrg_denom.length > 0) {
      this.mostrarVentanaRecodificarSeleccionados = true;
      this.mostrarVentanaResumenRecodificacion = false;
    }else{
      swal({
        type: 'error',
        title: '¡ERROR!',
        text: 'No se ha seleccionado ninguna denominación para recodificar',
      })
      this.mostrarVentanaRecodificarSeleccionados = false;
      this.mostrarVentanaResumenRecodificacion = false;
    }
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.cargarListadoRecodificacion();
    }
  }

  cargarListadoRecodificacion(){
    this.filtro.cod_entidad = this.Cod_Entidad;
    this.spinner.show();
    this.SinabipMuebles.getListadoRecodificacion(this.filtro).subscribe((data : any) =>{  
      this.spinner.hide();

      if (data.data.error == true){
        if (data.data.reco.hasOwnProperty('cod_entidad')) { 
            this.router.navigate(['error500']);
            return;
        }else if (data.data.reco.hasOwnProperty('grupo')) {
            this.router.navigate(['error500']);
            return;
          }else if (data.data.reco.hasOwnProperty('clase')) {
            this.router.navigate(['error500']);
            return;
          }else if (data.data.reco.hasOwnProperty('page')) {
            this.router.navigate(['error500']);
            return;
          }else if (data.data.reco.hasOwnProperty('records')) {
            this.router.navigate(['error500']);
            return;
          }
        }
      
      this.dataRecodificacion = data.data;
      this.total = (this.dataRecodificacion.reco.length > 0) ? this.dataRecodificacion.reco[0].TOTAL : 0;
    });
  }

  almacenarDenominaciones(estado,denom){
    if(estado == true){
      this.Arrg_denom.push(denom);
    }else{
      this.posicion = this.Arrg_denom.indexOf(denom);
      this.Arrg_denom.splice(this.posicion,1);
    }
  }

  BorrarFiltro(){
    this.filtro.tipo_bien = '';
    this.filtro.nombre_bien = '';
    this.filtro.grupo = -1,
    this.filtro.clase = -1
  }

  ProcesarRecodificacion():void{
    this.paramRecod.cod_entidad = this.Cod_Entidad;
    this.paramRecod.oficio = this.registerForm.get('oficio').value;
    this.paramRecod.informe = this.registerForm.get('informe').value;
    this.paramRecod.fecha = this.registerForm.get('fecha').value;
    this.paramRecod.denominaciones = this.Arrg_denom.toString();
    this.spinner.show();
    this.SinabipMuebles.postProcesarRecodificacion(this.paramRecod).subscribe((data : any) =>{
      this.spinner.hide();

      if (data.data.error == true){
        if (data.data.reco.hasOwnProperty('cod_entidad')) { 
          this.router.navigate(['error500']);
            return;
        }else if (data.data.reco.hasOwnProperty('oficio')) {
          swal({
            type: 'error',
            title: 'Validacion de Datos',
            text: data.data.reco.oficio[0],
            })
            return;
        }else if (data.data.reco.hasOwnProperty('informe')) {
          swal({
            type: 'error',
            title: 'Validacion de Datos',
            text: data.data.reco.informe[0],
            })
          return;
        }else if (data.data.reco.hasOwnProperty('fecha')) {
          swal({
            type: 'error',
            title: 'Validacion de Datos',
            text: data.data.reco.fecha[0],
            })
          return;
        }

      }

      this.estado_recodificacion = data.data.Resultado.estado_recodificacion;
      this.codigo_recodificacion = data.data.Resultado.codigo_recodificacion;
      this.dataDetalleRecodificacion = data.data.RecodDetalle;
      if (this.estado_recodificacion == 'RECODIFICACION_SATISFACTORIA'){
        swal({
          position: 'center', 
          type: 'success',
          title: 'Proceso de recodificación finalizado correctamente',
          showConfirmButton: false,
          timer: 3000 
        })
        this.mostrarVentanaRecodificarSeleccionados = false;
        this.mostrarVentanaResumenRecodificacion = true;
        this.Arrg_denom = [];
        this.registerForm.reset();
        this.cargarListadoRecodificacion();
      }else{
        swal({
          type: 'error',
          title: '¡ERROR!',
          text: 'Ocurrió un error al procesar la recodificación',
        })
      }
    })
  }

  FinalizarRecodificacion(){
    this.mostrarVentanaRecodificarSeleccionados = false;
    this.mostrarVentanaResumenRecodificacion = false;
    this.Arrg_denom = [];
    this.registerForm.reset();
    this.cargarListadoRecodificacion();

    swal({
      position: 'center', 
      type: 'success',
      title: 'Recodificación Finalizada',
      showConfirmButton: false,
      timer: 3000 
    })
  }

  ImprimirReporteRecodificacion(){
    this.paramRecod.cod_entidad = this.Cod_Entidad;
    this.paramRecod.denominaciones = this.Arrg_denom.toString();
    this.spinner.show();
    window.open(this.SinabipMuebles.API_URL+"ImprimirResumenRecodificacion/"+
        this.paramRecod.cod_entidad+"/"+
        this.codigo_recodificacion+"/"+
        this.paramRecod.oficio+"/"+
        this.paramRecod.informe+"/"+
        this.paramRecod.fecha,'_blank');
    this.spinner.hide();
  }
}