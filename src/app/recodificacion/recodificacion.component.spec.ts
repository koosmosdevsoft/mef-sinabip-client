import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecodificacionComponent } from './recodificacion.component';

describe('ActosadquisicionComponent', () => {
  let component: RecodificacionComponent;
  let fixture: ComponentFixture<RecodificacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecodificacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecodificacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
