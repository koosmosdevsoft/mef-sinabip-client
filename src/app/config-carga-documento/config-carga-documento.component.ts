import { Component, OnInit, ViewChild  } from '@angular/core';
import { routerTransition } from '../router.animations';
import { SinabipmueblesService } from '../services/sinabipmuebles.service';
import { NgbModalConfig, NgbModal, ModalDismissReasons, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AngularFileUploaderComponent } from 'angular-file-uploader';
import swal from'sweetalert2';

@Component({
  selector: 'app-config-carga-documento',
  templateUrl: './config-carga-documento.component.html',
  styleUrls: ['./config-carga-documento.component.css'],
	animations: [routerTransition()],
	providers: [NgbModalConfig, NgbModal]
})
export class ConfigCargaDocumentoComponent implements OnInit {

  
	Cod_Entidad: string = sessionStorage.getItem("Cod_Entidad");
  Cod_Usuario: string = sessionStorage.getItem("Cod_Usuario");
  
  dataActosAdquisicion: any = [];
  dataListado_Forma_Acto: any = [];
  
	//Paginacion 
	itemsPerPage: number = 20;
	page: any = 1;
	previousPage: any;
	total: any = 0;


	filtro = {
		fecha:
		{
			month: 10,
			year: 2018
		},
		cod_entidad: this.Cod_Entidad,
		forma_adquisicion: '',
		tipo_acto: '',
		nro_documento: '',
		estado: '1',
		fechaDesde:'1988-01-01',
		fechaHasta:'2019-08-08',
		page: this.page, records: this.itemsPerPage
  }
  
	nro_documento_url: string = '';

  afuConfig = {
		uploadAPI: {
			url: sessionStorage.getItem("Adjuntar_DocActa"),
		},
		hideResetBtn: true,
		uploadBtnText: "Adjuntar Archivo",
		uploadMsgText: "",
		formatsAllowed: ".PDF,.pdf",
		maxSize: 50
  };
  
  cant_util : any = 0;
  cant_alt : any = 0;
  cant_baja : any = 0;
  cant_admin : any = 0;
  cant_disp : any = 0;
  mostrar_carga : any = null;
  NRO_DOCUMENTO: string = '';
  Adj_Documento: string = "";
	resetVar = false;
	closeResult: string;
	urlPDF;
  NomArchEstado = -1;
  
  NombArch_Dat = { 
    cod_entidad: this.Cod_Entidad, 
    id_acto: '', 
    tipo_acto: ''
  };

  NomArch_arg: any = [];
  NombArch: string;
  ElimArg_arg: any = [];
  NOMB_ARCHIVO: string;
  NRO_DOCUMENTO2: string;
  FCH_ADJUNTADO: string;

	@ViewChild('fileUpload1')
	private fileUpload1: AngularFileUploaderComponent;

  constructor(config: NgbModalConfig,	
    private formBuilder: FormBuilder,
		private SinabipMuebles: SinabipmueblesService,
		private spinner: NgxSpinnerService,
		private modalService: NgbModal,
		public Sinabip : SinabipmueblesService) { 
      this.cargarListadoActos();
      config.backdrop = 'static';
      config.keyboard = false;
    }

  ngOnInit() {
  }

	Actualiza_InfFinal() {
		this.NomArchEstado = 1;
	}

	cargarListadoActos() {
		this.filtro.cod_entidad = this.Cod_Entidad;
		if (this.filtro.nro_documento  == ''){
			this.nro_documento_url = '-';
		}else{
			this.nro_documento_url = this.filtro.nro_documento;
		}
		// console.log(this.filtro);
		this.spinner.show();
		this.SinabipMuebles.postListadoCargaActosDoc(this.filtro).subscribe((data: any) => {
			this.spinner.hide();
			this.dataActosAdquisicion = data.data;
			// console.log(this.dataActosAdquisicion);
			this.total = (this.dataActosAdquisicion.documento.length > 0) ? this.dataActosAdquisicion.documento[0].TOTAL : 0;
      this.cant_util = this.dataActosAdquisicion.datadocumacto[0].CANT_UTIL;
      this.cant_alt = this.dataActosAdquisicion.datadocumacto[0].CANT_ALT;
      this.cant_baja = this.dataActosAdquisicion.datadocumacto[0].CANT_BAJ;
      this.cant_admin = this.dataActosAdquisicion.datadocumacto[0].CANT_ADMIN;
      this.cant_disp = this.dataActosAdquisicion.datadocumacto[0].CANT_DISP;
      this.filtro.fechaHasta = this.dataActosAdquisicion.datadocumacto[0].ANNO+'-'+this.dataActosAdquisicion.datadocumacto[0].MES+'-'+this.dataActosAdquisicion.datadocumacto[0].DIA ;
		});
  }
  
	ListadoFormaActo() {
		// console.log(this.filtro);
		this.spinner.show();
		this.SinabipMuebles.postListadoFormaActo(this.filtro).subscribe((data: any) => {
			this.spinner.hide();
			this.dataListado_Forma_Acto = data.data;
			this.filtro.forma_adquisicion = ''
			// console.log(this.dataListado_Forma_Acto);
		});
  }
  
	buscar_actos(){
		if(this.filtro.tipo_acto !== '-1'){
			this.cargarListadoActos();
		}else{
			swal({
				type: 'warning',
				title: 'Datos Incompletos!!!',
				text: 'Ud. no ha indicado el tipo de acto',
				// footer: '<a href>Why do I have this issue?</a>'
			})
		}
	}

	BorrarFiltro() {
		this.filtro = {
			fecha:
			{
				month: 1,
				year: 2018
			},
			cod_entidad: this.Cod_Entidad,
			forma_adquisicion: '',
			tipo_acto: '',
			nro_documento: '',
			estado: '-1',
      fechaDesde:'1988-01-01',
      fechaHasta:'2019-08-08',
			page: this.page, records: this.itemsPerPage
		}
		this.dataActosAdquisicion.documento = [];
	}

  verDetalleActo(COD_ENTIDAD,ID_ACTO,TIPO_ACTO,NRO_DOCUMENTO,ID_ESTADO2){
    this.mostrar_carga = 0;
    if(ID_ESTADO2 == '1'){ 
      this.mostrar_carga = 1;
      console.log(ID_ESTADO2);
      setTimeout(() => {
        this.NRO_DOCUMENTO = NRO_DOCUMENTO;
        this.NombArch_Dat = { cod_entidad: COD_ENTIDAD, id_acto: ID_ACTO, tipo_acto: TIPO_ACTO };
        this.Adj_Documento = this.Sinabip.API_URL + "SubirDocActo/" + COD_ENTIDAD + "-" + ID_ACTO + "-" + TIPO_ACTO;
        sessionStorage.setItem("Adjuntar_DocActa", this.Adj_Documento);
        this.afuConfig.uploadAPI.url = this.Adj_Documento;
        this.fileUpload1.uploadAPI = this.Adj_Documento;
        this.NombreDoc(COD_ENTIDAD,ID_ACTO,TIPO_ACTO);
      }, 100);
    }
    if(ID_ESTADO2 == '3'){ 
      this.mostrar_carga = 2;
      this.NombreDoc(COD_ENTIDAD,ID_ACTO,TIPO_ACTO);
    }
  }

  desabilitar_ventana(){
    this.mostrar_carga = null;
  }
  
	open(content) {
		this.spinner.show();
		this.Sinabip.GetNombDocActo(this.NombArch_Dat.cod_entidad, this.NombArch_Dat.id_acto,this.NombArch_Dat.tipo_acto).subscribe((data: any) => {
			this.spinner.hide();
			this.NomArch_arg = data.data;
			this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
				this.closeResult = `Closed with: ${result}`;
			}, (reason) => {
			});
			this.NombArch = this.NomArch_arg.NOMB_ARCHIVO;
			this.urlPDF = this.Sinabip.API_URL + "VerPDFDocAct/" + this.NombArch;
		});
  }
  
  NombreDoc(COD_ENTIDAD,ID_ACTO,TIPO_ACTO){
    this.spinner.show();
		this.Sinabip.GetNombDocActo(COD_ENTIDAD, ID_ACTO,TIPO_ACTO).subscribe((data: any) => {
			this.spinner.hide();
      this.NomArch_arg = data.data;
      this.NOMB_ARCHIVO = this.NomArch_arg.NOMB_ARCHIVO;
      this.NRO_DOCUMENTO2 = this.NomArch_arg.NRO_DOCUMENTO;
      this.FCH_ADJUNTADO = this.NomArch_arg.FCH_ADJUNTADO;
		});
  }

	AceptarAdjunto() {
    this.mostrar_carga = null;
    this.cargarListadoActos();
	}
  
	EliminarArchivo() {
		this.spinner.show();
		this.Sinabip.GetEliminarArchivoDocActo(this.NombArch).subscribe((data: any) => {
			this.spinner.hide();
			this.ElimArg_arg = data.data;
		});
    this.fileUpload1.resetFileUpload();
	}

}
