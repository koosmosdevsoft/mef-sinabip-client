import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigCargaDocumentoComponent } from './config-carga-documento.component';

describe('ConfigCargaDocumentoComponent', () => {
  let component: ConfigCargaDocumentoComponent;
  let fixture: ComponentFixture<ConfigCargaDocumentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigCargaDocumentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigCargaDocumentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
