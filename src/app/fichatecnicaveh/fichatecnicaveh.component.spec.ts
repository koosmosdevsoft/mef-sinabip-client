import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FichatecnicavehComponent } from './fichatecnicaveh.component';

describe('FichatecnicavehComponent', () => {
  let component: FichatecnicavehComponent;
  let fixture: ComponentFixture<FichatecnicavehComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FichatecnicavehComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FichatecnicavehComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
