import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActosdisposicionComponent } from './actosdisposicion.component';

describe('ActosdisposicionComponent', () => {
  let component: ActosdisposicionComponent;
  let fixture: ComponentFixture<ActosdisposicionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActosdisposicionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActosdisposicionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
