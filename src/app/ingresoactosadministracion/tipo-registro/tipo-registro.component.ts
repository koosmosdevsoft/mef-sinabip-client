import { Component, OnInit, HostBinding, Input, Output, EventEmitter } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { SinabipmueblesService } from '../../services/sinabipmuebles.service';
import {NgbModalConfig, NgbModal, ModalDismissReasons, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators, NgControl } from '@angular/forms';
import swal from'sweetalert2';
import { DatePipe } from '@angular/common';  
import { SidebarComponent } from 'src/app/components/sidebar/sidebar.component'; 


@Component({
  selector: 'app-tipo-registro',
  templateUrl: './tipo-registro.component.html',
  styleUrls: ['./tipo-registro.component.css'],
  animations: [routerTransition()],
  providers: [NgbModalConfig, NgbModal, DatePipe] 
})

export class TipoRegistroComponent implements OnInit {
  @Input("tipo") tipoDoc = null;
  @Output("escuchapapi") header1 = new EventEmitter();
  evento = null;

  

  Cod_Entidad : string = sessionStorage.getItem("Cod_Entidad");
  Cod_Usuario : string = sessionStorage.getItem("Cod_Usuario");

  constructor
  (
    public datepipe: DatePipe,
    private formBuilder: FormBuilder,
    private SinabipMuebles : SinabipmueblesService, 
    private spinner: NgxSpinnerService,
    private modalService: NgbModal
  
  ) { }

  ngOnInit() {
    // this.sideBarService.change.subscribe(isOpen => {
    //   this.isOpen = isOpen;
    // });
  }

  mostrarVentanaTipoRegistroaa : boolean = false;
  mostrarVentanaRegistroIndividualaa : boolean = false;
  mostrarVentanaRegistroMasivo : boolean = false;
  modificando_registro: string = '';
  



/* Inicio de Funciones */

ventanaRegistroFormaIndividual(){
  
  this.mostrarVentanaTipoRegistroaa = false;
  this.mostrarVentanaRegistroIndividualaa = true;
  this.mostrarVentanaRegistroMasivo = false;
  this.modificando_registro = '';
}

mostrarVentana(ventana){
  this.header1.emit(ventana); 
  if (ventana == 'individual'){
    //this.ListadoTipoAdministracion_Recepcion();
  }
  // this.evento = ventana;
  // this.header.emit(this.evento);
}


}





/* Fin de Funciones */