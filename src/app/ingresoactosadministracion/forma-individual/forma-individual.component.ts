import { Component, OnInit, HostListener, Input, Output, EventEmitter} from '@angular/core';
import { routerTransition } from '../../router.animations';
import { SinabipmueblesService } from '../../services/sinabipmuebles.service';
import {NgbModalConfig, NgbModal, ModalDismissReasons, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators, NgControl } from '@angular/forms';
import swal from'sweetalert2';
import { DatePipe } from '@angular/common'; 
import { Alert } from 'selenium-webdriver';

@Component({
  selector: 'app-forma-individual',
  templateUrl: './forma-individual.component.html',
  styleUrls: ['./forma-individual.component.css'],
  animations: [routerTransition()],
  providers: [NgbModalConfig, NgbModal, DatePipe]
})

export class FormaIndividualComponent implements OnInit {
  @Input("registro_consultado_desdePadre") registro_Consultado = null;
  @Output("escuchaGuardadodeHijo") header1 = new EventEmitter();
  @Output("registro_Consultado") header2 = new EventEmitter();
  @Input("tipo") tipoDoc = null;

  @Output("_eventoGuardar1") eventoGuardar = new EventEmitter();
  @Output("_eventoEliminarDetalle") eventoEliminarDet = new EventEmitter();
  @Input("id_acto") id_Acto = "";

  evento = null;


  //Paginacion 
  itemsPerPage: number = 20;
  page: any = 1;
  previousPage: any;
  total : any = 0; 

  /* Declaracion de variables */

  Cod_Entidad : string = sessionStorage.getItem("Cod_Entidad");
  Cod_Usuario : string = sessionStorage.getItem("Cod_Usuario");
  registerForm: FormGroup;
  submitted = false;
  seleccionRows : string = null;

  parametros = { 
    cod_entidad   : this.Cod_Entidad,
    nro_grupo     : "-1",
    nro_clase     : "",
    denom_bien    : "",
    nro_bien      : "",
    cod_patrimonial : "",
    denom_patrimonial : "",
    page : this.page, records : this.itemsPerPage
  } 

  paramAgregar = {
    id_acto           : "",
    id_entidad        : this.Cod_Entidad,
    id_forma          : undefined,
    nro_documento     : "",
    fecha_documento   : "",
    fecha_inicio      : "",
    fecha_vencimiento : "",
    tipo_benef        : "",
    ruc_beneficiario  : "",
    motivo            : "",
    usua_creacion     : this.Cod_Usuario,
    codigo_patrimonial: "",
    denominacion_bien : "",
    cantidad          : "",
    valor_adq         : "",
    estado            : ""
  }

  paramEliminarBien = { 
    id_entidad  : this.Cod_Entidad,
    id_bien     : "",
    id_usuario  : ""
  }
  modal;

  COD_ID : number;
  mostrarVentanaRegistroIndividual : boolean = false;
  mostrarVentanaRegistroMasivo : boolean = false;
  flag_listado_catalogo : boolean = false;

  dataActosRecepcion : any = [];
  dataFormaIndividual : any = [];
  closeResult: string;
  dataListadoTipoAdministracion : any = [];
  datatipoBenef : any = [];
  dataEditarActos : any = [];
  flag_SeleccionCatalogo : boolean = false;
  flag_caracteristicas : boolean = false;
  TipoagregarBien    : string = "";
  id_altas : any = [];
  modificando_registro : string = "";
  dataEliminarBien : any = [];

  nro_documento : string = "";
  dataValidacionCaracter : any = [];
  dataGuardarBien : any = [];
  mostrarVentanaTipoRegistro : boolean = false;

  V_COD_UE_BIEN_PATRI: string;
  

  paramBien = {
    id_entidad  : this.Cod_Entidad,      
    id_bien     : "",
    formaAdquis : undefined,
    nroDocAdquis: "",
    fechaAdquis : "",
    codigopatri : "",
    denomBien   : "",
    marca       : "",
    modelo      : "",
    tipo        : "",
    color       : "",
    nroSerie    : "",
    dimension   : "",
    placa_matri : "",
    nroMotor    : "",
    nroChasis   : "",
    anioFabric  : "",
    raza        : "",
    especie     : "",
    edad        : "",
    otrasCaract : "",
    usoCta      : "P",
    TipoCta     : "",
    CtaContable : "",
    valorAdquis : "",
    porcDeprec  : "",
    asegurado   : "",
    estadoBien  : "",
    observacion : "",
    DEPRECIACION : "",
    XMARCA: "",
    XMODELO: "",
    XTIPO: "",
    XCOLOR: "",
    XSERIE: "",
    XDIMENSION: "",
    XPLACA: "",
    XMOTOR: "",
    XCHASIS: "",
    XANIO_FABRICACION: "",
    XRAZA: "",
    XESPECIE: "",
    XEDAD: "",
    XOTRAS_CARACTERISTICAS: ""

}

paramBusquedaserie = { 
  id_entidad : this.Cod_Entidad,
  nroSerie   : "",
  cod_bien   : ""
}
Busquedaserie_Arg: any = [];
dataresultado : Array<any> = [];

dataCaracteristicasBien : Array<any> = [];
cantidadduplicado_actos: number = 0;
 cantidadduplicado_bienes: number = 0;
 cantidaderrores_columnas: number = 0;
 //modificando_registro: string = '';
  


  /* Fin de Declaracion de Variables */

  constructor
  (
    public datepipe: DatePipe,
    private formBuilder: FormBuilder,
    private SinabipMuebles : SinabipmueblesService, 
    private spinner: NgxSpinnerService,
    private modalService: NgbModal,
    private router: Router
  ) 
  { 
    let date = new Date();
    //this.ListadoTipoAdministracion_Recepcion();
    
    
  }

 ngOnChanges(){
   if(this.id_Acto != ""){
     this.editarRecepcion(this.id_Acto);
   }
 }

//  cargardatadeacto(){

//  }

  ngOnInit() {
  
    
    this.registerForm = this.formBuilder.group({ 
      formaAdquisicion: ['', Validators.required],
      documentoActo: ['', Validators.required],
      fechaAdministracion: ['', [Validators.required]],
      fechaInicio: ['', [Validators.required]],
      fechaVencimiento: ['', [Validators.required]],
      tipoBeneficiario: ['', [Validators.required]],
      rucBeneficiario: ['', [Validators.required]],

    });

    this.ListadoTipoAdministracion_Recepcion();


  }

  get f() { return this.registerForm.controls; } 


  /* Funciones */
  CargarventanaRegistroFormaIndividual(){
    this.mostrarVentanaRegistroIndividual = true;
    this.mostrarVentanaRegistroMasivo = false;
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  ListadoCatalogoFormaIndividual(){ 
    this.spinner.show();
    console.log(this.parametros);
    this.SinabipMuebles.postListadoCatalogoFormaIndividual(this.parametros).subscribe((data : any) =>{ 
    this.spinner.hide();
    this.dataFormaIndividual = data.data; 
    //console.log(this.dataFormaIndividual)
    this.total = ( this.dataFormaIndividual.catalogo.length > 0 ) ? this.dataFormaIndividual.catalogo[0].TOTAL : 0;
    //console.log('cc');
    console.log(this.total);
    
  });
  }


  abrirVentaSeleccionCatalogo(obj, content) {
    
    this.flag_SeleccionCatalogo = true;
    this.paramAgregar.codigo_patrimonial = obj.NRO_BIEN;
    this.paramAgregar.denominacion_bien = obj.DENOM_BIEN;
    
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title',
    keyboard: false,
    size: 'lg',
    backdrop: 'static'}).result.then((result) => { 
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }


  AgregarBien_para_Recepcion():void{
    
    if(this.paramAgregar.codigo_patrimonial == '' && this.paramAgregar.denominacion_bien == '' ){
      swal(
        'Debe escoger un Bien Patrimonial', 
        '',
        'question'
      )
      return;
    }else if(this.paramAgregar.cantidad == '' || this.paramAgregar.valor_adq == '' ){
      swal(
        'Debe ingresar la cantidad y Valor de Adquisición',
        '',
        'question'
      )
      return;
    
      
    }else if(parseInt(this.paramAgregar.cantidad) > 50 || parseInt(this.paramAgregar.cantidad) < 1 ){
      swal(
        'Se debe registrar como mínimo 1 Bien y como máximo 50 Bienes',
        '',
        'question'
      )
      return;
    
    }else{

      this.paramAgregar.id_entidad = this.Cod_Entidad;
      this.paramAgregar.id_forma = this.registerForm.get('formaAdquisicion').value;
      this.paramAgregar.nro_documento = this.registerForm.get('documentoActo').value;
      this.paramAgregar.fecha_documento = this.registerForm.get('fechaAdministracion').value;
      this.paramAgregar.fecha_inicio = this.registerForm.get('fechaInicio').value;
      this.paramAgregar.fecha_vencimiento = this.registerForm.get('fechaVencimiento').value;
      this.paramAgregar.tipo_benef = this.registerForm.get('tipoBeneficiario').value;
      this.paramAgregar.ruc_beneficiario = this.registerForm.get('rucBeneficiario').value;
      this.paramAgregar.motivo = 'Prueba de Motivo';
      this.paramAgregar.usua_creacion = this.Cod_Usuario;

      

    if(this.paramAgregar.id_acto == ''){
      this.paramAgregar.id_acto = '-1';
    }

      console.log('id_acto:' + this.paramAgregar.id_acto);
  
    this.spinner.show();
    console.log(this.paramAgregar);
    this.SinabipMuebles.postAgregarBien_para_Recepcion(this.paramAgregar).subscribe((data : any) =>{ 
    this.spinner.hide();

    this.id_altas = data.data;
    this.paramAgregar.id_acto = this.id_altas.cabecera.ID;
    console.log('IDDD');
    console.log(this.paramAgregar.id_acto);
    console.log(this.id_altas.cabecera.ID);
    console.log(this.id_altas.cabecera.RESULTADO);

    if (this.id_altas.cabecera.RESULTADO == 'OK'){
      this.header1.emit('OK');
      //this.cargarListadoActosAdquisicion(); 
      this.editarRecepcion(this.paramAgregar.id_acto);
      this.modalService.dismissAll();
      swal({
        position: 'center',
        type: 'success',
        title: 'Se ha Registrado Satisfactoriamente!!!',
      })

    }else{
      swal({
        position: 'center',
        type: 'error',
        title: 'Ha ocurrido un error y no se ha registrado, vuelva a intentar nuevamente!!!',
      })
    }
 
     });

     
    }   
    
  }



  editarRecepcion(id_acto){
    //this.mostrarVentanaTipoRegistro = true;
    //this.title = "individual";

    this.paramAgregar.id_acto = id_acto;
    this.paramAgregar.id_entidad = this.Cod_Entidad;

    this.spinner.show();
    this.SinabipMuebles.postEditarRecepcion(this.paramAgregar).subscribe((data : any) =>{  
    this.spinner.hide();
    
    this.dataEditarActos = data.data;  
    console.log("this.dataEditarActos"); 
    console.log(this.dataEditarActos);

    // this.paramAgregar.id_forma = this.dataEditarActos.cabecera.COD_TIPO_ADMIN;
    // this.paramAgregar.nro_documento = this.dataEditarActos.cabecera.NRO_DOCUMENTO;
    // this.paramAgregar.fecha_documento = this.dataEditarActos.cabecera.FECHA_DOCUMENTO;
    this.paramAgregar.estado = this.dataEditarActos.cabecera.ESTADO;

    this.seleccionRows = id_acto;

    sessionStorage.setItem("nro_resolucion","zzz");

    this.registerForm.get('formaAdquisicion').setValue(this.dataEditarActos.cabecera.COD_TIPO_ADMIN);
    this.registerForm.get('documentoActo').setValue(this.dataEditarActos.cabecera.NRO_DOCUMENTO);
    this.registerForm.get('fechaAdministracion').setValue(this.dataEditarActos.cabecera.FECHA_DOCUMENTO);
    this.registerForm.get('fechaInicio').setValue(this.dataEditarActos.cabecera.FECHA_INICIA_ACTO_ADMINIS);
    this.registerForm.get('fechaVencimiento').setValue(this.dataEditarActos.cabecera.FECHA_VENCE_ACTO_ADMINIS);
    this.registerForm.get('tipoBeneficiario').setValue(this.dataEditarActos.cabecera.COD_TIPO_BENEF);
    this.registerForm.get('rucBeneficiario').setValue(this.dataEditarActos.cabecera.RUC_BENEF_ADMINIS);

    
    });
    

    // this.mostrarVentanaTipoRegistro = true;
    // this.mostrarVentanaRegistroIndividual = true;
    // this.modificando_registro = 'M';
    // this.cantidadduplicado_bienes = 0;
    // this.cantidadduplicado_actos = 0;  
    // this.cantidaderrores_columnas = 0;
        
     
  }


  


  keyPress(event: any) {
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {    
        // invalid character, prevent input
        event.preventDefault();
    }
  }

  keyPresspuntos(event: any) {
    const pattern = /[0-9.]/;
    const inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {    
        // invalid character, prevent input
        event.preventDefault();
    }
  }

  
  onSubmit(){
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) { 
      swal({
        type: 'error',
        title: 'Datos Incompletos!!!',
        text: 'No se ha ingresado la información necesaria para continuar',
        // footer: '<a href>Why do I have this issue?</a>'
      })
        return;
    }
    else
    {
      this.Guardar_Actos_Recepcion();
    }
  }



  Guardar_Actos_Recepcion():void{

    this.paramAgregar.id_entidad = this.Cod_Entidad; 
    this.spinner.show();
    console.log(this.paramAgregar);

    this.SinabipMuebles.postValidar_Caracteritica_Bienes_Recepcion(this.paramAgregar).subscribe((data : any) =>{
      this.spinner.hide();

      if (data.data.error == true){
        if (data.data.reco.hasOwnProperty('id_entidad')) { 
          this.router.navigate(['error500']);
          return;
        }else if (data.data.reco.hasOwnProperty('id_acto')) {
            this.router.navigate(['error500']);
            return;
        }
        
      }

      this.dataValidacionCaracter = data.data; 
      
      if(this.dataValidacionCaracter.length > 0){

        swal({
          position: 'center',
          type: 'error',
          title: 'Falta ingresar las caracteristicas a los Bienes',
          
        })

      }else{


        /**/ 

        swal({
          title: 'Una vez que guarde, ya no podrá modificar los Bienes del Acto',
          text: "¡No podrás revertir esto!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonText: 'Cancelar',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, quiero Guardar!'
          
        }).then((result) => {
        if (result.value) {
          this.spinner.show();
          
          this.paramAgregar.id_forma = this.registerForm.get('formaAdquisicion').value;
          this.paramAgregar.nro_documento = this.registerForm.get('documentoActo').value;
          this.paramAgregar.fecha_documento = this.registerForm.get('fechaAdministracion').value;
          this.paramAgregar.fecha_inicio = this.registerForm.get('fechaInicio').value;
          this.paramAgregar.fecha_vencimiento = this.registerForm.get('fechaVencimiento').value;
          this.paramAgregar.tipo_benef = this.registerForm.get('tipoBeneficiario').value;
          this.paramAgregar.ruc_beneficiario = this.registerForm.get('rucBeneficiario').value;

          console.log(this.paramAgregar);

          this.SinabipMuebles.postGuardar_Actos_Recepcion(this.paramAgregar).subscribe((data : any) =>{ 
          this.spinner.hide();
          this.dataGuardarBien = data.data; 
          //this.ListadoTipoAdministracion_Recepcion();
          this.mostrarVentanaTipoRegistro = false;
          this.mostrarVentanaRegistroIndividual = false; 

          this.eventoGuardar.emit('');

            swal({
              position: 'center',
              type: 'success',
              title: 'Se ha Registrado Satisfactoriamente!!!',

            })

          });
          
          
        }
    })

        /**/ 

    }
      
  });

    
  }



  Eliminar_Bien_Recepcion(id_bien){
    this.paramEliminarBien.id_bien = id_bien;
    this.paramEliminarBien.id_entidad = this.Cod_Entidad;
    //this.paramEliminarBien.id_usuario = this.Cod_Usuario;

    swal({
      title: '¿Esta Ud. seguro de eliminar el Bien?',
      text: "¡No podrás revertir esto!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6', 
      cancelButtonText: 'Cancelado',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Eliminar!'
      
    }).then((result) => {
    if (result.value) {

      this.spinner.show();
      this.SinabipMuebles.postEliminar_Bien_Recepcion(this.paramEliminarBien).subscribe((data : any) =>{
        this.spinner.hide();
        this.dataEliminarBien = data.data; 
        
        if (this.dataEliminarBien[0].RESULTADO == 'CORRECTO'){ 
          this.COD_ID =  this.dataEliminarBien[0].COD_ID;
          console.log(this.COD_ID);
          this.editarRecepcion(this.COD_ID);
          
          swal(
            'Eliminado!',
            'El registro ha sido eliminado.',
            'success'
          )
          this.eventoEliminarDet.emit('');
      }else{
        swal(
          'Error!',
          'Ocurrio un inconveniente',
          'error'
        )
      }

      
      });
    
      
      
    }
})

  }


  open1(content, id_bien) { 
    this.flag_caracteristicas = true;
    this.flag_SeleccionCatalogo = false;
    


    this.V_COD_UE_BIEN_PATRI = id_bien;

    this.paramBien.id_entidad = this.Cod_Entidad;     
    this.paramBien.id_bien = id_bien;   

    this.spinner.show();
    this.modal=this.SinabipMuebles.postDatos_Caracteristicas_Bien_Recepcion(this.paramBien).subscribe((data : any) =>{  
      this.spinner.hide();

      this.dataCaracteristicasBien = data.data;
      console.log(data.data);
      
      let acto = data.data; 
      this.paramBien.formaAdquis = acto.CaracteristicasBien[0].NOM_FORM_ADQUIS;      
      this.paramBien.nroDocAdquis = acto.CaracteristicasBien[0].NRO_DOCUMENTO_ADQUIS;   
      this.paramBien.fechaAdquis = acto.CaracteristicasBien[0].FECHA_DOCUMENTO_ADQUIS;
      
      this.paramBien.fechaAdquis =this.datepipe.transform(this.paramBien.fechaAdquis, 'dd-MM-yyyy');
      
      this.paramBien.codigopatri = acto.CaracteristicasBien[0].CODIGO_PATRIMONIAL; 
      this.paramBien.denomBien = acto.CaracteristicasBien[0].DENOMINACION_BIEN; 
      

      this.paramBien.marca = acto.CaracteristicasBien[0].MARCA; 
      this.paramBien.modelo = acto.CaracteristicasBien[0].MODELO; 
      this.paramBien.tipo = acto.CaracteristicasBien[0].TIPO; 
      this.paramBien.color = acto.CaracteristicasBien[0].COLOR; 
      this.paramBien.nroSerie = acto.CaracteristicasBien[0].SERIE; 
      this.paramBien.dimension = acto.CaracteristicasBien[0].DIMENSION; 
      this.paramBien.placa_matri = acto.CaracteristicasBien[0].PLACA; 
      this.paramBien.nroMotor = acto.CaracteristicasBien[0].NRO_MOTOR; 
      this.paramBien.nroChasis = acto.CaracteristicasBien[0].NRO_CHASIS; 
      this.paramBien.anioFabric = acto.CaracteristicasBien[0].ANIO_FABRICACION; 
      this.paramBien.raza = acto.CaracteristicasBien[0].RAZA; 
      this.paramBien.especie = acto.CaracteristicasBien[0].ESPECIE; 
      this.paramBien.edad = acto.CaracteristicasBien[0].EDAD; 
      this.paramBien.otrasCaract = acto.CaracteristicasBien[0].OTRAS_CARACT; 
      this.paramBien.DEPRECIACION = acto.CaracteristicasBien[0].DEPRECIACION; 
      
      this.paramBien.usoCta = (acto.CaracteristicasBien[0].USO_CUENTA == "0") ? "P" : acto.CaracteristicasBien[0].USO_CUENTA;
      this.paramBien.TipoCta = (acto.CaracteristicasBien[0].TIP_CUENTA == "0") ? "" : acto.CaracteristicasBien[0].TIP_CUENTA;
      this.paramBien.CtaContable = (acto.CaracteristicasBien[0].COD_CTA_CONTABLE == "0") ? "" : acto.CaracteristicasBien[0].COD_CTA_CONTABLE;

      this.paramBien.valorAdquis = (Math.round(acto.CaracteristicasBien[0].VALOR_ADQUIS * 100) / 100) + '';
 
      this.paramBien.porcDeprec = acto.CaracteristicasBien[0].PORC_DEPREC; 

      this.paramBien.asegurado = (acto.CaracteristicasBien[0].OPC_ASEGURADO == "0") ? "" : acto.CaracteristicasBien[0].OPC_ASEGURADO;
      this.paramBien.estadoBien = (acto.CaracteristicasBien[0].COD_ESTADO_BIEN == "0") ? "" : acto.CaracteristicasBien[0].COD_ESTADO_BIEN;

      this.paramBien.XMARCA = (acto.CaracteristicasBien[0].XMARCA == null) ? "" : acto.CaracteristicasBien[0].XMARCA;
      this.paramBien.XMODELO = (acto.CaracteristicasBien[0].XMODELO == null) ? "" : acto.CaracteristicasBien[0].XMODELO;
      this.paramBien.XTIPO = (acto.CaracteristicasBien[0].XTIPO == null) ? "" : acto.CaracteristicasBien[0].XTIPO;
      this.paramBien.XCOLOR = (acto.CaracteristicasBien[0].XCOLOR == null) ? "" : acto.CaracteristicasBien[0].XCOLOR;
      this.paramBien.XSERIE = (acto.CaracteristicasBien[0].XSERIE == null) ? "" : acto.CaracteristicasBien[0].XSERIE;
      this.paramBien.XDIMENSION = (acto.CaracteristicasBien[0].XDIMENSION == null) ? "" : acto.CaracteristicasBien[0].XDIMENSION;
      this.paramBien.XPLACA = (acto.CaracteristicasBien[0].XPLACA == null) ? "" : acto.CaracteristicasBien[0].XPLACA;
      this.paramBien.XMOTOR = (acto.CaracteristicasBien[0].XMOTOR == null) ? "" : acto.CaracteristicasBien[0].XMOTOR;
      this.paramBien.XCHASIS = (acto.CaracteristicasBien[0].XCHASIS == null) ? "" : acto.CaracteristicasBien[0].XCHASIS;
      this.paramBien.XANIO_FABRICACION = (acto.CaracteristicasBien[0].XANIO_FABRICACION == null) ? "" : acto.CaracteristicasBien[0].XANIO_FABRICACION;
      this.paramBien.XRAZA = (acto.CaracteristicasBien[0].XRAZA == null) ? "" : acto.CaracteristicasBien[0].XRAZA;
      this.paramBien.XESPECIE = (acto.CaracteristicasBien[0].XESPECIE == null) ? "" : acto.CaracteristicasBien[0].XESPECIE;
      this.paramBien.XEDAD = (acto.CaracteristicasBien[0].XEDAD == null) ? "" : acto.CaracteristicasBien[0].XEDAD;
      this.paramBien.XOTRAS_CARACTERISTICAS = (acto.CaracteristicasBien[0].XOTRAS_CARACTERISTICAS == null) ? "" : acto.CaracteristicasBien[0].XOTRAS_CARACTERISTICAS;

      this.cantidadduplicado_bienes = 0;
      this.cantidadduplicado_actos = 0;  
      this.cantidaderrores_columnas = 0;
      this.paramBien.observacion = acto.CaracteristicasBien[0].OBSERVACION; 
  
    });-
    

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title',
    keyboard: false,
    size: 'lg',
    backdrop: 'static'}).result.then((result) => { 
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }


  Guardar_Caracteristicas_Bien():void{

    this.paramBusquedaserie = { 
      id_entidad : this.Cod_Entidad,
      nroSerie    : this.paramBien.nroSerie,
      cod_bien : this.V_COD_UE_BIEN_PATRI
    }

		this.spinner.show();
		this.SinabipMuebles.postBqserieExiste(this.paramBusquedaserie).subscribe((data: any) => {
			this.spinner.hide();
      this.Busquedaserie_Arg = data.data;
      console.log(this.Busquedaserie_Arg);
      console.log(this.Busquedaserie_Arg.ESTADO );

    if (this.Busquedaserie_Arg.ESTADO == '1'){
      swal({
        position: 'center',
        type: 'warning',
        title: 'La serie ya existe en otro Codigo Patrimonial',
        showConfirmButton: true
        //timer: 2000
      })
    }else
      if (this.paramBien.usoCta == ''){
        swal({
          position: 'center',
          type: 'warning',
          title: 'Debe especificar el uso de la cuenta',
          showConfirmButton: true
          //timer: 2000
        })
      }else
        if (this.paramBien.TipoCta == ''){
          swal({
            position: 'center',
            type: 'warning',
            title: 'Debe especificar el Tipo de cuenta',
            showConfirmButton: true
            //timer: 2000
          })
        }else
          if (this.paramBien.CtaContable == ''){
            swal({
              position: 'center',
              type: 'warning',
              title: 'Debe especificar la Cuenta Contable',
              showConfirmButton: true
              //timer: 2000
            })
          }else
            if (this.paramBien.valorAdquis == '' || this.paramBien.valorAdquis <= '0'){
              swal({
                position: 'center',
                type: 'warning',
                title: 'Debe ingresar el Valor de Adquisición',
                showConfirmButton: true
                //timer: 2000
              })
                }else
                  if (this.paramBien.asegurado == ''){
                    swal({
                      position: 'center',
                      type: 'warning',
                      title: 'Debe especificar si se encuentra asegurado',
                      showConfirmButton: true
                      //timer: 2000
                    })
                  }else
                    if (this.paramBien.estadoBien == ''){
                      swal({
                        position: 'center',
                        type: 'warning',
                        title: 'Debe especificar estado del bien',
                        showConfirmButton: true
                        //timer: 2000
                      })
                    }else
                    {

                      this.spinner.show();
                      this.SinabipMuebles.postGuardar_Caracteristicas_Bien(this.paramBien).subscribe((data : any) =>{
                      this.spinner.hide();
                      
                      console.log(this.paramBien);
                      this.dataresultado = data.data;
                      if(this.dataresultado[0].RESULTADO == "ERROR DE VALIDACION"){
                  
                        swal({
                          position: 'center',
                          type: 'success',
                          title: 'Debe ingresar las caracteristicas que exige el Tipo del Bien',
                          showConfirmButton: true
                          //timer: 2000
                        })
                  
                      }
                       
                      
                      else
                      {
                                    
                          this.modalService.dismissAll();
                          swal({
                            position: 'center',
                            type: 'success',
                            title: 'Se Actualizaron las caracteristicas Satisfactoriamente!!!',
                            showConfirmButton: false,
                            timer: 2000
                          })
                  
                      }
                    });

                    }
    });
  }




  loading(){

  }

  onSubmit2() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) { 
      swal({
        type: 'error',
        title: 'Datos Incompletosss!!!',
        text: 'No se ha ingresado la información necesaria para continuar',
        // footer: '<a href>Why do I have this issue?</a>'
      })
        return;
    }
    else
    {
      // this.Guardar_Actos();
    }
    
    
  }


  ListadoTipoAdministracion_Recepcion(){
    this.spinner.show();
    this.SinabipMuebles.postListadoTipoAdministracion_Recepcion('').subscribe((data : any) =>{ 
    this.spinner.hide();
    this.dataListadoTipoAdministracion = data.data;
    this.registerForm.get('formaAdquisicion').setValue(undefined);
    this.registerForm.get('tipoBeneficiario').setValue(undefined);
    //this.submitted=false;
    console.log(this.dataListadoTipoAdministracion);
    
    });
  }


  abrirListaCatalogoBienes(content) { 

    this.flag_listado_catalogo = true;
    this.flag_SeleccionCatalogo = false;
    this.flag_caracteristicas = false;
    
    this.parametros = { 
      cod_entidad   : this.Cod_Entidad,
      nro_grupo     : "-1",
      nro_clase     : "-1",
      denom_bien    : "",
      nro_bien      : "",
      cod_patrimonial : "",
      denom_patrimonial : "",
      page : this.page, records : this.itemsPerPage 
    } 
    this.paramAgregar.codigo_patrimonial = "";
    this.paramAgregar.denominacion_bien = "";
    this.paramAgregar.cantidad = "";
    this.paramAgregar.valor_adq = "";
    this.dataFormaIndividual = [];

    this.onSubmit2();
    if(this.registerForm.valid){
      
      this.spinner.show();
      this.modal=this.SinabipMuebles.postListadoCatalogoBienes(this.parametros).subscribe((data : any) =>{ 
        this.spinner.hide();
        this.dataFormaIndividual = data.data;
        console.log(this.dataFormaIndividual);
      });
      
  
      this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', 
      keyboard: false,
      size: 'lg',
      backdrop: 'static'}).result.then((result) => { 
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`; 
      });
    }else
    {

      swal({
        type: 'error',
        title: 'Datos Incompletos!!!',
        text: 'No se ha ingresado la información necesaria para continuar',
        // footer: '<a href>Why do I have this issue?</a>'
      })


    }

  }


  mostrarVentana(ventana){
    this.header1.emit(ventana);
    if (ventana == 'individual'){
      //this.ListadoTipoAdministracion_Recepcion();
    }
    // this.evento = ventana;
    // this.header.emit(this.evento);
  }


  


  

  /* Fin de Funciones */

}
