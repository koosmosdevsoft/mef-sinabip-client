import { Component, OnInit, Input, HostListener, Output, EventEmitter } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { SinabipmueblesService } from '../../services/sinabipmuebles.service';
import {NgbModalConfig, NgbModal, ModalDismissReasons, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators, NgControl } from '@angular/forms';
import swal from'sweetalert2';
import { DatePipe } from '@angular/common'; 
import { SidebarComponent } from 'src/app/components/sidebar/sidebar.component';
  


@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css'],
  animations: [routerTransition()],
  providers: [NgbModalConfig, NgbModal, DatePipe]  
})
export class ListadoComponent implements OnInit {
  

  registerForm: FormGroup;
  title = "hola";
  id_recepcion = "zzzz";
  registro_consultado : string = "";
  guardo : string = "";
  Cod_Entidad : string = sessionStorage.getItem("Cod_Entidad");
  Cod_Usuario : string = sessionStorage.getItem("Cod_Usuario");
	RutaSinabip : string = sessionStorage.getItem("RutaSinabip");

  

  constructor
  (

    public datepipe: DatePipe,
    private formBuilder: FormBuilder,
    private SinabipMuebles : SinabipmueblesService, 
    private spinner: NgxSpinnerService,
    private modalService: NgbModal,
    private router: Router
  ) 
  {
    let token = sessionStorage.getItem("token");  
    let ruta = sessionStorage.getItem("RutaSinabip")
    if(token === null){
      // window.location.replace(ruta+"SGISBN/System/sinabip.php");
      window.location.replace("./SGISBN/System/sinabip.php");
    }
  }

  ngOnInit() {

    this.filtro.estado = '-1';
    this.filtro.fecha.month = -1;
    this.filtro.forma_adquisicion = -1;
    this.Listado_Recepcion_Bienes();

  }

  ngOnChanges(){
    if (this.guardo == 'OK'){
      alert(this.guardo);
      this.Listado_Recepcion_Bienes();
    }
    
  }

  //Paginacion 
  itemsPerPage: number = 20;
  page: any = 1;
  previousPage: any;
  total : any = 0; 

  filtro = {
    fecha : 
    {
      month : 10,
      year  : 2019
    },
    cod_entidad       : this.Cod_Entidad,
    forma_adquisicion : -1,
    nro_documento     : '',
    estado            : '1',
    page : this.page, records : this.itemsPerPage
  }
  

  dataActosAdquisicion : any = [];
  dataAnio = [];
  dataMes = [];
  paramAgregar = {
    TipoagregarBien   : "",
    id_acto           : "",
    id_entidad        : this.Cod_Entidad,
    id_forma          : undefined,
    nro_documento     : "",
    fecha_documento   : "",
    usua_creacion     : this.Cod_Usuario,
    codigo_patrimonial: "",
    denominacion_bien : "",
    cantidad          : "",
    valor_adq         : "",
    estado            : ""
  }

  mostrarVentanaTipoRegistro : boolean = false;
  mostrarVentanaRegistroIndividual : boolean = false;
  mostrarVentanaRegistroMasivo :boolean =false;

  dataEditarActos : any = [];
  dataEliminarBien : any = [];
  seleccionRows : string = null;
  cantidadduplicado_actos: number = 0;
  cantidadduplicado_bienes: number = 0;
  cantidaderrores_columnas: number = 0;
  modificando_registro: string = '';

  paramEliminarActo = {
    id_entidad   : "",
    id_acto      : ""
  }


  /* Inicio de Funciones */

  Listado_Recepcion_Bienes(){ 
    this.filtro.cod_entidad = this.Cod_Entidad;
    this.spinner.show();
    this.SinabipMuebles.getListado_Recepcion_Bienes(this.filtro).subscribe((data : any) =>{   
    this.spinner.hide();

    if (data.data.error == true){
			if (data.data.reco.hasOwnProperty('cod_entidad')) { 
				this.router.navigate(['error500']);
				return;
			}else if (data.data.reco.hasOwnProperty('nro_documento')) {
        swal({
					type: 'error',
					title: 'Validacion de Datos',
					text: data.data.reco.nro_documento[0],
				  })
				  return;
      }else if (data.data.reco.hasOwnProperty('page')) {
				  this.router.navigate(['error500']);
				  return;
			}else if (data.data.reco.hasOwnProperty('records')) {
        this.router.navigate(['error500']);
        return;
			
      }
    }
   
    this.dataActosAdquisicion = data.data;
    this.dataAnio = this.dataActosAdquisicion.anios;
    this.dataMes = this.dataActosAdquisicion.mes;
    this.paramAgregar.id_forma = undefined;
    this.total = ( this.dataActosAdquisicion.documento.length > 0 ) ? this.dataActosAdquisicion.documento[0].TOTAL : 0;
    console.log(this.dataActosAdquisicion);
    console.log(this.total);
    
    });
  }

  ventanaTipoRegistro(){
    
    this.mostrarVentanaTipoRegistro = true;
    this.title = "hola";
    this.paramAgregar.id_acto = "";
    
  }

  datosdelhijo(data){
    this.title = data;
  }



  guardoRecepcionHijo(data){

    this.guardo = data;
    this.Listado_Recepcion_Bienes();
  }

  registro_Consultado_padre(data){
    this.registro_consultado = data;
    alert('aaa');
    
  }

 
  eventoGuardar(data){
    this.title = data;
    this.mostrarVentanaTipoRegistro = false; 
    if (this.title == ''){
      this.Listado_Recepcion_Bienes();
    } 
  }

  eventoEliminarDet(data){
    this.title = data;
    this.mostrarVentanaTipoRegistro = false; 
    if (this.title == ''){
      this.Listado_Recepcion_Bienes();
    } 
  }

  


  BorrarFiltro(){ 
    let fechita = new Date();
    let yy = fechita.getFullYear();

    this.filtro = { 
      fecha : 
      {
        month : -1,
        year  : yy
      },
    cod_entidad       : this.Cod_Entidad,
    forma_adquisicion : -1,
    nro_documento     : '',
    estado            : '-1',
    page : this.page, records : this.itemsPerPage

  }
  }

  editarRecepcion(id_acto){
    this.mostrarVentanaTipoRegistro = true;
    this.title = "individual";

    this.paramAgregar.id_acto = id_acto;
    this.paramAgregar.id_entidad = this.Cod_Entidad;

    this.spinner.show();
    this.SinabipMuebles.postEditarRecepcion(this.paramAgregar).subscribe((data : any) =>{  
    this.spinner.hide();
    
    if (data.data.error == true){
			if (data.data.reco.hasOwnProperty('id_entidad')) { 
				this.router.navigate(['error500']);
				return;
			}else if (data.data.reco.hasOwnProperty('id_acto')) {
				  this.router.navigate(['error500']);
				  return;
			}
			
		}

    this.dataEditarActos = data.data;  
    this.paramAgregar.id_forma = this.dataEditarActos.cabecera.COD_TIPO_ADMIN;
    this.paramAgregar.nro_documento = this.dataEditarActos.cabecera.NRO_DOCUMENTO;
    this.paramAgregar.fecha_documento = this.dataEditarActos.cabecera.FECHA_DOCUMENTO;
    this.paramAgregar.estado = this.dataEditarActos.cabecera.ESTADO;

    this.seleccionRows = id_acto;

    //this.registerForm.get('formaAdquisicion').setValue(this.dataEditarActos.cabecera.COD_TIPO_ADMIN);
    this.registro_consultado = this.dataEditarActos.cabecera.COD_TIPO_ADMIN;
    this.paramAgregar.id_forma = this.dataEditarActos.cabecera.COD_TIPO_ADMIN;

    
    
    });
    

    this.mostrarVentanaTipoRegistro = true;
    this.mostrarVentanaRegistroIndividual = true;
    this.modificando_registro = 'M';
    this.cantidadduplicado_bienes = 0;
    this.cantidadduplicado_actos = 0;  
    this.cantidaderrores_columnas = 0;
        
     
  }



  Eliminar_Recepcion(cod_entidad, id_acto){
    
    this.paramEliminarActo = {
      id_entidad   : cod_entidad,
      id_acto      : id_acto
    }

    swal({
      title: '¿Está Ud. seguro de eliminar el Acto?',
      text: "¡No podrás revertir esto!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonText: 'Cancelado',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Eliminar!'
      
    }).then((result) => {
    if (result.value) {
      this.spinner.show();
      console.log(this.paramEliminarActo);
      this.SinabipMuebles.postEliminar_Recepcion(this.paramEliminarActo).subscribe((data : any) =>{
        this.spinner.hide();

        if (data.data.error == true){
          if (data.data.reco.hasOwnProperty('id_entidad')) { 
            this.router.navigate(['error500']);
            return;
          }else if (data.data.reco.hasOwnProperty('id_acto')) {
              this.router.navigate(['error500']);
              return;
          }
          
        }

        this.dataEliminarBien = data.data; 
        //console.log(this.dataEliminarBien);
        
        if (this.dataEliminarBien[0].RESULTADO == 'OK'){ 

          this.Listado_Recepcion_Bienes();
          this.mostrarVentanaTipoRegistro = false;
          this.mostrarVentanaRegistroIndividual = false; 
          
          swal(
            'Eliminado!',
            'El registro ha sido eliminado.',
            'success'
          )
      }else{
        swal(
          'Error!',
          'Ocurrio un inconveniente',
          'error'
        )
      }

      
      });
    
      
      
    }
  })
    
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.Listado_Recepcion_Bienes();
    }  
  }
  
  resetearpag(){
    this.filtro.page = 1;
    this.Listado_Recepcion_Bienes();
  }

  

  /* Fin de Funciones */



}
