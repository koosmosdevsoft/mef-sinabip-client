import { TestBed, inject } from '@angular/core/testing';

import { SinabipmueblesService } from './sinabipmuebles.service';

describe('SinabipmueblesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SinabipmueblesService]
    });
  });

  it('should be created', inject([SinabipmueblesService], (service: SinabipmueblesService) => {
    expect(service).toBeTruthy();
  }));
});
