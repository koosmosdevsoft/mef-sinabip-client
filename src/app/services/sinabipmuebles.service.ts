import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  	providedIn: 'root'
})
export class SinabipmueblesService {
	API_URL = environment.api;

	constructor(private httpClient: HttpClient) { }


	/**** 1) Listado de Menu Principal Muebles ****/
	getModuloMuebles(id,token) {
		return this.httpClient.get(this.API_URL + "ModuloMuebles/"+ id+"/"+token);
	}

	getListadoHistoricoInv(id) {
		let token =sessionStorage.getItem("token");
		let headers = new HttpHeaders().set('Authorization', token)
		return this.httpClient.get(this.API_URL + "ListadoHistoricoInv/" + id,{ headers: headers });
	}

	
	getTotalInvActivos(id) {
		return this.httpClient.get(this.API_URL + "TotalInvActivo/" + id);
	}

	getListadoHabilitadoInv(id) {
		return this.httpClient.get(this.API_URL + "ListadoHabilitadoInv/" + id);
	}

	PostListadoPrediosInvEntidad(id) {
		return this.httpClient.post(this.API_URL + "ListadoPrediosInvEntidad", id);
	}


	/**** listado predios por entidad inv ****/
	getHabilitarBotonInv(id) {
		return this.httpClient.get(this.API_URL + "HabilitarBotonInv/" + id);
	}

	/**** buscar predio por idpredio ****/
	getListadoPadronPredios_Id(idpredio) {
		return this.httpClient.get(this.API_URL + "ListadoPadronPredios_Id/" + idpredio);
	}

	PostRegistroInvSinBienes(id) {
		return this.httpClient.post(this.API_URL + "RegistroInvSinBienes", id);
	}

	PostListadoNombreAdjunto(id) {
		return this.httpClient.post(this.API_URL + "ListadoNombreAdjunto", id);
	}

	PostEliminarAdjunto(id) {
		return this.httpClient.post(this.API_URL + "EliminarAdjunto", id);
	}

	PostEnviarAdjunto(id) {
		return this.httpClient.post(this.API_URL + "EnviarAdjunto", id);
	}

	getListadoPreviaInvFinal(id, periodo) {
		return this.httpClient.get(this.API_URL + "ListadoPreviaInvFinal/" + id + "/" + periodo);
	}

	getListadoUsuario_Id(id,inv) {
		return this.httpClient.get(this.API_URL + "ListadoUsuario_Id/" + id + "/" + inv);
	}

	getListadoPreviaDetalleInvFinal(id, page, records) {
		return this.httpClient.get(this.API_URL + "ListadoPreviaDetalleInvFinal/" + id + "/" + page + "/" + records);
	}

	PostRegistrarcabInventario(id) {
		return this.httpClient.post(this.API_URL + "RegistrarcabInventario", id);
	}

	PostNombArchInffActConc(id) {
		return this.httpClient.post(this.API_URL + "NombArchInffActConc", id);
	}

	getEliminarArchivo(id, estado) {
		return this.httpClient.get(this.API_URL + "EliminarArchivo/" + id + "/" + estado);
	}

	getNombArchInffActConcUnion(id) {
		return this.httpClient.get(this.API_URL + "NombArchInffActConcUnion/" + id);
	}

	PostFinalizarInvertario(id) {
		return this.httpClient.post(this.API_URL + "FinalizarInvertario", id);
	}

	getNombreEntidad(id) {
		return this.httpClient.get(this.API_URL + "NombreEntidad/" + id);
	}

	PostEliminarcabInventario(id) {
		return this.httpClient.post(this.API_URL + "EliminarcabInventario", id);
	}

	PostEstadoFinalInv(id) {
		let token =sessionStorage.getItem("token");
		let headers = new HttpHeaders().set('Authorization', token)
		return this.httpClient.post(this.API_URL + "EstadoFinalInv" , id,{ headers: headers });
	}

	PostValidaResponsablePredio(id) {
		return this.httpClient.post(this.API_URL + "ValidaResponsablePredio", id);
	}


	getBuscarEntidad(id,token) {
		return this.httpClient.get(this.API_URL + "BuscarEntidad/" + id+"/"+token);
	}
	
	PostEliminarArchivoInvPdf(id) {
		return this.httpClient.post(this.API_URL + "EliminarArchivoInvPdf", id);
	}

	getValidaAsignacionInv(id) {
		return this.httpClient.get(this.API_URL + "ValidaAsignacionInv/" + id);
	}
	

	//*** 2) ASIGNACION INICIO */*
	getListadopersonal(id) {
		let token =sessionStorage.getItem("token");
		return this.httpClient.get(this.API_URL + "Listadopersonal/" + id+"/"+token);
	}

	getListadoTipoBienEntidad(id) {
		return this.httpClient.get(this.API_URL + "ListadoTipoBienEntidad/" + id);
	}

	PostListadoAsignarPerson(id) {
		return this.httpClient.post(this.API_URL + "ListadoAsignarPerson", id);
	}

	getListadoComboAsig() {
		return this.httpClient.get(this.API_URL + "ListadoComboAsig");
	}

	PostListadoFechaPecosa(id) {
		return this.httpClient.post(this.API_URL + "ListadoFechaPecosa", id);
	}

	PostActualizaPecosa(id) {
		return this.httpClient.post(this.API_URL + "ActualizaPecosa", id);
	}

	PostListadoPersonalAsignar(id) {
		return this.httpClient.post(this.API_URL + "ListadoPersonalAsignar", id);
	}

	PostListadopredarea(id) {
		return this.httpClient.post(this.API_URL + "Listadopredarea", id);
	}

	getPersonalNomb(id, idpre) {
		return this.httpClient.get(this.API_URL + "PersonalNomb/" + id + "/" + idpre);
	}

	PostResumenPatrimonialAsig(id) {
		return this.httpClient.post(this.API_URL + "ResumenPatrimonialAsig", id);
	}

	PostAgregarCabAsignacion(id) {
		return this.httpClient.post(this.API_URL + "AgregarCabAsignacion", id);
	}

	PostEliminarCabAsignacion(id) {
		return this.httpClient.post(this.API_URL + "EliminarCabAsignacion", id);
	}

	PostAgregarDetAsignacion(id) {
		return this.httpClient.post(this.API_URL + "AgregarDetAsignacion", id);
	}

	getListaobtTodosCP(id) {
		return this.httpClient.get(this.API_URL + "ListaobtTodosCP/" + id);
	}

	PostCargarAsigTXT(id) {
		return this.httpClient.post(this.API_URL + "CargarAsigTXT", id);
	}

	postValidarAsigTXT(data) {
		return this.httpClient.post(this.API_URL + "ValidarAsigTXT", data);
	}

	postFinalizarAsigTXT(data) {
		return this.httpClient.post(this.API_URL + "FinalizarAsigTXT", data);
	}



	/* 3) DEVOLUCION DE BIENES */
	PostListadoPersonalDevolucion(data) {
		return this.httpClient.post(this.API_URL + "ListadoPersonalDevolucion", data);
	}

	PostListadoDevolucionPerson(data) {
		return this.httpClient.post(this.API_URL + "ListadoDevolucionPerson", data);
	}

	getListaobtTodosCPDevol(id, idpers) {
		return this.httpClient.get(this.API_URL + "ListaobtTodosCPDevol/" + id + "/" + idpers);
	}

	getCantBienesaDevolver(id, idpers) {
		return this.httpClient.get(this.API_URL + "CantBienesaDevolver/" + id + "/" + idpers);
	}

	PostAgregarCabDevolucion(data) {
		return this.httpClient.post(this.API_URL + "AgregarCabDevolucion", data);
	}

	PostEliminarCabDevolucion(data) {
		return this.httpClient.post(this.API_URL + "EliminarCabDevolucion", data);
	}

	PostAgregarDetDevolucion(data) {
		return this.httpClient.post(this.API_URL + "AgregarDetDevolucion", data);
	}

	
	/* 4) TRANSLADO */
	PostListadoPersonalTraslado(data) {
		return this.httpClient.post(this.API_URL + "ListadoPersonalTraslado", data);
	}

	getCantBienesaTraslado(id, idpers) {
		return this.httpClient.get(this.API_URL + "CantBienesaTraslado/" + id + "/" + idpers);
	}

	PostListadoTrasladoPerson(data) {
		return this.httpClient.post(this.API_URL + "ListadoTrasladoPerson", data);
	}

	GetListadoTipoTraslado() {
		return this.httpClient.get(this.API_URL + "ListadoTipoTraslado");
	}

	PostAgregarCabTraslado(data) {
		return this.httpClient.post(this.API_URL + "AgregarCabTraslado", data);
	}

	GetListadoTipoTrasladoxid(tipo) {
		return this.httpClient.get(this.API_URL + "ListadoTipoTrasladoxid/" + tipo);
	}

	PostEliminarCabTraslado(data) {
		return this.httpClient.post(this.API_URL + "EliminarCabTraslado", data);
	}

	PostAgregarDetTraslado(data) {
		return this.httpClient.post(this.API_URL + "AgregarDetTraslado", data);
	}

	/* 4) REPORTES */
	GetListadoGrupo() {
		return this.httpClient.get(this.API_URL + "ListadoGrupo");
	}

	GetListadoClase(id) {
		return this.httpClient.get(this.API_URL + "ListadoClase/" + id);
	}

	GetListadoEstadoBien() {
		return this.httpClient.get(this.API_URL + "ListadoEstadoBien");
	}

	GetListadoTipoMotivo() {
		return this.httpClient.get(this.API_URL + "ListadoTipoMotivo");
	}

	PostListadoSituacionActual(data) {
		return this.httpClient.post(this.API_URL + "ListadoSituacionActual", data);
	}

	GetListadoCaracteristicas(id, codBienPat) {
		return this.httpClient.get(this.API_URL + "ListadoCaracteristicas/" + id + '/' + codBienPat); 
	}

	GetListadoCuentaContable() {
		return this.httpClient.get(this.API_URL + "ListadoCuentaContable");
	}

	GetListadoUbicacionActual(id, codBienPat) {
		return this.httpClient.get(this.API_URL + "ListadoUbicacionActual/" + id + '/' + codBienPat);
	}

	postImportarFormatoTXT(data) {
		return this.httpClient.post(this.API_URL + "ImportarFormatoTXT", data)
	}

	postImportarFormatoTXTxGrupo(data) {
		return this.httpClient.post(this.API_URL + "ImportarFormatoTXTxGrupo", data)
	}

	postListadoAsignarPersonReporte(data) {
		return this.httpClient.post(this.API_URL + "ListadoAsignarPersonReporte", data)
	}
	
	postListadoAsignarPersonReporteDet(data) {
		return this.httpClient.post(this.API_URL + "ListadoAsignarPersonReporteDet", data)
	}

	
	/* 5) TRANSLADO */
	PostListadoModificacionValorNeto(data) {
		return this.httpClient.post(this.API_URL + "ListadoModificacionValorNeto", data);
	}

	PostActualizarDepreciacion(data) {
		return this.httpClient.post(this.API_URL + "ActualizarDepreciacion", data);
	}

	PostValidaFechaDepreciacion(data) {
		return this.httpClient.post(this.API_URL + "ValidaFechaDepreciacion", data);
	}

	PostValidaDepreciacionestado(data) {
		let token =sessionStorage.getItem("token");
		let headers = new HttpHeaders().set('Authorization', token)
		return this.httpClient.post(this.API_URL + "ValidaDepreciacionestado" , data,{ headers: headers });
		// return this.httpClient.post(this.API_URL + "ValidaDepreciacionestado", data);
	}

	GetListaFechaDepreciacionSelec() {
		return this.httpClient.get(this.API_URL + "ListaFechaDepreciacionSelec");
	}


	PostGuardarTipoDepreciacion(data) {
		return this.httpClient.post(this.API_URL + "GuardarTipoDepreciacion", data);
	}

	PostListaFechaPecosaActualiza(data) {
		let token =sessionStorage.getItem("token");
		let headers = new HttpHeaders().set('Authorization', token)
		return this.httpClient.post(this.API_URL + "ListaFechaPecosaActualiza" , data,{ headers: headers });
		// return this.httpClient.post(this.API_URL + "ListaFechaPecosaActualiza", data);
	}


	postCargarDepreciacionTXT(data) {
		return this.httpClient.post(this.API_URL + "CargarDepreciacionTXT", data)
	}

	postCargareliminartxt(data) {
		return this.httpClient.post(this.API_URL + "CargareliminarTXT", data)
	}

	postValidarDepreciacionTXT(data) {
		return this.httpClient.post(this.API_URL + "ValidarDepreciacionTXT", data)
	}
	
	postValidarEliminarTXT(data) {
		return this.httpClient.post(this.API_URL + "ValidarEliminarTXT", data)
	}

	postListadoErroresCargaMasivaDepreciacion(data) {
		return this.httpClient.post(this.API_URL + "ListadoErroresCargaMasivaDepreciacion", data)
	}

	postListadoErroresCargaMasivaEliminar(data) {
		return this.httpClient.post(this.API_URL + "ListadoErroresCargaMasivaEliminar", data)
	}

	postFinalizarDepreciacionTXT(data) {
		return this.httpClient.post(this.API_URL + "FinalizarDepreciacionTXT", data)
	}

	postFinalizarEliminacionTXT(data) {
		return this.httpClient.post(this.API_URL + "FinalizarEliminacionTXT", data)
	}

	postImportarFormatoExcel(data) {
		return this.httpClient.post(this.API_URL + "ImportarFormatoExcel", data)
	}
	postImportarFormatoExcelxGrupo(data) {
		return this.httpClient.post(this.API_URL + "ImportarFormatoExcelxGrupo", data)
	}
	

	

	/* 6) ACTOS DE ADQUISICION */
	getListadoActosAdquisicion(data) {
		let token =sessionStorage.getItem("token");
		let headers = new HttpHeaders().set('Authorization', token)
		return this.httpClient.post(this.API_URL + "ListadoActosAdquisicion" , data,{ headers: headers });
		// return this.httpClient.post(this.API_URL + "ListadoActosAdquisicion", data);
	}


	postRegistrarActosAdquisicion(data) {
		let token =sessionStorage.getItem("token");
		let headers = new HttpHeaders().set('Authorization', token)
		return this.httpClient.post(this.API_URL + "RegistrarActosAdquisicion" , {username:data},{ headers: headers });
		// return this.httpClient.post(this.API_URL + "RegistrarActosAdquisicion", {
		// 	username: data
		// });
	}


	postListadoCatalogoFormaIndividual(data) {
		let token =sessionStorage.getItem("token");
		let headers = new HttpHeaders().set('Authorization', token)
		return this.httpClient.post(this.API_URL + "ListadoCatalogoFormaIndividual" , data,{ headers: headers });
		// return this.httpClient.post(this.API_URL + "ListadoCatalogoFormaIndividual", data);
	}

	postListadoBienesEliminados(data) {
		let token =sessionStorage.getItem("token");
		let headers = new HttpHeaders().set('Authorization', token)
		return this.httpClient.post(this.API_URL + "ListadoBienesEliminados" , data,{ headers: headers });
		// return this.httpClient.post(this.API_URL + "ListadoBienesEliminados", data);
	}


	postRegistrarActosAdquisicion_Captura_Errores(data) {
		return this.httpClient.post(this.API_URL + "RegistrarActosAdquisicion_Captura_Errores", {
			username: data
		});
	}


	getActualizarActosAdquisicion(id) {
		return this.httpClient.get(this.API_URL + "ActualizarActosAdquisicion/" + id);
	}

	postRegistrarBienes(data) {
		return this.httpClient.post(this.API_URL + "RegistrarBienes", data);
	}

	postEditarActoAdquisicion(data) {
		return this.httpClient.post(this.API_URL + "EditarActoAdquisicion", data);
	}

	postAgregarBien_Actos(data) {
		return this.httpClient.post(this.API_URL + "AgregarBien_Actos", data);
	}

	postEliminar_Actos(data) {
		return this.httpClient.post(this.API_URL + "Eliminar_Actos", data);
	}

	postGuardar_Actos(data) {
		return this.httpClient.post(this.API_URL + "Guardar_Actos", data);
	}

	postDatos_Caracteristicas_Bien(data) {
		return this.httpClient.post(this.API_URL + "Datos_Caracteristicas_Bien", data)
	}

	postGuardar_Caracteristicas_Bien(data) {
		return this.httpClient.post(this.API_URL + "Guardar_Caracteristicas_Bien", data)
	}

	postCargarTXT(data) {
		return this.httpClient.post(this.API_URL + "CargarTXT", data)
	}

	postValidarTXT(data) {
		return this.httpClient.post(this.API_URL + "ValidarTXT", data)
	}

	postFinalizarTXT(data) {
		return this.httpClient.post(this.API_URL + "FinalizarTXT", data)
	}

	postObservaciones_Validacion(data) {
		return this.httpClient.post(this.API_URL + "Observaciones_Validacion", data)
	}

	postListadoErroresCargaMasiva(data) {
		return this.httpClient.post(this.API_URL + "ListadoErroresCargaMasiva", data)
	}

	postEliminar_Bien_Proceso(data) {
		return this.httpClient.post(this.API_URL + "Eliminar_Bien_Proceso", data)
	}
	postListadoErroresAsigCargaMasiva(data) {
		return this.httpClient.post(this.API_URL + "ListadoErroresAsigCargaMasiva", data)
	}

	postValidar_Caracteritica_Bienes(data) {
		return this.httpClient.post(this.API_URL + "Validar_Caracteritica_Bienes", data)
	}

	postBqserieExiste(data) {
		return this.httpClient.post(this.API_URL + "BqserieExiste", data)
	}
	

	/* 7) RECODIFICACION */
	getListadoRecodificacion(data){
		return this.httpClient.post(this.API_URL+"ListadoRecodificacion",data);    
	}

	postProcesarRecodificacion(data){
		return this.httpClient.post(this.API_URL+"ProcesarRecodificacion",data);    
	}

	postImprimirResumenRecodificacion(data){
		return this.httpClient.post(this.API_URL+"ImprimirResumenRecodificacion",data);    
	}

	/* 8) ELIMINACION */
	postListadoEliminacion(data){
		return this.httpClient.post(this.API_URL+"ListadoEliminacion",data);    
	}

	postProcesarEliminacion(data){
		return this.httpClient.post(this.API_URL+"ProcesarEliminacion",data);    
	}

	postImprimirResumenEliminacion(data){
		return this.httpClient.post(this.API_URL+"ImprimirResumenEliminacion",data);    
	}
	
	/* 9) PERSONAL */
	postListadoPersonal(data){
		let token =sessionStorage.getItem("token");
		let headers = new HttpHeaders().set('Authorization', token)
		return this.httpClient.post(this.API_URL + "ListadoPersonal" , data,{ headers: headers });
		// return this.httpClient.post(this.API_URL+"ListadoPersonal",data);    
	}
	
	postProcesarBajaPersonal(data){
		return this.httpClient.post(this.API_URL+"ProcesarBajaPersonal",data);    
	}

	postProcesarEliminarPersonal(data){
		return this.httpClient.post(this.API_URL+"ProcesarEliminarPersonal",data);    
	}

	postRegistrarPersonal(data){
		return this.httpClient.post(this.API_URL+"RegistrarPersonal",data);    
	}

	postObtenerDataPersonal(data){
		return this.httpClient.post(this.API_URL+"ObtenerDataPersonal",data);    
	}

	postEditarPersonal(data){
		return this.httpClient.post(this.API_URL+"EditarPersonal",data);    
	}
	

	/* 10) ACTO DE BAJAS */
	postListadoActosBajas(data) {
		let token =sessionStorage.getItem("token");
		let headers = new HttpHeaders().set('Authorization', token)
		return this.httpClient.post(this.API_URL + "ListadoActosBajas" , data,{ headers: headers });
		// return this.httpClient.post(this.API_URL + "ListadoActosBajas", data);
	}

	postRegistrarActosBajas(data) {
		let token =sessionStorage.getItem("token");
		let headers = new HttpHeaders().set('Authorization', token)
		return this.httpClient.post(this.API_URL + "RegistrarActosBajas" , {username: data},{ headers: headers });
		// return this.httpClient.post(this.API_URL + "RegistrarActosBajas", {
		// 	username: data
		// });
	}


	postRegistrarActosBajas_Captura_Errores(data) {
		return this.httpClient.post(this.API_URL + "RegistrarActosBajas_Captura_Errores", {
			username: data
		});
	}


	getActualizarActosBajas(id) {
		return this.httpClient.get(this.API_URL + "ActualizarActosBajas/" + id);
	}


	postListadoBienesFormaIndividual(data) {
		return this.httpClient.post(this.API_URL + "ListadoBienesFormaIndividual", data);
	}


	postCargarBajaTXT(data) {
		return this.httpClient.post(this.API_URL + "CargarBajaTXT", data)
	}

	postValidarBajaTXT(data) {
		return this.httpClient.post(this.API_URL + "ValidarBajaTXT", data)
	}

	postFinalizarBajaTXT(data) {
		return this.httpClient.post(this.API_URL + "FinalizarBajaTXT", data)
	}


	postListadoErroresCargaMasivaBaja(data) {
		return this.httpClient.post(this.API_URL + "ListadoErroresCargaMasivaBaja", data)
	}

	posteditarActoBaja(data) {
		return this.httpClient.post(this.API_URL + "editarActoBaja", data);
	}

	postEliminar_ActosBaja(data) {
		return this.httpClient.post(this.API_URL + "Eliminar_ActosBaja", data);
	}

	postAgregar_Bienes_al_Detalle(data) {
		return this.httpClient.post(this.API_URL + "Agregar_Bienes_al_Detalle", data);
	}

	postEliminacion_Detalles_Acto(data) {
		return this.httpClient.post(this.API_URL + "Eliminacion_Detalles_Acto", data);
	}

	postGuardar_ActosBaja(data) {
		return this.httpClient.post(this.API_URL + "Guardar_ActosBaja", data);
	}


	PostValidarPDFBajaCatalogo(data) {
		return this.httpClient.post(this.API_URL + "ValidarPDFBajaCatalogo", data);
	}

	PostActualizaPDFBaja(data){
		return this.httpClient.post(this.API_URL+"ActualizaPDFBaja",data);
	}

	postConsultar_Bienes_Baja_a_Eliminar(data){
	return this.httpClient.post(this.API_URL+"Consultar_Bienes_Baja_a_Eliminar",data);
	}




	/*11) ACTO DE ADMINISTRACION */
	postListadoActosAdministracion(data) {
		let token =sessionStorage.getItem("token");
		let headers = new HttpHeaders().set('Authorization', token)
		return this.httpClient.post(this.API_URL + "ListadoActosAdministracion" , data,{ headers: headers });
		// return this.httpClient.post(this.API_URL + "ListadoActosAdministracion", data);
	}

	posteditarActoAdministracion(data) {
		let token =sessionStorage.getItem("token");
		let headers = new HttpHeaders().set('Authorization', token)
		return this.httpClient.post(this.API_URL + "editarActoAdministracion" , data,{ headers: headers });
		// return this.httpClient.post(this.API_URL + "editarActoAdministracion", data);
	}

	postDatosListadosFormaIndividual(data) {
		let token =sessionStorage.getItem("token");
		let headers = new HttpHeaders().set('Authorization', token)
		return this.httpClient.post(this.API_URL + "DatosListadosFormaIndividual" , data,{ headers: headers });
		// return this.httpClient.post(this.API_URL + "DatosListadosFormaIndividual", data);
	}

	postAgregar_Bienes_al_Detalle_Administracion(data) {
		let token =sessionStorage.getItem("token");
		let headers = new HttpHeaders().set('Authorization', token)
		return this.httpClient.post(this.API_URL + "Agregar_Bienes_al_Detalle_Administracion" , data,{ headers: headers });
		// return this.httpClient.post(this.API_URL + "Agregar_Bienes_al_Detalle_Administracion", data);
	}

	postGuardar_ActosAdministracion(data) {
		let token =sessionStorage.getItem("token");
		let headers = new HttpHeaders().set('Authorization', token)
		return this.httpClient.post(this.API_URL + "Guardar_ActosAdministracion" , data,{ headers: headers });
		// return this.httpClient.post(this.API_URL + "Guardar_ActosAdministracion", data);
	}


	postEliminacion_Detalles_Administracion(data) {
		return this.httpClient.post(this.API_URL + "Eliminacion_Detalles_Administracion", data);
	}

	postEliminar_ActosAdministracion(data) {
		return this.httpClient.post(this.API_URL + "Eliminar_ActosAdministracion", data);
	}

	postListadoBienesFormaIndividual_Administracion(data) {
		return this.httpClient.post(this.API_URL + "ListadoBienesFormaIndividual_Administracion", data);
	}

	postCargarAdministracionTXT(data) {
		return this.httpClient.post(this.API_URL + "CargarAdministracionTXT", data)
	}


	postValidarAdministracionTXT(data) {
		return this.httpClient.post(this.API_URL + "ValidarAdministracionTXT", data)
	}

	postListadoErroresCargaMasivaAdministracion(data) {
		return this.httpClient.post(this.API_URL + "ListadoErroresCargaMasivaAdministracion", data)
	}

	postFinalizarAdministracionTXT(data) {
		return this.httpClient.post(this.API_URL + "FinalizarAdministracionTXT", data)
	}

	postcomprobar_RUC(data) {
		return this.httpClient.post(this.API_URL + "comprobar_RUC", data)
	}

	


	
	/* 12) ACTO DE DISPOSICION */
	postListadoActosDisposicion(data) {
		let token =sessionStorage.getItem("token");
		let headers = new HttpHeaders().set('Authorization', token)
		return this.httpClient.post(this.API_URL + "ListadoActosDisposicion" , data,{ headers: headers });
		// return this.httpClient.post(this.API_URL + "ListadoActosDisposicion", data);
	}

	postDatosListadosFormaIndividual_Disposicion(data) {
		let token =sessionStorage.getItem("token");
		let headers = new HttpHeaders().set('Authorization', token)
		return this.httpClient.post(this.API_URL + "DatosListadosFormaIndividual_Disposicion" , data,{ headers: headers });
		// return this.httpClient.post(this.API_URL + "DatosListadosFormaIndividual_Disposicion", data);
	}

	postListadoBienesFormaIndividual_Disposicion(data) {
		let token =sessionStorage.getItem("token");
		let headers = new HttpHeaders().set('Authorization', token)
		return this.httpClient.post(this.API_URL + "ListadoBienesFormaIndividual_Disposicion" , data,{ headers: headers });
		// return this.httpClient.post(this.API_URL + "ListadoBienesFormaIndividual_Disposicion", data);
	}

	postGuardar_ActosDisposicion(data) {
		let token =sessionStorage.getItem("token");
		let headers = new HttpHeaders().set('Authorization', token)
		return this.httpClient.post(this.API_URL + "Guardar_ActosDisposicion" , data,{ headers: headers });
		// return this.httpClient.post(this.API_URL + "Guardar_ActosDisposicion", data);
	}

	posteditarActoDisposicion(data) {
		let token =sessionStorage.getItem("token");
		let headers = new HttpHeaders().set('Authorization', token)
		return this.httpClient.post(this.API_URL + "editarActoDisposicion" , data,{ headers: headers });
		// return this.httpClient.post(this.API_URL + "editarActoDisposicion", data);
	}

	postEliminacion_Detalles_Disposicion(data) {
		return this.httpClient.post(this.API_URL + "Eliminacion_Detalles_Disposicion", data);
	}

	postEliminar_ActosDisposicion(data) {
		return this.httpClient.post(this.API_URL + "Eliminar_ActosDisposicion", data);
	}

	postCargarDisposicionTXT(data) {
		return this.httpClient.post(this.API_URL + "CargarDisposicionTXT", data)
	}


	postValidarDisposicionTXT(data) {
		return this.httpClient.post(this.API_URL + "ValidarDisposicionTXT", data)
	}

	postListadoErroresCargaMasivaDisposicion(data) {
		return this.httpClient.post(this.API_URL + "ListadoErroresCargaMasivaDisposicion", data)
	}

	postFinalizarDisposicionTXT(data) {
		return this.httpClient.post(this.API_URL + "FinalizarDisposicionTXT", data)
	}

	postcomprobar_RUC_disposicion(data) {
		return this.httpClient.post(this.API_URL + "comprobar_RUC_disposicion", data)
	}


	postRegistrarActosDisposicion_Captura_Errores(data) {
		return this.httpClient.post(this.API_URL + "RegistrarActosDisposicion_Captura_Errores", {
			username: data
		});
	}


	/* 13) REPORTE DE ACTOS */

	postListadoActos(data) {
		return this.httpClient.post(this.API_URL + "ListadoActos", data);
	}

	postListadoFormaActo(data) {
		return this.httpClient.post(this.API_URL + "ListadoFormaActo", data);
	}

	postverDetalleActo(data) {
		return this.httpClient.post(this.API_URL + "verDetalleActo", data);
	}

	
	GetFichaTecnicaContar(id, data) {
		return this.httpClient.get(this.API_URL + "FichaTecnicaContar/" + id + '/' + data);
	}


	/* 14) RECEPCION DE BIENES POR ACTO DE ADMINISTRACION */
	getListado_Recepcion_Bienes(data) {
		return this.httpClient.post(this.API_URL + "Listado_Recepcion_Bienes", data);
	}


	postListadoTipoAdministracion_Recepcion(data) {
		return this.httpClient.post(this.API_URL + "ListadoTipoAdministracion_Recepcion", data);
	}

	postListadoTipoBeneficiario(data) {
		return this.httpClient.post(this.API_URL + "ListadoTipoBeneficiario", data);
	}

	postListadoCatalogoBienes(data) {
		return this.httpClient.post(this.API_URL + "ListadoCatalogoBienes", data);
	}

	postAgregarBien_para_Recepcion(data) {
		return this.httpClient.post(this.API_URL + "AgregarBien_para_Recepcion", data);
	}

	postEditarRecepcion(data) {
		return this.httpClient.post(this.API_URL + "EditarRecepcion", data);
	}

	


	postEliminar_Recepcion(data) {
		return this.httpClient.post(this.API_URL + "Eliminar_Recepcion", data);
	}


	postCargarRecepcionTXT(data) {
		return this.httpClient.post(this.API_URL + "CargarRecepcionTXT", data)
	}

	postValidarRecepcionTXT(data) {
		return this.httpClient.post(this.API_URL + "ValidarRecepcionTXT", data)
	}

	postFinalizarRecepcionTXT(data) {
		return this.httpClient.post(this.API_URL + "FinalizarRecepcionTXT", data)
	}

	postListadoErroresCargaMasivarRecepcion(data) {
		return this.httpClient.post(this.API_URL + "ListadoErroresCargaMasivarRecepcion", data)
	}

	postValidar_Caracteritica_Bienes_Recepcion(data) {
		return this.httpClient.post(this.API_URL + "Validar_Caracteritica_Bienes_Recepcion", data)
	}

	postDatos_Caracteristicas_Bien_Recepcion(data) {
		return this.httpClient.post(this.API_URL + "Datos_Caracteristicas_Bien_Recepcion", data)
	}

	postGuardar_Actos_Recepcion(data) {
		return this.httpClient.post(this.API_URL + "Guardar_Actos_Recepcion", data);
	}

	postEliminar_Bien_Recepcion(data) {
		return this.httpClient.post(this.API_URL + "Eliminar_Bien_Recepcion", data)
	}


	/* 15) ENTREGA - REINGRESO DE BIENES POR ACTO DE ADMINISTRACION */
	getListado_Entrega_Bienes(data) {
		return this.httpClient.post(this.API_URL + "Listado_Entrega_Bienes", data);
	}


	postListadoTipoAdministracion_Entrega(data) {
		return this.httpClient.post(this.API_URL + "ListadoTipoAdministracion_Entrega", data);
	}


	postListadoTipoBeneficiarioEntrega(data) {
		return this.httpClient.post(this.API_URL + "ListadoTipoBeneficiarioEntrega", data);
	}

	postListadoCatalogoBienesEntrega(data) {
		return this.httpClient.post(this.API_URL + "ListadoCatalogoBienesEntrega", data);
	}

	postAgregarBien_para_Entrega(data) {
		return this.httpClient.post(this.API_URL + "AgregarBien_para_Entrega", data);
	}

	postEditarEntrega(data) {
		return this.httpClient.post(this.API_URL + "EditarEntrega", data);
	}


	postEliminar_Entrega(data) {
		return this.httpClient.post(this.API_URL + "Eliminar_Entrega", data);
	}


	postCargarEntregaTXT(data) {
		return this.httpClient.post(this.API_URL + "CargarEntregaTXT", data)
	}

	postValidarEntregaTXT(data) {
		return this.httpClient.post(this.API_URL + "ValidarEntregaTXT", data)
	}

	postFinalizarEntregaTXT(data) {
		return this.httpClient.post(this.API_URL + "FinalizarEntregaTXT", data)
	}


	postListadoErroresCargaMasivarEntrega(data) {
		return this.httpClient.post(this.API_URL + "ListadoErroresCargaMasivarEntrega", data)
	}




	postListadoBienesXActoAdministracion(data) {
		return this.httpClient.post(this.API_URL + "ListadoBienesXActoAdministracion", data);
	}

	postAgregar_Bienes_al_Detalle_Reingreso(data) {
		return this.httpClient.post(this.API_URL + "Agregar_Bienes_al_Detalle_Reingreso", data);
	}

	postGuardar_Entrega_Reingreso(data) {
		return this.httpClient.post(this.API_URL + "Guardar_Entrega_Reingreso", data);
	}

	postListadoCatalogoBienes_Reingreso(data) {
		return this.httpClient.post(this.API_URL + "ListadoCatalogoBienes_Reingreso", data);
	}

	postEliminarItemReingreso(data) {
		return this.httpClient.post(this.API_URL + "EliminarItemReingreso", data);
	}
	
	/* 16) DEVOLUCION DE BIENES POR ACTO DE ADMINISTRACION */
	postListadoDevolAdministracion(data) {
		return this.httpClient.post(this.API_URL + "ListadoDevolAdministracion", data);
	}

	posteditarDevolAdministracion(data) {
		return this.httpClient.post(this.API_URL + "editarDevolAdministracion", data);
	}

	postListadoBienesDevolFormaIndividual_Administracion(data) {
		return this.httpClient.post(this.API_URL + "ListadoBienesDevolFormaIndividual_Administracion", data);
	}

	postGuardar_DevolAdministracion(data) {
		return this.httpClient.post(this.API_URL + "Guardar_DevolAdministracion", data);
	}

	postEliminacion_Devol_Administracion(data) {
		return this.httpClient.post(this.API_URL + "Eliminar_ActosDevAdministracion", data);
	}

	postEliminacion_Detalles_DevAdministracion(data) {
		return this.httpClient.post(this.API_URL + "Eliminacion_Detalles_DevAdministracion", data);
	}

	postAgregar_Bienes_al_Detalle_DevAdministracion(data) {
		return this.httpClient.post(this.API_URL + "Agregar_Bienes_al_Detalle_Dev_Administracion", data);
	}

	postCargarDevAdministracionTXT(data) {
		return this.httpClient.post(this.API_URL + "CargarDevAdministracionTXT", data)
	}

	postValidarDevAdministracionTXT(data) {
		return this.httpClient.post(this.API_URL + "ValidarDevAdministracionTXT", data)
	}

	postFinalizarDevAdministracionTXT(data) {
		return this.httpClient.post(this.API_URL + "FinalizarDevAdministracionTXT", data)
	}
	
	/* 17) CONFIGURACION DE CARGA DE DOC */
	postListadoCargaActosDoc(data) {
		return this.httpClient.post(this.API_URL + "ListadoCargaActosDoc", data)
	}

	GetEliminarArchivoDocActo(data) {
		return this.httpClient.get(this.API_URL + "EliminarArchivoDocActo/" + data);
	}

	GetNombDocActo(cod_entidad,id_acto,tipo_acto) {
		return this.httpClient.get(this.API_URL + "NombDocActo/"+ cod_entidad + '/' +  id_acto + '/' + tipo_acto );
	}


	private modals: any[] = [];

	add(modal: any) {
		// add modal to array of active modals
		this.modals.push(modal);
	}

	remove(id: string) {
		// remove modal from array of active modals
		this.modals = this.modals.filter(x => x.id !== id);
	}

	open(id: string) {
		// open modal specified by id
		let modal: any = this.modals.filter(x => x.id === id)[0];
		modal.open();
	}

	close(id: string) {
		// close modal specified by id
		let modal: any = this.modals.filter(x => x.id === id)[0];
		modal.close();
	}

	validaToken(data){
		return this.httpClient.get(this.API_URL + "validaTokenAuth/"+ data );
	}

}
