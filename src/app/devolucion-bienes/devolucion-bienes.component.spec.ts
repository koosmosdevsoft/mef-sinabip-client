import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevolucionBienesComponent } from './devolucion-bienes.component';

describe('DevolucionBienesComponent', () => {
  let component: DevolucionBienesComponent;
  let fixture: ComponentFixture<DevolucionBienesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevolucionBienesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevolucionBienesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
