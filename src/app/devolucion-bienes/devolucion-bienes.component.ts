import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { SinabipmueblesService } from '../services/sinabipmuebles.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';

@Component({
	selector: 'app-devolucion-bienes',
	templateUrl: './devolucion-bienes.component.html',
	styleUrls: ['./devolucion-bienes.component.css'],
	animations: [routerTransition()],
	providers: [NgbModalConfig, NgbModal]
})
export class DevolucionBienesComponent implements OnInit {

	ListPers_Arg: Array<any> = [];
	CantPers_Arg: Array<any> = [];
	// id : string = '2550';
	// idusuario : string = '9610';
	id: string = sessionStorage.getItem("Cod_Entidad");
	idusuario: string = sessionStorage.getItem("Cod_Usuario");
	RutaSinabip : string = sessionStorage.getItem("RutaSinabip");
	//Paginacion 
	itemsPerPage: number = 20;
	page: any = 1;
	previousPage: any;
	total: any = 0;

	filtro = {
		id: this.id, nrodocument: '', appaterpers: '', apmaterpers: '', nombre: '', page: this.page, records: this.itemsPerPage
	}

	constructor(config: NgbModalConfig, private Sinabip: SinabipmueblesService,
		private route: ActivatedRoute,
		private router: Router,
		private spinner: NgxSpinnerService,
		private modalService: NgbModal) {

			let token = sessionStorage.getItem("token");  
			let ruta = sessionStorage.getItem("RutaSinabip")
			if(token === null){
				// window.location.replace(ruta+"SGISBN/System/sinabip.php");
				window.location.replace("./SGISBN/System/sinabip.php");
			}
			sessionStorage.setItem("Cod_Entidad", this.id);
			sessionStorage.setItem("Cod_Usuario", this.idusuario);
			this.ListadoPersonalDevolucion();
		}

	ngOnInit() {
	}

	ListadoPersonalDevolucion() {
		this.spinner.show();
		this.Sinabip.PostListadoPersonalDevolucion(this.filtro).subscribe((data: any) => {
			this.spinner.hide();

			if (data.data.error == true){
				if (data.data.reco.hasOwnProperty('id')) { 
					  this.router.navigate(['error500'])
					  return;
				}else if (data.data.reco.hasOwnProperty('nrodocument')) {
					swal({
						type: 'error',
						title: 'Validacion de Datos',
						text: data.data.reco.nrodocument[0],
					  })
					  return;
				}else if (data.data.reco.hasOwnProperty('page')) {
					this.router.navigate(['error500'])
					return;
				}else if (data.data.reco.hasOwnProperty('records')) {
					this.router.navigate(['error500'])
					return;
				}
			}

			this.ListPers_Arg = data.data;
			this.total = (this.ListPers_Arg.length > 0) ? this.ListPers_Arg[0].Total : 0;
		});
	}

	loadPage(page: number) {
		if (page !== this.previousPage) {
			this.previousPage = page;
			this.ListadoPersonalDevolucion();
		}
	}

	resetearpag() {
		this.filtro.page = 1;
		this.ListadoPersonalDevolucion();
	}

	resetearlimpiarpag() {
		this.page = 1;
		this.Limpiar_ListadoDevolPerson();
	}

	Limpiar_ListadoDevolPerson() {
		this.filtro = {
			id: this.id, nrodocument: '', appaterpers: '', apmaterpers: '', nombre: '', page: this.page, records: this.itemsPerPage
		}
		this.spinner.show();
		this.Sinabip.PostListadoPersonalDevolucion(this.filtro).subscribe((data: any) => {
			this.spinner.hide();
			this.ListPers_Arg = data.data;
		});
	}

	ver(idpredio) {
		this.spinner.show();
		this.Sinabip.getCantBienesaDevolver(this.id, idpredio).subscribe((data: any) => {
			this.spinner.hide();
			

			if (data.data.error == true){
				if (data.data.reco.hasOwnProperty('id')) { 
					swal({
						type: 'error',
						title: 'Validacion de Datos',
						text: data.data.reco.documento[0],
					  })
					  return;
				}else if (data.data.reco.hasOwnProperty('idpers')) {
					  this.router.navigate(['error500']);
					  return;
				}
				
			}

			this.CantPers_Arg = data.data;
			this.total = (this.CantPers_Arg.length > 0) ? this.CantPers_Arg[0].TOTAL : 0;
			if (this.total >= 1) {
				sessionStorage.setItem('Devolucion_Personal', idpredio);
				this.router.navigate(["devolucion-bienes-det"]);
			} else {
				swal(
					'Aviso!!',
					'Personal no cuenta con bienes para Devolver!',
					'info'
				)
			}
		});
	}
}
